﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.IO;
using System.Text;
using System.Text.RegularExpressions;
using System.Security.Cryptography;
namespace portal.Helpers
{
    public class UtilityHelper
    {
        public static string SecurityKey = "VsoftGroup.Com";
        public static string Encrypt(string toEncrypt, bool useHashing)
        {
            byte[] keyArray;
            byte[] toEncryptArray = UTF8Encoding.UTF8.GetBytes(toEncrypt);

            // Get the key from config file
            string key = SecurityKey;
            //System.Windows.Forms.MessageBox.Show(key);
            if (useHashing)
            {
                MD5CryptoServiceProvider hashmd5 = new MD5CryptoServiceProvider();
                keyArray = hashmd5.ComputeHash(UTF8Encoding.UTF8.GetBytes(key));
                hashmd5.Clear();
            }
            else
                keyArray = UTF8Encoding.UTF8.GetBytes(key);

            TripleDESCryptoServiceProvider tdes = new TripleDESCryptoServiceProvider();
            tdes.Key = keyArray;
            tdes.Mode = CipherMode.ECB;
            tdes.Padding = PaddingMode.PKCS7;

            ICryptoTransform cTransform = tdes.CreateEncryptor();
            byte[] resultArray = cTransform.TransformFinalBlock(toEncryptArray, 0, toEncryptArray.Length);
            tdes.Clear();
            return Convert.ToBase64String(resultArray, 0, resultArray.Length);
        }
        /// <summary>
        /// DeCrypt a string using dual encryption method. Return a DeCrypted clear string
        /// </summary>
        /// <param name="cipherString">encrypted string</param>
        /// <param name="useHashing">Did you use hashing to encrypt this data? pass true is yes</param>
        /// <returns></returns>
        public static string Decrypt(string cipherString, bool useHashing)
        {
            byte[] keyArray;
            cipherString = cipherString.Replace(" ", "+");
            byte[] toEncryptArray = Convert.FromBase64String(cipherString);

            //Get your key from config file to open the lock!
            string key = SecurityKey;

            if (useHashing)
            {
                MD5CryptoServiceProvider hashmd5 = new MD5CryptoServiceProvider();
                keyArray = hashmd5.ComputeHash(UTF8Encoding.UTF8.GetBytes(key));
                hashmd5.Clear();
            }
            else
                keyArray = UTF8Encoding.UTF8.GetBytes(key);

            TripleDESCryptoServiceProvider tdes = new TripleDESCryptoServiceProvider();
            tdes.Key = keyArray;
            tdes.Mode = CipherMode.ECB;
            tdes.Padding = PaddingMode.PKCS7;

            ICryptoTransform cTransform = tdes.CreateDecryptor();
            byte[] resultArray = cTransform.TransformFinalBlock(toEncryptArray, 0, toEncryptArray.Length);

            tdes.Clear();
            return UTF8Encoding.UTF8.GetString(resultArray);
        }

        public static readonly string[] Image = new string[] { ".jpg", ".jpeg", ".png", ".bmp", ".gif", ".tif" };

        public static bool IsImage(string FileName)
        {
            string Extenstion = Path.GetExtension(FileName).ToLower();
            if (Image.Contains(Extenstion))
                return true;
            return false;
        }

        public static string ExtractYoutubeId(string Link)
        {
            string Pattern = @"https?:\/\/(www.)?youtu(be\.com|\.be)\/(watch\?v=)?([A-Za-z0-9._%-]*)(\&\S+)?";
            Regex r = new Regex(Pattern);
            Match m = r.Match(Link);
            if (m.Groups.Count >= 4)
                return m.Groups[4].Value;
            return string.Empty;
        }

        private static readonly string[] VietNamChar = new string[]
        {
        "aAeEoOuUiIdDyY-",
        "áàạảãâấầậẩẫăắằặẳẵ",
        "ÁÀẠẢÃÂẤẦẬẨẪĂẮẰẶẲẴ",
        "éèẹẻẽêếềệểễ",
        "ÉÈẸẺẼÊẾỀỆỂỄ",
        "óòọỏõôốồộổỗơớờợởỡ",
        "ÓÒỌỎÕÔỐỒỘỔỖƠỚỜỢỞỠ",
        "úùụủũưứừựửữ",
        "ÚÙỤỦŨƯỨỪỰỬỮ",
        "íìịỉĩ",
        "ÍÌỊỈĨ",
        "đ",
        "Đ",
        "ýỳỵỷỹ",
        "ÝỲỴỶỸ",
        "*<>?!%$#^&(){}[]`+=;,.':|/\\\" _"
        };
        public static string RemoveAccentsFromText(string str)
        {
            for (int i = 1; i < VietNamChar.Length - 1; i++)
            {
                for (int j = 0; j < VietNamChar[i].Length; j++)
                    str = str.Replace(VietNamChar[i][j], VietNamChar[0][i - 1]);
            }
            for (int j = 0; j < VietNamChar[VietNamChar.Length - 1].Length - 1; j++)
            {
                str = str.Replace(VietNamChar[VietNamChar.Length - 1][j], VietNamChar[0][VietNamChar.Length - 1 - 1]);
            }
            return str;
        }
        public static string RemoveHTMLTags(string source)
        {
            if (string.IsNullOrEmpty(source))
            {
                return string.Empty;
            }
            string expn = "<.*?>";
            return Regex.Replace(source, expn, string.Empty);
        }

        public static string ConvertMonthToDisplay(int Month)
        {
            string result = "";
            if(Month == 0)
            {
                result = "Xem trọn đời";
            }
            else if(Month < 12 || Month % 12 != 0)
            {
                result = Month + " tháng";
            }
            else
            {
                result = (Month / 12) + " năm";
            }
            return result;
        }
    }
}
