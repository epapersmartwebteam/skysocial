﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using com.paypal.sdk.profiles;
using com.paypal.sdk.core;
using com.paypal.sdk.util;
using com.paypal.sdk.services;
using System.Threading;
using System.Globalization;
using portal.Models;

namespace portal.Helpers
{
    public class Paypal_Checkout
    {
        public static bool IsSandbox = Convert.ToBoolean(System.Configuration.ConfigurationManager.AppSettings.Get("PaymentSandbox"));            

        public static string MerchantId = System.Configuration.ConfigurationManager.AppSettings.Get("PPMerchantId");
        public static string UserName = System.Configuration.ConfigurationManager.AppSettings.Get("PPUserName");
        public static string Password = System.Configuration.ConfigurationManager.AppSettings.Get("PPPass");
        public static string SuccessUrl = IsSandbox ? System.Configuration.ConfigurationManager.AppSettings.Get("SandboxPPSuccessUrl") : System.Configuration.ConfigurationManager.AppSettings.Get("PPSuccessUrl");
        public static string CancelUrl = IsSandbox ? System.Configuration.ConfigurationManager.AppSettings.Get("SandboxPPCancelUrl") : System.Configuration.ConfigurationManager.AppSettings.Get("PPCancelUrl");
                
        public static string PayPalEnviroment = IsSandbox ? "sandbox" : "live";
        

        public static double USDConvertRate = double.Parse(System.Configuration.ConfigurationManager.AppSettings.Get("USDConvertRate"));

        public static double ConvertToUSD(int VND)
        {
            return VND / USDConvertRate;
        }
        #region "SetExpressCheckout"
        public static string createPayPalTOKEN(string API_UserName, string API_Password, string API_Signature, string Environment, string ReturnUrl, string CancelUrl,
                UserPaymentLog PaymentLog, string ItemName, out NVPCodec Result)
        {
            string TOKEN = "";

            SetProfile.SessionProfile = IsSandbox ? SetProfile.DefaultProfile : SetProfile.CreateAPIProfile(API_UserName, API_Password, API_Signature, "", "", Environment, "", "", "", "");
            NVPCallerServices caller = PayPalAPI.PayPalAPIInitialize();
            NVPCodec encoder = new NVPCodec();

            encoder["METHOD"] = "SetExpressCheckout";
            encoder["CANCELURL"] = CancelUrl;
            encoder["RETURNURL"] = ReturnUrl;
            encoder["HDRIMG"] = "http://files.myxteam.com/chat/22587/1/1487652033998/title.png";
            encoder["PAYFLOWCOLOR"] = "0094D9";
            encoder["NOSHIPPING"] = "1";
            encoder["LANDINGPAGE"] = "Billing";
            encoder["SOLUTIONTYPE"] = "Sole";
            encoder["USERSELECTEDFUNDINGSOURCE"] = "CreditCard";
            encoder["REQCONFIRMSHIPPING"] = "0";
            encoder["PAYMENTREQUEST_0_PAYMENTACTION"] = "Sale";
            encoder["PAYMENTREQUEST_0_CURRENCYCODE"] = "USD";

            encoder["PAYMENTREQUEST_0_INVNUM"] = PaymentLog.PaymentCode;
            encoder["L_PAYMENTREQUEST_0_NAME0"] = "Thanh toán trên hệ thống Sky Social";
            encoder["L_PAYMENTREQUEST_0_DESC0"] = "Mua " + ItemName;

            //convert to USD
            double ItemAmountUSD = System.Math.Round(ConvertToUSD(PaymentLog.Total ?? 0), 2);

            CultureInfo curCult = Thread.CurrentThread.CurrentCulture;
            Thread.CurrentThread.CurrentCulture = new CultureInfo("en");

            string Amount = ItemAmountUSD.ToString("#,##0.00");

            Thread.CurrentThread.CurrentCulture = curCult;

            encoder["L_PAYMENTREQUEST_0_AMT0"] = Amount;
            encoder["L_PAYMENTREQUEST_0_QTY0"] = "1";
            encoder["L_PAYMENTREQUEST_0_ITEMCATEGORY0"] = "Digital";


            encoder["PAYMENTREQUEST_0_ITEMAMT"] = Amount;//System.Math.Round(Amount, 2).ToString("##0.00");
            encoder["PAYMENTREQUEST_0_AMT"] = Amount;//System.Math.Round(Amount, 2).ToString("##0.00");

            string pStrrequestforNvp = encoder.Encode();
            string pStresponsenvp = caller.Call(pStrrequestforNvp);

            NVPCodec decoder = new NVPCodec();
            decoder.Decode(pStresponsenvp);
            Result = decoder;
            string strAck = decoder["ACK"];
            if (strAck != null && (strAck == "Success" || strAck == "SuccessWithWarning"))
            {
                TOKEN = decoder["TOKEN"];
            }

            return TOKEN;
        }

        public static string buildCheckoutUrl(string Environment, string TOKEN)
        {
            string ExpCheckoutUrl = "";
            string host = "www";
            if (Environment.ToLower() == "sandbox")
                host += ".sandbox";
            host += ".paypal.com";
            ExpCheckoutUrl = "https://" + host + "/cgi-bin/webscr?cmd=_express-checkout" + "&token=" + TOKEN;
            return ExpCheckoutUrl;
        }
        #endregion
        public static bool GetExpressCheckoutDetail(string API_UserName, string API_Password, string API_Signature, string Environment, string TOKEN, out string PayerID, out string Total, out string TrackingID, out string OrderCode, out NVPCodec Result)
        {
            bool IsSuccess = false;
            SetProfile.SessionProfile = IsSandbox ? SetProfile.DefaultProfile : SetProfile.CreateAPIProfile(API_UserName, API_Password, API_Signature, "", "", Environment, "", "", "", "");
            NVPCallerServices caller = PayPalAPI.PayPalAPIInitialize();
            NVPCodec encoder = new NVPCodec();
            encoder["METHOD"] = "GetExpressCheckoutDetails";
            encoder["TOKEN"] = TOKEN;

            string pStrrequestforNvp = encoder.Encode();
            string pStresponsenvp = caller.Call(pStrrequestforNvp);

            NVPCodec decoder = new NVPCodec();
            decoder.Decode(pStresponsenvp);

            PayerID = "";
            Total = "";
            TrackingID = "";
            OrderCode = "";
            Result = decoder;
            string strAck = decoder["ACK"];
            if (strAck != null && (strAck == "Success" || strAck == "SuccessWithWarning"))
            {
                OrderCode = decoder["INVNUM"];
                TrackingID = decoder["CORRELATIONID"];
                PayerID = decoder["PAYERID"];
                Total = decoder["AMT"];
                IsSuccess = true;
            }
            return IsSuccess;
        }

        public static bool DoExpressCheckout(string API_UserName, string API_Password, string API_Signature, string Environment, string TOKEN, string PayerID, string Amount, string PaymentType, string Currency, out string Error)
        {
            bool IsSuccess = false;
            SetProfile.SessionProfile = IsSandbox ? SetProfile.DefaultProfile : SetProfile.CreateAPIProfile(API_UserName, API_Password, API_Signature, "", "", Environment, "", "", "", "");
            NVPCallerServices caller = PayPalAPI.PayPalAPIInitialize();
            NVPCodec encoder = new NVPCodec();
            encoder["METHOD"] = "DoExpressCheckoutPayment";
            encoder["TOKEN"] = TOKEN;
            encoder["PAYERID"] = PayerID;
            encoder["AMT"] = Amount;
            encoder["PAYMENTACTION"] = PaymentType;
            encoder["CURRENCYCODE"] = Currency;

            string pStrrequestforNvp = encoder.Encode();
            string pStresponsenvp = caller.Call(pStrrequestforNvp);

            NVPCodec decoder = new NVPCodec();
            decoder.Decode(pStresponsenvp);

            string strAck = decoder["ACK"];

            string strError = "<b>Error Code:</b>" + decoder["L_ERRORCODE0"] + "<br />";
            strError += "<b>Error Message:</b>" + decoder["L_LONGMESSAGE0"];

            if (strAck != null && (strAck == "Success" || strAck == "SuccessWithWarning"))
            {
                IsSuccess = true;
            }
            Error = strError;
            return IsSuccess;
        }
    }

    public class PayPalAPI
    {


        public PayPalAPI()
        {
        }

        public static NVPCallerServices PayPalAPIInitialize()
        {
            NVPCallerServices caller = new NVPCallerServices();
            caller.APIProfile = SetProfile.SessionProfile;
            return caller;

        }

    }

    public class SetProfile
    {

        /// <summary>
        /// Required designer variable.
        /// </summary
        public static readonly IAPIProfile DefaultProfile = CreateAPIProfile(Constants.API_USERNAME, Constants.API_PASSWORD, Constants.API_SIGNATURE, Constants.CERTIFICATE, Constants.PRIVATE_KEY_PASSWORD, "sandbox", Constants.SUBJECT, Constants.OAUTH_SIGNATURE, Constants.OAUTH_TOKEN, Constants.OAUTH_TIMESTAMP);

        public static IAPIProfile CreateAPIProfile(string apiUsername, string apiPassword, string signature, string CertificateFile_Cer, string PrivateKeyPassword_Cer, String stage, string subject, string oauth_Signature, string oauth_Token, string oauth_Timestamp)
        {
            {
                IAPIProfile profile = ProfileFactory.createSignatureAPIProfile();
                profile.APIUsername = apiUsername;
                profile.APIPassword = apiPassword;
                profile.Environment = stage;
                profile.Subject = subject;
                profile.APISignature = signature;
                return profile;
            }
        }
        public static void SetDefaultProfile()
        {
            SessionProfile = DefaultProfile;
        }

        public static IAPIProfile SessionProfile
        {
            get
            {
                return (IAPIProfile)HttpContext.Current.Session[Constants.PROFILE_KEY];
            }
            set
            {
                HttpContext.Current.Session[Constants.PROFILE_KEY] = value;
            }
        }
    }

    public class Constants
    {
        /// <summary>
        /// Modify these values if you want to use your own profile.
        /// </summary>

        /* 
         *                                                                         *
         * WARNING: Do not embed plaintext credentials in your application code.   *
         * Doing so is insecure and against best practices.                        *
         *                                                                         *
         * Your API credentials must be handled securely. Please consider          *
         * encrypting them for use in any production environment, and ensure       *
         * that only authorized individuals may view or modify them.               *
         *                                                                         *
         */

        public const string ENVIRONMENT = "sandbox";
        public const string PAYPAL_URL = "https://www.sandbox.paypal.com";
        public const string ECURLLOGIN = "https://developer.paypal.com";
        public const string SUBJECT = "";


        public const string PROFILE_KEY = "Profile";
        public const string PAYMENT_ACTION_PARAM = "paymentAction";
        public const string PAYMENT_TYPE_PARAM = "paymentType";


        public const string STANDARD_EMAIL_ADDRESS = "testvs_1326179007_biz@gmail.com";
        public const string WEBSCR_URL = PAYPAL_URL + "/cgi-bin/webscr";

        ///sandbox 3t credentials
        public const string PRIVATE_KEY_PASSWORD = "";
        public const string API_USERNAME = "testvs_1326179007_biz_api1.gmail.com";
        public const string API_PASSWORD = "1326179032";
        public const string API_SIGNATURE = "AqiNgL9NGZpLypuI4M98bFOluVYuAWeQ68bp5KtVxH-sE.IUyvjkF60v";
        public const string CERTIFICATE = "";

        //Permission
        public const string OAUTH_SIGNATURE = "";
        public const string OAUTH_TOKEN = "";
        public const string OAUTH_TIMESTAMP = "";

    }
}