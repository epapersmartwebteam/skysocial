﻿using System;
using System.Collections.Specialized;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using portal.Models;
using RestSharp;
using System.Net;
using Newtonsoft.Json;

namespace portal.Helpers
{
    public class ReturnResult
    {
        public int ErrorCode { get; set; }
        public string Message { get; set; }
        public Object Data { get; set; }
    }
    public class APIHelper
    {
        public static string AuthorizationKey = "Authorization";
        public static string OSKey = "OS";

        public static string CDNUrl = System.Configuration.ConfigurationManager.AppSettings.Get("CDNURL");
        public static string Token = "airpodpro";
        public static RestRequest AddCookieToRequest(RestRequest request, HttpCookieCollection cookies)
        {
            if (cookies["connect.sid"] != null)
            {
                request.AddCookie("connect.sid", cookies["connect.sid"].Value);
            }
            if (cookies["ARRAffinity"] != null)
            {
                request.AddHeader("ARRAffinity", cookies["ARRAffinity"].Value);
            }
            if (cookies["OS"] != null)
            {
                request.AddHeader("OS", cookies["OS"].Value);
            }
            return request;
        }

        public static RestRequest AddTokenToRequest(RestRequest request, HttpRequestBase requestBase)
        {
            if (!string.IsNullOrEmpty(requestBase.Headers[AuthorizationKey]))
            {
                request.AddHeader(AuthorizationKey, requestBase.Headers[AuthorizationKey]);
            }
            else if(requestBase.Cookies[AuthorizationKey] != null)
            {
                request.AddHeader(AuthorizationKey, requestBase.Cookies[AuthorizationKey].Value);
            }

            if (!string.IsNullOrEmpty(requestBase.Headers[OSKey]))
            {
                request.AddHeader(OSKey, requestBase.Headers[OSKey]);
            }
            else if(requestBase.Cookies[OSKey] != null)
            {
                request.AddHeader(OSKey, requestBase.Cookies[OSKey].Value);
            }
            return request;
        }

        public static ReturnResult DeserializeToReturnResult(string Data)
        {
            ReturnResult result = new ReturnResult();
            //string strResult = Data.Replace("\\\"", "\"");
            //strResult = strResult.Replace("\\\"", "\"");
            //strResult = strResult.Substring(1);
            //strResult = strResult.Substring(0, strResult.Length - 1);
            result = JsonConvert.DeserializeObject<ReturnResult>(Data);

            return result;
        }

        public static string APIUrl = System.Configuration.ConfigurationManager.AppSettings.Get("APIURL");

        #region User
        public static User GetUserInfo(HttpRequestBase requestBase)
        {
            User m = new User();

            string Method = "/user/info";
            var client = new RestClient(APIUrl);
            var request = new RestRequest(Method, RestSharp.Method.POST);

            request = AddTokenToRequest(request, requestBase);

            IRestResponse response = client.Execute(request);
            if (response.StatusCode == HttpStatusCode.OK || response.StatusCode == HttpStatusCode.Accepted)
            {
                ReturnResult result = DeserializeToReturnResult(response.Content);
                if (result.ErrorCode == 0)
                    m = JsonConvert.DeserializeObject<User>(result.Data.ToString());
            }
            else
            {
                DAL.CreateAPILog(string.Join(";", request.Parameters.Select(c => c.Value).ToList()), response.StatusDescription);
            }
            return m;
        }
        #endregion

        #region Payment
        public static ReturnResult CreatePaymentLog(HttpRequestBase requestBase,
                int MethodId, int Price, string DiscountCode, int Discount,
                int PayTypeId, int ItemId, bool IsDigital, bool IsVipPurchase, int VipMonth, string Description)
        {
            ReturnResult result = new ReturnResult() { ErrorCode = 114 };

            string Method = "/payment/add";
            var client = new RestClient(APIUrl);
            var request = new RestRequest(Method, RestSharp.Method.POST);

            request = AddTokenToRequest(request, requestBase);

            request.AddParameter("MethodId", MethodId);
            request.AddParameter("Price", Price);
            request.AddParameter("DiscountCode", DiscountCode);
            request.AddParameter("Discount", Discount);
            request.AddParameter("PayTypeId", PayTypeId);
            request.AddParameter("ItemId", ItemId);
            request.AddParameter("IsDigital", IsDigital);
            request.AddParameter("IsVipPurchase", IsVipPurchase);
            request.AddParameter("VipMonth", VipMonth);
            request.AddParameter("Description", Description);

            IRestResponse response = client.Execute(request);
            if (response.StatusCode == HttpStatusCode.OK || response.StatusCode == HttpStatusCode.Accepted)
            {
                result = DeserializeToReturnResult(response.Content);
                if (result.ErrorCode == 0)
                    result.Data = JsonConvert.DeserializeObject<UserPaymentLog>(result.Data.ToString());
            }
            else
            {
                DAL.CreateAPILog(string.Join(";", request.Parameters.Select(c => c.Value).ToList()), response.StatusDescription);
                result.Message = response.StatusDescription;

            }
            return result;
        }

        public static ReturnResult CreatePaymentLogFromMobileCard(HttpRequestBase requestBase,
                int Price, string DiscountCode, int Discount,
                int PayTypeId, int ItemId, bool IsDigital, bool IsVipPurchase, int VipMonth,
                string CardType, int CardAmount, string CardPIN, string CardSerial)
        {
            ReturnResult result = new ReturnResult() { ErrorCode = 114 };

            string Method = "/payment/addFromMobileCard";
            var client = new RestClient(APIUrl);
            var request = new RestRequest(Method, RestSharp.Method.POST);

            request = AddTokenToRequest(request, requestBase);

            request.AddParameter("Price", Price);
            request.AddParameter("DiscountCode", DiscountCode);
            request.AddParameter("Discount", Discount);
            request.AddParameter("PayTypeId", PayTypeId);
            request.AddParameter("ItemId", ItemId);
            request.AddParameter("IsDigital", IsDigital);
            request.AddParameter("IsVipPurchase", IsVipPurchase);
            request.AddParameter("VipMonth", VipMonth);
            request.AddParameter("CardType", CardType);
            request.AddParameter("CardAmount", CardAmount);
            request.AddParameter("CardPIN", CardPIN);
            request.AddParameter("CardSerial", CardSerial);

            IRestResponse response = client.Execute(request);
            if (response.StatusCode == HttpStatusCode.OK || response.StatusCode == HttpStatusCode.Accepted)
            {
                result = DeserializeToReturnResult(response.Content);
                if (result.ErrorCode == 0)
                    result.Data = JsonConvert.DeserializeObject<UserPaymentLog>(result.Data.ToString());
            }
            else
            {
                DAL.CreateAPILog(string.Join(";", request.Parameters.Select(c => c.Value).ToList()), response.StatusDescription);
                result.Message = response.StatusDescription;

            }
            return result;
        }

        public static ReturnResult CreatePaymentLogFromPassCode(HttpRequestBase requestBase,
               string PassCode)
        {
            ReturnResult result = new ReturnResult() { ErrorCode = 114 };

            string Method = "/payment/addFromPasscode";
            var client = new RestClient(APIUrl);
            var request = new RestRequest(Method, RestSharp.Method.POST);

            request = AddTokenToRequest(request, requestBase);

            request.AddParameter("PassCode", PassCode);

            IRestResponse response = client.Execute(request);
            if (response.StatusCode == HttpStatusCode.OK || response.StatusCode == HttpStatusCode.Accepted)
            {
                result = DeserializeToReturnResult(response.Content);
                if (result.ErrorCode == 0)
                    result.Data = JsonConvert.DeserializeObject<UserPaymentLog>(result.Data.ToString());
            }
            else
            {
                DAL.CreateAPILog(string.Join(";", request.Parameters.Select(c => c.Value).ToList()), response.StatusDescription);
                result.Message = response.StatusDescription;

            }
            return result;
        }

        public static ReturnResult UpdatePaymentIsPay(HttpRequestBase requestBase,
                string PaymentCode, string TrackingId)
        {
            ReturnResult result = new ReturnResult() { ErrorCode = 114 };

            string Method = "/payment/updateIsPay";
            var client = new RestClient(APIUrl);
            var request = new RestRequest(Method, RestSharp.Method.POST);

            request = AddTokenToRequest(request, requestBase);

            request.AddParameter("PaymentCode", PaymentCode);
            request.AddParameter("TrackingId", TrackingId);

            IRestResponse response = client.Execute(request);
            if (response.StatusCode == HttpStatusCode.OK || response.StatusCode == HttpStatusCode.Accepted)
            {
                result = DeserializeToReturnResult(response.Content);
                if (result.ErrorCode == 0)
                    result.Data = JsonConvert.DeserializeObject<UserPaymentLog>(result.Data.ToString());
            }
            else
            {
                DAL.CreateAPILog(string.Join(";", request.Parameters.Select(c => c.Value).ToList()), response.StatusDescription);
                result.Message = response.StatusDescription;
            }
            return result;
        }

        public static ReturnResult UpdatePaymentIsPaySystem(long UserId,
                string PaymentCode, string TrackingId)
        {
            ReturnResult result = new ReturnResult() { ErrorCode = 114 };

            string Method = "/payment/systemUpdateIsPay";
            var client = new RestClient(APIUrl);
            var request = new RestRequest(Method, RestSharp.Method.POST);

            request.AddParameter("Token", Token);
            request.AddParameter("UserId", UserId);
            request.AddParameter("OS", "Others");
            request.AddParameter("PaymentCode", PaymentCode);
            request.AddParameter("TrackingId", TrackingId);

            IRestResponse response = client.Execute(request);
            if (response.StatusCode == HttpStatusCode.OK || response.StatusCode == HttpStatusCode.Accepted)
            {
                result = DeserializeToReturnResult(response.Content);
                if (result.ErrorCode == 0)
                    result.Data = JsonConvert.DeserializeObject<UserPaymentLog>(result.Data.ToString());
            }
            else
            {
                DAL.CreateAPILog(string.Join(";", request.Parameters.Select(c => c.Value).ToList()), response.StatusDescription);
                result.Message = response.StatusDescription;
            }
            return result;
        }

        public static ReturnResult UpdatePaymentIsPayFromMobileCard(HttpRequestBase requestBase,
                string PaymentCode, string TrackingId, bool IsPaid, string CardErrorCode, string CardErrorMessage)
        {
            ReturnResult result = new ReturnResult() { ErrorCode = 114 };

            string Method = "/payment/updateIsPayFromMobileCard";
            var client = new RestClient(APIUrl);
            var request = new RestRequest(Method, RestSharp.Method.POST);

            request = AddTokenToRequest(request, requestBase);

            request.AddParameter("PaymentCode", PaymentCode);
            request.AddParameter("TrackingId", TrackingId);
            request.AddParameter("IsPaid", IsPaid);
            request.AddParameter("CardErrorCode", CardErrorCode);
            request.AddParameter("CardErrorMessage", CardErrorMessage);

            IRestResponse response = client.Execute(request);
            if (response.StatusCode == HttpStatusCode.OK || response.StatusCode == HttpStatusCode.Accepted)
            {
                result = DeserializeToReturnResult(response.Content);
                if (result.ErrorCode == 0)
                    result.Data = JsonConvert.DeserializeObject<UserPaymentLog>(result.Data.ToString());
            }
            else
            {
                DAL.CreateAPILog(string.Join(";", request.Parameters.Select(c => c.Value).ToList()), response.StatusDescription);
                result.Message = response.StatusDescription;
            }
            return result;
        }
        public static ReturnResult GetPaymentLogInfoSystem(string PaymentCode)
        {
            ReturnResult result = new ReturnResult() { ErrorCode = 114 };

            string Method = "/payment/systemInfo";
            var client = new RestClient(APIUrl);
            var request = new RestRequest(Method, RestSharp.Method.POST);

            request.AddParameter("Token", Token);
            request.AddParameter("PaymentCode", PaymentCode);

            IRestResponse response = client.Execute(request);
            if (response.StatusCode == HttpStatusCode.OK || response.StatusCode == HttpStatusCode.Accepted)
            {
                if (response != null)
                {
                    result = DeserializeToReturnResult(response.Content);
                    if (result.ErrorCode == 0)
                    {
                        if (result.Data != null)
                        {
                            result.Data = JsonConvert.DeserializeObject<UserPaymentLog>(result.Data.ToString());
                        }
                        else
                        {
                            result.ErrorCode = 101;
                            result.Message = "Không tồn tại " + PaymentCode;
                            result.Data = new UserPaymentLog();
                        }
                    }
                }

                else
                {
                    result = new ReturnResult { ErrorCode = 101, Message = "Không tồn tại" + PaymentCode };
                }
            }
            else
            {
                DAL.CreateAPILog(string.Join(";", request.Parameters.Select(c => c.Value).ToList()), response.StatusDescription);
                result.Message = response.StatusDescription;
            }
            return result;
        }
        public static ReturnResult GetPaymentLogInfo(HttpRequestBase requestBase, string PaymentCode)
        {
            ReturnResult result = new ReturnResult() { ErrorCode = 114 };

            string Method = "/payment/info";
            var client = new RestClient(APIUrl);
            var request = new RestRequest(Method, RestSharp.Method.POST);

            request = AddTokenToRequest(request, requestBase);

            request.AddParameter("PaymentCode", PaymentCode);
            IRestResponse response = client.Execute(request);
            if (response.StatusCode == HttpStatusCode.OK || response.StatusCode == HttpStatusCode.Accepted)
            {
                if (response != null)
                {
                    result = DeserializeToReturnResult(response.Content);
                    if (result.ErrorCode == 0)
                    {
                        if (result.Data != null)
                        {
                            result.Data = JsonConvert.DeserializeObject<UserPaymentLog>(result.Data.ToString());
                        }
                        else
                        {
                            result.ErrorCode = 101;
                            result.Message = "Không tồn tại " + PaymentCode;
                            result.Data = new UserPaymentLog();
                        }
                    }
                }

                else
                {
                    result = new ReturnResult { ErrorCode = 101, Message = "Không tồn tại" + PaymentCode };
                }
            }
            else
            {
                DAL.CreateAPILog(string.Join(";", request.Parameters.Select(c => c.Value).ToList()), response.StatusDescription);
                result.Message = response.StatusDescription;
            }
            return result;
        }
        #endregion

        #region vip config
        private static string ParseVipLevel(int VipLevelId)
        {
            string VipLevel = "";
            switch (VipLevelId)
            {
                case 1:
                    {
                        VipLevel = "VVIP";
                        break;
                    }
                case 0:
                default:
                    {
                        VipLevel = "VIP";
                        break;
                    }
            }
            return VipLevel;
        }

        public static ReturnResult GetVipPackConfig(int ConfigId)
        {
            ReturnResult result = new ReturnResult() { ErrorCode = 114 };
            string Method = "/vippack/info";
            var client = new RestClient(APIUrl);
            var request = new RestRequest(Method, RestSharp.Method.POST);
            request.AddParameter("ConfigId", ConfigId);

            IRestResponse response = client.Execute(request);
            if (response.StatusCode == HttpStatusCode.OK || response.StatusCode == HttpStatusCode.Accepted)
            {
                result = DeserializeToReturnResult(response.Content);
                if (result.ErrorCode == 0)
                {
                    AppVipSellConfig m = JsonConvert.DeserializeObject<AppVipSellConfig>(result.Data.ToString());

                    m.VipLevel = ParseVipLevel(m.VipLevelId);

                    result.Data = m;
                }
            }
            else
            {
                DAL.CreateAPILog(string.Join(";", request.Parameters.Select(c => c.Value).ToList()), response.StatusDescription);
                result.Message = response.StatusDescription;
            }
            return result;
        }


        public static ReturnResult GetVipPackConfigs()
        {
            ReturnResult result = new ReturnResult() { ErrorCode = 114 };
            string Method = "/vippack/list";
            var client = new RestClient(APIUrl);
            var request = new RestRequest(Method, RestSharp.Method.POST);
            request.AddParameter("Active", 1);

            IRestResponse response = client.Execute(request);
            if (response.StatusCode == HttpStatusCode.OK || response.StatusCode == HttpStatusCode.Accepted)
            {
                result = DeserializeToReturnResult(response.Content);
                if (result.ErrorCode == 0)
                {
                    List<AppVipSellConfig> lst = JsonConvert.DeserializeObject<List<AppVipSellConfig>>(result.Data.ToString());

                    for (var i = 0; i < lst.Count; i++)
                    {
                        lst[i].VipLevel = ParseVipLevel(lst[i].VipLevelId);
                    }
                    result.Data = lst;
                }
            }
            else
            {
                DAL.CreateAPILog(string.Join(";", request.Parameters.Select(c => c.Value).ToList()), response.StatusDescription);
                result.Message = response.StatusDescription;
            }
            return result;
        }
        #endregion

        #region Media Info
        public static ReturnResult GetMediaSellConfig(HttpRequestBase requestBase, int MediaId)
        {
            ReturnResult result = new ReturnResult() { ErrorCode = 114 };

            string Method = "/media/id";
            var client = new RestClient(APIUrl);
            var request = new RestRequest(Method, RestSharp.Method.POST);

            request = AddTokenToRequest(request, requestBase);

            request.AddParameter("MediaId", MediaId);
            request.AddParameter("GetPriceInfo", 1);

            IRestResponse response = client.Execute(request);
            if (response.StatusCode == HttpStatusCode.OK || response.StatusCode == HttpStatusCode.Accepted)
            {
                if (response != null)
                {
                    result = DeserializeToReturnResult(response.Content);
                    if (result.ErrorCode == 0)
                    {
                        if (result.Data != null)
                        {
                            result.Data = JsonConvert.DeserializeObject<Media>(result.Data.ToString());
                        }
                        else
                        {
                            result.ErrorCode = 101;
                            result.Message = "Không tồn tại";
                            result.Data = new Media();
                        }
                    }
                }
                else
                {
                    result = new ReturnResult { ErrorCode = 101, Message = "Không tồn tại" };
                }
            }
            else
            {
                DAL.CreateAPILog(string.Join(";", request.Parameters.Select(c => c.Value).ToList()), response.StatusDescription);
                result.Message = response.StatusDescription;
            }
            return result;
        }
        #endregion
    }
}