﻿function IsEmail(email) {
    var regex = /^([a-zA-Z0-9_.+-])+\@(([a-zA-Z0-9-])+\.)+([a-zA-Z0-9]{2,4})+$/;
    return regex.test(email);
}
function IsMobilePhone(mb) {
    var regex = /^((01){1}([0-9]{9})|(09){1}([0-9]{8})|(((086)|(088)|(089)){1}([0-9]{7})))$/;
    return regex.test(mb);
}

function replaceTextWithEmailAddress(e) {
    var t = /([a-z0-9_\.-]+)@([\da-z\.-]+)\.([a-z\.]{2,6})/;
    var n = e.replace(t, '<a href="mailto:$1@$2.$3">$1@$2.$3</a>'); return n
}
function replaceTextWithYouTube(e) {
    var regex = /https?:\/\/(www.)?youtu(be\.com|\.be)\/(watch\?v=)?([A-Za-z0-9._%-]*)(\&\S+)?/ig;
    return e.replace(regex, '<iframe class="youtube-player" type="text/html" style="max-width:100%;width:500px;min-height:300px" src="http://www.youtube.com/embed/$4" frameborder="0"></iframe>');
}
function replaceTextWithVideo(e) {
    var videoRegex = /\.(mp4|ogg)$/;
    return e.replace(/(\b(?:https?|ftp):\/\/[a-z0-9-+&@#\/%?=~_|!:,.;]*[a-z0-9-+&@#\/%=~_|])/gim,
        function (str) {
            if (!str.match(videoRegex)) {
                return (str);
            } else {
                return ('<video style="max-width:100%;width:500px" controls><source src="' + str + '" /></video>');
            }
        });
}
function replaceTextWithImage(e) {
    var imgRegex = /\.(jpg|jpeg|png|gif|bmp)$/;
    return e.replace(/(\b(?:https?|ftp):\/\/[a-z0-9-+&@#\/%?=~_|!:,.;]*[a-z0-9-+&@#\/%=~_|])/gim,
        function (str) {
            if (!str.match(imgRegex)) {
                return (str);
            }
            else {
                return ('<img src="' + str + '" style="max-width:80%" />');
            }
        });
}
function replaceTextWithUrl(e) {
    var t = /((href|src)=["']|)(\b(https?|ftp|file):\/\/[-A-Z0-9+&@#\/%?=~_|!:,.;]*[-A-Z0-9+&@#\/%=~_|])/ig;
    return e.replace(t, function () {
        return arguments[1] ? arguments[0] : "<a target='_blank' href=\"" + arguments[3] + '">' + arguments[3] + "</a>"
    })
}
function replaceTextWithLink(e) {
    var t = replaceTextWithEmailAddress(e);
    t = replaceTextWithYouTube(t);
    t = replaceTextWithVideo(t);
    t = replaceTextWithImage(t);
    t = replaceTextWithUrl(t);
    return t;
}
function formatBytes(bytes, decimals) {
    if (bytes == 0) return '0 Byte';
    var k = 1024;
    var dm = decimals + 1 || 3;
    var sizes = ['Bytes', 'KB', 'MB', 'GB', 'TB', 'PB', 'EB', 'ZB', 'YB'];
    var i = Math.floor(Math.log(bytes) / Math.log(k));
    return (bytes / Math.pow(k, i)).toPrecision(dm) + ' ' + sizes[i];
}
function pad(number, length) {
    var str = '' + number;
    while (str.length < length) {
        str = '0' + str;
    }
    return str;
}

function initSlimScroll(el) {
    $(el).each(function() {
        if ($(this).attr("data-initialized")) {
            return; // exit
        }

        var height;

        if ($(this).attr("data-height")) {
            height = $(this).attr("data-height");
        } else {
            height = $(this).css('height');
        }

        $(this).slimScroll({
            allowPageScroll: true, // allow page scroll when the element scroll is ended
            size: '7px',
            color: ($(this).attr("data-handle-color") ? $(this).attr("data-handle-color") : '#bbb'),
            wrapperClass: ($(this).attr("data-wrapper-class") ? $(this).attr("data-wrapper-class") : 'slimScrollDiv'),
            railColor: ($(this).attr("data-rail-color") ? $(this).attr("data-rail-color") : '#eaeaea'),
            position: 'right',
            height: height,
            alwaysVisible: ($(this).attr("data-always-visible") == "1" ? true : false),
            railVisible: ($(this).attr("data-rail-visible") == "1" ? true : false),
            disableFadeOut: true
        });

        $(this).attr("data-initialized", "1");
    });
}