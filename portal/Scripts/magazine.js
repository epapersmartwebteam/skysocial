﻿/*
 * Magazine sample
*/
function findPageObjectByPageNumber(data, value) {
    for (var i = 0; i < data.length; i++) {
        if (data[i].PageNumber == value) {
            return (data[i]);
        }
    }
}
var viewerMargin = 70;

function addPage(page, book, pageData) {

    var id, pages = book.turn('pages');

    // Create a new element for this page
    var element = $('<div class=""/>', {});

    // Add the page to the flipbook
    if (book.turn('addPage', element, page)) {

        // Add the initial HTML
        // It will contain a loader indicator and a gradient
        element.html('<div class="gradient"></div><div class="loader"></div>');

        // Load the page
        loadPage(page, element, pageData);
    }

}

function loadPage(page, pageElement, pageData) {
    var obj = findPageObjectByPageNumber(pageData, page);
    switch (obj.PageType) {
        case 1:
            {
                // Create an image element
                var img = $('<img />');

                img.mousedown(function (e) {
                    e.preventDefault();
                });

                img.load(function () {

                    // Set the size
                    $(this).css({ width: '100%', height: '100%' });

                    // Add the image to the page after loaded

                    $(this).appendTo(pageElement);

                    // Remove the loader indicator

                    
                    pageElement.find('.loader').remove();
                });
                
                // Load the page
                var link = obj.FullImageUrl;
                img.attr('src', link);
                break;
            }
        case 2:
            {
                var div = $('<div></div>');
                div.html(obj.HTML);

                var imgs = div.find('img');

                imgs.each(function () {
                    $(this).load(function () {
                        // Set the size
                        $(this).css('max-width', '98%');
                    });
                });

                pageElement.addClass('overflow');
                
                div.appendTo(pageElement);
                pageElement.find('.loader').remove();
                pageElement.find('.gradient').remove();

                break;
            }
        case 3:
            {
                var ratioHeight = 0;
                pageElement.addClass('overflow');
                if (obj.VideoUrl != '') {
                    if (obj.VideoUrl.indexOf('youtube.com') > -1 || obj.VideoUrl.indexOf('youtu.be') > -1) {
                        //youtube video
                        var videoid = obj.VideoUrl.match(/(?:https?:\/{2})?(?:w{3}\.)?youtu(?:be)?\.(?:com|be)(?:\/watch\?v=|\/)([^\s&]+)/);

                        ratioHeight = (pageData.length == 1 ? (pageWidth / 2) : pageWidth) * (315 / 560);

                        var frame = $('<iframe  class="region videoregion" width="' + ((pageData.length == 1 ? (pageWidth / 2) : pageWidth) - 10) + '" height="' + ratioHeight + '"  src="http://www.youtube.com/embed/' + videoid[1] + '?rel=0" frameborder="0" allowfullscreen></iframe>');

                        frame.appendTo(pageElement);
                        
                    }
                    else {
                        var video = '<video class="video-mp4 region videoregion " data-id="' + obj.PageID + '" controls ><source type="audio/mp4" src="' + obj.VideoUrl + '"></video>';
                        $(video).appendTo(pageElement);
                        
                    }
                }
                var div = $('<div style="margin-top:' + (ratioHeight + 10) + 'px"></div>');
                div.html(obj.HTML);

                var imgs = div.find('img');

                imgs.each(function () {
                    $(this).load(function () {
                        // Set the size
                        $(this).css('max-width', '98%');
                    });
                });

                pageElement.addClass('overflow');
                div.appendTo(pageElement);

                pageElement.find('.loader').remove();
                pageElement.find('.gradient').remove();
                break;
            }
    }


    //loadRegions(page, pageElement, urlPrefix);

}

// Zoom in / Zoom out

function zoomTo(event) {

    setTimeout(function () {
        if ($('.magazine-viewport').data().regionClicked) {
            $('.magazine-viewport').data().regionClicked = false;
        } else {
            if ($('.magazine-viewport').zoom('value') == 1) {
                $('.magazine-viewport').zoom('zoomIn', event);
            } else {
                $('.magazine-viewport').zoom('zoomOut');
            }
        }
    }, 1);

}

// Load large page

function loadLargePage(page, pageElement, pageData) {
    var obj = findPageObjectByPageNumber(pageData, page);
    switch (obj.PageType) {
        case 1:
            {
                var img = $('<img />');

                img.load(function () {

                    var prevImg = pageElement.find('img');
                    $(this).css({ width: '100%', height: '100%' });
                    $(this).appendTo(pageElement);
                    prevImg.remove();

                });

                // Loadnew page
                var link = obj.FullImageUrl;
                img.attr('src', link);
                break;
            }
    }

}

// Load small page

function loadSmallPage(page, pageElement, pageData) {
    var obj = findPageObjectByPageNumber(pageData, page);
    switch (obj.PageType) {
        case 1:
            {
                var img = pageElement.find('img');

                img.css({ width: '100%', height: '100%' });

                img.unbind('load');
                // Loadnew page
                var link = obj.FullImageUrl;
                img.attr('src', link);
                break;
            }
    }
}

// http://code.google.com/p/chromium/issues/detail?id=128488

function isChrome() {

    return navigator.userAgent.indexOf('Chrome') != -1;

}

function disableControls(page) {
    if (page == 1)
        $('.previous-button').hide();
    else
        $('.previous-button').show();

    if (page == $('.magazine').turn('pages'))
        $('.next-button').hide();
    else
        $('.next-button').show();
}

// Set the width and height for the viewport

function resizeViewport() {
    //tự thêm vào
    //$('body').height($(window).height());

    var width = $(window).width(),
		height = $(window).height() - viewerMargin,
		options = $('.magazine').turn('options');
    if ($('.magazine').length > 0) {
        $('.magazine').removeClass('animated');

        $('.magazine-viewport').css({
            width: width,
            height: height
        }).
        zoom('resize');


        if ($('.magazine').turn('zoom') == 1) {
            var bound = calculateBound({
                width: options.width,
                height: options.height,
                boundWidth: Math.min(options.width, width),
                boundHeight: Math.min(options.height, height)
            });

            if (bound.width % 2 !== 0)
                bound.width -= 1;


            if (bound.width != $('.magazine').width() || bound.height != $('.magazine').height()) {

                $('.magazine').turn('size', bound.width, bound.height);

                if ($('.magazine').turn('page') == 1)
                    $('.magazine').turn('peel', 'br');

                $('.next-button').css({ height: bound.height, backgroundPosition: '-38px ' + (bound.height / 2 - 32 / 2) + 'px' });
                $('.previous-button').css({ height: bound.height, backgroundPosition: '-4px ' + (bound.height / 2 - 32 / 2) + 'px' });
            }

            $('.magazine').css({ top: -bound.height / 2, left: -bound.width / 2 });
        }

        var magazineOffset = $('.magazine').offset(),
            boundH = height - magazineOffset.top - $('.magazine').height();
        //marginTop = (boundH - $('.thumbnails > div').height()) / 2;

        //if (marginTop<0) {
        //	$('.thumbnails').css({height:1});
        //} else {
        //	$('.thumbnails').css({height: boundH});
        //	$('.thumbnails > div').css({marginTop: marginTop});
        //}

        if (magazineOffset.top < $('.made').height())
            $('.made').hide();
        else
            $('.made').show();

        $('.magazine').addClass('animated');
    }
}


// Number of views in a flipbook

function numberOfViews(book) {
    return book.turn('pages') / 2 + 1;
}

// Current view in a flipbook

function getViewNumber(book, page) {
    return parseInt((page || book.turn('page')) / 2 + 1, 10);
}

function moveBar(yes) {
    if (Modernizr && Modernizr.csstransforms) {
        $('#slider .ui-slider-handle').css({ zIndex: yes ? -1 : 10000 });

    }
}

function setPreview(view) {

    var previewWidth = 112,
		previewHeight = 73,
		previewSrc = 'pages/preview.jpg',
		preview = $(_thumbPreview.children(':first')),
		numPages = (view == 1 || view == $('#slider').slider('option', 'max')) ? 1 : 2,
		width = (numPages == 1) ? previewWidth / 2 : previewWidth;

    _thumbPreview.
		addClass('no-transition').
		css({
		    width: width + 15,
		    height: previewHeight + 15,
		    top: -previewHeight - 30,
		    left: ($($('#slider').children(':first')).width() - width - 15) / 2
		});

    preview.css({
        width: width,
        height: previewHeight
    });

    if (preview.css('background-image') === '' ||
		preview.css('background-image') == 'none') {

        preview.css({ backgroundImage: 'url(' + previewSrc + ')' });

        setTimeout(function () {
            _thumbPreview.removeClass('no-transition');
        }, 0);

    }

    preview.css({
        backgroundPosition:
            '0px -' + ((view - 1) * previewHeight) + 'px'
    });
}

// Width of the flipbook when zoomed in

function largeMagazineWidth() {

    return 2214;

}

// decode URL Parameters

function decodeParams(data) {

    var parts = data.split('&'), d, obj = {};

    for (var i = 0; i < parts.length; i++) {
        d = parts[i].split('=');
        obj[decodeURIComponent(d[0])] = decodeURIComponent(d[1]);
    }

    return obj;
}

// Calculate the width and height of a square within another square

function calculateBound(d) {

    var bound = { width: d.width, height: d.height };

    if (bound.width > d.boundWidth || bound.height > d.boundHeight) {

        var rel = bound.width / bound.height;

        if (d.boundWidth / rel > d.boundHeight && d.boundHeight * rel <= d.boundWidth) {

            bound.width = Math.round(d.boundHeight * rel);
            bound.height = d.boundHeight;

        } else {

            bound.width = d.boundWidth;
            bound.height = Math.round(d.boundWidth / rel);

        }
    }

    return bound;
}


// Load regions

function loadRegions(page, element, urlPrefix) {

    $.getJSON(urlPrefix + 'pages/' + page + '-regions.json').
		done(function (data) {

		    $.each(data, function (key, region) {
		        addRegion(region, element);
		    });
		});
}

// Add region

function addRegion(region, pageElement) {

    var reg = $('<div />', { 'class': 'region  ' + region['class'] }),
		options = $('.magazine').turn('options'),
		pageWidth = options.width / 2,
		pageHeight = options.height;

    reg.css({
        top: Math.round(region.y / pageHeight * 100) + '%',
        left: Math.round(region.x / pageWidth * 100) + '%',
        width: Math.round(region.width / pageWidth * 100) + '%',
        height: Math.round(region.height / pageHeight * 100) + '%'
    }).attr('region-data', $.param(region.data || ''));


    reg.appendTo(pageElement);
}

// Process click on a region

function regionClick(event) {

    var region = $(event.target);

    if (region.hasClass('region')) {

        $('.magazine-viewport').data().regionClicked = true;

        setTimeout(function () {
            $('.magazine-viewport').data().regionClicked = false;
        }, 100);

        var regionType = $.trim(region.attr('class').replace('region', ''));

        return processRegion(region, regionType);

    }

}

// Process the data of every region

function processRegion(region, regionType) {

    data = decodeParams(region.attr('region-data'));

    switch (regionType) {
        case 'link':

            window.open(data.url);

            break;
        case 'zoom':

            var regionOffset = region.offset(),
				viewportOffset = $('.magazine-viewport').offset(),
				pos = {
				    x: regionOffset.left - viewportOffset.left,
				    y: regionOffset.top - viewportOffset.top
				};

            $('.magazine-viewport').zoom('zoomIn', pos);

            break;
        case 'to-page':

            $('.magazine').turn('page', data.page);

            break;
    }

}