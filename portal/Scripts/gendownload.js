﻿$(document).ready(function () {
    var script_tag = document.getElementById('script-downloadhub');
    var query = script_tag.src.replace(/^[^\?]+\??/, '');
    // Parse the querystring into arguments and parameters
    var vars = query.split("&");
    var args = {};
    for (var i = 0; i < vars.length; i++) {
        var pair = vars[i].split("=");
        // decodeURI doesn't expand "+" to a space.
        args[pair[0]] = decodeURI(pair[1]).replace(/\+/g, ' ');
    }
    var Hub = args['Hub'];
    getInfoSiteHub(Hub);

});

function getInfoSiteHub(Hub)
{
    $.ajax({
        url: '/Download/GetInfoHub?Hub='+ Hub,
        type: 'GET',
        crossDomain: true,
        beforeSend: function (request) {
            request.setRequestHeader("Access-Control-All-Headers", "Origin, X-Requested-With, Content-Type, Accept, Key");
            request.setRequestHeader("Access-Control-Allow-Origin", "*");
            request.setRequestHeader("Access-Control-Allow-Methods", "GET,HEAD,OPTIONS,POST,PUT");
            request.setRequestHeader("Access-Control-Allow-Credentials", "true");
            request.setRequestHeader("Access-Control-Allow-Headers", "Access-Control-Allow-Headers, Origin,Accept, X-Requested-With, Content-Type, Access-Control-Request-Method, Access-Control-Request-Headers");
        },
        contentType: 'application/json; charset=utf-8',
        success: function (r) {
            gencode(r)
        }, error: function () {

        }
    })
}

function gencode(Hub)
{
    var objHub = JSON.parse(JSON.stringify(Hub));
    console.log(objHub);
    var menu = '';
    var item = '';
    $.each(objHub.Menus,function(i,o)
    {
        item+='<div style="padding-left:5px;">' +
        '   <p><img src="/Content/pics/ic_logo_epapersmart.png" style="width:15px;" />  ' + o.MenuName + '</p>' +
      ' </div>'
    })
    if (objHub.MenuColor == '') {
        menu = ' <div style="background:#273238 ;width: 130px;height: 322px;position: absolute;top: 112px;left: 26px; color:white;">' +
                                           ' <div>' +
                                                '<img src="' + objHub.LogoUrl + '" style="width: 60px" />' +
                                        '    </div>' +
                                          '  <div style="text-align:left;font-size:9px;height: 264px;overflow: hidden;">' +
                                               ' <p style="padding-top:20px;font-size:11px">Mục lục</p>' +
                                                item+
                                                '<p style="padding:5px 0;font-size:11px">Khác</p>' +
                                               ' <div style="padding-left:5px;">' +
                                                  '  <p><img src="/Content/pics/ic_share.png" style="width:15px;" />  Chia sẻ</p>' +
                                                   ' <p><img src="/Content/pics/ic_rating.png" style="width:15px;" />  Đánh giá</p>' +
                                                   ' <p><img src="/Content/pics/ic_code_while.png" style="width:15px;" />  Nhập mã</p>' +
                                                    '<p><img src="/Content/pics/ic_shortcut.png" style="width:15px;" />  Tạo shortcut</p>' +
                                                   ' <p><img src="/Content/pics/ic_logo_epapersmart.png" style="width:15px;" />  Cài đặt</p>' +
                                              '  </div>' +
                                            '</div>' +
                                       ' </div>';
    }
    else
    {
        menu = ' <div style="'+objHub.MenuColor+';width: 130px;height: 322px;position: absolute;top: 112px;left: 26px; color:white;">' +
                                           ' <div>' +
                                                '<img src="@Model.LogoUrl" style="width: 60px" />' +
                                        '    </div>' +
                                          '  <div style="text-align:left;font-size:9px;height: 264px;overflow: hidden;">' +
                                               ' <p style="padding-top:20px;font-size:11px">Mục lục</p>' +
                                                item +
                                                '<p style="padding:5px 0;font-size:11px">Khác</p>' +
                                               ' <div style="padding-left:5px;">' +
                                                  '  <p><img src="Content/pics/ic_share.png" style="width:15px;" />  Chia sẻ</p>' +
                                                   ' <p><img src="/Content/pics/ic_rating.png" style="width:15px;" />  Đánh giá</p>' +
                                                   ' <p><img src="/Content/pics/ic_code_while.png" style="width:15px;" />  Nhập mã</p>' +
                                                    '<p><img src="/Content/pics/ic_shortcut.png" style="width:15px;" />  Tạo shortcut</p>' +
                                                   ' <p><img src="/Content/pics/ic_logo_epapersmart.png" style="width:15px;" />  Cài đặt</p>' +
                                              '  </div>' +
                                            '</div>' +
                                       ' </div>';
    }

    var html = '<div class="container-fluid">'+
    '<div class="fullscreen background">'+
        '<div class="row">'+
            '<div class="col-md-12 col-sm-12 col-xs-12" style="background-color:white;">'+
              ' <p style="font-size:35px;text-align:center;">'+
              '  <span><img src="' + objHub.LogoUrl + '" class="img-responsive" id="hublogo"></span><span id="hubname-logo">' + objHub.HubName + '</span>' +
              '</p> '+
               
                '<div class="col-md-12 text-center">'+
                    
                   ' <div class="col-md-12 col-sm-12 col-xs-12 hero_text">'+
                   '  <span>“Hubs App - ứng dụng trãi nghiệm các kho nội dung số (Hubs) được tổ chức trên nền tảng quản lý và phát hành nội dung - eHubly”</span>'+
                   ' </div>'+
                    
               ' </div>'+
               
           ' </div>' +
                
    '<div class="col-md-12 hero_app_link text-center">'+
                '<div class="col-md-6 col-sm-6 col-xs-12 " style="padding-top:3em;">'+
                      '  <h2>Trải nghiệm trên Smartphone</h2>'+
    ' <h4 class="text-center">- Cài đặt <b>Hubs App - eContent Portal</b></h4>'+
    ' <a href="@ViewBag.Android">'+
    ' <img src="/Content/pics/google_play.png" class="img-responsive">' +
    '</a>'+
                       ' <a href="@ViewBag.iOS">'+
                            '<img src="/Content/pics/app_store.png" class="img-responsive">' +
                       ' </a>'+
                       ' <h4 class="text-center">- Truy cập vào <b style="color:orangered"> '+objHub.HubName+'</b></h4>'+
                       ' <div class="col-md-6 col-sm-6 col-xs-12">'+
                          '  <div class="l-v-spaced">'+
                             '   <div>'+
                                 '   <div style="height:160px;width:160px;padding-top:70px;margin:0 auto;font-size: 20px;" class="large-font l-padded l-v-top-spaced strong l-h-spaced crd-gray">' + objHub.HubCode + '</div>' +
                               ' </div>'+
                           ' </div>'+
                           ' <p class="large-font strong l-v-bottom-half-spaced text-center">Nhập mã passphrase </p>'+
                        '</div>'+
                        '<div class="col-md-6 col-sm-6 col-xs-12">'+
                           ' <div class="l-v-spaced" style="height:160px;">'+
                               ' <div class="l-inline-block" style="padding:0;min-width:0;">'+
                                   ' <div class="text-center"><img style="height:160px;width:160px;" class="l-block" src="https://chart.googleapis.com/chart?chs=160x160&amp;cht=qr&amp;chl="'+objHub.HubCode+'"&amp;choe=UTF-8&amp;chld=L|0"></div>'+
                               ' </div>'+
                            '</div>'+
                           ' <p class="large-font strong l-v-bottom-half-spaced text-center">Quét mã QR</p>'+
                        '</div>'+
                    '</div>'+
                    '<div class="col-md-6 col-sm-6 col-xs-12">'+
                        '<div id="pool-mobileapp">'+
                           ' <div style="background:url(/Content/pics/mobile-theme1.png);width:100%;height:450px;background-size: 100%;">'+
                               menu+
                           ' </div>'+
                        '</div>'+
                    '</div>'+

                '</div>'+

               
                '<div class="col-md-12 col-sm-12 col-xs-12" id="part-2-downhub">'+
                   ' <div class="col-md-6 col-sm-6 col-xs-12" id="download-webapp">'+

                       ' <h2 class="text-center">Trải nghiệm trên Webapp</h2>'+
                        '<h4 class="text-center">Truy cập vào <a href="http://portal.ehubly.com/Hub/"'+objHub.HubCode+'"" style="text-decoration:none;color:orangered">portal.ehubly.com</a></h4>'+

                    '</div>'+

                    '<div class="col-md-6 col-sm-6 col-xs-12 text-center">'+
                        ' <img src="/Content/pics/webapp.png"/> ' +
                    '</div>'+
              '  </div>'+
            '</div>'+



       ' </div>'+

'</div>'+

    '<script>' +

    'window.isAndroid = function () {' +
        'var ua = navigator.userAgent.toLowerCase();' +
        'var isAndroid = ua.indexOf("android") > -1;' +
        'return isAndroid;' +
    '};' +
    'window.iOScheck = function () {' +
       ' var check = false;' +
        '(function (a) {' +
           ' if (/ip(hone|od|ad)/i.test(a))' +
                'check = true' +
        '})(navigator.userAgent || navigator.vendor || window.opera);' +
        'return check;' +
    '};' +

    'function checkIsMobilePhone() {' +
        'if (window.iOScheck()) {' +
            'window.location.href = https://itunes.apple.com/us/app/hubs-app-digital-publishing/id1092456305?mt=8;' +
        '}' +
        'else if (window.isAndroid()) {' +
            'window.location.href = https://play.google.com/store/apps/details?id=ehubly.android;' +
       ' }' +
   ' };' +
    '$(document).ready(function () {' +
        'checkIsMobilePhone();' +
    '});' +

    'function resize() {'+
        'if ($(window).width() < 768) {'+
            '$("#hubname-logo").addClass("col-xs-12");'+

    '}'+
        'else {'+
           ' $("#hubname-logo").removeClass("col-xs-12");' +
    ' }'+
    '}'+
    '$(window).resize(function () {'+
    ' resize();'+
    '});'+

    '</script>';

    $('#parent-promote-app').append(html);
}