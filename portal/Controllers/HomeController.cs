﻿using System;
using System.Collections.Specialized;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

using portal.Models;
using portal.Helpers;

using API_NganLuong;
using com.paypal.sdk.util;

namespace portal.Controllers
{
    public class HomeController : Controller
    {
        public ActionResult Index()
        {
            return View();
        }

        public ActionResult Header()
        {
            return PartialView();
        }
        public ActionResult Footer()
        {
            return PartialView();
        }

        public ActionResult Media(int id, bool v, string os)
        {
            if (Request.Headers["Authorization"] == null)
            {
                return Redirect(Url.Content("~/Error") + "?code=114");
            }

            ViewBag.OS = os;
            //var cookies = Request.Cookies;
            if (!string.IsNullOrEmpty(os))
            {
                Request.Headers.Add("OS", os);
            }

            PayItemModel m = new PayItemModel();
            ReturnResult result = APIHelper.GetMediaSellConfig(Request, id);
            if (result.ErrorCode == 0)
            {
                Media mA = (Media)result.Data;
                m = PayItemModel.ToModel(mA, v);
            }
            else
            {
                return Redirect(Url.Content("~/Error") + "?code=" + result.ErrorCode);
            }
            ViewBag.Vip = v;

            //if (Request.Cookies["connect.sid"] != null)
            //{
            //    HttpCookie ConnectCookie = new HttpCookie("connect.sid");
            //    ConnectCookie.Value = Request.Cookies["connect.sid"].Value;
            //    ConnectCookie.Expires = DateTime.Now.AddHours(1);
            //    Response.SetCookie(ConnectCookie);

            //}
            //if (Request.Cookies["ARRAffinity"] != null)
            //{
            //    HttpCookie ARRACookie = new HttpCookie("ARRAffinity");
            //    ARRACookie.Value = Request.Cookies["ARRAffinity"].Value;
            //    ARRACookie.Expires = DateTime.Now.AddHours(1);
            //    Response.SetCookie(ARRACookie);
            //}
            //if (!string.IsNullOrEmpty(os))
            //{
            //    HttpCookie OSCookie = new HttpCookie("OS");
            //    OSCookie.Value = os;
            //    OSCookie.Expires = DateTime.Now.AddHours(1);
            //    Response.SetCookie(OSCookie);
            //}

            if (Request.Headers["Authorization"] != null)
            {
                Response.Headers.Add("Authorization", Request.Headers["Authorization"]);
                HttpCookie AuthorizationCookie = new HttpCookie("Authorization");
                AuthorizationCookie.Value = Request.Headers["Authorization"];
                AuthorizationCookie.Expires = DateTime.UtcNow.AddHours(8);
                Response.SetCookie(AuthorizationCookie);
            }

            if (Request.Headers["OS"] != null)
            {
                Response.Headers.Add("OS", Request.Headers["OS"]);
                HttpCookie OSCookie = new HttpCookie("OS");
                OSCookie.Value = Request.Headers["OS"];
                OSCookie.Expires = DateTime.UtcNow.AddHours(8);
                Response.SetCookie(OSCookie);
            }

            return View(m);
            //return Redirect(Url.Content("~/PaymentForm") + "?id=" + id + "&v=" + v + "&t=1");
        }

        public ActionResult Vip(int? id, string os)
        {
            if (Request.Headers["Authorization"] == null)
            {
                return Redirect(Url.Content("~/Error") + "?code=114");
            }


            ViewBag.OS = os;

            if (!string.IsNullOrEmpty(os))
            {
                Request.Headers.Add("OS", os);
            }

            if (id.HasValue)
            {
                return Redirect(Url.Content("~/PaymentForm") + "?id=" + id.Value + "&v=false&t=0");
            }
            List<AppVipSellConfig> lst = new List<AppVipSellConfig>();
            ReturnResult result = APIHelper.GetVipPackConfigs();
            if (result.ErrorCode == 0)
            {
                lst = (List<AppVipSellConfig>)result.Data;
            }
            else
            {
                return Redirect(Url.Content("~/Error") + "?code=" + result.ErrorCode);
            }

            //if (Request.Cookies["connect.sid"] != null)
            //{
            //    HttpCookie ConnectCookie = new HttpCookie("connect.sid");
            //    ConnectCookie.Value = Request.Cookies["connect.sid"].Value;
            //    ConnectCookie.Expires = DateTime.Now.AddHours(1);
            //    Response.SetCookie(ConnectCookie);

            //}
            //if (Request.Cookies["ARRAffinity"] != null)
            //{
            //    HttpCookie ARRACookie = new HttpCookie("ARRAffinity");
            //    ARRACookie.Value = Request.Cookies["ARRAffinity"].Value;
            //    ARRACookie.Expires = DateTime.Now.AddHours(1);
            //    Response.SetCookie(ARRACookie);

            //}
            //if (!string.IsNullOrEmpty(os))
            //{
            //    HttpCookie OSCookie = new HttpCookie("OS");
            //    OSCookie.Value = os;
            //    OSCookie.Expires = DateTime.Now.AddHours(1);
            //    Response.SetCookie(OSCookie);
            //}

            if (Request.Headers["Authorization"] != null)
            {
                Response.Headers.Add("Authorization", Request.Headers["Authorization"]);
                HttpCookie AuthorizationCookie = new HttpCookie("Authorization");
                AuthorizationCookie.Value = Request.Headers["Authorization"];
                AuthorizationCookie.Expires = DateTime.UtcNow.AddHours(8);
                Response.SetCookie(AuthorizationCookie);
            }

            if (Request.Headers["OS"] != null)
            {
                Response.Headers.Add("OS", Request.Headers["OS"]);
                HttpCookie OSCookie = new HttpCookie("OS");
                OSCookie.Value = Request.Headers["OS"];
                OSCookie.Expires = DateTime.UtcNow.AddHours(8);
                Response.SetCookie(OSCookie);
            }

            return View(lst);
        }

        public ActionResult PaymentForm(int id, int t, bool v, string os)
        {
            PayItemModel m = new PayItemModel();
            ViewBag.ItemId = id;
            ViewBag.ItemType = t;
            ViewBag.VipUser = v;

            if (t == 0) //vip
            {
                ReturnResult result = APIHelper.GetVipPackConfig(id);
                if (result.ErrorCode == 0)
                {
                    AppVipSellConfig mA = (AppVipSellConfig)result.Data;
                    if (mA.ConfigId >= 0 && mA.Active.HasValue && mA.Active.Value)
                    {
                        m = PayItemModel.ToModel(mA);
                    }
                    else
                    {
                        //Gói vip ko tồn tại hoặc bị 
                        return Redirect(Url.Content("~/Error") + "?code=102");
                    }
                }
                else
                {
                    return Redirect(Url.Content("~/Error") + "?code=" + result.ErrorCode);
                }

            }
            else if (t == 1) //media
            {
                ReturnResult result = APIHelper.GetMediaSellConfig(Request, id);
                if (result.ErrorCode == 0)
                {
                    Media mA = (Media)result.Data;
                    m = PayItemModel.ToModel(mA, v);

                }
                else
                {
                    return Redirect(Url.Content("~/Error") + "?code=" + result.ErrorCode);
                }
            }

            //if (Request.Cookies["connect.sid"] != null)
            //{
            //    HttpCookie ConnectCookie = new HttpCookie("connect.sid");
            //    ConnectCookie.Value = Request.Cookies["connect.sid"].Value;
            //    ConnectCookie.Expires = DateTime.Now.AddHours(1);
            //    Response.SetCookie(ConnectCookie);

            //}
            //if (Request.Cookies["ARRAffinity"] != null)
            //{
            //    HttpCookie ARRACookie = new HttpCookie("ARRAffinity");
            //    ARRACookie.Value = Request.Cookies["ARRAffinity"].Value;
            //    ARRACookie.Expires = DateTime.Now.AddHours(1);
            //    Response.SetCookie(ARRACookie);

            //}
            //if (!string.IsNullOrEmpty(os))
            //{
            //    HttpCookie OSCookie = new HttpCookie("OS");
            //    OSCookie.Value = os;
            //    OSCookie.Expires = DateTime.Now.AddHours(1);
            //    Response.SetCookie(OSCookie);
            //}

            if (!string.IsNullOrEmpty(os))
            {
                Response.AddHeader("OS", os);
            }

            if (!string.IsNullOrEmpty(Request.Headers["Authorization"]))
            {
                Response.AddHeader("Authorization", Request.Headers["Authorization"]);
            }

            if (Request.Cookies["Authorization"] != null)
            {
                Response.Headers.Add("Authorization", Request.Cookies["Authorization"].Value);
                HttpCookie AuthorizationCookie = new HttpCookie("Authorization");
                AuthorizationCookie.Value = Request.Cookies["Authorization"].Value;
                AuthorizationCookie.Expires = DateTime.UtcNow.AddHours(8);
                Response.SetCookie(AuthorizationCookie);
            }

            if (Request.Cookies["OS"] != null)
            {
                Response.Headers.Add("OS", Request.Cookies["OS"].Value);
                HttpCookie OSCookie = new HttpCookie("OS");
                OSCookie.Value = Request.Cookies["OS"].Value;
                OSCookie.Expires = DateTime.UtcNow.AddHours(8);
                Response.SetCookie(OSCookie);
            }
            return View(m);
        }

        public JsonResult CreateOrder(int ItemId, int PayTypeId, int PayMethodId, string Bank)
        {
            PaymentResult rp = CreateCustomerOrder(Request, ItemId, PayTypeId, PayMethodId, Bank);
            return Json(rp, JsonRequestBehavior.AllowGet);
        }

        private PaymentResult CreateCustomerOrder(HttpRequestBase requestBase, int ItemId, int PayTypeId, int PayMethodId, string Bank)
        {
            PaymentResult rp = new PaymentResult() { Success = false };
            int Price = 0;
            string ItemName = "";
            bool IsDigital = true;
            bool IsVipPurchase = false;
            int VipMonth = 0;
            User mU = APIHelper.GetUserInfo(requestBase);
            switch (PayTypeId)
            {
                case 1:
                    {
                        ReturnResult res = APIHelper.GetMediaSellConfig(requestBase, ItemId);
                        if (res.ErrorCode == 0)
                        {
                            Media m = (Media)res.Data;
                            ItemName = m.MediaType + ": " + m.Title;
                            Price = (mU.IsVip ?? false) ? (m.VipPrice ?? (m.Price ?? 0)) : (m.Price ?? 0);
                        }
                        break;
                    }
                case 0:
                    {
                        IsVipPurchase = true;
                        ReturnResult res = APIHelper.GetVipPackConfig(ItemId);
                        if (res.ErrorCode == 0)
                        {
                            AppVipSellConfig m = (AppVipSellConfig)res.Data;
                            ItemName = "Gói " + m.VipLevel + ": " + m.Month + " tháng.";
                            Price = m.Price ?? 0;
                            VipMonth = m.Month ?? 1;
                        }
                        break;
                    }
            }

            ReturnResult result = APIHelper.CreatePaymentLog(requestBase, PayMethodId, Price, "", 0, PayTypeId, ItemId, IsDigital, IsVipPurchase, VipMonth, ItemName);
            if (result.ErrorCode == 0)
            {
                UserPaymentLog m = (UserPaymentLog)result.Data;
                if (Price <= 0)
                {
                    ReturnResult res = APIHelper.UpdatePaymentIsPay(requestBase, m.PaymentCode, "0 đồng");
                    if (res.ErrorCode == 0)
                    {
                        rp.Success = true;
                        rp.ErrorCode = 1;
                    }
                }
                else
                {
                    switch (PayMethodId)
                    {
                        case 1: //thẻ nội địa
                        case 3: //thẻ quốc tế
                            {
                                string NLPaymentMethod = PayMethodId == 1 ? "ATM_ONLINE" : "VISA"; //thẻ quốc tế hoặc nội địa

                                RequestInfo info = new RequestInfo();
                                info.Merchant_id = NganLuongInfo.MerchantSiteCode;
                                info.Merchant_password = NganLuongInfo.SecurePass;
                                info.Receiver_email = NganLuongInfo.Receiver;
                                info.cur_code = "vnd";
                                info.bank_code = Bank ?? string.Empty;
                                info.Order_code = m.PaymentCode;

                                double FinaTotal = (double)(m.Total ?? 0);
                                String price = (FinaTotal).ToString("0");

                                info.Total_amount = price;
                                info.fee_shipping = "0";
                                info.Discount_amount = "0";

                                String transaction_info = "Thanh toán " + ItemName;
                                info.order_description = transaction_info; //truyền xuống luôn
                                info.Payment_type = "1";//giao dịch ngay

                                info.return_url = NganLuongInfo.SuccessUrl;
                                info.cancel_url = NganLuongInfo.CancelUrl;

                                info.Buyer_fullname = mU.FullName;
                                info.Buyer_email = string.IsNullOrEmpty(mU.Email) ? DAL.DefaultPaymentEmail : mU.Email;
                                info.Buyer_mobile = mU.Phone ?? DAL.DefaultPaymentMobile;

                                APICheckoutV3 objNLChecout = new APICheckoutV3();
                                ResponseInfo res1 = objNLChecout.GetUrlCheckout(info, NLPaymentMethod);

                                if (res1.Error_code == "00")
                                {
                                    rp.Success = true;
                                    rp.Result = res1.Checkout_url;
                                }
                                else
                                {
                                    rp.Success = false;
                                    rp.Result = res1.Description;
                                }

                                break;
                            }
                        case 4: //PayPal
                            {
                                string API_UserName = Paypal_Checkout.UserName;
                                string API_Password = Paypal_Checkout.Password;
                                string API_Signature = Paypal_Checkout.MerchantId;
                                string ReturnUrl = Paypal_Checkout.SuccessUrl;
                                string CancelUrl = Paypal_Checkout.CancelUrl;


                                NVPCodec Result = new NVPCodec();
                                string TOKEN = Paypal_Checkout.createPayPalTOKEN(API_UserName, API_Password, API_Signature, Paypal_Checkout.PayPalEnviroment, ReturnUrl, CancelUrl, m, ItemName, out Result);

                                if (TOKEN != "")
                                {
                                    rp.Success = true;
                                    rp.Result = Paypal_Checkout.buildCheckoutUrl(Paypal_Checkout.PayPalEnviroment, TOKEN);
                                }
                                else
                                {
                                    rp.Success = false;
                                    rp.Result = "Lỗi Paypal: " + Result.ToString();
                                }
                                break;
                            }
                        case 2: //chuyển khoản
                            {
                                rp.Success = true;
                                rp.Result = m.PaymentCode;
                                break;
                            }
                    }
                }
            }
            else
            {
                rp.Success = false;
                rp.Result = result.Message;
            }

            return rp;
        }

        public ActionResult Error(int code)
        {
            ViewBag.ErrorCode = code;
            return View();
        }

        public ActionResult Cancel(string m)
        {
            ViewBag.Method = m;
            return View();
        }

        public ActionResult PaySuccess(string m)
        {
            ViewBag.Message = m;
            return View();
        }

        public ActionResult NLResult()
        {
            ReturnResult m = new ReturnResult();
            string Token = Request["token"];

            RequestCheckOrder info = new RequestCheckOrder();
            info.Merchant_id = NganLuongInfo.MerchantSiteCode;
            info.Merchant_password = NganLuongInfo.SecurePass;
            info.Token = Token;

            APICheckoutV3 objNLChecout = new APICheckoutV3();
            ResponseCheckOrder res = objNLChecout.GetTransactionDetail(info);

            //HttpCookieCollection cookies = Request.Cookies;

            if (res.errorCode == "00")
            {
                switch (res.transactionStatus)
                {
                    case "00":
                    case "01":
                        {
                            string PayCode = res.order_code;
                            string TrackingId = res.transactionId;
                            //bool HasCookies = false;
                            //if (Request.Cookies["connect.sid"] != null)
                            //{
                            //    HasCookies = true;
                            //}
                            //lấy thông tin đơn hàng
                            ReturnResult res0;
                            //if (HasCookies)
                            //{
                            //    res0 = APIHelper.GetPaymentLogInfo(cookies, PayCode);
                            //}
                            //else
                            //{
                            res0 = APIHelper.GetPaymentLogInfoSystem(PayCode);
                            //}
                            if (res0.ErrorCode == 0)
                            {
                                UserPaymentLog mLog = (UserPaymentLog)res0.Data;
                                if (mLog.PaymentLogId > 0)
                                {
                                    //todo: gửi mail hoặc SMS báo thành công kèm mã code nếu có
                                    //System.Threading.Tasks.Task.Factory.StartNew(() =>
                                    //{
                                    //    SendOrderSuccessMail(mLog.PayCode, mLog.PlanName, mLog.Price, mLog.Month, mLog.AddingMonth,
                                    //        mLog.Total, mLog.Discount, mLog.DiscountPrice, mLog.FinalTotal, mLog.PaymentMethodName, UserId, UserName, Email);
                                    //});

                                    ReturnResult res1;
                                    //if (HasCookies)
                                    //{
                                    //    res1  = APIHelper.UpdatePaymentIsPay(cookies, PayCode, TrackingId);
                                    //}
                                    //else
                                    //{
                                    long UserId = mLog.UserId ?? 0;
                                    res1 = APIHelper.UpdatePaymentIsPaySystem(UserId, PayCode, TrackingId);
                                    //}

                                    if (res1.ErrorCode == 0)
                                    {
                                        m.ErrorCode = 0;
                                        m.Message = "Bạn đã thanh toán thành công " + mLog.Description;
                                    }
                                    else
                                    {
                                        m.ErrorCode = 100;
                                        m.Message = "Thanh toán thành công, tuy nhiên đã có lỗi xảy ra khi cập nhật hệ thống. Vui lòng liên hệ để được hỗ trợ.";
                                    }
                                }
                                else
                                {
                                    m.ErrorCode = 101;
                                    m.Message = "Không tìm được đơn hàng với Mã thanh toán: " + PayCode + ". Vui lòng liên hệ để được hỗ trợ.";
                                }
                            }
                            else
                            {
                                m.ErrorCode = 100;
                                m.Message = "Thanh toán thành công, tuy nhiên đã có lỗi xảy ra khi cập nhật hệ thống. Vui lòng liên hệ để được hỗ trợ.";
                            }

                            break;
                        }
                    case "02":
                        {
                            m.ErrorCode = 0;
                            m.Message = "Thanh toán đang được Review bên cổng thanh toán. Khi Review thành công thì hệ thống sẽ tự động cập nhật!";
                            break;
                        }
                }
            }
            else
            {
                m.ErrorCode = 102;
                switch (res.errorCode)
                {
                    case "00":
                        m.Message = Resources.PaymentResource.ErrorCode00;
                        m.ErrorCode = 0;
                        break;
                    case "99":
                        m.Message = Resources.PaymentResource.ErrorCode99;
                        break;
                    case "03":
                        m.Message = Resources.PaymentResource.ErrorCode03;
                        break;
                    case "04":
                        m.Message = Resources.PaymentResource.ErrorCode04;
                        break;
                    case "05":
                        m.Message = Resources.PaymentResource.ErrorCode05;
                        break;
                    case "06":
                        m.Message = Resources.PaymentResource.ErrorCode06;
                        break;
                    case "07":
                        m.Message = Resources.PaymentResource.ErrorCode07;
                        break;
                    case "08":
                        m.Message = Resources.PaymentResource.ErrorCode08;
                        break;
                    case "09":
                        m.Message = Resources.PaymentResource.ErrorCode09;
                        break;
                    case "10":
                        m.Message = Resources.PaymentResource.ErrorCode10;
                        break;
                    case "11":
                        m.Message = Resources.PaymentResource.ErrorCode11;
                        break;
                    case "12":
                        m.Message = Resources.PaymentResource.ErrorCode12;
                        break;
                    case "29":
                        m.Message = Resources.PaymentResource.ErrorCode29;
                        break;
                    case "81":
                        m.Message = Resources.PaymentResource.ErrorCode81;
                        break;
                    case "110":
                        m.Message = Resources.PaymentResource.ErrorCode110;
                        break;
                    case "111":
                        m.Message = Resources.PaymentResource.ErrorCode111;
                        break;
                    case "113":
                        m.Message = Resources.PaymentResource.ErrorCode113;
                        break;
                    case "114":
                        m.Message = Resources.PaymentResource.ErrorCode114;
                        break;
                    case "115":
                        m.Message = Resources.PaymentResource.ErrorCode115;
                        break;
                    case "118":
                        m.Message = Resources.PaymentResource.ErrorCode118;
                        break;
                    case "119":
                        m.Message = Resources.PaymentResource.ErrorCode119;
                        break;
                    case "120":
                        m.Message = Resources.PaymentResource.ErrorCode120;
                        break;
                    case "121":
                        m.Message = Resources.PaymentResource.ErrorCode121;
                        break;
                    case "122":
                        m.Message = Resources.PaymentResource.ErrorCode122;
                        break;
                    case "123":
                        m.Message = Resources.PaymentResource.ErrorCode123;
                        break;
                    case "124":
                        m.Message = Resources.PaymentResource.ErrorCode124;
                        break;
                    case "125":
                        m.Message = Resources.PaymentResource.ErrorCode125;
                        break;
                    case "126":
                        m.Message = Resources.PaymentResource.ErrorCode126;
                        break;
                    case "127":
                        m.Message = Resources.PaymentResource.ErrorCode127;
                        break;
                    case "128":
                        m.Message = Resources.PaymentResource.ErrorCode128;
                        break;
                    case "129":
                        m.Message = Resources.PaymentResource.ErrorCode129;
                        break;
                    case "130":
                        m.Message = Resources.PaymentResource.ErrorCode130;
                        break;
                    case "131":
                        m.Message = Resources.PaymentResource.ErrorCode131;
                        break;
                    case "132":
                        m.Message = Resources.PaymentResource.ErrorCode132;
                        break;
                    case "133":
                        m.Message = Resources.PaymentResource.ErrorCode133;
                        break;
                    case "134":
                        m.Message = Resources.PaymentResource.ErrorCode134;
                        break;
                    case "135":
                        m.Message = Resources.PaymentResource.ErrorCode135;
                        break;
                    case "140":
                        m.Message = Resources.PaymentResource.ErrorCode140;
                        break;
                }
            }

            return View(m);
        }

        public ActionResult PPResult()
        {
            ReturnResult m = new ReturnResult();
            string TOKEN = Request.QueryString["TOKEN"];
            string PayerID, Total, TrackingID, OrderCode, Currency = "", PaymentType = "";
            Currency = "USD";
            PaymentType = "Sale";
            string API_UserName = Paypal_Checkout.UserName;
            string API_Password = Paypal_Checkout.Password;
            string API_Signature = Paypal_Checkout.MerchantId;
            NVPCodec decoder = new NVPCodec();
            bool Result = true;

            //HttpCookieCollection cookies = Request.Cookies;

            Result = Paypal_Checkout.GetExpressCheckoutDetail(API_UserName, API_Password, API_Signature, Paypal_Checkout.PayPalEnviroment, TOKEN, out PayerID, out Total, out TrackingID, out OrderCode, out decoder);

            if (Result)
            {
                string Error = "";
                //success
                if (Paypal_Checkout.DoExpressCheckout(API_UserName, API_Password, API_Signature, Paypal_Checkout.PayPalEnviroment, TOKEN, PayerID, Total, PaymentType, Currency, out Error))
                {
                    ReturnResult res0 = APIHelper.GetPaymentLogInfoSystem(OrderCode); //luôn luôn trả về ErrorCode = 0
                    if (res0.ErrorCode == 0)
                    {
                        UserPaymentLog mLog = (UserPaymentLog)res0.Data;
                        if (mLog.PaymentLogId > 0)
                        {
                            //todo: gửi mail hoặc SMS báo thành công kèm mã code nếu có
                            //System.Threading.Tasks.Task.Factory.StartNew(() =>
                            //{
                            //    SendOrderSuccessMail(mLog.PayCode, mLog.PlanName, mLog.Price, mLog.Month, mLog.AddingMonth,
                            //        mLog.Total, mLog.Discount, mLog.DiscountPrice, mLog.FinalTotal, mLog.PaymentMethodName, UserId, UserName, Email);
                            //});

                            //ReturnResult res1 = APIHelper.UpdatePaymentIsPay(cookies, OrderCode, PayerID);

                            long UserId = mLog.UserId ?? 0;
                            ReturnResult res1 = APIHelper.UpdatePaymentIsPaySystem(UserId, OrderCode, PayerID);

                            if (res1.ErrorCode == 0)
                            {
                                m.ErrorCode = 0;
                                m.Message = "Bạn đã thanh toán thành công " + mLog.Description;
                            }
                            else
                            {
                                m.ErrorCode = 100;
                                m.Message = "Thanh toán thành công, tuy nhiên đã có lỗi xảy ra khi cập nhật hệ thống. Vui lòng liên hệ để được hỗ trợ.";
                            }
                        }
                        else
                        {
                            m.ErrorCode = 101;
                            m.Message = "Không tìm được đơn hàng với Mã thanh toán: " + OrderCode + ". Vui lòng liên hệ để được hỗ trợ.";
                        }
                    }
                    else
                    {
                        m.ErrorCode = 100;
                        m.Message = "Thanh toán thành công, tuy nhiên đã có lỗi xảy ra khi cập nhật hệ thống. Vui lòng liên hệ để được hỗ trợ.";
                    }
                }
                else
                {
                    m.ErrorCode = 101;
                    m.Message = "Kết quả thanh toán không hợp lệ.";
                }
            }
            else
            {
                m.ErrorCode = 101;
                m.Message = "Kết quả thanh toán không hợp lệ.";
            }
            return View(m);
        }
    }
}