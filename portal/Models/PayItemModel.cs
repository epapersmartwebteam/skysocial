﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using portal.Helpers;

namespace portal.Models
{
    public class PayItemModel
    {
        public int ItemId { get; set; }
        public int ItemType { get; set; }
        public string MediaType { get; set; }
        public string ItemName { get; set; }
        public string ItemDescription { get; set; }
        public string ItemImage { get; set; }
        public bool EnableMobileCardPaid { get; set; }
        public int Price { get; set; }
        public int CardAmount { get; set; }

        public static PayItemModel ToModel(Media m, bool VipUser)
        {
            return new PayItemModel()
            {
                ItemId = m.MediaId,
                ItemName = m.Title,
                ItemDescription = m.Description,
                ItemImage = m.Thumbnail,
                MediaType = m.MediaType,
                ItemType = 1,
                Price = VipUser ? (m.VipPrice ?? (m.Price ?? 0)) : (m.Price ?? 0),
                EnableMobileCardPaid = m.EnableMobileCardPaid ?? false,
                CardAmount = m.MobileCardPrice ?? 0
            };
        }

        public static PayItemModel ToModel(AppVipSellConfig m)
        {
            return new PayItemModel()
            {
                ItemId = m.ConfigId,
                ItemType = 0,
                Price = m.Price ?? 0,
                EnableMobileCardPaid = m.EnableMobileCardPaid ?? false,
                CardAmount = m.MobileCardPrice ?? 0
            };
        }
    }
}