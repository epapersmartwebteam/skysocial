﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace portal.Models
{
    public class PaymentResult
    {
        public int ErrorCode { get; set; }
        public string ErrorMessage { get; set; }
        public bool IsFirstRegister { get; set; }
        //public CustomerModel Data { get; set; }
        public bool Success { get; set; }
        public string Result { get; set; }
    }
}