﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace portal.Models
{
    public class DAL
    {
        public static string DefaultPaymentEmail = "john.doe@gmail.com";
        public static string DefaultPaymentMobile = "0987654321";
        public static void CreateAPILog(string Request, string Response)
        {
            using (eHubStarEntities db = new eHubStarEntities())
            {
                db.PaymentAPIErrorLogs.Add(new PaymentAPIErrorLog() { Request = Request, Response = Response });
                db.SaveChanges();
            }
        }
    }
}