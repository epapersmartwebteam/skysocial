﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity;
using System.Globalization;
using System.Web.Security;

namespace Web.Models
{
   public class Login
    {
        public string UserName { get; set; }
        public string Password { get; set; }
    }
}
