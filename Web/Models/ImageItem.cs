﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Web.Models
{
    public class LoginResponseInfo
    {
        public string ErrorCode { get; set; }
        public TokenInfo data { get; set; }
        public dynamic Message { get; set; }
    }

    public class TokenInfo
    {
        public string Token { get; set; }
    }
    public class ResponseImageInfo
    {
        public string ErrorCode { get; set; }
        public ImageItemInfo data { get; set; }
        public string Message { get; set; }
    }

    public class ImageItemInfo
    {
        public DataImageInfo ImageItem { get; set; }
    }

    public class DataImageInfo
    {
        public string ImageItemId { get; set; }
        public List<ImageContent> ItemContent { get; set; }
        public string Description { get; set; }
        public string CreateDate { get; set; }
        public string LikeCount { get; set; }
        public string CommentCount { get; set; }
        public string ShareCount { get; set; }
        public Owner Owner { get; set; }
        //public List<Comments> Comments { get; set; }
        public string Location { get; set; }

    }
    public class MediaObject
    {
        public string Link { get; set; }
        public int Width { get; set; }
        public int Height { get; set; }
    }
    public class AdditionInfo
    {
        public string Icon { get; set; }
        public string Text { get; set; }
        public string Link { get; set; }
        public string Target { get; set; }
    }
    public class ImageContent
    {
        public string ItemType { get; set; }
        public MediaObject Large { get; set; }
        public MediaObject Medium { get; set; }
        public MediaObject Small { get; set; }
        public AdditionInfo Info { get; set; }
    }

    public class Owner
    {
        public string UserName { get; set; }
        public string AvatarSmall { get; set; }
    }
    public class Comments
    {
        public string Comment { get; set; }
        public User User { get; set; }
    }

    public class User
    {
        public string UserName { get; set; }
        public string AvatarSmall { get; set; }

    }
}