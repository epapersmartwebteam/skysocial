﻿using Newtonsoft.Json;
using RestSharp;
using RestSharp.Authenticators;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Web.Models;

using System.Threading.Tasks;

namespace Web.Controllers
{
    public class DAL
    {
        public static string sysname = System.Configuration.ConfigurationManager.AppSettings.Get("sysname");
        public static string syspass = System.Configuration.ConfigurationManager.AppSettings.Get("syspass");
        public static string apiUrl = System.Configuration.ConfigurationManager.AppSettings.Get("apiUrl");

        public static string virtualPath = System.Configuration.ConfigurationManager.AppSettings.Get("virtualPath");

        public static string loginResource = "user/login";
        public static string graphQLResource = "data";

        public static string insightClickResource = "insight/addClick";

     
        public static ResponseImageInfo GetInfo(string ImageItemId)
        {   

            var client = new RestClient(apiUrl);

            var request = new RestRequest(loginResource, Method.POST);
            request.AddParameter("username", sysname, ParameterType.GetOrPost);
            request.AddParameter("password", syspass, ParameterType.GetOrPost);
            //client.CookieContainer = new System.Net.CookieContainer();
            IRestResponse response0 = client.Execute(request);
            var dataToken = JsonConvert.DeserializeObject<LoginResponseInfo>(response0.Content);

            ResponseImageInfo data = new ResponseImageInfo() { };

            if (dataToken.ErrorCode == "0")
            {
                var token = dataToken.data.Token;
                var request1 = new RestRequest(graphQLResource, Method.POST);
                var query = "{ImageItem(Id:" + ImageItemId + ")";
                query += "{ ImageItemId,Description,Location, CreateDate, ";
                query += "ItemContent{ItemType,Large{Link, Width, Height}, Medium{Link, Width, Height}, Info{Icon,Text, Link, Target}}, ";
                query += "LikeCount, CommentCount, ShareCount,Owner{UserName, AvatarSmall}}}";
                request1.AddParameter("query", query);
                request1.AddHeader("Authorization", "bearer " + token);
                IRestResponse response = client.Execute(request1);
                 data = JsonConvert.DeserializeObject<ResponseImageInfo>(response.Content);
            }
            return data;
        }

        public static void AddEngagement(long UserId, long PostId)
        {
            var client = new RestClient(apiUrl);

            var request = new RestRequest(insightClickResource, Method.POST);
            var json = new { UserId = UserId, PostId = PostId };
            request.RequestFormat = DataFormat.Json;
            request.AddBody(json);
            IRestResponse response = client.Execute(request);
        }
    }
}