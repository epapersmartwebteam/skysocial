﻿namespace FastDataAccess.DataAccess
{
    using Microsoft.VisualBasic;
    using Microsoft.VisualBasic.CompilerServices;
    using System;
    using System.IO;
    using System.Security.Cryptography;
    using System.Text;

    public class PortalSecurity
    {
        public string Decrypt(string strKey, string strData)
        {
            string str2 = "";
            if (StringType.StrCmp(strKey, "", false) == 0)
            {
                return strData;
            }
            int num = Strings.Len(strKey);
            if (num < 0x10)
            {
                strKey = strKey + Strings.Left("XXXXXXXXXXXXXXXX", 0x10 - Strings.Len(strKey));
            }
            else if (num > 0x10)
            {
                strKey = Strings.Left(strKey, 0x10);
            }
            byte[] bytes = Encoding.UTF8.GetBytes(Strings.Left(strKey, 8));
            byte[] rgbIV = Encoding.UTF8.GetBytes(Strings.Right(strKey, 8));
            byte[] buffer = new byte[strData.Length + 1];
            try
            {
                buffer = Convert.FromBase64String(strData);
            }
            catch (Exception exception1)
            {
                ProjectData.SetProjectError(exception1);
                str2 = "";
                ProjectData.ClearProjectError();
            }
            if (StringType.StrCmp(str2, "", false) == 0)
            {
                try
                {
                    DESCryptoServiceProvider provider = new DESCryptoServiceProvider();
                    MemoryStream stream2 = new MemoryStream();
                    CryptoStream stream = new CryptoStream(stream2, provider.CreateDecryptor(bytes, rgbIV), CryptoStreamMode.Write);
                    stream.Write(buffer, 0, buffer.Length);
                    stream.FlushFinalBlock();
                    str2 = Encoding.UTF8.GetString(stream2.ToArray());
                }
                catch (Exception exception2)
                {
                    ProjectData.SetProjectError(exception2);
                    str2 = "";
                    ProjectData.ClearProjectError();
                }
            }
            return str2;
        }

        public string Encrypt(string strKey, string strData)
        {
            if (StringType.StrCmp(strKey, "", false) == 0)
            {
                return strData;
            }
            int num = Strings.Len(strKey);
            if (num < 0x10)
            {
                strKey = strKey + Strings.Left("XXXXXXXXXXXXXXXX", 0x10 - Strings.Len(strKey));
            }
            else if (num > 0x10)
            {
                strKey = Strings.Left(strKey, 0x10);
            }
            byte[] bytes = Encoding.UTF8.GetBytes(Strings.Left(strKey, 8));
            byte[] rgbIV = Encoding.UTF8.GetBytes(Strings.Right(strKey, 8));
            byte[] buffer = Encoding.UTF8.GetBytes(strData);
            DESCryptoServiceProvider provider = new DESCryptoServiceProvider();
            MemoryStream stream2 = new MemoryStream();
            CryptoStream stream = new CryptoStream(stream2, provider.CreateEncryptor(bytes, rgbIV), CryptoStreamMode.Write);
            stream.Write(buffer, 0, buffer.Length);
            stream.FlushFinalBlock();
            return Convert.ToBase64String(stream2.ToArray());
        }
    }
}

