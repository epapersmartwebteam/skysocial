﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Web.Models;

namespace Web.Controllers
{
    public class SessionManage
    {
        public static Login SystemLogin
        {
            get
            {
                return System.Web.HttpContext.Current.Session["SystemLogin"] as Login;
            }
            set
            {
                System.Web.HttpContext.Current.Session["SystemLogin"] = value;
            }
        }

        public static Login UserLogin
        {
            get
            {
                return System.Web.HttpContext.Current.Session["UserLogin"] as Login;
            }
            set
            {
                System.Web.HttpContext.Current.Session["UserLogin"] = value;
            }
        }
    }
}