﻿using FastDataAccess.DataAccess;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Text.RegularExpressions;
using System.Web;
using System.Web.Mvc;
using Web.Models;
using System.Threading.Tasks;

namespace Web.Controllers
{
    public class DetectController : Controller
    {
        //
        // GET: /Detect/

        public ActionResult Index(string Id)
        {
            ViewBag.Id = Id;
            if (!string.IsNullOrEmpty(Id))
            {
                ResponseImageInfo data = new ResponseImageInfo();
                if (Session["UserLogin"] == null)
                {
                    Id = Regex.Replace(Id, " ", "+");
                    PortalSecurity de = new PortalSecurity();
                    string ItemId = de.Decrypt("winkjoyproject", Id);
                    ViewBag.ItemId = ItemId;
                    //string ItemId = Id;

                    data = DAL.GetInfo(ItemId);
                    ViewBag.ImageItemId = ItemId;
                }

                if (data != null && data.data != null && data.data.ImageItem != null)
                {   
                    return View(data);
                }
                else
                {
                    ViewBag.Status = "NoData";
                    return View("NoData");
                }
            }
            else
            {
                ViewBag.Status = "NoId";
                return View("NoData");
            }
        }

    }
}
