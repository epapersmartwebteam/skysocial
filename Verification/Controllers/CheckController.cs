﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;


namespace Verification.Controllers
{
    public class CheckController : Controller
    {

        // GET: Check
        [HttpPost]
        public JsonResult Index(string url)
        {
            ReturnResult result = new ReturnResult();

            result = DAL.VerifyEmailLink(url);

            return Json(result, JsonRequestBehavior.AllowGet);
        }
    }        
}