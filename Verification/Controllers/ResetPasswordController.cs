﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using RestSharp;
namespace Verification.Controllers
{
    public class ResetPasswordController : Controller
    {
        // GET: ResetPassword
        public ActionResult Index()
        {
            if (Request.QueryString["mode"] == "resetPassword")
            {
                string ActionCode = Request.QueryString["oobCode"];
                string ContinueUrl = Request.QueryString["continueUrl"];

                RestResponseCookie cookie;

                ReturnResult result =  DAL.VerifyPasswordResetLink(ActionCode, ContinueUrl, out cookie);
                if (result.ErrorCode == 0)
                {
                    Session["Connect.sid"] = cookie;
                    ViewBag.ConnectSID = cookie.Value;
                }
                else
                {
                    return RedirectToAction("ResetError", "ResetPassword", new { ErrorMessage = result.Message != null ? result.Message.ToString() : null });
                }
            }
            else
            {
                return RedirectToAction("ResetError", "ResetPassword", new { ErrorMessage = "Link không hợp lệ" });
            }
            return View();
        }

        public ActionResult ResetError(string ErrorMessage, bool IsInfo = false)
        {
            if (string.IsNullOrEmpty(ErrorMessage))
            {
                ErrorMessage = "Lỗi xảy ra khi xác thực";
            }
            ViewBag.ErrorMessage = ErrorMessage;
            ViewBag.IsInfo = IsInfo;
            return View();
        }

        [HttpPost]
        public JsonResult Update(string password, string connectId)
        {
            ReturnResult result = new ReturnResult();
            if (Session["Connect.sid"] != null)
            {
                RestResponseCookie cookie = Session["Connect.sid"] as RestResponseCookie;
                connectId = cookie.Value;
            }
            if (!string.IsNullOrEmpty(connectId))
            {
                result = DAL.UpdatePassword(connectId, password);
            }
            else
            {
                result.ErrorCode = 101;
                result.Message = "Phiên truy cập đã quá hạn!";
            }
            return Json(result, JsonRequestBehavior.AllowGet);
        }


    }
}