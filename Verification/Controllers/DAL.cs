﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Newtonsoft.Json;
using RestSharp;
namespace Verification.Controllers
{
    public class ReturnResult
    {
        public int ErrorCode { get; set; }
        public object Message { get; set; }
        public object Data { get; set; }
    }
    public class DAL
    {
        public static string apiUrl = System.Configuration.ConfigurationManager.AppSettings.Get("apiUrl");
        public static string verifyEmailLinkResource = "user/verification/verifyEmail";
        public static string verifyResetPasswordCodeResource = "user/verifyPasswordResetCode";
        public static string updatePasswordResource = "user/updatePassword";

        public static ReturnResult VerifyEmailLink(string url)
        {
            ReturnResult result;
            var client = new RestClient(apiUrl);
            var request = new RestRequest(verifyEmailLinkResource, Method.POST);
            var json = new { Url = url };
            request.RequestFormat = DataFormat.Json;
            request.AddBody(json);

            IRestResponse response = client.Execute(request);
            if (response.StatusCode == System.Net.HttpStatusCode.OK || response.StatusCode == System.Net.HttpStatusCode.Accepted)
            {
                string Content = response.Content;
                result = JsonConvert.DeserializeObject<ReturnResult>(response.Content);
            }
            else
            {
                result = new ReturnResult()
                {
                    ErrorCode = 500
                };
            }
            return result;
        }

        public static ReturnResult VerifyPasswordResetLink(string actionCode, string continueUrl, out RestResponseCookie cookie)
        {
            ReturnResult result;
            var client = new RestClient(apiUrl);
            var request = new RestRequest(verifyResetPasswordCodeResource, Method.POST);
            var json = new { ActionCode = actionCode, ContinueUrl = continueUrl };
            request.RequestFormat = DataFormat.Json;
            request.AddBody(json);

            IRestResponse response = client.Execute(request);
            if (response.StatusCode == System.Net.HttpStatusCode.OK || response.StatusCode == System.Net.HttpStatusCode.Accepted)
            {
                string Content = response.Content;
                result = JsonConvert.DeserializeObject<ReturnResult>(response.Content);
                var Cookies = response.Cookies;
                cookie = Cookies.FirstOrDefault(c => c.Name == "connect.sid");
                
            }
            else
            {
                result = new ReturnResult()
                {
                    ErrorCode = 500
                };
                cookie = null;
            }
            return result;
        }

        public static ReturnResult UpdatePassword(string connectId, string password)
        {
            ReturnResult result;
            var client = new RestClient(apiUrl);
            var request = new RestRequest(updatePasswordResource, Method.POST);
            var json = new { Password = password };
            request.RequestFormat = DataFormat.Json;
            request.AddBody(json);
            request.AddCookie("connect.sid", connectId);

            IRestResponse response = client.Execute(request);
            if (response.StatusCode == System.Net.HttpStatusCode.OK || response.StatusCode == System.Net.HttpStatusCode.Accepted)
            {
                string Content = response.Content;
                result = JsonConvert.DeserializeObject<ReturnResult>(response.Content);
            }
            else
            {
                result = new ReturnResult()
                {
                    ErrorCode = 500
                };
            }
            return result;
        }
    }
}
