﻿// Import the required libraries
var express = require('express');
var session = require('express-session');
var redis = require("redis");
var RedisStore = require('connect-redis')(session);

var graphql = require('graphql');
var graphqlHTTP = require('express-graphql');
var passport = require('passport');
var uuid = require('node-uuid');
var bodyParser = require('body-parser');

var blocked = require('blocked');

////authentication
//require('./auth.js');

//GraphQL schema
var schema = require('./schema/schema').schema;

var result = require('./models/result');

var osHelper = require('./helper/osHelper');

var ensureLogin = require('./helper/sessionLoginHelper').ensureLogin;

var logger = require('./helper/logHelper').logger;

var config = require('./myConfig');



//port
var port = process.env.PORT || 3000;

//dùng express framework
var app = express();

var client = redis.createClient(config.redisConfig);

var redisOptions = {
    client: client
};

//const redisSession = session({
//    genid: function (req) {
//        return uuid.v4();
//    },
//    secret: 'instagramvn',
//    store: new RedisStore(redisOptions),
//    resave: false,
//    saveUninitialized: false,

//    //7 ngày * 86400 (số giây / 1 ngày) 
//    expires: new Date(Date.now() + (7 * 86400 * 1000)),
//    //cookie: { secure: false, httpOnly: true, path: '/', maxAge: (2 * 86400 * 1000) }
//});

////passport's session piggy-backs on express-session
//app.use(redisSession);

app.use(bodyParser.text({ type: 'application/graphql' }));
app.use(bodyParser.urlencoded({ extended: true }));
app.use(bodyParser.json());
//app.use(passport.initialize());
//app.use(passport.session());

//trả về file html để biết app đang chạy ko
app.get('/', function (req, res) {
    res.sendFile(__dirname + '/xxx.html');
});

//kết nối socket.io
var server = require('http').createServer(app);
//app.use(function (req, res, next) {
//    //if (!req.session) {
//    //    return next(new Error('oh no')); // handle error
//    //}
//    next(); // otherwise continue
//});
//app.use(function (req, res, next) {
    
//    if (req.session && req.session.passport && req.session.passport.user) {
//        var os = osHelper.getOSFromRequest(req);
     
//        req.session.passport.user.OS = os;
//    }
//    next();
//});

var redisCacheHelper = require('./models/redisCacheHelper');

redisCacheHelper.connect(() => {
        
    //các hàm về user từ API
    require('./api/user')(app, passport, result);

    require('./api/upload')(app, passport, result);

    require('./api/userSave')(app, passport);

    require('./api/insight')(app, passport, result);

    require('./api/sync')(app, passport, result);

    //các hàm về image từ API
    require('./api/imageItem')(app, passport, result);

    //các hàm về search từ API
    require('./api/search')(app, passport, result);

    require('./api/system')(app, passport, result);

    require('./api/media')(app, passport, result);

    require('./api/shop')(app, passport, result);


    //GraphQL có kiểm tra đã đăng nhập
    app.post('/data', [ensureLogin], graphqlHTTP(request => ({ schema: schema, context: request.session, pretty: true })));

    blocked(function (ms) {
        if (ms > 30) {
            logger.debug("PROCESS Blocked at", ms);
        }
    }, { threshold: 1 });
    
    //bắt đầu listen port
    server.listen(port);

    require('./syncMTPPost').startAppSync();

    logger.fatal('GraphQL server running on http://localhost:3000/data');

    //var fb = require('./chat/firebaseHelper');
    //fb.createUser(fb.getSkyUID(1), 'sontungmtp', (result) => {
    //    logger.debug(result);
    //});

    //fb.createCustomToken(fb.getSkyUID(1), (result) => {
    //    logger.debug(result);
    //});

    
});