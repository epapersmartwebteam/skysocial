﻿//import GraphQL
var graphql = require('graphql');

//import lớp đọc db
var db = require('../models/all');

//import hàm GetProjection
var projection = require('./getProjection');

//import các hàm notify
var notify = require('../Notify/caller');

//import các hằng biến toàn cục
var CDNUrl = require('../myConfig').CDNUrl;

var querySchema = require('./querySchema');

var imageMutationResolve = require('./resolve/imageMutation');
var imageCommentMutationResolve = require('./resolve/imageCommentMutation');
var userMutationResolve = require('./resolve/userMutation');

//////////////////
//meida
var mediaCommentMutationResolve = require('./resolve/mediaCommentMutation');

exports.mutationSchema = new graphql.GraphQLObjectType({
    name: 'MyMutations',
    description: 'Các hàm thay đổi data',
    fields: () => ({

        //cập nhật avatar user
        UpdateUserAvatar: {
            type: querySchema.userType,
            args: {
                ItemGUID: { type: new graphql.GraphQLNonNull(graphql.GraphQLString) },
                ImageExtension: { type: new graphql.GraphQLNonNull(graphql.GraphQLString) }
            },
            resolve: (root, args, session, options) => {
                var ast = projection.getProjection(options.fieldASTs[0]);
                return new Promise(function (resolve, reject) {
                    //cho trả về AvatarMedium để update sang ChatCore
                    if (!ast) {
                        ast = [];
                    }
                    ast.push('AvatarMedium');
                    userMutationResolve.updateUserAvatar(session, args, db, ast, function (err, data) {
                        if (!err) {
                            resolve(data);
                        }
                        else {
                            reject();
                        }
                    });
                });
            }
        },

        //cập nhật profile user
        UpdateUserProfile: {
            type: querySchema.userType,
            args: {
                UserName: { type: graphql.GraphQLString },
                Email: { type: graphql.GraphQLString },
                Phone: { type: graphql.GraphQLString },
                FullName: { type: graphql.GraphQLString },
                Gender: { type: graphql.GraphQLInt },
                Birthday: { type: graphql.GraphQLString },
                Favorite: { type: graphql.GraphQLString },
                SocialInfo: { type: graphql.GraphQLString },
                Zodiac: {
                    type: graphql.GraphQLString
                },
                MaritalStatus: {
                    type: graphql.GraphQLString
                },
                Address: {
                    type: graphql.GraphQLString
                }
            },
            resolve: (root, args, session, options) => {
                var ast = projection.getProjection(options.fieldASTs[0]);
                return new Promise(function (resolve, reject) {
                    userMutationResolve.updateUserProfile(session, args, db, ast, function (err, data) {
                        if (!err) {
                            resolve(data);
                        }
                        else {
                            
                            reject(err);
                        }
                    });
                });
            }
        },

        //cập nhật mật khẩu user
        UpdateUserPassword: {
            type: querySchema.userType,
            args: {
                CurrentPassword: { type: new graphql.GraphQLNonNull(graphql.GraphQLString) },
                NewPassword: { type: new graphql.GraphQLNonNull(graphql.GraphQLString) }
            },
            resolve: (root, args, session, options) => {
                var ast = projection.getProjection(options.fieldASTs[0]);
                return new Promise(function (resolve, reject) {
                    userMutationResolve.updateUserPassword(session, args, db, ast, function (err, data) {
                        if (!err) {

                            resolve(data);
                        }
                        else {
                            reject('CurrentPassword');
                        }
                    });
                });
            }
        },

        //Follow user khác
        FollowUser: {
            type: querySchema.userType,
            args: {
                UserId: { type: new graphql.GraphQLNonNull(graphql.GraphQLFloat) }
            },
            resolve: (root, args, session, options) => {
                var ast = projection.getProjection(options.fieldASTs[0]);
                return new Promise(function (resolve, reject) {
                    userMutationResolve.followUser(session, args, db, ast, function (err, data) {
                        if (!err) {

                            //notify tới người đc follow
                            var userId = session.passport.user.UserId;
                            var targetUserid = args.UserId;
                            notify.notifyHasFollowing(userId, targetUserid, function (err, dumb) {
                            });

                            resolve(data);
                        }
                        else {
                            reject();
                        }
                    });
                });
            }
        },

        //Bỏ Follow user khác
        UnfollowerUser: {
            type: querySchema.userType,
            args: {
                UserId: { type: new graphql.GraphQLNonNull(graphql.GraphQLFloat) }
            },
            resolve: (root, args, session, options) => {
                var ast = projection.getProjection(options.fieldASTs[0]);
                return new Promise(function (resolve, reject) {
                    userMutationResolve.unfollowUser(session, args, db, ast, function (err, data) {
                        if (!err) {

                            resolve(data);
                        }
                        else {
                            reject();
                        }
                    });
                });
            }
        },

        //Đồng ý follow
        AcceptUserFollow: {
            type: querySchema.userType,
            args: {
                UserId: { type: new graphql.GraphQLNonNull(graphql.GraphQLFloat) }
            },
            resolve: (root, args, session, options) => {
                var ast = projection.getProjection(options.fieldASTs[0]);
                return new Promise(function (resolve, reject) {
                    userMutationResolve.acceptUserFollow(session, args, db, ast, function (err, data) {
                        if (!err) {

                            //notify tới người đc follow
                            var userId = session.passport.user.UserId;
                            var targetUserid = args.UserId;
                            notify.notifyAcceptedFollowing(userId, targetUserid, function (err, dumb) {
                            });
                            resolve(data);
                        }
                        else {
                            reject();
                        }
                    });
                });
            }
        },


        //follow hashTags
        FollowHashtags: {
            type: querySchema.userType,
            args: {
                Hashtags: { type: new graphql.GraphQLList(querySchema.hashtagType) }
            },
            resolve: (root, args, session, options) => {
                var ast = projection.getProjection(options.fieldASTs[0]);
                return new Promise(function (resolve, reject) {
                    userMutationResolve.followHashtag(session, args, db, ast, function (err, data) {
                        if (!err) {
                            resolve(data);
                        }
                        else {
                            reject();
                        }
                    });
                });
            }
        },

        UnfollowHashtags: {
            type: querySchema.userType,
            args: {
                Hashtags: { type: new graphql.GraphQLList(querySchema.hashtagType) }
            },
            resolve: (root, args, session, options) => {
                var ast = projection.getProjection(options.fieldASTs[0]);
                return new Promise(function (resolve, reject) {
                    userMutationResolve.unfollowHashtag(session, args, db, ast, function (err, data) {
                        if (!err) {
                            resolve(data);
                        }
                        else {
                            reject();
                        }
                    });
                });
            }
        },

        /**
        Liên quan tới Image
        */
        //thêm mới ImageItem
        AddImageItem: {
            type: querySchema.imageItemType,
            args: {
                ItemGUID: { type: new graphql.GraphQLNonNull(graphql.GraphQLString) },
                ImageExtension: { type: new graphql.GraphQLNonNull(graphql.GraphQLString) },
                IsPrivate: { type: new graphql.GraphQLNonNull(graphql.GraphQLBoolean) },
                ImageWidth: { type: new graphql.GraphQLNonNull(graphql.GraphQLInt) },
                ImageHeight: { type: new graphql.GraphQLNonNull(graphql.GraphQLInt) },
                Description: { type: graphql.GraphQLString },
                Location: { type: graphql.GraphQLString },
                Lat: { type: graphql.GraphQLString },
                Long: { type: graphql.GraphQLString },
                Hashtags: { type: new graphql.GraphQLList(graphql.GraphQLString) },
                UserTags: { type: new graphql.GraphQLList(graphql.GraphQLString) }
            },
            resolve: (root, args, session, options) => {

                var ast = projection.getProjection(options.fieldASTs[0]);
                
                return new Promise(function (resolve, reject) {
                    imageMutationResolve.createImageItem(session, args, db, function (err, data) {
                        if (!err) {

                            //trước khi trả về cần notify tới cho những user khác có follow: user đã upload hình mới 
                            //todo: kiểm tra User có nhận notify có hình mới từ User này ko
                            var userId = session.passport.user.UserId;
                            notify.notifyNewImageUpload(userId, data.ImageItemId, function (error, dumb) {

                            });

                            //todo: phần realtime socket làm bên socket.io

                            resolve(data);
                        }
                        else {
                            reject();
                        }
                    });
                });
            }
        },

        //cập nhật thông tin hình
        UpdateImageItem: {
            type: querySchema.imageItemType,
            args: {
                ImageItemId: { type: new graphql.GraphQLNonNull(graphql.GraphQLFloat) },
                IsPrivate: { type: graphql.GraphQLBoolean },
                Description: { type: graphql.GraphQLString },
                ItemData: { type: graphql.GraphQLString },
                Location: { type: graphql.GraphQLString },
                Lat: { type: graphql.GraphQLString },
                Long: { type: graphql.GraphQLString },
                Hashtags: { type: new graphql.GraphQLList(graphql.GraphQLString) },
                UserTags: { type: new graphql.GraphQLList(graphql.GraphQLString) }
            },
            resolve: (root, args, session, options) => {

                var ast = projection.getProjection(options.fieldASTs[0]);

                return new Promise(function (resolve, reject) {
                    imageMutationResolve.updateImageItem(session, args, db, ast, function (err, data) {
                        if (!err) {

                            resolve(data);
                        }
                        else {
                            reject();
                        }
                    });
                });
            }
        },

        //update Imageitem like hoặc ko like
        UpdateImageItemLiked: {
            type: querySchema.imageItemType,
            args: {
                ImageItemId: { type: new graphql.GraphQLNonNull(graphql.GraphQLFloat) },
                IsLiked: { type: new graphql.GraphQLNonNull(graphql.GraphQLBoolean) },
                LikeEmotionId: { type: graphql.GraphQLInt }
            },
            resolve: (root, args, session, options) => {

                var ast = projection.getProjection(options.fieldASTs[0]);
                return new Promise(function (resolve, reject) {
                    imageMutationResolve.updateImageItemLiked(session, args, db, ast, function (err, data) {
                        if (!err) {

                            var userId = session.passport.user.UserId;

                            if (args.IsLiked == true) {

                                //trước khi trả về cần notify tới cho chủ sở hữu hình: có user like hình

                                if (data) {
                                    notify.notifyNewLike(userId, data.ImageItemId, function (err, dumb) {
                                    });

                                    //notify tới những user đang follow người thực hiện like hình
                                    notify.notifyFollowingHasLike(userId, data.ImageItemId, function (err, dumb) {
                                    });
                                }
                            }
                            else {

                                //đánh dấu xóa khi user unlike
                                db.userNotify.updateUserNotifyLikeIsDelete(userId, args.ImageItemId, function (err, dumb) {

                                });
                            }
                            resolve(data);
                        }
                        else {
                            reject();
                        }
                    });
                });
            }
        },

        //share lại hình
        ReshareImageItem: {
            type: querySchema.imageItemType,
            args: {
                ImageItemId: { type: new graphql.GraphQLNonNull(graphql.GraphQLFloat) },
                ShareTo: { type: new graphql.GraphQLNonNull(graphql.GraphQLString) }
            },
            resolve: (root, args, session, options) => {
                var ast = projection.getProjection(options.fieldASTs[0]);
                return new Promise(function (resolve, reject) {
                    imageMutationResolve.reshareImageItem(session, args, db, ast, function (err, data) {
                        if (!err) {
                            //if (data) {
                            //    //trước khi trả về cần notify tới cho những user khác có follow: user đã share hình
                            //    if (args.ShareTo.toLowerCase() == 'wall') {
                            //        var userId = session.passport.user.UserId;
                            //        notify.notifyFollowingHasShare(userId, data.ImageItemId, function (err, dumb) {
                            //        });
                            //    }
                            //}
                            resolve(data);
                        }
                        else {
                            reject();
                        }
                    });
                });
            }
        },

        //thêm comment
        AddImageComment: {
            type: querySchema.imageCommentType,
            args: {
                ImageItemId: { type: new graphql.GraphQLNonNull(graphql.GraphQLFloat) },
                Comment: { type: new graphql.GraphQLNonNull(graphql.GraphQLString) },
                ReplyToId: { type: graphql.GraphQLFloat }
            },
            resolve: (root, args, session, options) => {
                var ast = projection.getProjection(options.fieldASTs[0]);
                return new Promise(function (resolve, reject) {
                    imageCommentMutationResolve.addImageComment(session, args, db, ast, function (err, data) {
                        if (!err) {
                            if (data) {
                                //trước khi trả về cần notify tới cho những user khác có tương tác trên hình: có comment mới
                                var userId = session.passport.user.UserId;
                                notify.notifyNewComment(userId, data.ImageItemId, data.ItemId, function (err, dumb) {
                                });

                                //notify tới những user đang follow người thực hiện like hình
                                notify.notifyFollowingHasComment(userId, data.ImageItemId, data.ItemId, function (err, dumb) {
                                });
                            }
                            resolve(data);
                        }
                        else {
                            reject();
                        }
                    });
                });
            }
        },

        //update comment
        UpdateImageComment: {
            type: querySchema.imageCommentType,
            args: {
                CommentId: { type: new graphql.GraphQLNonNull(graphql.GraphQLFloat) },
                Comment: { type: new graphql.GraphQLNonNull(graphql.GraphQLString) }
            },
            resolve: (root, args, session, options) => {
                var ast = projection.getProjection(options.fieldASTs[0]);
                return new Promise(function (resolve, reject) {
                    imageCommentMutationResolve.updateImageComment(session, args, db, ast, function (err, data) {
                        if (!err) {

                            //todo: trước khi trả về cần notify tới cho những user khác có tương tác trên hình: update comment

                            resolve(data);
                        }
                        else {
                            reject();
                        }
                    });
                });
            }
        },

        //delete comment
        DeleteImageComment: {
            type: querySchema.imageItemType,
            args: {
                CommentId: { type: new graphql.GraphQLNonNull(graphql.GraphQLFloat) }
            },
            resolve: (root, args, session, options) => {
                var ast = projection.getProjection(options.fieldASTs[0]);
                return new Promise(function (resolve, reject) {
                    imageCommentMutationResolve.deleteImageComment(session, args, db, ast, function (err, data) {
                        if (!err) {

                            //xóa tất cả notify liên quan tới comment này
                            db.userNotify.deleteUserNotifyCommentWhenDeleteComment(args.CommentId, function (err1, dumb) {
                            });

                            //todo: trước khi trả về cần realtime tới cho những user khác có tương tác trên hình: đã xóa comment


                            resolve(data);
                        }
                        else {
                            reject();
                        }
                    });
                });
            }
        },

        ////////////////////////////////////
        //Media

        //AddMediaComment: {
        //    type: querySchema.mediaCommentType,
        //    args: {
        //        MediaId: { type: new graphql.GraphQLNonNull(graphql.GraphQLFloat) },
        //        Comment: { type: new graphql.GraphQLNonNull(graphql.GraphQLString) }
        //    },
        //    resolve: (root, args, session, options) => {
        //        var ast = projection.getProjection(options.fieldASTs[0]);
        //        return new Promise(function (resolve, reject) {
        //            mediaCommentMutationResolve.addMediaComment(session, args, db, ast, function (err, data) {
        //                if (!err) {

        //                    //trước khi trả về cần notify tới cho những user khác có tương tác trên hình: có comment mới
        //                    var userId = session.passport.user.UserId;
        //                    //notify.notifyNewComment(userId, data.ImageItemId, data.ItemId, function (err, dumb) {
        //                    //});

        //                    ////notify tới những user đang follow người thực hiện like hình
        //                    //notify.notifyFollowingHasComment(userId, data.ImageItemId, data.ItemId, function (err, dumb) {
        //                    //});
        //                    resolve(data);
        //                }
        //                else {
        //                    reject();
        //                }
        //            });
        //        });
        //    }
        //},

        //UpdateMediaComment: {
        //    type: querySchema.mediaCommentType,
        //    args: {
        //        CommentId: { type: new graphql.GraphQLNonNull(graphql.GraphQLFloat) },
        //        Comment: { type: new graphql.GraphQLNonNull(graphql.GraphQLString) }
        //    },
        //    resolve: (root, args, session, options) => {
        //        var ast = projection.getProjection(options.fieldASTs[0]);
        //        return new Promise(function (resolve, reject) {
        //            mediaCommentMutationResolve.updateMediaComment(session, args, db, ast, function (err, data) {
        //                if (!err) {

        //                    //todo: trước khi trả về cần notify tới cho những user khác có tương tác trên hình: update comment

        //                    resolve(data);
        //                }
        //                else {
        //                    reject();
        //                }
        //            });
        //        });
        //    }
        //},
        ////delete comment
        //DeleteMediaComment: {
        //    type: querySchema.mediaType,
        //    args: {
        //        CommentId: { type: new graphql.GraphQLNonNull(graphql.GraphQLFloat) }
        //    },
        //    resolve: (root, args, session, options) => {
        //        var ast = projection.getProjection(options.fieldASTs[0]);
        //        return new Promise(function (resolve, reject) {
        //            mediaCommentMutationResolve.deleteMediaComment(session, args, db, ast, function (err, data) {
        //                if (!err) {

        //                    //xóa tất cả notify liên quan tới comment này
        //                    //db.userNotify.deleteUserNotifyCommentWhenDeleteComment(args.CommentId, function (err1, dumb) {
        //                    //});

        //                    //todo: trước khi trả về cần realtime tới cho những user khác có tương tác trên hình: đã xóa comment


        //                    resolve(data);
        //                }
        //                else {
        //                    reject();
        //                }
        //            });
        //        });
        //    }
        //},

    })
});