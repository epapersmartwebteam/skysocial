﻿exports.getProjection = function (fieldASTs) {
    var obj = fieldASTs.selectionSet.selections.reduce(function (projections, selection) {
        projections[selection.name.value] = 1;
        return projections;
    }, {});

    //chuyển đổi JSON object sang kiểu Array lấy Key
    return Object.keys(obj).map(function (k) { return k; });
};