﻿var async = require('async');
var _ = require('lodash');
//import GraphQL
var graphql = require('graphql');

//import lớp đọc db
var db = require('../models/all');

var redisCacheHelper = require('../models/redisCacheHelper');

//import hàm GetProjection
var projection = require('./getProjection');

//import các hằng biến toàn cục
var CDNUrl = require('../myConfig').CDNUrl;
var myConfig = require('../myConfig');
var errorCode = require('../errorCode');
//import lớp utility
var utilityHelper = require('../helper/utility');

var cryptoEngine = require('../helper/cryptoEngine');

var storageHelper = require('../helper/storageHelper');

var dateHelper = require('../helper/dateHelper');
var logger = require('../helper/logHelper').logger;
//Khai báo các type riêng

var socialInfoType = new graphql.GraphQLObjectType({
    name: 'SocialInfo',
    description: 'SocialInfo Model',
    fields: () => ({
        NameCode: {
            type: graphql.GraphQLString,
            description: 'Mã Social: Web, Facebook, Twitter, Instagram, Youtube'
        },
        Link: {
            type: graphql.GraphQLString,
            description: 'Link của user'
        }
    })
});
exports.socialInfoType = socialInfoType;

var hashtagType = new graphql.GraphQLInputObjectType({
    name: 'HashtagType',
    description: 'Hashtag Model',
    fields: () => ({
        HashtagId: {
            type: graphql.GraphQLInt
        },
        Hashtag: {
            type: graphql.GraphQLString
        }
    })
});
exports.hashtagType = hashtagType;

//User
var userType = new graphql.GraphQLObjectType({
    name: 'User',
    description: 'User Model',
    fields: () => ({
        UserId: {
            type: graphql.GraphQLFloat,
            description: 'ID tăng dần trong DB'
        },
        UserGUID: {
            type: graphql.GraphQLString,
            description: 'Map UserID từ db cũ'
        },
        UserName: {
            type: graphql.GraphQLString,
            description: 'Dùng để đăng kí, đăng nhập thay cho Email & Phone'
        },
        Email: {
            type: graphql.GraphQLString,
            description: 'Dùng cho đăng nhập'
        },
        EmailVerified: {
            type: graphql.GraphQLBoolean,
            description: "Dùng để xác định Email đã xác thực hay chưa"
        },
        Phone: {
            type: graphql.GraphQLString,
            description: 'Dùng cho đăng nhập'
        },
        PhoneVerified: {
            type: graphql.GraphQLBoolean,
            description: "Dùng để xác định Phone đã xác thực hay chưa"
        },
        FacebookId: {
            type: graphql.GraphQLString,
            description: 'Dùng cho đăng nhập'
        },
        FullName: {
            type: graphql.GraphQLString,
            description: ''
        },
        Gender: {
            type: graphql.GraphQLInt,
            description: '0: nữ, 1: nam, -1: không xác định'
        },
        Birthday: {
            type: graphql.GraphQLString,
            description: ''
        },
        Zodiac: {
            type: graphql.GraphQLString,
            description: 'Aquarius, Pisces, Aries, Taurus, Gemini, Cancer, Leo, Virgo, Libra, Scorpio, Sagittarius, Capricorn'
        },
        MaritalStatus: {
            type: graphql.GraphQLString,
            description: 'Single, Marriage, Widow, Relationship, Complicated'
        },
        AvatarOriginal: {
            type: graphql.GraphQLString,
            description: 'Kích thước gốc: Tối đa 1080x1080',
            //resolve: function (parent) {
            //    return CDNUrl + parent.AvatarOriginal;
            //}
        },
        AvatarSmall: {
            type: graphql.GraphQLString,
            description: '96x96',
            //resolve: function (parent) {
            //    return CDNUrl + parent.AvatarSmall;
            //}
        },
        AvatarMedium: {
            type: graphql.GraphQLString,
            description: '512x500',
            //resolve: function (parent) {
            //    return CDNUrl + parent.AvatarMedium;
            //}
        },
        Job: {
            type: graphql.GraphQLString,
            description: ''
        },
        Address: {
            type: graphql.GraphQLString,
            description: ''
        },
        Favorite: {
            type: graphql.GraphQLString,
            description: ''
        },
        Biography: {
            type: graphql.GraphQLString,
            description: ''
        },
        CountryId: {
            type: graphql.GraphQLString
        },
        CountryName: {
            type: graphql.GraphQLString
        },
        CreateDate: {
            type: graphql.GraphQLFloat,
            description: ''
        },
        CreateOS: {
            type: graphql.GraphQLString,
            description: ''
        },
        LastLoginDate: {
            type: graphql.GraphQLFloat,
            description: ''
        },
        LastLoginOS: {
            type: graphql.GraphQLString,
            description: ''
        },
        HashtagSuggested: {
            type: graphql.GraphQLBoolean,
            description: ''
        },
        IsVip: {
            type: graphql.GraphQLBoolean,
            description: ''
        },
        VipLevelId: {
            type: graphql.GraphQLInt,
            resolve: function (parent, args, session, options) {
                return parent.VipLevelId == null ? -1 : parent.VipLevelId;
            }
        },
        VipLevel: {
            type: graphql.GraphQLString,
            resolve: function (parent, args, session, options) {
                var vipLevel = 'SKY';
                switch (parent.VipLevelId) {
                    case 0: {
                        vipLevel = 'VIP';
                        break;
                    }
                    case 1: {
                        vipLevel = 'VVIP';
                    }
                }
                return vipLevel;
            }
        },
        IsVipTrial: {
            type: graphql.GraphQLBoolean
        },
        VipStartDate: {
            type: graphql.GraphQLFloat,
            description: ''
        },
        VipEndDate: {
            type: graphql.GraphQLFloat,
            description: ''
        },
        Beta: {
            type: graphql.GraphQLBoolean
        },
        SocialInfo: {
            type: new graphql.GraphQLList(socialInfoType),
            resolve: function (parent, args, session, options) {
                return parent.SocialInfo;
            }
        },
        ImageCount: {
            type: graphql.GraphQLInt,
            description: ''
        },
        FollowingCount: {
            type: graphql.GraphQLInt,
            description: ''
        },
        FollowerCount: {
            type: graphql.GraphQLInt,
            description: ''
        },
        GroupCount: {
            type: graphql.GraphQLInt,
        },
        ImageItems: {
            type: new graphql.GraphQLList(imageItemType),
            args: {
                LastItemId: {
                    type: graphql.GraphQLString
                },
                LastItemDate: {
                    type: graphql.GraphQLFloat
                },
                Count: {
                    type: graphql.GraphQLInt
                }
            },
            resolve: function (parent, args, session, options) {

                var ast = projection.getProjection(options.fieldASTs[0]);

                return new Promise(function (resolve, reject) {
                    db.imageItem.getImageListByUserId(session.passport.user.UserId, parent.UserId, args.LastItemId, args.LastItemDate, args.Count, ast, function (err, data) {

                        if (!err) {
                            resolve(data);
                        }
                        else {
                            reject();
                        }
                    });
                });
            }
        },
        Followings: {
            type: new graphql.GraphQLList(userType),
            args: {
                LastItemId: {
                    type: graphql.GraphQLFloat
                },
                LastItemDate: {
                    type: graphql.GraphQLFloat
                },
                Count: {
                    type: graphql.GraphQLInt
                },
            },
            resolve: function (parent, args, session, options) {
                var ast = projection.getProjection(options.fieldASTs[0]);

                return new Promise(function (resolve, reject) {
                    db.user.getUserFollowingList(parent.UserId, args.LastItemId, args.LastItemDate, args.Count, ast, function (err, data) {
                        if (!err) {
                            resolve(data);
                        }
                        else {
                            reject();
                        }
                    });
                });
            }
        },
        Followers: {
            type: new graphql.GraphQLList(userType),
            args: {
                LastItemId: {
                    type: graphql.GraphQLFloat
                },
                LastItemDate: {
                    type: graphql.GraphQLFloat
                },
                Count: {
                    type: graphql.GraphQLInt
                },
            },
            resolve: function (parent, args, session, options) {
                var ast = projection.getProjection(options.fieldASTs[0]);

                return new Promise(function (resolve, reject) {
                    db.user.getUserFollowerList(parent.UserId, args.LastItemId, args.LastItemDate, args.Count, ast, function (err, data) {
                        if (!err) {
                            resolve(data);
                        }
                        else {
                            reject();
                        }
                    });
                });
            }
        },
        ChatUserId: {
            type: graphql.GraphQLString
        },
        ChatActive: {
            type: graphql.GraphQLBoolean
        }
    })
});
exports.userType = userType;

var mediaObjectType = new graphql.GraphQLObjectType({
    name: 'MediaObject',
    description: 'Media Object Model',
    fields: () => ({
        Link: {
            type: graphql.GraphQLString
        },
        Width: {
            type: graphql.GraphQLInt
        },
        Height: {
            type: graphql.GraphQLInt
        },
        SizeInKb: {
            type: graphql.GraphQLInt
        },
        LengthInSecond: {
            type: graphql.GraphQLInt
        },
        //LinkSAS: {
        //    type: graphql.GraphQLString,
        //    resolve: function (parent, args, session) {
        //        azureHelper.generateUploadAvatarSAS
        //        return parent.Link;
        //    }
        //}
    })
});
exports.mediaObjectType = mediaObjectType;

var mediaAdditionInfoType = new graphql.GraphQLObjectType({
    name: 'MediaAdditionInfo',
    description: 'Media Addition',
    fields: () => ({
        Icon: {
            type: graphql.GraphQLString
        },
        Text: {
            type: graphql.GraphQLString
        },
        Link: {
            type: graphql.GraphQLString
        },
        Target: {
            type: graphql.GraphQLString
        }
    })
});
exports.mediaAdditionInfoType = mediaAdditionInfoType;

var itemType = new graphql.GraphQLObjectType({
    name: 'Item',
    description: 'Item Model',
    fields: () => ({
        ItemType: {
            type: graphql.GraphQLString
        },
        Large: {
            type: mediaObjectType
        },
        Medium: {
            type: mediaObjectType
        },
        Small: {
            type: mediaObjectType
        },
        Info: {
            type: mediaAdditionInfoType
        }
    })
});
exports.itemType = itemType;

var linkPreviewType = new graphql.GraphQLObjectType({
    name: 'LinkPreview',
    description: 'LinkPreview Model',
    fields: () => ({
        Link: {
            type: graphql.GraphQLString
        },
        Title: {
            type: graphql.GraphQLString
        },
        Caption: {
            type: graphql.GraphQLString
        },
        Image: {
            type: graphql.GraphQLString
        },
        SiteName: {
            type: graphql.GraphQLString
        }
    })
});
exports.linkPreviewType = linkPreviewType;

var itemDataType = new graphql.GraphQLObjectType({
    name: 'ItemData',
    description: 'ItemData Model',
    fields: () => ({
        LinkPreview: {
            type: linkPreviewType
        }
    })
});
exports.itemDataType = itemDataType;

//ImageItem
var imageItemType = new graphql.GraphQLObjectType({
    name: 'ImageItem',
    description: 'ImageItem Model',
    fields: () => ({

        ImageItemId: {
            type: graphql.GraphQLFloat,
            description: ''
        },
        ItemGUID: {
            type: graphql.GraphQLString,
            description: ''
        },
        ItemContent: {
            type: new graphql.GraphQLList(itemType),
            description: 'JSON mảng Item.',
            resolve: function (parent, args, session) {
                return parent.ItemContent;
            }
        },
        ItemType: {
            type: graphql.GraphQLString,
            resolve: function (parent) {
                return parent.ItemType ? parent.ItemType : db.imageItem.imageItemTypes.INSTA;
            }
        },
        ItemData: {
            type: itemDataType,
            description: 'Post Aditional Data',
            resolve: function (parent, args, session) {
                return parent.ItemData;
            }
        },
        CreateDate: {
            type: graphql.GraphQLFloat,
            description: ''
        },
        CreateOS: {
            type: graphql.GraphQLString,
            description: ''
        },
        LastModifyDate: {
            type: graphql.GraphQLFloat,
            description: ''
        },
        LastModifyOS: {
            type: graphql.GraphQLString,
            description: ''
        },
        Description: {
            type: graphql.GraphQLString,
            description: ''
        },
        Location: {
            type: graphql.GraphQLString,
            description: ''
        },
        Lat: {
            type: graphql.GraphQLFloat,
            description: ''
        },
        Long: {
            type: graphql.GraphQLFloat,
            description: ''
        },
        IsPrivate: {
            type: graphql.GraphQLBoolean,
            description: ''
        },
        LikeCount: {
            type: graphql.GraphQLFloat,
            description: ''
        },
        CommentCount: {
            type: graphql.GraphQLFloat,
            description: ''
        },
        ShareCount: {
            type: graphql.GraphQLFloat,
            description: ''
        },
        IsLiked: {
            type: graphql.GraphQLBoolean,
            resolve: function (parent, args, session) {
                return new Promise(function (resolve, reject) {
                    db.imageLike.checkUserLiked(session.passport.user.UserId, parent.ImageItemId, function (err, isLiked) {
                        if (!err) {
                            resolve(isLiked);
                        }
                        else {
                            reject();
                        }
                    });
                });
            }
        },
        IsShared: {
            type: graphql.GraphQLBoolean,
            resolve: function (parent, args, session) {
                return new Promise(function (resolve, reject) {
                    db.imageShare.checkUserShared(session.passport.user.UserId, parent.ImageItemId, function (err, isShared) {
                        if (!err) {
                            resolve(isShared);
                        }
                        else {
                            reject();
                        }
                    });
                });
            }
        },
        IsOwner: {
            type: graphql.GraphQLBoolean,
            resolve: function (parent, args, session) {
                return ((session.passport.user.UserId === parent.UserId) ? 1 : 0);
            }
        },
        Owner: {
            type: userType,
            resolve: function (parent, args, session, options) {

                var ast = projection.getProjection(options.fieldASTs[0]);

                return new Promise(function (resolve, reject) {
                    db.user.getUserById(parent.UserId, ast, function (err, data) {

                        if (!err) {
                            resolve(data);
                        }
                        else
                            reject();
                    });
                });
            }
        },
        WebLink: {
            type: graphql.GraphQLString,
            resolve: function (parent) {
                return myConfig.WebLink + cryptoEngine.encryptText(parent.ImageItemId);
            }
        },
        Comments: {
            type: new graphql.GraphQLList(imageCommentType),
            args: {
                LastItemId: {
                    type: graphql.GraphQLFloat
                },
                LastItemDate: {
                    type: graphql.GraphQLFloat
                },
                Count: {
                    type: graphql.GraphQLInt
                },
                ReplyToId: {
                    type: graphql.GraphQLFloat
                }
            },
            resolve(parent, args, session, options) {
                var ast = projection.getProjection(options.fieldASTs[0]);

                if (myConfig.cacheConfig.enableCache && parent.SourceCall == 'FeatureFeeds') {
                    var itemCount = args.Count;
                    if (!itemCount) {
                        itemCount = 2;
                    }
                    return new Promise(function (resolve, reject) {
                        redisCacheHelper.getFeaturePostComment(parent.ImageItemId, itemCount, (result) => {

                            if (result.ErrorCode == 0) {
                                var comments = result.Data;
                                _.each(comments, (c, idx) => {
                                    comments[idx].SourceCall = parent.SourceCall;
                                });
                                resolve(comments);
                            }
                            else {
                                reject();
                            }
                        });
                    });
                }
                else {
                    return new Promise(function (resolve, reject) {
                        db.imageComment.getImageCommentByImageId(parent.ImageItemId, args.ReplyToId, args.LastItemId, args.LastItemDate, args.Count, ast,
                            function (err, data) {

                                if (!err) {
                                    resolve(data);
                                }
                                else
                                    reject();
                            });
                    });
                }
            }
        },
        Likes: {
            type: new graphql.GraphQLList(imageLikeType),
            args: {
                LastItemId: {
                    type: graphql.GraphQLFloat
                },
                LastItemDate: {
                    type: graphql.GraphQLFloat
                },
                Count: {
                    type: graphql.GraphQLInt
                }
            },
            resolve(parent, args, session, options) {
                var ast = projection.getProjection(options.fieldASTs[0]);

                return new Promise(function (resolve, reject) {
                    db.imageLike.getImageLikeByImageId(parent.ImageItemId, args.LastItemId, args.LastItemDate, args.Count, ast,
                        function (err, data) {
                            if (!err) {
                                resolve(data);
                            }
                            else
                                reject();
                        });
                });
            }
        },
        Shares: {
            type: new graphql.GraphQLList(imageShareType),
            args: {
                LastItemId: {
                    type: graphql.GraphQLFloat
                },
                LastItemDate: {
                    type: graphql.GraphQLFloat
                },
                Count: {
                    type: graphql.GraphQLInt
                }
            },
            resolve(parent, args, session, options) {
                var ast = projection.getProjection(options.fieldASTs[0]);

                return new Promise(function (resolve, reject) {
                    db.imageShare.getImageShareByImageId(parent.ImageItemId, args.LastItemId, args.LastItemDate, args.Count, ast,
                        function (err, data) {
                            if (!err) {
                                resolve(data);
                            }
                            else
                                reject();
                        });
                });
            }
        },
        IsSaved: {
            type: graphql.GraphQLBoolean,
            resolve: function (parent, args, session) {
                return new Promise(function (resolve, reject) {
                    db.userSave.checkUserSaveImage(session.passport.user.UserId, parent.ImageItemId, function (err, isSaved) {
                        if (!err) {
                            resolve(isSaved);
                        }
                        else {
                            reject();
                        }
                    });
                });
            }
        },
        SavedDate: {
            type: graphql.GraphQLFloat,
            resolve: function (parent, args, session) {
                return new Promise(function (resolve, reject) {

                    if (parent.SavedDate) {
                        resolve(parent.SavedDate);
                    }
                    else {
                        db.userSave.getUserImageSavedDate(session.passport.user.UserId, parent.ImageItemId, function (err, savedDate) {

                            if (!err) {
                                resolve(savedDate);
                            }
                            else {
                                reject();
                            }
                        });
                    }


                });
            }
        },
        SourceCall: {
            type: graphql.GraphQLString
        }
    })
});
exports.imageItemType = imageItemType;

//ImageComment
var imageCommentType = new graphql.GraphQLObjectType({
    name: 'ImageComment',
    description: 'ImageComment Model',
    fields: () => ({
        ItemId: {
            type: graphql.GraphQLFloat,
            description: ''
        },
        ImageItemId: {
            type: graphql.GraphQLFloat
        },
        ImageItem: {
            type: imageItemType,
            resolve: function (parent, args, session, options) {
                var ast = projection.getProjection(options.fieldASTs[0]);
                var userId = session.passport.user.UserId;
                return new Promise(function (resolve, reject) {
                    db.imageItem.getImageItemById(userId, parent.ImageItemId, ast, function (err, data) {
                        if (!err) {
                            resolve(data);
                        }
                        else
                            reject();
                    });
                });
            }
        },
        User: {
            type: userType,
            resolve: function (parent, args, session, options) {
                var ast = projection.getProjection(options.fieldASTs[0]);

                if (parent.SourceCall == 'FeatureFeeds') {
                    return new Promise(function (resolve, reject) {
                        if (parent.User) {
                            resolve(parent.User);
                        }
                        else {
                            reject();
                        }
                    });
                }
                else {
                    return new Promise(function (resolve, reject) {
                        db.user.getUserById(parent.UserId, ast, function (err, data) {
                            if (!err) {
                                resolve(data);
                            }
                            else
                                reject();
                        });
                    });
                }
            }
        },
        Comment: {
            type: graphql.GraphQLString,
            description: ''
        },
        //IsDelete: {
        //    type: graphql.GraphQLBoolean,
        //    description: ''
        //},
        ReplyToId: {
            type: graphql.GraphQLInt,
            description: ''
        },
        ReplyTo: {
            type: imageCommentType,
            resolve: function (parent, args, session, options) {
                var ast = projection.getProjection(options.fieldASTs[0]);

                return new Promise(function (resolve, reject) {
                    db.imageComment.getImageCommentById(parent.ReplyToId, ast, function (err, data) {
                        if (!err) {
                            resolve(data);
                        }
                        else
                            reject();
                    });
                });
            }
        },
        Edited: {
            type: graphql.GraphQLBoolean,
            description: ''
        },
        LikeCount: {
            type: graphql.GraphQLInt,
            description: '',
        },
        IsLiked: {
            type: graphql.GraphQLBoolean,
            resolve: function (parent, args, session) {
                return new Promise(function (resolve, reject) {
                    db.imageComment.checkImageCommentIsLiked(session.passport.user.UserId, parent.ItemId, function (err, isLiked) {
                        if (!err) {
                            resolve(isLiked);
                        }
                        else {
                            reject();
                        }
                    });
                });
            }
        },
        ReplyCount: {
            type: graphql.GraphQLInt,
            description: ''
        },
        CreateDate: {
            type: graphql.GraphQLFloat,
            description: ''
        },
        CreateOS: {
            type: graphql.GraphQLString,
            description: ''
        },
        LastModifyDate: {
            type: graphql.GraphQLFloat,
            description: ''
        },
        LastModifyOS: {
            type: graphql.GraphQLString,
            description: ''
        }
    })
});
exports.imageCommentType = imageCommentType;

//ImageLike
var imageLikeType = new graphql.GraphQLObjectType({
    name: 'ImageLike',
    description: 'ImageLike Model',
    fields: () => ({
        ItemId: {
            type: graphql.GraphQLFloat,
            description: ''
        },
        ImageItemId: {
            type: graphql.GraphQLFloat
        },
        User: {
            type: userType,
            resolve: function (parent, args, session, options) {
                var ast = projection.getProjection(options.fieldASTs[0]);

                return new Promise(function (resolve, reject) {
                    db.user.getUserById(parent.UserId, ast, function (err, data) {
                        if (!err) {
                            resolve(data);
                        }
                        else
                            reject();
                    });
                });
            }
        },
        LikeEmotionId: {
            type: graphql.GraphQLInt,
            description: ''
        },
        CreateDate: {
            type: graphql.GraphQLFloat,
            description: ''
        }
    })
});
exports.imageLikeType = imageLikeType;

//ImageShare
var imageShareType = new graphql.GraphQLObjectType({
    name: 'ImageShare',
    description: 'ImageShare Model',
    fields: () => ({
        ItemId: {
            type: graphql.GraphQLFloat,
            description: ''
        },
        ImageItemId: {
            type: graphql.GraphQLFloat
        },
        User: {
            type: userType,
            resolve: function (parent, args, session, options) {
                var ast = projection.getProjection(options.fieldASTs[0]);

                return new Promise(function (resolve, reject) {
                    db.user.getUserById(parent.UserId, ast, function (err, data) {
                        if (!err) {
                            resolve(data);
                        }
                        else
                            reject();
                    });
                });
            }
        },
        ShareTo: {
            type: graphql.GraphQLString,
            description: ''
        },
        CreateDate: {
            type: graphql.GraphQLFloat,
            description: ''
        }
    })
});
exports.imageShareType = imageShareType;

/////////////////////////////////////////////////////////////////////////////////////////
//Media
var mediaType = new graphql.GraphQLObjectType({
    name: 'Media',
    description: 'Media Model',
    fields: () => ({
        MediaId: {
            type: graphql.GraphQLInt,
            description: ''
        },
        MediaGUID: {
            type: graphql.GraphQLString
        },
        MediaType: {
            type: graphql.GraphQLString
        },
        Title: {
            type: graphql.GraphQLString
        },
        Description: {
            type: graphql.GraphQLString
        },
        Link: {
            type: graphql.GraphQLString
        },
        Cover: {
            type: graphql.GraphQLString
        },
        Thumbnail: {
            type: graphql.GraphQLString
        },
        SortId: {
            type: graphql.GraphQLInt
        },
        IsPublish: {
            type: graphql.GraphQLBoolean
        },
        PublishAfter: {
            type: graphql.GraphQLFloat
        },
        IsFree: {
            type: graphql.GraphQLBoolean
        },
        Price: {
            type: graphql.GraphQLInt
        },
        VipPrice: {
            type: graphql.GraphQLInt
        },
        EnableMobileCardPaid: {
            type: graphql.GraphQLBoolean
        },
        MobileCardPrice: {
            type: graphql.GraphQLInt
        },
        EnableIOSInAppPaid: {
            type: graphql.GraphQLBoolean
        },
        IOSInAppTier: {
            type: graphql.GraphQLString
        },
        IOSInAppPrice: {
            type: graphql.GraphQLInt
        },
        IOSInAppProceeds: {
            type: graphql.GraphQLInt
        },
        VipNeedBuy: {
            type: graphql.GraphQLBoolean
        },
        Version: {
            type: graphql.GraphQLInt
        },
        Author: {
            type: graphql.GraphQLString
        },
        ReleaseYear: {
            type: graphql.GraphQLString
        },
        CreateDate: {
            type: graphql.GraphQLFloat
        },
        LastModifyDate: {
            type: graphql.GraphQLFloat
        },
        LikeCount: {
            type: graphql.GraphQLInt
        },
        CommentCount: {
            type: graphql.GraphQLInt
        }
    })
});

//MediaComment
var mediaCommentType = new graphql.GraphQLObjectType({
    name: 'MediaComment',
    description: 'MediaComment Model',
    fields: () => ({
        ItemId: {
            type: graphql.GraphQLFloat,
            description: ''
        },
        MediaId: {
            type: graphql.GraphQLInt
        },
        Media: {
            type: mediaType,
            resolve: function (parent, args, session, options) {
                var ast = projection.getProjection(options.fieldASTs[0]);
                var userId = session.passport.user.UserId;
                return new Promise(function (resolve, reject) {
                    db.imageItem.getImageItemById(userId, parent.ImageItemId, ast, function (err, data) {
                        if (!err) {
                            resolve(data);
                        }
                        else
                            reject();
                    });
                });
            }
        },
        User: {
            type: userType,
            resolve: function (parent, args, session, options) {
                var ast = projection.getProjection(options.fieldASTs[0]);

                return new Promise(function (resolve, reject) {
                    db.user.getUserById(parent.UserId, ast, function (err, data) {
                        if (!err) {
                            resolve(data);
                        }
                        else
                            reject();
                    });
                });
            }
        },
        Comment: {
            type: graphql.GraphQLString,
            description: ''
        },
        //IsDelete: {
        //    type: graphql.GraphQLBoolean,
        //    description: ''
        //},
        ReplyToId: {
            type: graphql.GraphQLInt,
            description: ''
        },
        ReplyTo: {
            type: mediaCommentType,
            resolve: function (parent, args, session, options) {
                var ast = projection.getProjection(options.fieldASTs[0]);

                return new Promise(function (resolve, reject) {
                    db.mediaComment.getMediaCommentById(parent.ReplyToId, ast, function (err, data) {
                        if (!err) {
                            resolve(data);
                        }
                        else
                            reject();
                    });
                });
            }
        },
        Edited: {
            type: graphql.GraphQLBoolean,
            description: ''
        },
        LikeCount: {
            type: graphql.GraphQLInt,
            description: '',
        },
        ReplyCount: {
            type: graphql.GraphQLInt,
            description: ''
        },
        CreateDate: {
            type: graphql.GraphQLFloat,
            description: ''
        },
        CreateOS: {
            type: graphql.GraphQLString,
            description: ''
        },
        LastModifyDate: {
            type: graphql.GraphQLFloat,
            description: ''
        },
        LastModifyOS: {
            type: graphql.GraphQLString,
            description: ''
        },
    })
});
exports.mediaCommentType = mediaCommentType;

////////////////////////////////////////////////////////////////////////////////////////
//query schema
exports.querySchema = new graphql.GraphQLObjectType({
    name: 'MyQuery',
    fields: () => ({

        //truy vấn user
        User: {
            type: userType,
            args: {
                Id: { type: graphql.GraphQLFloat },
                UserName: { type: graphql.GraphQLString }
            },
            resolve: function (parent, args, session, options) {
                //lấy danh sách các trường cần select trong User
                var ast = projection.getProjection(options.fieldASTs[0]);
                return new Promise(function (resolve, reject) {
                    if (args.Id != undefined && args.Id != null) {
                        db.user.getUserById(args.Id, ast, function (err, data) {

                            if (!err) {
                                resolve(data);
                            }
                            else {
                                reject();
                            }
                        });
                    }
                    else if (args.UserName != undefined && args.UserName != null) {
                        db.user.getUserByUserName(args.UserName, ast, function (err, data) {

                            if (!err) {
                                resolve(data);
                            }
                            else {
                                reject();
                            }
                        });
                    }
                    else {
                        reject();
                    }
                });
            }
        },

        //Suggest User
        SuggestFollow: {
            type: new graphql.GraphQLList(userType),
            args: {
                Count: {
                    type: graphql.GraphQLInt
                },
                IsFacebook: {
                    type: graphql.GraphQLBoolean
                },
                IsRelative: {
                    type: graphql.GraphQLBoolean
                }
            },
            resolve: function (parent, args, session, options) {
                var ast = projection.getProjection(options.fieldASTs[0]);
                var userId = session.passport.user.UserId;

                return new Promise(function (resolve, reject) {
                    var facebookId;
                    var IsFacebook = args.IsFacebook;
                    var IsRelative = args.IsRelative;
                    if (IsRelative) {
                        db.user.suggestUserByUserInfo(userId, args.Count, ast, function (err, data) {
                            if (!err) {
                                resolve(data);
                            }
                            else {
                                reject();
                            }
                        });
                    }
                    else if (IsFacebook == undefined || IsFacebook == null || IsFacebook == false) {
                        var maxCase = 1;

                        //nếu số lượng đã follow > MIN thì bắt đầu suggest những User do User đang Follow Follow 
                        if (session.passport.user.FollowingCount >= myConfig.MinFollowingStartSuggest) {
                            maxCase = 2;
                        }

                        var caseSuggest = utilityHelper.getRandomInt(0, maxCase);

                        switch (caseSuggest) {
                            case 0:
                                {
                                    //theo Post
                                    db.user.suggestUserHighPost(userId, args.Count, ast, function (err, data) {
                                        if (!err) {
                                            resolve(data);
                                        }
                                        else {
                                            reject();
                                        }
                                    });
                                    break;
                                }
                            case 1:
                                {
                                    //theo Follower
                                    db.user.suggestUserHighFollower(userId, args.Count, ast, function (err, data) {
                                        if (!err) {
                                            resolve(data);
                                        }
                                        else {
                                            reject();
                                        }
                                    });
                                    break;
                                }
                            case 2:
                                {
                                    var hasFBId = false;
                                    if (session.passport.user.FacebookId != undefined && session.passport.user.FacebookId != null && session.passport.user.FacebookId != '') {
                                        hasFBId = true;
                                    }

                                    var caseRelative = 0;

                                    if (hasFBId) {
                                        caseRelative = utilityHelper.getRandomInt(0, 1);
                                    }

                                    switch (caseRelative) {
                                        case 0: {
                                            //follow của follow mà user chưa follow (^.^)
                                            db.user.suggestUserRelative(userId, args.Count, ast, function (err, data) {
                                                if (!err) {
                                                    resolve(data);
                                                }
                                                else {
                                                    reject();
                                                }
                                            });
                                            break;
                                        }
                                        case 1: {
                                            facebookId = session.passport.user.FacebookId;

                                            db.user.suggestFacebookFriends(userId, facebookId, args.Count, ast, function (err, data) {
                                                if (!err) {
                                                    resolve(data);
                                                }
                                                else {
                                                    reject();
                                                }
                                            });
                                            break;
                                        }
                                    }
                                    break;
                                }
                        }
                    }
                    else {
                        facebookId = session.passport.user.FacebookId;

                        db.user.suggestFacebookFriends(userId, facebookId, args.Count, ast, function (err, data) {
                            if (!err) {
                                resolve(data);
                            }
                            else {
                                reject();
                            }
                        });
                    }
                });
            }
        },

        //Image
        ImageItem: {
            type: imageItemType,
            args: {
                Id: { type: graphql.GraphQLFloat }
            },
            resolve: function (parent, args, session, options) {
                var userId = session.passport.user.UserId;
                //lấy danh sách các trường cần select trong ImageItem
                var ast = projection.getProjection(options.fieldASTs[0]);
                return new Promise(function (resolve, reject) {
                    db.imageItem.getImageItemById(userId, args.Id, ast, function (err, data) {
                        if (!err) {
                            resolve(data);
                        }
                        else {
                            reject();
                        }
                    });
                });
            }
        },

        //danh sách comment của 1 hình
        Comments: {
            type: new graphql.GraphQLList(imageCommentType),
            args: {
                ImageItemId: {
                    type: new graphql.GraphQLNonNull(graphql.GraphQLFloat)
                },
                LastItemId: {
                    type: graphql.GraphQLFloat
                },
                LastItemDate: {
                    type: graphql.GraphQLFloat
                },
                Count: {
                    type: graphql.GraphQLInt
                },
                ReplyToId: {
                    type: graphql.GraphQLFloat
                }
            },
            resolve: function (parent, args, session, options) {

                var ast = projection.getProjection(options.fieldASTs[0]);

                return new Promise(function (resolve, reject) {
                    db.imageComment.getImageCommentByImageId(args.ImageItemId, args.ReplyToId, args.LastItemId, args.LastItemDate, args.Count, ast,
                        function (err, data) {

                            if (!err) {
                                resolve(data);
                            }
                            else
                                reject();
                        });
                });
            }
        },

        //lấy danh Image của 1 User, nếu ko có UserId thì lấy từ Session
        ImageItemList: {
            type: new graphql.GraphQLList(imageItemType),
            args: {
                UserId: {
                    type: graphql.GraphQLFloat
                },
                LastItemId: {
                    type: graphql.GraphQLFloat
                },
                LastItemDate: {
                    type: graphql.GraphQLFloat
                },
                Count: {
                    type: graphql.GraphQLInt
                }
            },
            resolve: function (parent, args, session, options) {

                var ast = projection.getProjection(options.fieldASTs[0]);

                var userId = args.UserId;
                if (userId == undefined || userId == null) {
                    userId = session.passport.user.UserId;
                }

                return new Promise(function (resolve, reject) {
                    db.imageItem.getImageListByUserId(session.passport.user.UserId, userId, args.LastItemId, args.LastItemDate, args.Count, ast, function (err, data) {
                        if (!err) {
                            resolve(data);
                        }
                        else {
                            reject();
                        }
                    });
                });
            }
        },

        ImageItemListByHashtag: {
            type: new graphql.GraphQLList(imageItemType),
            args: {
                Hashtag: {
                    type: graphql.GraphQLString
                },
                LastItemId: {
                    type: graphql.GraphQLFloat
                },
                LastItemDate: {
                    type: graphql.GraphQLFloat
                },
                Count: {
                    type: graphql.GraphQLInt
                }
            },
            resolve: function (parent, args, session, options) {

                var ast = projection.getProjection(options.fieldASTs[0]);

                return new Promise(function (resolve, reject) {
                    db.imageItem.getImageListByHashtag(session.passport.user.UserId, args.Hashtag, args.LastItemId, args.LastItemDate, args.Count, ast, function (err, data) {
                        if (!err) {
                            resolve(data);
                        }
                        else {
                            reject();
                        }
                    });
                });
            }
        },

        //lấy news feed theo user
        Newsfeed: {
            type: new graphql.GraphQLList(imageItemType),
            args: {
                LastItemId: {
                    type: graphql.GraphQLFloat
                },
                LastItemDate: {
                    type: graphql.GraphQLFloat
                },
                Count: {
                    type: graphql.GraphQLInt
                }
            },
            resolve: function (parent, args, session, options) {

                var ast = projection.getProjection(options.fieldASTs[0]);

                return new Promise(function (resolve, reject) {
                    db.imageItem.getNewsfeedByUserId(session.passport.user.UserId, args.LastItemId, args.LastItemDate, args.Count, ast, function (err, data) {
                        if (!err) {
                            resolve(data);
                        }
                        else {
                            reject();
                        }
                    });
                });
            }
        },

        //lấy news feed của tab feature
        FeatureFeeds: {
            type: new graphql.GraphQLList(imageItemType),
            args: {
                LastItemId: {
                    type: graphql.GraphQLFloat
                },
                LastItemDate: {
                    type: graphql.GraphQLFloat
                },
                Count: {
                    type: graphql.GraphQLInt
                },
                GetNewPost: {
                    type: graphql.GraphQLBoolean
                }
            },
            resolve: function (x, args, session, options) {

                var ast = projection.getProjection(options.fieldASTs[0]);
                var lastId = args.LastItemId;
                var lastDate = args.LastItemDate;
                var count = args.Count;
                var getNewPost = args.GetNewPost;

                var userId = session.passport.user.UserId;

                return new Promise(function (resolve, reject) {

                    //db.imageItem.getAllFeaturefeeds((resss) => {

                    //});
                    //db.imageItem.checkToClearCacheMTP(288, () => { });

                    //db.imageItem.getFeaturefeeds(lastId, lastDate, count, ast, function (err, data) {

                    db.imageItem.getFeatureFeedsFromCache(lastId, lastDate, count, getNewPost, (result) => {

                        if (result.ErrorCode == errorCode.success) {
                            var data = result.Data;
                            data = _.map(data, (item) => {
                                item = utilityHelper.checkAndParseToJson(item);

                                //gán vào để bắt trong resolve lấy comment
                                item.SourceCall = "FeatureFeeds";
                                return db.imageItem.parseItemContent(item, userId);
                            });



                            resolve(data);
                        }
                        else {
                            reject(result.ErrorMessage);
                        }
                    });
                });
            }
        },

        //khám phá
        Explores: {
            type: new graphql.GraphQLList(imageItemType),
            args: {
                LastItemId: {
                    type: graphql.GraphQLFloat
                },
                LastItemDate: {
                    type: graphql.GraphQLFloat
                },
                Count: {
                    type: graphql.GraphQLInt
                }
            },
            resolve: function (parent, args, session, options) {

                var ast = projection.getProjection(options.fieldASTs[0]);

                return new Promise(function (resolve, reject) {
                    db.imageItem.getExploreByUserId(session.passport.user.UserId, args.LastItemId, args.Count, ast, function (err, data) {
                        if (!err) {
                            resolve(data);
                        }
                        else {
                            reject();
                        }
                    });
                });
            }
        },

        //lấy danh sách post hình đã save
        SavedImages: {
            type: new graphql.GraphQLList(imageItemType),
            args: {
                UserId: {
                    type: graphql.GraphQLFloat
                },
                CollectionId: {
                    type: graphql.GraphQLInt
                },
                LastItemDate: {
                    type: graphql.GraphQLFloat
                },
                Count: {
                    type: graphql.GraphQLInt
                }
            },
            resolve: function (x, args, session, options) {

                var ast = projection.getProjection(options.fieldASTs[0]);
                var collectionId = args.CollectionId;
                var lastDate = args.LastItemDate;
                var count = args.Count;
                return new Promise(function (resolve, reject) {
                    var viewUserId = session.passport.user.UserId;
                    var userId = viewUserId;
                    if (args.UserId) {
                        userId = args.UserId;
                    }
                    db.imageItem.getSaveImages(viewUserId, userId, collectionId, lastDate, count, ast, function (err, data) {
                        if (!err) {
                            resolve(data);
                        }
                        else {
                            reject();
                        }
                    });
                });
            }
        },

        //lấy các post nhiều like nhất
        MostLikePosts: {
            type: new graphql.GraphQLList(imageItemType),
            args: {
                TopN: {
                    type: graphql.GraphQLInt
                },
                FromDate: {
                    type: graphql.GraphQLFloat
                },
                ToDate: {
                    type: graphql.GraphQLFloat
                }
            },
            resolve: function (parent, args, session, options) {

                var ast = projection.getProjection(options.fieldASTs[0]);

                return new Promise(function (resolve, reject) {
                    db.imageItem.getMostLikePosts(session.passport.user.UserId, args.TopN, args.FromDate, args.ToDate, ast, function (err, data) {
                        if (!err) {
                            resolve(data);
                        }
                        else {
                            reject();
                        }
                    });
                });
            }
        },

        //lấy các post nhiều comment
        MostCommentPosts: {
            type: new graphql.GraphQLList(imageItemType),
            args: {
                TopN: {
                    type: graphql.GraphQLInt
                },
                FromDate: {
                    type: graphql.GraphQLFloat
                },
                ToDate: {
                    type: graphql.GraphQLFloat
                }
            },
            resolve: function (parent, args, session, options) {

                var ast = projection.getProjection(options.fieldASTs[0]);

                return new Promise(function (resolve, reject) {
                    db.imageItem.getMostCommentPosts(session.passport.user.UserId, args.TopN, args.FromDate, args.ToDate, ast, function (err, data) {
                        if (!err) {
                            resolve(data);
                        }
                        else {
                            reject();
                        }
                    });
                });
            }
        },

        //lấy các post nhiều share
        MostSharePosts: {
            type: new graphql.GraphQLList(imageItemType),
            args: {
                TopN: {
                    type: graphql.GraphQLInt
                },
                FromDate: {
                    type: graphql.GraphQLFloat
                },
                ToDate: {
                    type: graphql.GraphQLFloat
                }
            },
            resolve: function (parent, args, session, options) {

                var ast = projection.getProjection(options.fieldASTs[0]);

                return new Promise(function (resolve, reject) {
                    db.imageItem.getMostSharePosts(session.passport.user.UserId, args.TopN, args.FromDate, args.ToDate, ast, function (err, data) {
                        if (!err) {
                            resolve(data);
                        }
                        else {
                            reject();
                        }
                    });
                });
            }
        },

        MostInteractivePosts: {
            type: new graphql.GraphQLList(imageItemType),
            args: {
                EachTopN: {
                    type: graphql.GraphQLInt
                },
                FromDate: {
                    type: graphql.GraphQLFloat
                },
                ToDate: {
                    type: graphql.GraphQLFloat
                }
            },
            resolve: function (parent, args, session, options) {

                var ast = projection.getProjection(options.fieldASTs[0]);

                var eachTopN = args.EachTopN;
                var fromDate = args.FromDate;
                var toDate = args.ToDate;

                if (!eachTopN) {
                    eachTopN = myConfig.adminPostReportEachCount;
                }

                if (!fromDate) {
                    fromDate = dateHelper.getHourIntervalStart(4);
                }

                if (!toDate) {
                    toDate = dateHelper.getHourIntervalEnd(4);
                }
                logger.debug('MostInteractivePosts', fromDate, toDate);
                return new Promise(function (resolve, reject) {
                    var result = [];
                    var mostShare = [];
                    var mostLike = [];
                    var mostComment = [];

                    var getMostShare = (cb) => {
                        //db.imageItem.getMostSharePosts(session.passport.user.UserId, eachTopN, fromDate, toDate, ast,
                        //    function (err, data) {
                        //    if (!err) {
                        //        mostShare = data;
                        //        cb();
                        //    }
                        //    else {
                        //        cb();
                        //    }
                        //});
                        cb();
                    };

                    var getMostComments = (cb) => {
                        db.imageItem.getMostCommentPosts(session.passport.user.UserId, eachTopN, fromDate, toDate, ast, function (err, data) {
                            if (!err) {
                                mostComment = data;
                                cb();
                            }
                            else {
                                cb();
                            }
                        });
                    };

                    var getMostLikes = (cb) => {
                        db.imageItem.getMostLikePosts(session.passport.user.UserId, eachTopN, fromDate, toDate, ast, function (err, data) {
                            if (!err) {
                                mostLike = data;
                                cb();
                            }
                            else {
                                cb();
                            }
                        });
                    };


                    async.parallel([getMostComments, getMostLikes, getMostShare], (err) => {
                        if (err) {
                            reject();
                        }
                        else {
                            logger.debug('mostLike', mostLike);
                            logger.debug('mostComment', mostComment);

                            result = Array.from(new Set([...mostLike, ...mostComment, ...mostShare]));
                            result = _.uniqBy(result, 'ImageItemId');
                            result = _.filter(result, (item) => {
                                return (item.IsDelete != true);
                            });
                            logger.debug(result);
                            if (result.length < eachTopN * 2) {
                                db.imageItem.getLastMostPosts(session.passport.user.UserId, ast, (err, data) => {

                                    if (err) {
                                        resolve(result);
                                    }
                                    else {
                                        if (data && data.length > 0) {
                                            result = Array.from(new Set([...result, ...data]));
                                            result = _.uniqBy(result, 'ImageItemId');
                                            result = _.filter(result, (item) => {
                                                return (item.IsDelete != true);
                                            });
                                            resolve(result);
                                        }
                                        else {
                                            resolve(result);
                                        }
                                    }
                                });
                            }
                            else {
                                resolve(result);
                            }
                        }
                    });
                });
            }
        },
    })
});