﻿var redisCacheHelper = require('../../models/redisCacheHelper');
var dateHelper = require('../../helper/dateHelper');
module.exports = {
    addImageComment: function (session, args, db, ast, callback) {
        var userId = session.passport.user.UserId;
        var imageItemId = args.ImageItemId;
        var comment = args.Comment;
        var os = session.passport.user.OS;
        var replyToId = args.ReplyToId;

        db.imageComment.createImageComment(userId, imageItemId, comment, replyToId, os, function (err, commentId) {
            if (err) {
                callback(err);
            }
            else {
                //db.imageComment.getImageCommentById(commentId, ast, function (err1, data) {
                //    if (err1) {
                //        callback(err1);
                //    }
                //    else {
                //        redisCacheHelper.updatePostCommentCountToFeatureTab(imageItemId, true, () => { });
                //        redisCacheHelper.updateFeaturePostComment(imageItemId, {
                //            ItemId: commentId,
                //            Comment: comment,
                //            CreateDate: dateHelper.getUnixTimeStamp(),
                //            UserId: userId
                //        }, () => {
                //        });
                //        callback(null, data);
                //    }
                //});
                callback(null, {
                    ItemId: commentId,
                    ImageItemId: imageItemId,
                    ReplyToId: replyToId,
                    Comment: comment
                });
            }
        });
    },

    updateImageComment: function (session, args, db, ast, callback) {
        var userId = session.passport.user.UserId;
        var commentId = args.CommentId;
        var comment = args.Comment;
        var os = session.passport.user.OS;

        db.imageComment.updateImageComment(commentId, comment, os, function (err, dumb) {

            if (err) {
                callback(err);
            }
            else {

                //db.imageComment.getImageCommentById(commentId, ast, function (err1, data) {

                //    if (err1) {
                //        callback(err1);
                //    }
                //    else {
                //        callback(null, data);
                //    }
                //});
                callback(null, {
                    ItemId: commentId
                });
            }
        });
    },

    deleteImageComment: function (session, args, db, ast, callback) {
        var userId = session.passport.user.UserId;
        var commentId = args.CommentId;
        var os = session.passport.user.OS;

        db.imageComment.deleteImageComment(commentId, ast, function (err, imageItemId) {
            if (err) {
                callback(err);
            }
            else {
                redisCacheHelper.updatePostCommentCountToFeatureTab(imageItemId, false, () => { });
                redisCacheHelper.deleteFeaturePostComment(imageItemId, commentId, () => { });

                //db.imageItem.getImageItemById(userId, imageItemId, ast, function (err1, data) {
                //    if (err1) {
                //        callback(err1);
                //    }
                //    else {
                //        callback(null, data);
                //    }
                //});

                callback(null, {
                    ImageItemId: imageItemId,
                    ItemId: commentId
                });
            }
        });
    }
};