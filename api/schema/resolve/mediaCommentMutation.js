﻿module.exports = {
    addMediaComment: function (session, args, db, ast, callback) {
        var userId = session.passport.user.UserId;
        var mediaId = args.MediaId;
        var comment = args.Comment;
        var os = session.passport.user.OS;
        var replyToId = args.ReplyToId;

        db.mediaComment.createMediaComment(userId, mediaId, comment, replyToId, os, function (err, commentId) {
            if (err) {
                callback(err);
            }
            else {
                db.mediaComment.getMediaCommentById(commentId, ast, function (err1, data) {
                    if (err1) {
                        callback(err1);
                    }
                    else {
                        callback(null, data);
                    }
                });
            }
        });
    },

    updateMediaComment: function (session, args, db, ast, callback) {
        var userId = session.passport.user.UserId;
        var commentId = args.CommentId;
        var comment = args.Comment;
        var os = session.passport.user.OS;

        db.mediaComment.updateMediaComment(commentId, comment, os, function (err, dumb) {

            if (err) {
                callback(err);
            }
            else {

                db.mediaComment.getMediaCommentById(commentId, ast, function (err1, data) {

                    if (err1) {
                        callback(err1);
                    }
                    else {
                        callback(null, data);
                    }
                });
            }
        });
    },

    deleteMediaComment: function (session, args, db, ast, callback) {
        var userId = session.passport.user.UserId;
        var commentId = args.CommentId;
        var os = session.passport.user.OS;

        db.mediaComment.deleteMediaComment(commentId, function (err, mediaId) {
            if (err) {
                callback(err);
            }
            else {
                db.media.getMediaById(mediaId, ast, function (err1, data) {
                    if (err1) {
                        callback(err1);
                    }
                    else {
                        callback(null, data);
                    }
                });
            }
        });
    }
};