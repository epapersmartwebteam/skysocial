﻿var moment = require('moment');
var momentDateTimeFormat = require('../../myConfig').momentDateTimeFormat;

var cryptoEngine = require('../../helper/cryptoEngine');

var storageHelper = require('../../helper/storageHelper');

//import lớp imageHelper
var imageHelper = require('../../helper/imageHelper');

var phoneHelper = require('../../helper/phoneHelper');

var myConfig = require('../../myConfig');

var coreChatAPI = require('../../chat/chatCore');
var coreChatFirebase = require('../../chat/firebaseHelper');

module.exports = {

    //cập nhật avatar user
    updateUserAvatar: function (session, args, db, ast, callback) {
        var userInfo = session.passport.user;
        var userId = userInfo.UserId;
        var itemGUID = args.ItemGUID ? args.ItemGUID : '';

        var avatarOriginal = imageHelper.genearateImageOriginalUrl(storageHelper.containers.avatar, userInfo.UserGUID, itemGUID, args.ImageExtension);
        var avatarMedium = imageHelper.genearateImageMediumUrl(storageHelper.containers.avatar, userInfo.UserGUID, itemGUID, args.ImageExtension);
        var avatarSmall = imageHelper.genearateImageSmallUrl(storageHelper.containers.avatar, userInfo.UserGUID, itemGUID, args.ImageExtension);

        db.user.updateUserAvatar(userId, avatarOriginal, avatarMedium, avatarSmall, function (err, dumb) {
            console.log(err);
            if (!err) {
                console.log('updateUserAvatar');
                /**
                   nếu thêm vào db thành công thì gọi web job để tạo thumbnail
                   Large tạm thời ko tạo mà dùng luôn hình original
                */
                ////tạo Medium                           
                //azureHelper.createAvatarMediumSizeImageQueue(userId, avatarOriginal, function () {

                //    //gán callback vào để non-block chứ ko có nhảy vào đây
                //});
                ////tạo small
                //azureHelper.createAvatarSmallSizeImageQueue(userId, avatarOriginal, function () {
                //    //gán callback vào để non-block chứ ko có nhảy vào đây
                //});

                db.user.getUserById(userId, ast, function (error, data) {
                    //    console.log(error, data);
                    if (!error) {
                        //        var chatUserId = data.ChatUserId;
                        avatarMedium = data.AvatarMedium;
                        //        console.log('ChatUserId', chatUserId);
                        //        console.log('ChatUserId', avatarMedium);
                    }
                    else {
                        avatarMedium = myConfig.CDNUrl + avatarMedium;
                    }
                    //        if (chatUserId && avatarMedium) {
                    coreChatAPI.updateUserAvatar(coreChatFirebase.getSkyUID(userId), avatarMedium, () => { });
                    //    }
                    //}
                    return callback(error, data);
                });
            }
            else {
                return callback(err);
            }
        });
    },

    //cập nhật profile user
    updateUserProfile: function (session, args, db, ast, callback) {
        var userInfo = session.passport.user;
        var userId = userInfo.UserId;
        var newUserName = args.UserName;
        var newEmail = args.Email;
        var newPhone = args.Phone;
        var newFullName = args.FullName;
        var newGender = args.Gender;
        var newBirthDay = args.Birthday;
        var newFavorite = args.Favorite;

        var newZodiac = args.Zodiac;
        var newMaritalStatus = args.MaritalStatus;
        var newAddress = args.Address;

        var newSocialInfo = args.SocialInfo;

        var userName;
        var email;
        var phone;
        var fullName;
        var gender;
        var birthDay;
        var favorite;

        var zodiac;
        var maritalStatus;
        var address;

        var socialInfo;

        if (newUserName != undefined && newUserName != null) {

            //có thay đổi UserName thì set vào. Mặc định là bên client đã gọi hàm check username tồn tại
            if (newUserName != userInfo.UserName) {
                userName = newUserName;
            }
        }

        if (newEmail != undefined && newEmail != null) {

            //có thay đổi Email thì set vào. Mặc định là bên client đã gọi hàm check Email tồn tại
            if (newEmail != userInfo.Email) {
                email = newEmail;
            }
        }

        if (newPhone != undefined && newPhone != null) {

            //có thay đổi Email thì set vào. Mặc định là bên client đã gọi hàm check Email tồn tại
            var oldPhone = phoneHelper.patchValidPhoneNumber(userInfo.Phone);
            newPhone = phoneHelper.patchValidPhoneNumber(newPhone);
            if (newPhone != oldPhone) {
                phone = newPhone;
            }
        }

        if (newFullName != undefined && newFullName != null) {

            //có thay đổi Email thì set vào. Mặc định là bên client đã gọi hàm check Email tồn tại
            if (newFullName != userInfo.FullName) {
                fullName = newFullName;
            }
        }

        if (newGender != undefined && newGender != null) {

            //có thay đổi Email thì set vào. Mặc định là bên client đã gọi hàm check Email tồn tại
            if (newGender != userInfo.Gender) {
                gender = newGender;
            }
        }

        if (newBirthDay != undefined && newBirthDay != null) {

            //có thay đổi Email thì set vào. Mặc định là bên client đã gọi hàm check Email tồn tại
            if (newBirthDay != userInfo.BirthDay) {
                birthDay = newBirthDay;
            }
        }

        if (newFavorite != undefined && newFavorite != null) {

            //có thay đổi Email thì set vào. Mặc định là bên client đã gọi hàm check Email tồn tại
            if (newFavorite != userInfo.Favorite) {
                favorite = newFavorite;
            }
        }

        if (newSocialInfo != undefined && newSocialInfo != null) {
            if (newSocialInfo != userInfo.SocialInfo) {
                socialInfo = newSocialInfo;
            }
        }

        if (newZodiac != undefined && newZodiac != null) {
            if (newZodiac != userInfo.Zodiac) {
                zodiac = newZodiac;
            }
        }

        if (newMaritalStatus != undefined && newMaritalStatus != null) {
            if (newMaritalStatus != userInfo.MaritalStatus) {
                maritalStatus = newMaritalStatus;
            }
        }

        if (newAddress != undefined && newAddress != null) {
            if (newAddress != userInfo.Address) {
                address = newAddress;
            }
        }

        //nếu tất cả đều ko có thay đổi thì khỏi phải gọi hàm update db, chỉ cần trả về thông tin user
        if (userName == undefined && email == undefined && phone == undefined && fullName == undefined &&
            gender == undefined && birthDay == undefined && favorite == undefined && socialInfo == undefined) {

            db.user.getUserById(userId, ast, function (error, data) {
                return callback(error, data);
            });
        }
        else {

            db.user.checkUserProfileExistBeforeUpdate(userId, userName, email, phone, function (errCheck, existedField) {
                if (errCheck) {

                    return callback(errCheck);
                }
                else {
                    if (existedField != '') {
                        return callback(existedField);
                    }
                    else {
                        //có thay đổi thì ghi
                        db.user.updateUserProfile(userId, userName, email, phone, fullName, gender, birthDay, favorite, socialInfo, zodiac, maritalStatus, address, function (err, dumb) {
                            if (!err) {
                                db.user.getUserById(userId, ast, function (error, data) {
                                    if (!error) {
                                        if (userName != undefined) {
                                            var chatUserId = data.ChatUserId;
                                            userName = data.UserName;

                                            coreChatAPI.updateUserName(coreChatFirebase.getSkyUID(userId), userName, () => { });

                                            session.passport.user.UserName = userName;
                                        }
                                        if (email != undefined) {
                                            session.passport.user.Email = email;
                                        }
                                        if (phone != undefined) {
                                            session.passport.user.Phone = phone;
                                        }
                                        if (fullName != undefined) {
                                            session.passport.user.FullName = fullName;
                                        }
                                        if (gender != undefined) {
                                            session.passport.user.Gender = gender;
                                        }
                                        if (birthDay != undefined) {
                                            session.passport.user.Birthday = birthDay;
                                        }
                                        if (favorite != undefined) {
                                            session.passport.user.Favorite = favorite;
                                        }
                                        if (socialInfo != undefined) {
                                            session.passport.user.SocialInfo = socialInfo;
                                        }
                                        if (zodiac != undefined) {
                                            session.passport.user.Zodiac = zodiac;
                                        }
                                        if (maritalStatus != undefined) {
                                            session.passport.user.MaritalStatus = maritalStatus;
                                        }
                                        if (address != undefined) {
                                            session.passport.user.Address = address;
                                        }
                                    }
                                    return callback(error, data);
                                });
                            }
                            else {
                                return callback(err);
                            }
                        });
                    }
                }
            });
        }
    },

    //cập nhật mật khẩu user
    updateUserPassword: function (session, args, db, ast, callback) {
        var userInfo = session.passport.user;
        var userId = userInfo.UserId;

        var currentPassword = args.CurrentPassword;
        var newPassword = args.NewPassword;

        currentPassword = (currentPassword != '') ? cryptoEngine.encryptText(currentPassword) : '';
        newPassword = cryptoEngine.encryptText(newPassword);


        if (session.passport.user.Password == '' || currentPassword == userInfo.Password) {

            //pass cũ đúng mới update
            db.user.updateUserPassword(userId, newPassword, function (err, dumb) {
                if (!err) {
                    db.user.getUserById(userId, ast, function (error, data) {
                        if (!error) {
                            session.passport.user.Password = newPassword;
                        }
                        return callback(error, data);
                    });
                }
                else {
                    return callback(err);
                }
            });
        }
        else {

            return callback('CurrentPassword incorrect');
        }
    },


    //add follow
    followUser: function (session, args, db, ast, callback) {
        var userInfo = session.passport.user;
        var fromUserId = userInfo.UserId;
        var toUserId = args.UserId;

        db.user.addFollower(fromUserId, toUserId, false, function (err, dumb) {
            if (!err) {

                //todo: có thể cải thiện tốc độ bằng cách đọc lại session ko đọc db
                db.user.getUserById(fromUserId, ast, function (error, data) {
                    return callback(error, data);
                });
            }
            else {
                return callback(err);
            }
        });
    },

    //bỏ follow
    unfollowUser: function (session, args, db, ast, callback) {
        var userInfo = session.passport.user;
        var fromUserId = userInfo.UserId;
        var toUserId = args.UserId;

        db.user.removeFollower(fromUserId, toUserId, function (err, dumb) {
            if (!err) {

                //todo: có thể cải thiện tốc độ bằng cách đọc lại session ko đọc db
                db.user.getUserById(fromUserId, ast, function (error, data) {
                    return callback(error, data);
                });
            }
            else {
                return callback(err);
            }
        });
    },

    //đồng ý follow
    acceptUserFollow: function (session, args, db, ast, callback) {
        var userInfo = session.passport.user;
        var toUserId = userInfo.UserId;

        var fromUserId = args.UserId;

        db.user.acceptUserFollow(fromUserId, toUserId, function (err, dumb) {
            if (!err) {

                //todo: có thể cải thiện tốc độ bằng cách đọc lại session ko đọc db
                db.user.getUserById(toUserId, ast, function (error, data) {
                    return callback(error, data);
                });
            }
            else {
                return callback(err);
            }
        });
    },

    followHashtag: function (session, args, db, ast, callback) {
        var userInfo = session.passport.user;
        var userId = userInfo.UserId;
        var hashtags = args.Hashtags;

        db.userHashtag.followHashtag(userId, hashtags, true, (err, dumb) => {
            if (!err) {

                //todo: có thể cải thiện tốc độ bằng cách đọc lại session ko đọc db
                db.user.getUserById(userId, ast, function (error, data) {
                    return callback(error, data);
                });
            }
            else {
                return callback(err);
            }
        });
    },

    unfollowHashtag: function (session, args, db, ast, callback) {
        var userInfo = session.passport.user;
        var userId = userInfo.UserId;
        var hashtags = args.Hashtags;

        db.userHashtag.unfollowHashtag(userId, hashtags, (err, dumb) => {
            if (!err) {

                //todo: có thể cải thiện tốc độ bằng cách đọc lại session ko đọc db
                db.user.getUserById(userId, ast, function (error, data) {
                    return callback(error, data);
                });
            }
            else {
                return callback(err);
            }
        });
    },

};