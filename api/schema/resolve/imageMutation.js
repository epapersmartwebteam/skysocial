﻿var _ = require('lodash');
var moment = require('moment');
var momentDateTimeFormat = require('../../myConfig').momentDateTimeFormat;

//import lớp imageHelper
var imageHelper = require('../../helper/imageHelper');

var storageHelper = require('../../helper/storageHelper');

var myConfig = require('../../myConfig');

var redisCacheHelper = require('../../models/redisCacheHelper');

module.exports = {
    //createImageItem: function (session, args, db, callback) {
    //    var userInfo = session.passport.user;
    //    db.user.checkUserIsBanned(userInfo, function (isBanned) {
    //        if (isBanned) {
    //            return callback({
    //                ErrorCode: 6,
    //                Message: 'Banned'
    //            });
    //        }
    //        else {
    //            var userId = userInfo.UserId;
    //            var itemGUID = args.ItemGUID ? args.ItemGUID : '';
    //            var itemContent = args.ItemContent;
    //            //var imageOriginal = imageHelper.genearateImageOriginalUrl(userInfo.UserGUID, args.ItemGUID, args.ImageExtension);
    //            //var imageLarge = imageHelper.genearateImageLargeUrl(userInfo.UserGUID, args.ItemGUID, args.ImageExtension);
    //            //var imageMedium = imageHelper.genearateImageMediumUrl(userInfo.UserGUID, args.ItemGUID, args.ImageExtension);
    //            //var imageSmall = imageHelper.genearateImageSmallUrl(userInfo.UserGUID, args.ItemGUID, args.ImageExtension);

    //            //var imageOriginalWidth = args.ImageWidth;
    //            //var imageOriginalHeight = args.ImageHeight;
    //            //var imageLargeWidth = myConfig.LargeWidth;
    //            //var imageLargeHeight = imageHelper.getImageHeightFromWidthWithRatio(imageLargeWidth, imageOriginalWidth, imageOriginalHeight);
    //            //var imageMediumWidth = myConfig.MediumWidth;
    //            //var imageMediumHeight = imageHelper.getImageHeightFromWidthWithRatio(imageMediumWidth, imageOriginalWidth, imageOriginalHeight);
    //            //var imageSmallWidth = myConfig.SmallWidth;
    //            //var imageSmallHeight = imageHelper.getImageHeightFromWidthWithRatio(imageSmallWidth, imageOriginalWidth, imageOriginalHeight);

    //            var createOS = userInfo.OS;
    //            var description = args.Description ? args.Description : '';
    //            var isPrivate = args.IsPrivate ? args.IsPrivate : '';
    //            var location = args.Location ? args.Location : '';
    //            var lat = args.Lat ? args.Lat : 0;
    //            var long = args.Long ? args.Long : 0;
    //            var userTags = args.UserTags;
    //            var hashtags = args.Hashtags;


    //            db.imageItem.createImageItem(userId, itemGUID, imageOriginal, imageLarge, imageMedium, imageSmall,
    //                                        imageOriginalWidth, imageOriginalHeight, imageLargeWidth, imageLargeHeight,
    //                                        imageMediumWidth, imageMediumHeight, imageSmallWidth, imageSmallHeight,
    //                                        createOS, description, isPrivate, location, lat, long,
    //                        function (err, imageItemId) {

    //                            if (!err) {

    //                                /**
    //                                nếu thêm vào db thành công thì gọi web job để tạo thumbnail                            
    //                                */
    //                                //tạo large
    //                                azureHelper.createLargeSizeImageQueue(imageItemId, imageOriginal, function () {
    //                                    //gán callback vào để non-block chứ ko có nhảy vào đây
    //                                });

    //                                //tạo Medium                           
    //                                azureHelper.createMediumSizeImageQueue(imageItemId, imageOriginal, function () {
    //                                    //gán callback vào để non-block chứ ko có nhảy vào đây
    //                                });

    //                                //tạo small
    //                                azureHelper.createSmallSizeImageQueue(imageItemId, imageOriginal, function () {
    //                                    //gán callback vào để non-block chứ ko có nhảy vào đây
    //                                });

    //                                if (hashtags) {

    //                                    //todo: chưa xử lý lỗi nếu add hashtag bị lỗi
    //                                    db.imageItem.createImageHashtags(imageItemId, hashtags, function (err1, data1) {

    //                                    });
    //                                }

    //                                if (userTags) {

    //                                    //todo: chưa xử lý lỗi nếu add user tags bị lỗi
    //                                    db.imageItem.createImageUsertags(imageItemId, userTags, function (err2, data2) {

    //                                    });
    //                                }

    //                                //sau khi thêm xong thì gán trở ngược vào biến imageItem để trả về
    //                                var imageItem = {
    //                                    ImageItemId: imageItemId,
    //                                    ItemGUID: itemGUID,
    //                                    UserId: userId,
    //                                    ImageOriginal: myConfig.CDNUrl + imageOriginal,
    //                                    ImageLarge: myConfig.CDNUrl + imageLarge,
    //                                    ImageMedium: myConfig.CDNUrl + imageMedium,
    //                                    ImageSmall: myConfig.CDNUrl + imageSmall,
    //                                    CreateDate: moment().format(momentDateTimeFormat),
    //                                    CreateOS: createOS,
    //                                    LastModifyDate: moment().format(momentDateTimeFormat),
    //                                    LastModifyOS: createOS,
    //                                    Description: description,
    //                                    Location: location,
    //                                    Lat: lat,
    //                                    Long: long,
    //                                    IsPrivate: isPrivate,
    //                                    LikeCount: 0,
    //                                    CommentCount: 0,
    //                                    ShareCount: 0,
    //                                    ShareFromId: null
    //                                };

    //                                //trả về
    //                                return callback(null, imageItem);
    //                            }
    //                            else {

    //                                return callback(err);
    //                            }
    //                        });
    //        }
    //    });        
    //},

    updateImageItem: function (session, args, db, ast, callback) {
        var userInfo = session.passport.user;
        var userId = userInfo.UserId;
        var os = userInfo.OS;
        var imageItemId = args.ImageItemId;

        var description = (args.Description != undefined && args.Description != null) ? args.Description : undefined;
        var itemData = (args.ItemData != undefined && args.ItemData != null) ? args.ItemData : undefined;

        var isPrivate = args.IsPrivate ? args.IsPrivate : undefined;
        var location = (args.Location != undefined && args.Location != null) ? args.Location : undefined;
        var lat = args.Lat ? args.Lat : undefined;
        var long = args.Long ? args.Long : undefined;
        var userTags = (args.UserTags != undefined && args.UserTags != null) ? args.UserTags : undefined;
        var hashtags = (args.Hashtags != undefined && args.Hashtags != null) ? args.Hashtags : undefined;

        if (description == undefined && isPrivate == undefined && location == undefined && lat == undefined && long == undefined) {
            //userTags và hashTags tách ra từ description nên ko có description thì cũng ko cần thay đổi 2 cái này

            db.imageItem.getImageItemById(userId, imageItemId, ast, function (err, imageItem) {
                if (!err) {
                    return callback(null, imageItem);
                }
                else {
                    return callback(err);
                }
            });
        }
        else {
            db.imageItem.updateImageItem(userId, imageItemId, os, description, itemData, isPrivate, location, lat, long, function (err, dumb) {
                if (err) {
                    return callback(err);
                }
                else {
                    if (description != undefined) {
                        if (hashtags) {

                            //todo: chưa xử lý lỗi nếu add hashtag bị lỗi
                            db.imageItem.updateImageHashtags(imageItemId, hashtags, function (err1, data1) {

                            });
                        }

                        if (userTags) {

                            //todo: chưa xử lý lỗi nếu add user tags bị lỗi
                            db.imageItem.updateImageUsertags(imageItemId, userTags, function (err2, data2) {

                            });
                        }
                    }

                    //update vào cache
                    redisCacheHelper.updatePostInfoToFeatureTab(userId, imageItemId, description, isPrivate, location, lat, long, () => { });

                    db.imageItem.getImageItemById(userId, imageItemId, ast, function (err2, imageItem) {
                        if (!err2) {
                            return callback(null, imageItem);
                        }
                        else {
                            return callback(err2);
                        }
                    });
                }
            });
        }
    },

    updateImageItemLiked: function (session, args, db, ast, callback) {
        var userId = session.passport.user.UserId;
        var imageItemId = args.ImageItemId;
        var isLiked = args.IsLiked;
        var likeEmotionId = args.LikeEmotionId ? args.LikeEmotionId : 1;

        db.imageItem.updateImageItemLiked(userId, imageItemId, isLiked, likeEmotionId, function (err, dumb) {
            if (err) {
                return callback(err);
            }
            else {
                //add thêm Like count để trả về update vào cache
                //if (!ast || ast.length == 0) {
                //    ast = ['LikeCount'];
                //}
                //else {
                //    ast.push('LikeCount');
                //    ast = _.uniq(ast);
                //}
                //db.imageItem.getImageItemById(userId, imageItemId, ast, function (err1, data) {
                //    if (err1) {
                //        callback(err1);
                //    }
                //    else {

                //        //update vào cache

                //        if (data) {
                //            redisCacheHelper.updatePostExtraToFeatureTab(data.UserId, imageItemId, data.LikeCount, undefined, undefined, () => { });
                //        }
                //        callback(null, data);
                //    }
                //});

                //chỉ trả về ImageItemId
                callback(null, {
                    ImageItemId: imageItemId
                });
            }
        });
    },

    reshareImageItem: function (session, args, db, ast, callback) {
        var userId = session.passport.user.UserId;
        var imageItemId = args.ImageItemId;
        var shareTo = args.ShareTo;

        db.imageItem.reshareImageItem(userId, imageItemId, shareTo, function (err, dumb) {
            if (err) {
                return callback(err);
            }
            else {
                //add thêm Like count để trả về update vào cache
                //if (!ast || ast.length == 0) {
                //    ast = ['ShareCount'];
                //}
                //else {
                //    ast.push('ShareCount');
                //    ast = _.uniq(ast);
                //}
                //db.imageItem.getImageItemById(userId, imageItemId, ast, function (err1, data) {
                //    if (err1) {
                //        callback(err1);
                //    }
                //    else {
                //        if (data) {
                //            //update vào cache
                //            redisCacheHelper.updatePostExtraToFeatureTab(data.UserId, imageItemId, undefined, data.ShareCount, undefined, () => { });
                //        }
                //        callback(null, data);
                //    }
                //});

                callback(null, {
                    ImageItemId: imageItemId
                });
            }
        });
    }
};