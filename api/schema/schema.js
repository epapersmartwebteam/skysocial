﻿//import GraphQL
var graphql = require('graphql');

////import lớp đọc db
//var db = require('../models/all');

////import hàm GetProjection
//var projection = require('./getProjection');

////import các hằng biến toàn cục
//var CDNUrl = require('../myConfig').CDNUrl;

var querySchema = require('./querySchema').querySchema;
var mutationSchema = require('./mutationSchema').mutationSchema;

//GraphQL Schema
exports.schema = new graphql.GraphQLSchema({
    query: querySchema,
    mutation: mutationSchema
});