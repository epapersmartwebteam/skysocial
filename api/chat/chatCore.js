﻿var request = require('request');

var config = require('../myConfig');
var chatCore = config.chatCore;

var resultHelper = require('../models/result');

var createUser = (uid, name, email, phone, callback) => {
    var url = chatCore.chatCoreUrl + 'createCustomUser';
    request.post({
        uri: url,
        form: {
            providerId: chatCore.providerId,
            apiKey: chatCore.apiKey,
            uid: uid,
            name: name,
            email: email,
            phone: phone
        }
    },
        function (error, response, body) {
            if (!error && response.statusCode == 200) {
                var result = JSON.parse(body);
                callback(resultHelper.create(result.errorCode, result.error, result.data));
            }
            else {
                console.log(response);
                console.log(body);
                console.log(error);
                callback(resultHelper.serviceError(error));
            }
        }
    );
};


var updateUserName = (uid, name, callback) => {
    var url = chatCore.chatCoreUrl + 'updateCustomUserName';
    request.post({
        uri: url,
        form: {
            providerId: chatCore.providerId,
            apiKey: chatCore.apiKey,
            uid: uid,
            name: name
        }
    },
        function (error, response, body) {
            if (!error && response.statusCode == 200) {
                var result = JSON.parse(body);
                callback(resultHelper.create(result.errorCode, result.error, result.data));
            }
            else {
                callback(resultHelper.serviceError(error));
            }
        }
    );
};

var updateUserAvatar = (uid, avatar, callback) => {
    var url = chatCore.chatCoreUrl + 'updateCustomUserAvatar';
    request.post({
        uri: url,
        form: {
            providerId: chatCore.providerId,
            apiKey: chatCore.apiKey,
            uid: uid,
            avatar: avatar
        }
    },
        function (error, response, body) {
            if (!error && response.statusCode == 200) {
                var result = JSON.parse(body);
                callback(resultHelper.create(result.errorCode, result.error, result.data));
            }
            else {
                callback(resultHelper.serviceError(error));
            }
        }
    );
};

module.exports = {
    createUser: createUser,
    updateUserName: updateUserName,
    updateUserAvatar: updateUserAvatar
};