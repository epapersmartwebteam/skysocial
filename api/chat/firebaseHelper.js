﻿var admin = require("firebase-admin");

var serviceAccount = require('./chatcore-7120d-c1196112038a.json');

var logger = require('../helper/logHelper').logger;

var resultHelper = require('../models/result');

var config = require('../myConfig');

var skyInCoreChatPrefix = config.chatCore.userPrefix;

function initApp() {
    return admin.initializeApp({
        credential: admin.credential.cert(serviceAccount),
        databaseURL: "https://chatcore-7120d.firebaseio.com"
    }, "chatCore");
}
var chatCoreApp = initApp();

var getChatCoreAuth = () => {
    if (!chatCoreApp) {
        chatCoreApp = initApp();
    }

    return chatCoreApp.auth();
};

/**
 * Tạo uid trên firebase với mã là SKY-<UserId>
 * @param {any} userId
 * @returns {string} - UID trên firebase
 */
var getSkyUID = (userId) => {
    return skyInCoreChatPrefix + userId;
};

/**
 * Tạo custom token từ uid, uid là tên hệ thống sky-<UserId của hệ thống Sky>
 * @param {string} uid
 * @param {object} callback -- Trả về customToken
 */
var createCustomToken = (uid, callback) => {
    var chatCoreAuth = getChatCoreAuth();

    chatCoreAuth.createCustomToken(uid)
        .then(function (customToken) {
            // Send token back to client
            callback(resultHelper.success(customToken));
        })
        .catch(function (error) {
            logger.error('Error creating custom token:', error);
            callback(resultHelper.serviceError(error));
        });
};

/**
 * Cập nhật thông tin name 
 * @param {string} uid
 * @param {string} name
 * @param {object} callback -- Trả về userRecord
 */
var updateUserName = (uid, name, callback) => {
    var chatCoreAuth = getChatCoreAuth();

    chatCoreAuth.updateUser(uid, {
        displayName: name
    })
        .then(function (userRecord) {

            var user = userRecord.toJSON();
          
            callback(resultHelper.success(user));
        })
        .catch(function (error) {
            logger.error('Error update user', error);
            callback(resultHelper.serviceError(error));
        });
};

var getUser = (uid, callback) => {
    var chatCoreAuth = getChatCoreAuth();

    chatCoreAuth.getUser(uid)
        .then(function (userRecord) {
            
            var user = userRecord.toJSON();
            logger.debug('Successfully get user:', user);
            callback(resultHelper.success(user));
        })
        .catch(function (error) {
            logger.error('Error get user:', error);
            callback(resultHelper.serviceError(error));
        });

};

/**
 * 
 * @param {any} uid
 * @param {any} name
 * @param {any} callback
 */
var createUser = (uid, name, callback) => {
    var chatCoreAuth = getChatCoreAuth();

    chatCoreAuth.createUser(
        {
            uid: uid,
            displayName: name,
            disabled: false
        })
        .then(function (userRecord) {
            // See the UserRecord reference doc for the contents of userRecord.
            logger.debug('Successfully created new user:', userRecord);
            var user = userRecord.toJSON();
            callback(resultHelper.success(user));
        })
        .catch(function (error) {
            logger.error('Error creating new user:', error);
            if (error.errorInfo && error.errorInfo.code == 'auth/uid-already-exists') {
                getUser(uid, callback);
            }
            else {
                callback(resultHelper.serviceError(error));
            }
        });

};

module.exports = {
    getSkyUID: getSkyUID,
    createCustomToken: createCustomToken,
    createUser: createUser,
    updateUserName: updateUserName
};