﻿var _ = require('lodash');
var async = require('async');

var moment = require('moment');

var schedule = require('node-schedule');

var config = require('./myConfig');
var redisCacheHelper = require('./models/redisCacheHelper');
var logger = require('./helper/logHelper').logger;

var resultHelper = require('./models/result');

var db = require('./models/all');
var errorCode = require('./errorCode');

function startAppSync() {
    setTimeout(() => {
        if (config.cacheConfig.enableCache) {
            var initSync = () => {
                redisCacheHelper.initSyncPostToFeatureTab((result) => {
                    logger.debug('sync result', result);
                });
            };

            var getPostFromCache = () => {
                redisCacheHelper.getPostsFromFeatureTab(null, null, 2, false, (result) => {
                    if (result.ErrorCode == 0) {
                        if (result.Data.length == 0) {
                            logger.debug('post from redis', 'need sync');
                            initSync();
                        }
                        else {
                            logger.debug('post from redis', 'ok');
                        }
                    }
                    else {
                        logger.debug('post from redis', 'need sync');
                        initSync();
                    }
                });
            };
            
            getPostFromCache();
            
            //30s kiểm tra cache 1 lần
            var j = schedule.scheduleJob('*/30 * * * * *', function () {
                //logger.debug('getPostFromCache run check', moment().utc().format());
                getPostFromCache();
            });

            var p = schedule.scheduleJob('15 * * * * *', function () {
                //logger.debug('syncPostActivity run check', moment().utc().format());
                syncPostActivity();
            });
        }

    }, 1000);
}

function getPostActivityCount(lastItemId, count, hasCommentList, callback) {
    db.sync.featureTab.getFeatureFeedPostActivityCount(lastItemId, count, (result) => {
        logger.debug('getFeatureFeedPostActivityCount run with lastItemId', lastItemId);
        if (result.ErrorCode == errorCode.success) {
            var lst = result.Data;
            logger.debug('OK with items count', lst.length);

            async.eachLimit(lst, config.cacheConfig.parallelItemMax, (item, cb) => {
                if (item.CommentCount > 0) {
                    hasCommentList.push(item.ImageItemId);
                }

                redisCacheHelper.updatePostExtraToFeatureTab(item.UserId, item.ImageItemId, item.LikeCount, item.ShareCount, item.CommentCount, undefined, false,
                    () => {
                        cb();
                    });
            },
                (err) => {
                    logger.debug('hasCommentList', hasCommentList.length);
                    if (lst.length == count) {
                        getPostActivityCount(lst[lst.length - 1].ImageItemId, count, hasCommentList, callback);
                    }
                    else {
                        logger.debug('getFeatureFeedPostActivityCount done with commentList count', hasCommentList.length);
                        callback(hasCommentList);
                    }
                });
        }
        else {
            logger.error('getFeatureFeedPostActivityCount fail with', result);
            callback(hasCommentList);
        }
    });
}

function getPostHasComment(imageItemIds, callback) {
    var commentOfItems = [];
    async.eachLimit(imageItemIds, config.cacheConfig.parallelItemMax, (imageItemId, cb) => {
        db.imageComment.getTopCommentOfFeaturePost(imageItemId, config.cacheConfig.commentPerPost, (err, items) => {
            commentOfItems.push({ ImageItemId: imageItemId, Comments: items });
            cb();
        });
    }, (err) => {
        redisCacheHelper.syncPostCommentToFeatureTab(imageItemIds, commentOfItems, () => {
            callback(resultHelper.success());
        });
    });
}

function syncPostActivity() {
    var hasCommentList = [];
    getPostActivityCount(null, config.cacheConfig.itemPerSync, hasCommentList, (imageItemIds) => {
        if (imageItemIds && imageItemIds.length > 0) {
            getPostHasComment(imageItemIds, () => { });
        }
    });
}

module.exports = {
    startAppSync: startAppSync
};