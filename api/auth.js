﻿var passport = require('passport');
var LocalStrategy = require('passport-local').Strategy;
var db = require('./models/all');

var osHelper = require('./helper/osHelper');

var myConfig = require('./myConfig');
var dateHelper = require('./helper/dateHelper');

var logger = require('./helper/logHelper').logger;

passport.use('local', new LocalStrategy(
    { passReqToCallback: true },
    function (req, username, password, done) {
        db.user.login(username, password, function (err, data) {
            if (!err) {

                //lấy os và ghi log
                var os = osHelper.getOSFromRequest(req);
                db.user.updateLoginLog(data.UserId, os, function (err1, data1) {
                });
                try {
                    logger.debug('Login success:', data);
                }
                catch (ex) {
                }
                //trả về ko cần chờ

                
                if (data.Beta == true || data.RoleId == 1) {
                    data.ReleaseDate = 0;
                }
                else {
                    var now = dateHelper.getUnixTimeStamp();
                    data.ReleaseMessage = myConfig.releaseMessage;
                    if (now >= myConfig.releaseDate) {
                        data.ReleaseDate = 0;
                    }
                    else {
                        if (myConfig.releaseDate == -1) {
                            data.ReleaseDate = myConfig.releaseDate;
                        }
                        else {
                            var countDown = myConfig.releaseDate - dateHelper.getUnixTimeStamp();
                            if (countDown < 0) {
                                countDown = 0;
                            }
                            data.ReleaseDate = countDown;
                        }
                    }
                }

                return done(null, data);
            }
            else
                return done(err, null);
        });
    }
));

passport.use('localFacebook', new LocalStrategy(
    { passReqToCallback: true },
    function (req, username, password, done) {
        db.user.loginFacebook(username, function (err, data) {
            
            if (!err) {

                //lấy os và ghi log
                var os = osHelper.getOSFromRequest(req);
                if (data != undefined) {
                    db.user.updateLoginLog(data.UserId, os, function (err1, data1) {
                    });
                }

                if (data.Beta == true || data.RoleId == 1) {
                    data.ReleaseDate = 0;
                }
                else {
                    var now = dateHelper.getUnixTimeStamp();
                    data.ReleaseMessage = myConfig.releaseMessage;
                    if (now >= myConfig.releaseDate) {
                        data.ReleaseDate = 0;
                    }
                    else {
                        if (myConfig.releaseDate == -1) {
                            data.ReleaseDate = myConfig.releaseDate;
                        }
                        else {
                            var countDown = myConfig.releaseDate - dateHelper.getUnixTimeStamp();
                            if (countDown < 0) {
                                countDown = 0;
                            }
                            data.ReleaseDate = countDown;
                        }
                    }
                }
                //trả về ko cần chờ
                return done(null, data);
            }
            else {
                return done(err, null);
            }
        });
    }
));

passport.serializeUser(function (user, done) {
    
    logger.debug('serializeUser', user);
    done(null, user);
});

passport.deserializeUser(function (user, done) {
    
    logger.debug('deserializeUser', user);
    return done(null, user);
    //});   
});

