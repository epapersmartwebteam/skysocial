﻿var crypto = require('crypto');

module.exports = {
    encryptText: function (text) {
        if (text == '' || text == undefined || text == null)
            return text;

        var encoding = 'base64';
        var key = new Buffer('winkjoyp');
        var iv = new Buffer('rojectXX');
        var cipher_alg = 'des';

        var cipher = crypto.createCipheriv(cipher_alg, key, iv);

        encoding = encoding || "binary";

        var result = cipher.update(text, "utf8", encoding);
        result += cipher.final(encoding);

        return result;
    },
    decryptText: function (text) {

        if (text == '' || text == undefined || text == null)
            return text;

        var encoding = 'base64';
        var key = new Buffer('winkjoyp');
        var iv = new Buffer('rojectXX');
        var cipher_alg = 'des';
        var decipher = crypto.createDecipheriv(cipher_alg, key, iv);

        encoding = encoding || "binary";

        try {
            var result = decipher.update(text, encoding);
            result += decipher.final();
            return result;
        }
        catch (ex) {
            return null;
        }
    }
};