﻿var request = require('request');
var db = require('../models/all');
var myConfig = require('../myConfig');
var cryptoEngine = require('../helper/cryptoEngine');

var serviceUrl = myConfig.sendMailServiceUrl;
var serviceUserName = myConfig.sendMailUserName;
var servicePassword = myConfig.sendMailPassword;
var fromEmail = myConfig.sendMailEmail;
var fromName = myConfig.sendMailName;

var logger = require('./logHelper').logger;

function getToken(callback) {
    var apiName = 'authtoken';
    var url = serviceUrl + apiName;

    request.post({ uri: url, form: { grant_type: 'password', username: serviceUserName, password: servicePassword } },
        function (err, httpResponse, body) {
            if (!err && httpResponse.statusCode == 200) {
                var result = JSON.parse(body);
                var token = result ? result.access_token : '';
                callback(err, token);
            }
            else {
                logger.error('getToken', err);
                callback(err);
            }
        }
    );
}

function sendSingleMail(toEmail, toName, subject, html, callback) {
    var apiName = 'EmailSend/Send';
    var url = serviceUrl + apiName;

    var form = {
        Subject: subject,
        HTML: html,
        FromEmail: fromEmail,
        ReplyToEmail: fromEmail,
        FromName: fromName,
        ToEmail: toEmail,
        ToName: toName,
        ShowPermissionReminder: false,
        ShowUnsubscribeButton: false,
        EmbedMailUrl: false,
        Facebook: '',
        Twitter: ''
    };

    getToken(function (err, token) {
        if (!err) {
            request.post({ uri: url, 'auth': { 'bearer': token }, form: form }, function (error, response, body) {
                if (!error && response.statusCode == 200) {
                    var result = JSON.parse(body);
                    
                    if (result.ErrorCode == 0) {
                        callback(null, { SendResult: true });
                    }
                    else {
                        logger.warn('sendSingleMail', result);
                        callback(null, { SendResult: false });
                    }
                }
                else {
                    logger.error('sendSingleMail', error);
                    callback(error, { SendResult: false });
                }
            });
        }
        else {
            callback(err);
        }
    });
}

module.exports = {
    sendForgetPasswordMail: function (email, callback) {
        var html = myConfig.forgetPasswordMailTemplate;
        var subject = '[Fiseo] Quên mật khẩu';
        
        db.user.getUserByEmail(email, ['FullName'], function (err, user) {
            if (!err) {
                
                if (user) {
                    var password = cryptoEngine.decryptText(user.Password);

                    html = html.replace(/\[APPNAME\]/g, myConfig.appName);
                    html = html.replace(/\[USERNAME\]/g, user.UserName);
                    html = html.replace(/\[PASSWORD\]/g, password);
                    html = html.replace(/\[FROMEMAIL\]/g, fromEmail);

                    sendSingleMail(email, email, subject, html, function (error, data) {
                        return callback(error, data);
                    });
                }
                else {
                    return callback('NotExists');
                }
            }
            else {
                return callback(err);
            }
        });
    }
};