﻿var db = require('../models/all');

var async = require('async');

var errorCode = require('../errorCode');
var resultHelper = require('../models/result');
var PROMOTYPE = {
    VIPTRIAL: 'VIPTRIAL'
};

var promoPaymentMethodType = 8;

module.exports = {
    PROMOTYPE: PROMOTYPE,
    applyPromotionToUser: function (promotion, userId, os, callback) {
        if (!promotion) {
            callback(resultHelper.notExists());
        }
        else {
            var methodId = promoPaymentMethodType;
            var paymentCode = db.shop.paymentLog.generatePaymentCode(userId);
            var price = 0;
            var discountCode = '';
            var discount = 0;
            var total = 0;
            var payType;
            var isDigital = true;
            var isVipPurchase;
            var itemId = 1;
            var vipMonth = 0;
            var needUpdatePayment = false;
            var description = '';
            switch (promotion.PromoType) {
                case PROMOTYPE.VIPTRIAL: {

                    try {
                        var promoData = JSON.parse(promotion.PromoContent);
                        itemId = promoData.ConfigId;
                        vipMonth = promoData.Month;
                        description = 'Dùng thử gói VIP ' + vipMonth + ' tháng';
                    }
                    catch (ex) {

                    }
                    finally {
                        payType = 0;
                        isVipPurchase = true;
                        needUpdatePayment = true;
                    }
                    break;
                }
            }
            var finalResult;
            var createAndUpdatePayment = (cb) => {
                if (needUpdatePayment) {
                    db.shop.paymentLog.createPaymentLog(userId, paymentCode, methodId, price, discountCode, discount, total, payType, itemId, isDigital, isVipPurchase, vipMonth, os, description,
                        (result) => {
                            if (result.ErrorCode == errorCode.success) {
                                db.shop.paymentLog.updatePaymentLogIsPay(userId, paymentCode, '', true, null, null, os, (resX) => {
                                    switch (promotion.PromoType) {
                                        case PROMOTYPE.VIPTRIAL: {
                                            //ko cần update vip vì khi update payment thành công đã gọi trong SP
                                            //db.shop.vipConfig.updateUserVip(userId, true, vipMonth, (rex) => {
                                            //    if (rex.ErrorCode == errorCode.success) {

                                            //}

                                            //});
                                            break;
                                        }
                                    }
                                    finalResult = resX;
                                    cb();
                                });
                            }
                            else {
                                finalResult = result;
                                cb();
                            }
                        });
                }
                else {
                    finalResult = resultHelper.success();
                    cb();
                }
            };

            var addUserPromotion = (cb) => {
                db.promotion.addUserPromotion(userId, promotion.PromotionId, true, false, (x) => {
                    cb();
                });
            };

            async.series([createAndUpdatePayment, addUserPromotion], (err) => {
                callback(finalResult);
            });
        }
    }
};