﻿var _ = require('lodash');
var jwt = require('jsonwebtoken');
var resultHelper = require('../models/result');
var logger = require('./logHelper').logger;
var config = require('../myConfig');
var errorCodes = require('../errorCode');

////hàm kiểm tra đăng nhập theo session
//exports.ensureLogin = function ensureLogin(req, res, next) {
//    logger.info('ensureLogin', req.url, req.isAuthenticated());


//    if (!req.isAuthenticated || !req.isAuthenticated()) {
//        return sendErrors(req, res, 'Không có quyền truy cập! Vui lòng đăng nhập trước!!!');
//    }
//    return next();
//};

exports.parseLoginResultToToken = function parseLoginResultToToken(result, os) {
    if (result.ErrorCode == errorCodes.success) {
        var user = result.Data;
        user = _.pick(user, ["UserId", "UserGUID", "UserName", "Phone", "Email", "IsVip", "VipLevelId", "VipLevel", "IsVipTrial", "VipStartDate", "VipEndDate", "RoleId", "CanPost", "Password"]);
        user.OS = os;
        var token = jwt.sign(user, config.jwtSecret, { expiresIn: config.tokenExpireTime });
        result.Data.Token = token;
        result.Data.OS = os;
    }
    return result;
};

//hàm kiểm tra đăng nhập theo session
exports.ensureLogin = function ensureLogin(req, res, next) {

    const header = req.headers.authorization;

    if (typeof header !== 'undefined') {
        const bearer = header.split(' ');
        const token = bearer[1];

        req.token = token;

        jwt.verify(req.token, config.jwtSecret, (err, authorizedData) => {
            if (err) {
                return sendErrors(req, res, 'Không có quyền truy cập! Vui lòng đăng nhập trước!!!');
            }
            else {
                req.user = authorizedData;
                if (!req.session) {
                    req.session = {};
                }
                if (!req.session.passport) {
                    req.session.passport = {};
                }
                req.session.passport.user = authorizedData;
                return next();
            }
        });
    } else {
        //If header is undefined return Forbidden (403)
        return sendErrors(req, res, 'Không có quyền truy cập! Vui lòng đăng nhập trước!!!');
    }
};

function sendErrors(req, res, msgs) {
    logger.warn('HTTP 401', msgs, req.headers);
    return res.status(401).json(resultHelper.notAuthorize(msgs));
}