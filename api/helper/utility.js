﻿var randomstring = require("randomstring"); 
var uuid = require('node-uuid');

var logger = require('./logHelper');

/**
 * Returns a random number between min (inclusive) and max (exclusive)
 */
exports.getRandomArbitrary = function (min, max) {
    return Math.random() * (max - min) + min;
};

/**
 * Returns a random integer between min (inclusive) and max (inclusive)
 * Using Math.round() will give you a non-uniform distribution!
 */
exports.getRandomInt = function (min, max) {
    return Math.floor(Math.random() * (max - min + 1)) + min;
};

exports.convertToSQLBitType = function (value) {
    return (value == true || value == 'true' || value == 'True' || value == 1) ? 1 : 0;
};

exports.randomStringCharSet = {
    alphanumeric: 'alphanumeric',
    alphabetic: 'alphabetic',
    numeric: 'numeric',
    hex: 'hex'
};

exports.generateRandomString = function (length, charset) {

    return randomstring.generate({
        length: length, charset: charset, capitalization: 'uppercase'
    });
};

exports.convertToBool = function (value) {
    if (value == undefined || value == null)
        value = false;
    if (value == 'false')
        value = false;
    if (value == 1)
        value = true;
    if (value == 0)
        value = false;
    return value;
};

exports.checkIsString = function (value) {
    if (typeof value === 'string') {
        return true;
    }
    return false;
};

exports.checkAndParseToJson = function (item) {
    if (typeof item === 'string') {
        try {
            item = JSON.parse(item);
        }
        catch (ex) {
            logger.debug('checkAndParseToJson', ex);
        }
        finally {
            return item;
        }
    }
    else {
        return item;
    }
};

exports.uuid = function () {
    return uuid.v4({});
};

exports.isEmail = function (email) {
    var pat = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/g;
    var rg = RegExp(pat);
    return rg.test(email);
};