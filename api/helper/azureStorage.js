﻿var mime = require('mime-types');
var azure = require('azure-storage');

var configs = require('../myConfig');

var azureAccountName = configs.storage.azureAccountName;
var azureAccessKey = configs.storage.azureAccessKey;

var containers = configs.storage.containers;

var utility = require('../helper/utility');
const format = require('string-format');
var _ = require('lodash');

format.extend(String.prototype, {});

var MEDIATYPE = require('../models/hub/media').MEDIATYPE;

var imageHelper = require('./imageHelper');
var errorLog = require('../models/imageResizeErrorLog');

var logger = require('../helper/logHelper').logger;

function createResizeImageQueue(queueName, itemId, link, callback) {

    var queueService = azure.createQueueService(azureAccountName, azureAccessKey);

    queueService.createQueueIfNotExists(queueName, function (error) {
        if (!error) {
            queueService.createMessage(queueName, link, function (err, result, response) {
                if (err) {
                    logger.error('createResizeImageQueue', err);
                    errorLog.createImageResizeErrorLog(itemId, queueName, link, err);
                }
            });
        }
        else {
            logger.error('createResizeImageQueue', error);
            errorLog.createImageResizeErrorLog(itemId, queueName, link, error);
        }
    });
}

module.exports = {
    containers: containers,
    fixStorageLink: function (link) {
        if (link) {
            if (link.startsWith('http')) {
            }
            else {
                link = configs.storage.azure.StorageUrl + link;
            }
        }
        else {
            link = '';
        }
        return link;
    },
    generateUploadSAS: function (containerName, fileName, callback) {
        var blobService = azure.createBlobService(azureAccountName, azureAccessKey);
        if (!containerName) {
            containerName = containers.resource;
        }
        var blobName = utility.uuid() + '/' + fileName;
        var startDate = new Date();
        var expiryDate = new Date(startDate);
        expiryDate.setDate(startDate.getDate() + 1);
        startDate.setDate(startDate.getDate() - 1);

        var sharedAccessPolicy = {
            AccessPolicy: {
                Permissions: azure.BlobUtilities.SharedAccessPermissions.READ + azure.BlobUtilities.SharedAccessPermissions.WRITE,
                Start: startDate,
                Expiry: expiryDate
            }
        };

        var token = blobService.generateSharedAccessSignature(containerName, blobName, sharedAccessPolicy, {
            contentType: mime.lookup(fileName)
        });
        var sas = blobService.getUrl(containerName, blobName, token);
        var result = {
            SAS: sas,
            Link: containerName + '/' + blobName
        };

        return callback(null, result);
    },

    generateUploadAvatarSAS: function (userGUID, itemGUID, extension, callback) {
        var blobService = azure.createBlobService(azureAccountName, azureAccessKey);

        extension = extension.toLowerCase();
        var containerName = containers.avatar;
        var blobName = userGUID.toLowerCase() + '/' + itemGUID + '/{0}.' + extension;
        blobName = blobName.toLowerCase();

        var startDate = new Date();
        var expiryDate = new Date(startDate);
        expiryDate.setDate(startDate.getDate() + 1);
        startDate.setDate(startDate.getDate() - 1);

        var sharedAccessPolicy = {
            AccessPolicy: {
                Permissions: azure.BlobUtilities.SharedAccessPermissions.READ + azure.BlobUtilities.SharedAccessPermissions.WRITE,
                Start: startDate,
                Expiry: expiryDate
            }
        };

        var tokenOriginal = blobService.generateSharedAccessSignature(containerName, blobName.format('o'), sharedAccessPolicy,
            {
                contentType: mime.lookup(extension)
            });
        var sasUrlOriginal = blobService.getUrl(containerName, blobName.format('o'), tokenOriginal);

        var tokenMedium = blobService.generateSharedAccessSignature(containerName, blobName.format('m'), sharedAccessPolicy,
            {
                contentType: mime.lookup(extension)
            });
        var sasUrlMedium = blobService.getUrl(containerName, blobName.format('m'), tokenMedium);

        var tokenSmall = blobService.generateSharedAccessSignature(containerName, blobName.format('s'), sharedAccessPolicy,
            {
                contentType: mime.lookup(extension)
            });
        var sasUrlSmall = blobService.getUrl(containerName, blobName.format('s'), tokenSmall);

        var result = {
            Original: {
                SAS: sasUrlOriginal,
                Link: containerName + '/' + blobName.format('o')
            },
            Medium: {
                SAS: sasUrlMedium,
                Link: containerName + '/' + blobName.format('m')
            },
            Small: {
                SAS: sasUrlSmall,
                Link: containerName + '/' + blobName.format('s')
            }
        };

        return callback(null, result);
    },

    generateUploadMediaSAS: function (mediaGUID, mediaType, coverExtension, callback) {
        var blobService = azure.createBlobService(azureAccountName, azureAccessKey);

        coverExtension = coverExtension.toLowerCase();

        var startDate = new Date();
        var expiryDate = new Date(startDate);
        expiryDate.setDate(startDate.getDate() + 1);
        startDate.setDate(startDate.getDate() - 1);

        var sharedAccessPolicy = {
            AccessPolicy: {
                Permissions: azure.BlobUtilities.SharedAccessPermissions.READ + azure.BlobUtilities.SharedAccessPermissions.WRITE,
                Start: startDate,
                Expiry: expiryDate
            }
        };

        var result = {};
        var containerName = containers.media;

        var blobName = mediaGUID.toLowerCase() + '/{0}.{1}';
        blobName = blobName.toLowerCase();

        var tokenCover = blobService.generateSharedAccessSignature(containerName, blobName.format('cover', coverExtension), sharedAccessPolicy,
            {
                contentType: mime.lookup(coverExtension)
            });
        var sasUrlCover = blobService.getUrl(containerName, blobName.format('cover', coverExtension), tokenCover);

        var tokenThumb = blobService.generateSharedAccessSignature(containerName, blobName.format('thumb', coverExtension), sharedAccessPolicy,
            {
                contentType: mime.lookup(coverExtension)
            });
        var sasUrlThumb = blobService.getUrl(containerName, blobName.format('thumb', coverExtension), tokenThumb);
        var tokenLink, sasUrlLink;
        switch (mediaType) {
            case MEDIATYPE.PHOTO:
            case MEDIATYPE.AUDIO:
            case MEDIATYPE.VIDEO: {
                result = {
                    Cover: {
                        SAS: sasUrlCover,
                        Link: containerName + '/' + blobName.format('cover', coverExtension)
                    },
                    Thumb: {
                        SAS: sasUrlThumb,
                        Link: containerName + '/' + blobName.format('thumb', coverExtension)
                    },
                    Link: {
                        SAS: '',
                        Link: ''
                    }
                };
                break;
            }
            case MEDIATYPE.PDF: {
                tokenLink = blobService.generateSharedAccessSignature(containerName, blobName.format('file', 'pdf'), sharedAccessPolicy,
                    {
                        contentType: mime.lookup('pdf')
                    });
                sasUrlLink = blobService.getUrl(containerName, blobName.format('file', 'pdf'), tokenLink);

                result = {
                    Cover: {
                        SAS: sasUrlCover,
                        Link: containerName + '/' + blobName.format('cover', coverExtension)
                    },
                    Thumb: {
                        SAS: sasUrlThumb,
                        Link: containerName + '/' + blobName.format('thumb', coverExtension)
                    },
                    Link: {
                        SAS: sasUrlLink,
                        Link: containerName + '/' + blobName.format('file', 'pdf')
                    }
                };
                break;
            }
            case MEDIATYPE.EPUB: {
                tokenLink = blobService.generateSharedAccessSignature(containerName, blobName.format('file', 'epub'), sharedAccessPolicy,
                    {
                        contentType: mime.lookup('epub')
                    });
                sasUrlLink = blobService.getUrl(containerName, blobName.format('file', 'epub'), tokenLink);

                result = {
                    Cover: {
                        SAS: sasUrlCover,
                        Link: containerName + '/' + blobName.format('cover', coverExtension)
                    },
                    Thumb: {
                        SAS: sasUrlThumb,
                        Link: containerName + '/' + blobName.format('thumb', coverExtension)
                    },
                    Link: {
                        SAS: sasUrlLink,
                        Link: containerName + '/' + blobName.format('file', 'epub')
                    }
                };
                break;
            }
        }
        return callback(null, result);
    },

    generateUploadMediaItemSAS: function (mediaGUID, itemGUID, extension, coverExtension, callback) {
        var blobService = azure.createBlobService(azureAccountName, azureAccessKey);

        extension = extension.toLowerCase();
        coverExtension = coverExtension.toLowerCase();

        var startDate = new Date();
        var expiryDate = new Date(startDate);
        expiryDate.setDate(startDate.getDate() + 1);
        startDate.setDate(startDate.getDate() - 1);

        var sharedAccessPolicy = {
            AccessPolicy: {
                Permissions: azure.BlobUtilities.SharedAccessPermissions.READ + azure.BlobUtilities.SharedAccessPermissions.WRITE,
                Start: startDate,
                Expiry: expiryDate
            }
        };

        var result = {};
        var containerName = containers.media;

        var blobName = mediaGUID.toLowerCase() + '/' + itemGUID.toLowerCase() + '/{0}.{1}';
        blobName = blobName.toLowerCase();

        var tokenCover = blobService.generateSharedAccessSignature(containerName, blobName.format('cover', coverExtension), sharedAccessPolicy,
            {
                contentType: mime.lookup(coverExtension)
            });
        var sasUrlCover = blobService.getUrl(containerName, blobName.format('cover', coverExtension), tokenCover);

        var tokenThumb = blobService.generateSharedAccessSignature(containerName, blobName.format('thumb', coverExtension), sharedAccessPolicy,
            {
                contentType: mime.lookup(coverExtension)
            });
        var sasUrlThumb = blobService.getUrl(containerName, blobName.format('thumb', coverExtension), tokenThumb);

        var tokenFile = blobService.generateSharedAccessSignature(containerName, blobName.format('file', extension), sharedAccessPolicy,
            {
                contentType: mime.lookup(extension)
            });
        var sasUrlFile = blobService.getUrl(containerName, blobName.format('file', extension), tokenFile);

        var tokenFileHD = blobService.generateSharedAccessSignature(containerName, blobName.format('fileHD', extension), sharedAccessPolicy,
            {
                contentType: mime.lookup(extension)
            });
        var sasUrlFileHD = blobService.getUrl(containerName, blobName.format('fileHD', extension), tokenFileHD);

        result = {
            Cover: {
                SAS: sasUrlCover,
                Link: containerName + '/' + blobName.format('cover', coverExtension)
            },
            Thumb: {
                SAS: sasUrlThumb,
                Link: containerName + '/' + blobName.format('thumb', coverExtension)
            },
            File: {
                SAS: sasUrlFile,
                Link: containerName + '/' + blobName.format('file', extension)
            },
            FileHD: {
                SAS: sasUrlFileHD,
                Link: containerName + '/' + blobName.format('fileHD', extension)
            }
        };
        callback(null, result);
    },

    generateUploadImageSAS: function (userGUID, itemGUID, extensions, callback) {
        var blobService = azure.createBlobService(azureAccountName, azureAccessKey);

        var startDate = new Date();
        var expiryDate = new Date(startDate);
        expiryDate.setDate(startDate.getDate() + 1);
        startDate.setDate(startDate.getDate() - 1);

        var sharedAccessPolicy = {
            AccessPolicy: {
                Permissions: azure.BlobUtilities.SharedAccessPermissions.READ + azure.BlobUtilities.SharedAccessPermissions.WRITE,
                Start: startDate,
                Expiry: expiryDate
            }
        };

        var result = [];

        _.each(extensions, (extension, index) => {

            extension = extension.toLowerCase();
            var containerName = containers.post;

            var altExtension = extension;
            if (imageHelper.checkExtensionIsVideo(extension)) {
                altExtension = 'jpg';
            }

            var blobName = userGUID.toLowerCase() + '/' + itemGUID + '/' + index + '/{0}.{1}';
            blobName = blobName.toLowerCase();


            var tokenLarge = blobService.generateSharedAccessSignature(containerName, blobName.format('l', extension), sharedAccessPolicy,
                {
                    contentType: mime.lookup(extension)
                });
            var sasUrlLarge = blobService.getUrl(containerName, blobName.format('l', extension), tokenLarge);

            var tokenMedium = blobService.generateSharedAccessSignature(containerName, blobName.format('m', altExtension), sharedAccessPolicy,
                {
                    contentType: mime.lookup(extension)
                });
            var sasUrlMedium = blobService.getUrl(containerName, blobName.format('m', altExtension), tokenMedium);

            var tokenSmall = blobService.generateSharedAccessSignature(containerName, blobName.format('s', altExtension), sharedAccessPolicy,
                {
                    contentType: mime.lookup(extension)
                });
            var sasUrlSmall = blobService.getUrl(containerName, blobName.format('s', altExtension), tokenSmall);

            result.push({
                Large: {
                    SAS: sasUrlLarge,
                    Link: containerName + '/' + blobName.format('l', extension)
                },
                Medium: {
                    SAS: sasUrlMedium,
                    Link: containerName + '/' + blobName.format('m', altExtension)
                },
                Small: {
                    SAS: sasUrlSmall,
                    Link: containerName + '/' + blobName.format('s', altExtension)
                }
            });
        });

        return callback(null, result);
    },

    deleteMediaBlobs: function (mediaGUID, callback) {
        var blobService = azure.createBlobService(azureAccountName, azureAccessKey);

        var containerName = containers.media;
        var prefix = mediaGUID.toLowerCase();

        blobService.listBlobsSegmentedWithPrefix(containerName, prefix, null, function (error, result, response) {
            if (!error) {
                result.entries.forEach(function (item) {

                    blobService.deleteBlob(containerName, item.name, function (error, response) {
                        if (!error) {
                            // Blob has been deleted
                        }
                    });
                });
                callback(null, { Success: true });
            }
            else {

                callback(error);
            }
        });
    },

    deleteMediaItemBlobs: function (mediaGUID, itemGUID, callback) {
        var blobService = azure.createBlobService(azureAccountName, azureAccessKey);

        var containerName = containers.media;
        var prefix = mediaGUID.toLowerCase() + '/' + itemGUID.toLowerCase();

        blobService.listBlobsSegmentedWithPrefix(containerName, prefix, null, function (error, result, response) {
            if (!error) {
                result.entries.forEach(function (item) {

                    blobService.deleteBlob(containerName, item.name, function (error, response) {
                        if (!error) {
                            // Blob has been deleted
                        }
                    });
                });
                callback(null, { Success: true });
            }
            else {
                logger.error('deleteMediaItemBlobs', error);
                callback(error);
            }
        });
    },



    //xóa tất cả hình trong ImageItem
    deleteImageBlobs: function (userGUID, imageItemGUID, callback) {
        var blobService = azure.createBlobService(azureAccountName, azureAccessKey);

        var containerName = containers.post;
        var prefix = userGUID.toLowerCase() + '/' + imageItemGUID.toLowerCase();

        blobService.listBlobsSegmentedWithPrefix(containerName, prefix, null, function (error, result, response) {
            if (!error) {
                result.entries.forEach(function (item) {

                    blobService.deleteBlob(containerName, item.name, function (error, response) {
                        if (!error) {
                            // Blob has been deleted
                        }
                    });
                });
                callback(null, { Success: true });
            }
            else {
                logger.error('deleteImageBlobs', error);
                callback(error);
            }

        });
    },

    createLargeSizeImageQueue: function (imageItemId, link, callback) {
        var queueName = 'resizetolarge';
        createResizeImageQueue(queueName, imageItemId, link, callback);
    },

    createMediumSizeImageQueue: function (imageItemId, link, callback) {
        var queueName = 'resizetomedium';
        createResizeImageQueue(queueName, imageItemId, link, callback);
    },

    createSmallSizeImageQueue: function (imageItemId, link, callback) {
        var queueName = 'resizetosmall';
        createResizeImageQueue(queueName, imageItemId, link, callback);
    },

    createAvatarMediumSizeImageQueue: function (userId, link, callback) {
        var queueName = 'resizetoavatarmedium';
        createResizeImageQueue(queueName, userId, link, callback);
    },

    createAvatarSmallSizeImageQueue: function (userId, link, callback) {
        var queueName = 'resizetoavatarsmall';
        createResizeImageQueue(queueName, userId, link, callback);
    },

    createChatThumbnailSizeImageQueue: function (chatLogId, link, callback) {
        var queueName = 'resizetochatmedium';
        createResizeImageQueue(queueName, chatLogId, link, callback);
    }
};
