﻿module.exports = {

    //tạo ra đường dẫn file hình gốc
    genearateImageOriginalUrl: function (containerName, userGUID, itemGUID, imageExtension, callback) {

        if (!imageExtension.startsWith('.')) {
            imageExtension = '.' + imageExtension;
        }

        var result = containerName + '/' + userGUID.toLowerCase() + '/' + itemGUID.toLowerCase() + '/' + 'o' + imageExtension.toLowerCase();
        if (callback)
            callback(result);
        else {
            return result;
        }
    },

    genearateImageLargeUrl: function (containerName, userGUID, itemGUID, imageExtension, callback) {

        if (!imageExtension.startsWith('.')) {
            imageExtension = '.' + imageExtension;
        }

        var result = containerName + '/' + userGUID.toLowerCase() + '/' + itemGUID.toLowerCase() + '/' + 'l' + imageExtension.toLowerCase();
        if (callback) {
            callback(result);
        }
        else {
            return result;
        }
    },

    genearateImageMediumUrl: function (containerName, userGUID, itemGUID, imageExtension, callback) {

        if (!imageExtension.startsWith('.')) {
            imageExtension = '.' + imageExtension;
        }

        var result = containerName + '/' + userGUID.toLowerCase() + '/' + itemGUID.toLowerCase() + '/' + 'm' + imageExtension.toLowerCase();
        if (callback) {
            callback(result);
        }
        else {
            return result;
        }
    },

    genearateImageSmallUrl: function (containerName, userGUID, itemGUID, imageExtension, callback) {

        if (!imageExtension.startsWith('.')) {
            imageExtension = '.' + imageExtension;
        }

        var result = containerName + '/' + userGUID.toLowerCase() + '/' + itemGUID.toLowerCase() + '/' + 's' + imageExtension.toLowerCase();
        if (callback) {
            callback(result);
        }
        else {
            return result;
        }
    },

    //tính ratio hình
    getImageHeightFromWidthWithRatio: function (imageWidth, ratioWidth, ratioHeight) {
        var ratio = parseFloat(ratioWidth) / parseFloat(ratioHeight);
        return Math.round(parseFloat(imageWidth) / ratio);
    },

    //kiểm tra 1 link có phải là hình không
    checkLinkUrlIsImage: function (url) {
        return (url.match(/\.(jpeg|jpg|gif|png)(\?([A-Za-z_0-9&\-\+=]+)?)?$/i) != null);//kèm luôn parameter trong link image
    },

    //lấy extension của file
    getFileExtension: function (url) {
        return url.substring(url.lastIndexOf('.'));
    },

    checkExtensionIsVideo: function (ext) {
        return ext.match(/(mp4|mov|avi)$/i) != null;
    }
};