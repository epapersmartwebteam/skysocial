﻿var { parsePhoneNumberFromString } = require('libphonenumber-js');

exports.isMobile = function (phone) {
    var phoneNumber = parsePhoneNumberFromString(phone, 'VN');
    return phoneNumber && phoneNumber.isValid();
};

exports.patchValidPhoneNumber = function (phone) {
    var phoneNumber = parsePhoneNumberFromString(phone, 'VN');
    
    if (phoneNumber && phoneNumber.isValid()) {
        return phoneNumber.number;
    }
    else {
        phoneNumber = parsePhoneNumberFromString('+' + phone, 'VN');
    
        if (phoneNumber && phoneNumber.isValid() && phoneNumber.country == 'VN') {
            return phoneNumber.number;
        }
        else {
            return phone;
        }
    }
};

