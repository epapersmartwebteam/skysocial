﻿var moment = require('moment');

exports.getUnixTimeStamp = function () {
    return parseInt(moment.utc().format('X'));
};

exports.getHourIntervalStart = function (interval) {
    var start = moment.utc();
    start = start.startOf('hour');
    var hour = start.hour();
    hour = parseInt(hour / interval) * interval;
    start.hour(hour);
    return parseInt(start.format('X'));
};

exports.getHourIntervalEnd = function (interval) {
    var start = moment.utc();
    start = start.startOf('hour');
    var hour = start.hour();
    hour = parseInt(hour / interval) * interval;
    start.hour(hour);
    var end = moment(start).add(interval, 'hour');
    return parseInt(end.format('X'));
};