var mime = require('mime-types');
var AWS = require('aws-sdk');

var config = require('../myConfig');

var containers = config.storage.containers;


var s3Config = config.storage.cmcS3;

var s3 = new AWS.S3({
    accessKeyId: s3Config.accessKeyId,
    secretAccessKey: s3Config.secretAccessKey,
    endpoint: s3Config.endpoint,
    region: s3Config.region
});


var utility = require('../helper/utility');
const format = require('string-format');
var _ = require('lodash');

format.extend(String.prototype, {});

var MEDIATYPE = require('../models/hub/media').MEDIATYPE;

var imageHelper = require('./imageHelper');
var errorLog = require('../models/imageResizeErrorLog');

var logger = require('../helper/logHelper').logger;

module.exports = {
    containers: containers,
    fixStorageLink: function (link) {
        if (link) {
            if (link.startsWith('http')) {
            }
            else {
                link = s3Config.StorageUrl + link;
            }
        }
        else {
            link = '';
        }
        return link;
    },
    generateUploadSAS: function (containerName, fileName, callback) {

        if (!containerName) {
            containerName = containers.resource;
        }
        var blobName = containerName + '/' + utility.uuid() + '/' + fileName;

        var sas = s3.getSignedUrl("putObject",
            {
                Bucket: s3Config.bucket,
                ContentType: mime.lookup(fileName),
                ACL: 'public-read',
                Key: blobName,
                Expires: s3Config.expireSecond
            });

        var result = {
            SAS: sas,
            Link: blobName
        };

        return callback(null, result);
    },

    generateUploadAvatarSAS: function (userGUID, itemGUID, extension, callback) {

        extension = extension.toLowerCase();
        var containerName = containers.avatar;
        var blobName = containerName + '/' + userGUID.toLowerCase() + '/' + itemGUID + '/{0}.' + extension;
        blobName = blobName.toLowerCase();

        var sasUrlOriginal = s3.getSignedUrl("putObject",
            {
                Bucket: s3Config.bucket,
                ContentType: mime.lookup(extension),
                ACL: 'public-read',
                Key: blobName.format('o'),
                Expires: s3Config.expireSecond
            });

        var sasUrlMedium = s3.getSignedUrl("putObject",
            {
                Bucket: s3Config.bucket,
                ContentType: mime.lookup(extension),
                ACL: 'public-read',
                Key: blobName.format('m'),
                Expires: s3Config.expireSecond
            });

        var sasUrlSmall = s3.getSignedUrl("putObject",
            {
                Bucket: s3Config.bucket,
                ContentType: mime.lookup(extension),
                ACL: 'public-read',
                Key: blobName.format('s'),
                Expires: s3Config.expireSecond
            });

        var result = {
            Original: {
                SAS: sasUrlOriginal,
                Link: blobName.format('o')
            },
            Medium: {
                SAS: sasUrlMedium,
                Link: blobName.format('m')
            },
            Small: {
                SAS: sasUrlSmall,
                Link: blobName.format('s')
            }
        };

        return callback(null, result);
    },

    generateUploadMediaSAS: function (mediaGUID, mediaType, coverExtension, callback) {

        coverExtension = coverExtension.toLowerCase();

        var result = {};
        var containerName = containers.media;

        var blobName = containerName + '/' + mediaGUID.toLowerCase() + '/{0}.{1}';
        blobName = blobName.toLowerCase();

        var sasUrlCover = s3.getSignedUrl("putObject",
            {
                Bucket: s3Config.bucket,
                ContentType: mime.lookup(coverExtension),
                ACL: 'public-read',
                Key: blobName.format('cover', coverExtension),
                Expires: s3Config.expireSecond
            });

        var sasUrlThumb = s3.getSignedUrl("putObject",
            {
                Bucket: s3Config.bucket,
                ContentType: mime.lookup(coverExtension),
                ACL: 'public-read',
                Key: blobName.format('thumb', coverExtension),
                Expires: s3Config.expireSecond
            });

        var tokenLink, sasUrlLink;

        switch (mediaType) {
            case MEDIATYPE.PHOTO:
            case MEDIATYPE.AUDIO:
            case MEDIATYPE.VIDEO: {
                result = {
                    Cover: {
                        SAS: sasUrlCover,
                        Link: blobName.format('cover', coverExtension)
                    },
                    Thumb: {
                        SAS: sasUrlThumb,
                        Link: blobName.format('thumb', coverExtension)
                    },
                    Link: {
                        SAS: '',
                        Link: ''
                    }
                };
                break;
            }
            case MEDIATYPE.PDF: {

                sasUrlLink = s3.getSignedUrl("putObject",
                    {
                        Bucket: s3Config.bucket,
                        ContentType: mime.lookup('pdf'),
                        ACL: 'public-read',
                        Key: blobName.format('file', 'pdf'),
                        Expires: s3Config.expireSecond
                    });

                result = {
                    Cover: {
                        SAS: sasUrlCover,
                        Link: blobName.format('cover', coverExtension)
                    },
                    Thumb: {
                        SAS: sasUrlThumb,
                        Link: blobName.format('thumb', coverExtension)
                    },
                    Link: {
                        SAS: sasUrlLink,
                        Link: blobName.format('file', 'pdf')
                    }
                };
                break;
            }
            case MEDIATYPE.EPUB: {

                sasUrlLink = s3.getSignedUrl("putObject",
                    {
                        Bucket: s3Config.bucket,
                        ContentType: mime.lookup('epub'),
                        ACL: 'public-read',
                        Key: blobName.format('file', 'epub'),
                        Expires: s3Config.expireSecond
                    });

                result = {
                    Cover: {
                        SAS: sasUrlCover,
                        Link: blobName.format('cover', coverExtension)
                    },
                    Thumb: {
                        SAS: sasUrlThumb,
                        Link: blobName.format('thumb', coverExtension)
                    },
                    Link: {
                        SAS: sasUrlLink,
                        Link: blobName.format('file', 'epub')
                    }
                };
                break;
            }
        }
        return callback(null, result);
    },

    generateUploadMediaItemSAS: function (mediaGUID, itemGUID, extension, coverExtension, callback) {

        extension = extension.toLowerCase();
        coverExtension = coverExtension.toLowerCase();

        var result = {};
        var containerName = containers.media;

        var blobName = containerName + '/' + mediaGUID.toLowerCase() + '/' + itemGUID.toLowerCase() + '/{0}.{1}';
        blobName = blobName.toLowerCase();

        var sasUrlCover = s3.getSignedUrl("putObject",
            {
                Bucket: s3Config.bucket,
                ContentType: mime.lookup(coverExtension),
                ACL: 'public-read',
                Key: blobName.format('cover', coverExtension),
                Expires: s3Config.expireSecond
            });

        var sasUrlThumb = s3.getSignedUrl("putObject",
            {
                Bucket: s3Config.bucket,
                ContentType: mime.lookup(coverExtension),
                ACL: 'public-read',
                Key: blobName.format('thumb', coverExtension),
                Expires: s3Config.expireSecond
            });

        var sasUrlFile = s3.getSignedUrl("putObject",
            {
                Bucket: s3Config.bucket,
                ContentType: mime.lookup(extension),
                ACL: 'public-read',
                Key: blobName.format('file', extension),
                Expires: s3Config.expireSecond
            });

        var sasUrlFileHD = s3.getSignedUrl("putObject",
            {
                Bucket: s3Config.bucket,
                ContentType: mime.lookup(extension),
                ACL: 'public-read',
                Key: blobName.format('fileHD', extension),
                Expires: s3Config.expireSecond
            });

        result = {
            Cover: {
                SAS: sasUrlCover,
                Link: blobName.format('cover', coverExtension)
            },
            Thumb: {
                SAS: sasUrlThumb,
                Link: blobName.format('thumb', coverExtension)
            },
            File: {
                SAS: sasUrlFile,
                Link: blobName.format('file', extension)
            },
            FileHD: {
                SAS: sasUrlFileHD,
                Link: blobName.format('fileHD', extension)
            }
        };
        callback(null, result);
    },

    generateUploadImageSAS: function (userGUID, itemGUID, extensions, callback) {

        var result = [];
        var containerName = containers.post;

        _.each(extensions, (extension, index) => {

            extension = extension.toLowerCase();


            var altExtension = extension;
            if (imageHelper.checkExtensionIsVideo(extension)) {
                altExtension = 'jpg';
            }

            var blobName = containerName + '/' + userGUID.toLowerCase() + '/' + itemGUID + '/' + index + '/{0}.{1}';
            blobName = blobName.toLowerCase();

            var sasUrlLarge = s3.getSignedUrl("putObject",
                {
                    Bucket: s3Config.bucket,
                    ContentType: mime.lookup(extension),
                    ACL: 'public-read',
                    Key: blobName.format('l', extension),
                    Expires: s3Config.expireSecond
                });

            var sasUrlMedium = s3.getSignedUrl("putObject",
                {
                    Bucket: s3Config.bucket,
                    ContentType: mime.lookup(altExtension),
                    ACL: 'public-read',
                    Key: blobName.format('m', altExtension),
                    Expires: s3Config.expireSecond
                });

            var sasUrlSmall = s3.getSignedUrl("putObject",
                {
                    Bucket: s3Config.bucket,
                    ContentType: mime.lookup(altExtension),
                    ACL: 'public-read',
                    Key: blobName.format('s', altExtension),
                    Expires: s3Config.expireSecond
                });

            result.push({
                Large: {
                    SAS: sasUrlLarge,
                    Link: blobName.format('l', extension)
                },
                Medium: {
                    SAS: sasUrlMedium,
                    Link: blobName.format('m', altExtension)
                },
                Small: {
                    SAS: sasUrlSmall,
                    Link: blobName.format('s', altExtension)
                }
            });
        });

        return callback(null, result);
    },

    deleteMediaBlobs: function (mediaGUID, callback) {

        var containerName = containers.media;
        var prefix = containerName + '/' + mediaGUID.toLowerCase();

        s3.listObjectsV2({
            Bucket: s3Config.bucket,
            Prefix: prefix
        }, (err, data) => {
            if (err) {
                return callback(err);
            }
            if (data.Contents.length == 0) {
                return callback(null, { Success: true });
            }

            var params = { Bucket: s3Config.bucket };
            params.Delete = { Objects: [] };

            data.Contents.forEach(function (content) {
                params.Delete.Objects.push({ Key: content.Key });
            });

            s3.deleteObjects(params, function (err, data) {
                if (err) {
                    return callback(err);
                }
                else {
                    callback(null, { Success: true });
                }
            });
        });
    },

    deleteMediaItemBlobs: function (mediaGUID, itemGUID, callback) {

        var containerName = containers.media;
        var prefix = containerName + '/' + mediaGUID.toLowerCase() + '/' + itemGUID.toLowerCase();

        s3.listObjectsV2({
            Bucket: s3Config.bucket,
            Prefix: prefix
        }, (err, data) => {
            if (err) {
                return callback(err);
            }
            if (data.Contents.length == 0) {
                return callback(null, { Success: true });
            }

            var params = { Bucket: s3Config.bucket };
            params.Delete = { Objects: [] };

            data.Contents.forEach(function (content) {
                params.Delete.Objects.push({ Key: content.Key });
            });

            s3.deleteObjects(params, function (err, data) {
                if (err) {
                    return callback(err);
                }
                else {
                    callback(null, { Success: true });
                }
            });
        });
    },



    //x�a t?t c? h�nh trong ImageItem
    deleteImageBlobs: function (userGUID, imageItemGUID, callback) {
        var containerName = containers.post;
        var prefix = containerName + '/' + userGUID.toLowerCase() + '/' + imageItemGUID.toLowerCase();

        s3.listObjectsV2({
            Bucket: s3Config.bucket,
            Prefix: prefix
        }, (err, data) => {
            if (err) {
                return callback(err);
            }
            if (data.Contents.length == 0) {
                return callback(null, { Success: true });
            }

            var params = { Bucket: s3Config.bucket };
            params.Delete = { Objects: [] };

            data.Contents.forEach(function (content) {
                params.Delete.Objects.push({ Key: content.Key });
            });

            s3.deleteObjects(params, function (err, data) {
                if (err) {
                    return callback(err);
                }
                else {
                    callback(null, { Success: true });
                }
            });
        });
    },


};
