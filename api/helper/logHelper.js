﻿const log4js = require('log4js');
var myConfig = require('../myConfig');

log4js.configure({
    appenders: {
        //toFile: { //lưu file roll theo giờ và max là 10MB, 
        //    type: 'file',
        //    filename: 'logs/app/logs.log',
        //    maxLogSize: 10485760,
        //    pattern: '.yyyy-MM-dd-hh',
        //    keepFileExt: true,
        //    daysToKeep: 2
        //},
        toScreen: {
            type: 'stdout'
        },
        //paymentLog: {
        //    type: 'file',
        //    filename: 'logs/payment/payment.log',
        //    pattern: '.yyyy-MM-dd-hh',
        //    keepFileExt: true,
        //    daysToKeep: 0
        //}
    },
    categories: {
        default: {
            appenders: ['toScreen'],
            level: myConfig.logConfigs.level,
            enableCallStack: myConfig.logConfigs.enableCallStack
        },
        paymentInfo: {
            appenders: ['toScreen'],
            level: "INFO",
            enableCallStack: true
        },
        payment: {
            appenders: ['toScreen'],
            level: "Error",
            enableCallStack: true
        }
    },
    pm2: true
});

//let paused = false;
//process.on("log4js:pause", (value) => paused = value);

const logger = log4js.getLogger();
const loggerPayment = log4js.getLogger("payment");
const loggerPaymentInfo = log4js.getLogger("paymentInfo");

//while (!paused) {
    //logger.info("I'm logging, but I will stop once we start buffering");
    //logger.error("I'm logging, but I will stop once we start buffering");
//}

module.exports = {
    logger: logger,
    loggerPayment: loggerPayment,
    loggerPaymentInfo: loggerPaymentInfo
};