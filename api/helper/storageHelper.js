var configs = require('../myConfig');

var azureStorage = require('../helper/azureStorage');
var awsS3Storage = require('../helper/awsS3Storage');
var storageHelper;

switch (configs.storage.provider) {
    case 'cmcS3':
        {
            storageHelper = awsS3Storage;
            break;
        }
    case 'azure':
    default:
        {
            storageHelper = azureStorage;
            break;
        }
}

function fixStorageLink(link, provider) {
    
    if (link) {
        if (link.startsWith('http')) {
        }
        else {
            switch (provider) {
                case 'cmcS3':
                    {
                        link = awsS3Storage.fixStorageLink(link);
                        break;
                    }
                case 'azure':
                default:
                    {
                        link = azureStorage.fixStorageLink(link);
                        break;
                    }
            }
        }
    }
    else {
        link = '';
    }
    return link;
}

function fixAvatarLink(link, provider) {
    link = fixStorageLink(link, provider);

    if (link == '') {
        link = storageHelper.storageUrl + storageHelper.noAvatar;
    }
    return link;
}


exports.fixAvatarLink = fixAvatarLink;
exports.fixStorageLink = fixStorageLink;
exports.provider = configs.storage.provider;
exports.noAvatar = storageHelper.noAvatar;
exports.storageUrl = storageHelper.storageUrl;
exports.containers = storageHelper.containers;
exports.generateUploadSAS = storageHelper.generateUploadSAS;
exports.generateUploadAvatarSAS = storageHelper.generateUploadAvatarSAS;
exports.generateUploadMediaSAS = storageHelper.generateUploadMediaSAS;
exports.generateUploadMediaItemSAS = storageHelper.generateUploadMediaItemSAS;
exports.generateUploadImageSAS = storageHelper.generateUploadImageSAS;
exports.deleteMediaBlobs = storageHelper.deleteMediaBlobs;
exports.deleteMediaItemBlobs = storageHelper.deleteMediaItemBlobs;

