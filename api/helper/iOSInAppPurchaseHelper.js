﻿var request = require('request');
var myConfig = require('../myConfig');
var settings = myConfig.iOSInAppPurchaseSettings;
var resultHelper = require('../models/result');

var logger = require('./logHelper').logger;
var loggerPayment = require('./logHelper').loggerPayment;
var loggerPaymentInfo = require('./logHelper').loggerPaymentInfo;
function parseReceiptErrorCode(errorCode) {
    var msg = '';
    switch (errorCode) {
        case 21000: {
            msg = 'The App Store could not read the JSON object you provided.';
            break;
        }
        case 21002: {
            msg = 'The data in the receipt-data property was malformed or missing.';
            break;
        }
        case 21003: {
            msg = 'The receipt could not be authenticated.';
            break;
        }
        case 21004: {
            msg = 'The shared secret you provided does not match the shared secret on file for your account.';
            break;
        }
        case 21005: {
            msg = 'The receipt server is not currently available.';
            break;
        }
        case 21006: {
            msg = 'This receipt is valid but the subscription has expired. When this status code is returned to your server, the receipt data is also decoded and returned as part of the response.';
            msg += 'Only returned for iOS 6 style transaction receipts for auto - renewable subscriptions.';
            break;
        }
        case 21007: {
            msg = 'This receipt is from the test environment, but it was sent to the production environment for verification. Send it to the test environment instead.';
            break;
        }
        case 21008: {
            msg = 'This receipt is from the production environment, but it was sent to the test environment for verification. Send it to the production environment instead.';
            break;
        }
        case 21010: {
            msg = 'This receipt could not be authorized. Treat this the same as if a purchase was never made.';
            break;
        }
        //21100-21199
        default: {
            msg = 'Internal data access error.';
            break;
        }
    }
    return msg;
}
function verifyReceipt(receiptData, callback) {
    request({
        uri: settings.appStoreURL[settings.env] + settings.verifyReceiptFunction,
        method: 'POST',
        json: true,
        body: {
            "receipt-data": receiptData,
            "password": settings.sharedSecret
        }
    }, function (error, response, body) {
        if (error) {
            loggerPayment.error('verifyReceipt iOS In-app', error);
            callback(resultHelper.dataError(error));
        }
        else {
            var payload = body;
            if (payload.status == 0) {
                //thành công
                loggerPaymentInfo.info('verifyReceipt iOS In-app success', payload);
                callback(resultHelper.success(payload.receipt));
            }
            else {
                var errorMessage = parseReceiptErrorCode(payload.status);
                loggerPaymentInfo.error('verifyReceipt iOS In-app error', errorMessage, payload);
                callback(resultHelper.dataError(errorMessage));
            }
        }
    });
}

exports.verifyReceipt = verifyReceipt;