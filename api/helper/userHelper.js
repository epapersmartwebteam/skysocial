﻿exports.validateUserName = function (username) {
    var regex = RegExp(/^(?=.{5,30}$)[a-z0-9](?!.*[._]{2})(?:[\w]*|[a-z\d\._]*)[a-z0-9]$/);
    return regex.test(username);
};