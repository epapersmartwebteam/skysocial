var _ = require('lodash');
var firebase = require("firebase");
var config = require('../myConfig').firebase;

var passwordFirebase = config.password;//'7MUZL@84R!y$q%?J';
var serviceAccount = require('../firebase-adminsdk.json');
var admin = require("firebase-admin");

var logger = require('./logHelper').logger;

function initApp() {
    return admin.initializeApp({
        credential: admin.credential.cert(serviceAccount),
        databaseURL: config.databaseUrl
    });
}
var defaultApp = initApp();

var resultHelper = require('../models/result');
var cryptoEngine = require('../helper/cryptoEngine');

// Initialize Firebase
var firebaseConfig = {
    apiKey: config.apiKey,
    authDomain: config.authDomain,
    databaseURL: config.databaseUrl,
    projectId: config.projectId,
    storageBucket: config.storageBucket,
    messagingSenderId: config.messagingSenderId,
    appId: config.appId
};


// Initialize Firebase
firebase.initializeApp(firebaseConfig);
var actionCodeSettings = {
    // URL you want to redirect back to. The domain (www.example.com) for this
    // URL must be whitelisted in the Firebase Console.
    //url: 'https://services-sky.ehubstar.com/Verification/',
    url: config.verifyLink,
    //url: 'http://localhost:49672/',
    // This must be true.
    handleCodeInApp: true,
    //iOS: {
    //    bundleId: 'com.mtpentertainment.skysocial'
    //},
    //android: {
    //    packageName: 'ehubstar.sontung.mtp',
    //    installApp: true,
    //    minimumVersion: '12'
    //},
    dynamicLinkDomain: config.dynamicLinkDomain
};

function sendVerifiedLinkEmail(userId, email, callback) {
    var x = "email=" + email + "&userId=" + userId;

    var queryString = "?type=verify&id=" + encodeURI(cryptoEngine.encryptText(x));
    var currentActionCodeSetting = _.cloneDeep(actionCodeSettings);
    currentActionCodeSetting.url += queryString;


    firebase.auth().sendSignInLinkToEmail(email, currentActionCodeSetting)
        .then(function () {
            // The link was successfully sent. Inform the user.
            // Save the email locally so you don't need to ask the user for it again
            // if they open the link on the same device.
            logger.info('sendVerifiedLinkEmail success');
            callback(resultHelper.success());
        })
        .catch(function (error) {
            // Some error occurred, you can inspect the code: error.code
            logger.error('sendVerifiedLinkEmail', error);
            callback(resultHelper.dataError());
        });
    //callback(resultHelper.success());
}

function verifyLink(email, url, callback) {
    
    firebase.auth().signInWithEmailLink(email, url)
        .then(function (result) {
            
            //result.user.getIdToken().then((idToken) => {
            
            //});

            logger.info('verifyLink success');
            callback(resultHelper.success());
        })
        .catch(function (error) {
            logger.error('verifyLink', error);
            callback(resultHelper.dataError());
        });
}

function createUserOnFirebase(email, callback) {
    defaultApp.auth()
        .createUser({
            email: email,
            emailVerified: false,
            password: passwordFirebase,
            disabled: false
        })
        .then((userRecord) => {
            logger.info('createUserOnFirebase success', userRecord);
            callback(true);
        })
        .catch((errX) => {
            logger.error('createUserOnFirebase', errX);
            callback(false);
        });
}

function checkAndCreateUserOnFirebase(email, callback) {
    defaultApp.auth()
        .getUserByEmail(email)
        .then((userRecord) => {
            
            if (userRecord) {
                logger.info('checkAndCreateUserOnFirebase success', userRecord);
                callback(true);
            }
            else {
                createUserOnFirebase(email, callback);
            }
        })
        .catch((err) => {
            logger.error('createUserOnFirebase', err);
            createUserOnFirebase(email, callback);
        });
}

function sendResetPasswordEmail(userId, email, callback) {
    var x = "email=" + email + "&userId=" + userId;

    var queryString = "?type=verify&id=" + encodeURI(cryptoEngine.encryptText(x));
    var currentActionCodeSetting = _.cloneDeep(actionCodeSettings);
    currentActionCodeSetting.url += queryString;
    
    checkAndCreateUserOnFirebase(email, (resultX) => {
        if (resultX) {
            firebase.auth().sendPasswordResetEmail(email, currentActionCodeSetting)
                .then(function () {
                    // The link was successfully sent. Inform the user.
                    // Save the email locally so you don't need to ask the user for it again
                    // if they open the link on the same device.
                    logger.info('sendResetPasswordEmail success');
                    callback(resultHelper.success());
                })
                .catch(function (error) {
                    // Some error occurred, you can inspect the code: error.code
                    logger.error('sendResetPasswordEmail', error);
                    callback(resultHelper.dataError());
                });
        }
        else {
            callback(resultHelper.dataError());
        }
    });
}

function verifyPasswordResetCode(actionCode, callback) {
    
    firebase.auth().verifyPasswordResetCode(actionCode).then(function (email) {
        logger.info('verifyPasswordResetCode success', email);
        callback(resultHelper.success({
            Email: email
        }));
    })
        .catch(function (err) {
            logger.error('verifyPasswordResetCode', err);
            callback(resultHelper.dataError());
        });
}

var verifyTokenAndGetData = (token, callback) => {
    if (!defaultApp) {
        defaultApp = initApp();
    }

    var defaultAuth = defaultApp.auth();

    defaultAuth.verifyIdToken(token)
        .then((decodedToken) => {
            var phone = decodedToken.phone_number;
            logger.info('verifyTokenAndGetData success', phone);
            if (phone) {
                callback(resultHelper.success(phone));
            }
            else {
                callback(resultHelper.notExists());
            }
        })
        .catch((error) => {
            logger.error('verifyTokenAndGetData', error);
            callback(resultHelper.dataError(error));
        });
};

exports.sendVerifiedLinkEmail = sendVerifiedLinkEmail;
exports.verifyLink = verifyLink;
exports.sendResetPasswordEmail = sendResetPasswordEmail;
exports.verifyPasswordResetCode = verifyPasswordResetCode;
exports.verifyTokenAndGetData = verifyTokenAndGetData;
