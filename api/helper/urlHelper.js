﻿module.exports = {
    checkIsUrl: function (url) {
        return (url.match(/^(https?:\/\/)?([\da-z\.-]+)\.([a-z\.]{2,6})([\/\w \.-]*)*\/?$/i) != null);
    }
};