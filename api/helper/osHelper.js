﻿var mobileDetect = require('mobile-detect');
String.prototype.contains = function (it) { return this.indexOf(it) != -1; };
exports.getOSFromRequest = function (req) {
    if (req.headers.os != null) {
        return req.headers.os;
    }
    else {
        var md = new mobileDetect(req.headers['user-agent']);

        var ua = JSON.stringify(md.ua);
        var os = (ua.contains('iPhone') || ua.contains('iPad') || ua.contains('iOS') || ua.contains('Darwin') || ua.contains('Alamofire')) ? 'iOS' : (ua.contains('Windows') ? 'Windows' : (ua.contains('okhttp') ? 'Android' : 'Others'));

        //os = 'iOS';
        return os;
    }
};
