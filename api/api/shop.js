﻿var _ = require('lodash');
var async = require('async');

var db = require('../models/all');
var storageHelper = require('../helper/storageHelper');
var ensureLogin = require('../helper/sessionLoginHelper').ensureLogin;
var moment = require('moment');
var errorCode = require('../errorCode');
//import các hàm notify
var notify = require('../Notify/caller');
var config = require('../myConfig');
var resultHelper = require('../models/result');
var promotionHelper = require('../helper/promotionHelper');
var loggerPaymentInfo = require('../helper/logHelper').loggerPaymentInfo;

var inAppPurchaseHelper = require('../helper/iOSInAppPurchaseHelper');

var token = 'airpodpro';
module.exports = shopAPI;

function processAfterPaySuccess(userId, os, result) {
    if (result.ErrorCode == errorCode.success) {

        var item = result.Data;

        if (item.PayTypeId == config.PayType.Media) {
            var mediaId = item.ItemId;

            db.userLibrary.addUserLibrary(userId, mediaId, 0, 1, 0, os, () => {
            });
        }
    }
}

function shopAPI(app, passport) {

    //payment
    app.post('/payment/add',
        [ensureLogin],
        function (req, res, next) {
            var user = req.session.passport.user;
            var userId = user.UserId;
            var os = user.OS;
            var args = req.body;

            var methodId = args.MethodId;
            var price = args.Price;
            if (!price || price == 0) {
                price = 0;
            }
            var discountCode = args.DiscountCode;
            var discount = args.Discount;
            if (!discount) {
                discount = 0;
            }

            var payTypeId = args.PayTypeId;
            var itemId = args.ItemId;
            var isDigital = args.IsDigital;
            var isVipPurchase = args.IsVipPurchase;
            var vipMonth = args.VipMonth;

            var paymentCode = db.shop.paymentLog.generatePaymentCode(userId);
            var total = price - discount;
            var description = args.Description;

            db.shop.paymentLog.createPaymentLog(userId, paymentCode, methodId, price, discountCode, discount, total, payTypeId, itemId,
                isDigital, isVipPurchase, vipMonth, os, description, (result) => {
                    return res.json(result);
                });
        }
    );

    app.post('/payment/addFromInApp',
        [ensureLogin],
        function (req, res, next) {
            var user = req.session.passport.user;
            var userId = user.UserId;
            var os = user.OS;
            var args = req.body;

            var payTypeId = args.PayTypeId;
            var itemId = args.ItemId;
            var description = args.Description;
            var discount = 0;
            var discountCode = "";
            var isDigital = true;
            var isVipPurchase = true;
            var vipMonth = 0;
            var price = 0;
            var total = 0;

            var paymentCode = db.shop.paymentLog.generatePaymentCode(userId);
            var methodId = config.iOSInAppPurchaseSettings.methodId;

            var getItemInfo = (cb) => {
                switch (payTypeId) {
                    case config.PayType.Vip: {
                        db.shop.vipConfig.getVipConfigInfo(itemId, (result) => {
                            if (result.ErrorCode == 0) {
                                var vipConfig = result.Data;
                                price = vipConfig.IOSInAppPrice;
                                total = vipConfig.IOSInAppPrice;
                                isVipPurchase = true;
                                vipMonth = vipConfig.Month;
                                cb(null, null);
                            }
                            else {
                                var finalResult = result;
                                loggerPaymentInfo.error('/payment/addFromInApp getVipConfig error', result);
                                cb(null, finalResult);
                            }

                        });
                        break;
                    }
                    case config.PayType.Media: {
                        db.media.getMediaById(userId, itemId, db.media.mediaSchema, (err, media) => {
                            if (err) {
                                var finalResult = resultHelper.dataError(err);
                                loggerPaymentInfo.error('/payment/addFromInApp getMediaById error', finalResult);
                                cb(null, finalResult);
                            }
                            else {
                                price = media.IOSInAppPrice;
                                total = price;
                                isVipPurchase = false;
                                cb(null, null);
                            }
                        });
                        break;
                    }
                    default: {
                        var finalResult = resultHelper.notExists('PayType not exists');
                        loggerPaymentInfo.error('/payment/addFromInApp payType error', finalResult);
                        cb(null, finalResult);
                        break;
                    }
                }
            };

            var createPaymentLog = (finalResult, cb) => {
                if (finalResult) {
                    cb(null, finalResult);
                }
                else {
                    db.shop.paymentLog.createPaymentLog(userId, paymentCode, methodId, price, discountCode, discount, total, payTypeId, itemId,
                        isDigital, isVipPurchase, vipMonth, os, description,
                        (result) => {
                            cb(null, result);
                        });
                }
            };

            async.waterfall([getItemInfo, createPaymentLog], (err, result) => {
                return res.json(result);
            });
        }
    );

    app.post('/payment/addFromMobileCard',
        [ensureLogin],
        function (req, res, next) {
            var user = req.session.passport.user;
            var userId = user.UserId;
            var os = user.OS;
            var args = req.body;

            var price = args.Price;
            if (!price || price == 0) {
                price = 0;
            }
            var discountCode = args.DiscountCode;
            var discount = args.Discount;
            if (!discount) {
                discount = 0;
            }
            var total = price - discount;

            var payTypeId = args.PayTypeId;
            var itemId = args.ItemId;
            var isDigital = args.IsDigital;
            var isVipPurchase = args.IsVipPurchase;
            var vipMonth = args.VipMonth;

            var cardType = args.CardType;
            var cardAmount = args.CardAmount;
            var cardPIN = args.CardPIN;
            var cardSerial = args.CardSerial;

            var paymentCode = db.shop.paymentLog.generatePaymentCode(userId);

            db.shop.paymentLog.createPaymentLogFromMobileCard(userId, paymentCode, price, discountCode, discount, total, payTypeId, itemId,
                isDigital, isVipPurchase, vipMonth, cardType, cardAmount, cardPIN, cardSerial, os, (result) => {
                    return res.json(result);
                });
        }
    );

    app.post('/payment/addFromPasscode',
        [ensureLogin],
        function (req, res, next) {
            var user = req.session.passport.user;
            var userId = user.UserId;
            var os = user.OS;
            var args = req.body;

            var passCode = args.PassCode;

            var paymentCode = db.shop.paymentLog.generatePaymentCode(userId);

            db.shop.paymentLog.createPaymentLogFromPasscode(userId, paymentCode, passCode, os, (result) => {

                if (result.ErrorCode == errorCode.success) {
                    var item = result.Data;
                    if (item.PayTypeId == config.PayType.Media) {
                        var mediaId = item.ItemId;
                        db.userLibrary.addUserLibrary(userId, mediaId, 0, 1, 0, os, () => {
                        });
                    }
                }

                return res.json(result);
            });
        }
    );


    app.post('/payment/updateIsPay',
        [ensureLogin],
        function (req, res, next) {
            var user = req.session.passport.user;
            var userId = user.UserId;
            var os = user.OS;
            var args = req.body;

            var paymentCode = args.PaymentCode;
            var trackingId = args.TrackingId;
            var isPaid = 1, cardErrorCode, cardErrorMessage;

            db.shop.paymentLog.updatePaymentLogIsPay(userId, paymentCode, trackingId, isPaid, cardErrorCode, cardErrorMessage, os, (result) => {
                processAfterPaySuccess(userId, os, result);
                //notify hay gì đoá
                return res.json(result);
            });
        }
    );

    app.post('/payment/systemUpdateIsPay',

        function (req, res, next) {

            var args = req.body;
            var tk = args.Token;
            if (tk == token) {
                var userId = args.UserId;
                var os = args.OS;

                var paymentCode = args.PaymentCode;
                var trackingId = args.TrackingId;
                var isPaid = 1, cardErrorCode, cardErrorMessage;

                db.shop.paymentLog.updatePaymentLogIsPay(userId, paymentCode, trackingId, isPaid, cardErrorCode, cardErrorMessage, os, (result) => {

                    processAfterPaySuccess(userId, os, result);
                    //notify hay gì đoá
                    return res.json(result);
                });
            }
            else {
                return res.status(401).json(resultHelper.notAuthorize("Không có quyền truy cập! Vui lòng đăng nhập trước!!!"));
            }
        }
    );

    app.post('/payment/updateIsPayFromInApp',
        [ensureLogin],
        function (req, res) {
            var user = req.session.passport.user;
            var userId = user.UserId;
            var os = user.OS;
            var args = req.body;

            var paymentCode = args.PaymentCode;
            var receiptData = args.ReceiptData;

            inAppPurchaseHelper.verifyReceipt(receiptData, (resultX) => {
                if (resultX.ErrorCode == 0) { //xác thực thành công
                    var responseData = resultX.Data;
                    var receipt = responseData.receipt; //lấy thông tin receipt
                    var isPaid = true, cardErrorCode, cardErrorMessage;
                    db.shop.paymentLog.updatePaymentLogIsPay(userId, paymentCode, receipt.transaction_id, isPaid, cardErrorCode, cardErrorMessage, os,
                        (result) => {
                            processAfterPaySuccess(userId, os, result);
                            //notify hay gì đoá
                            return res.json(result);
                        });
                }
                else {
                    return res.json(resultX);
                }
            });
        }
    );

    app.post('/payment/updateIsPayFromMobileCard',
        [ensureLogin],
        function (req, res, next) {
            var user = req.session.passport.user;
            var userId = user.UserId;
            var os = user.OS;
            var args = req.body;

            var paymentCode = args.PaymentCode;
            var trackingId = args.TrackingId;
            var isPaid = args.IsPaid;
            var cardErrorCode = args.CardErrorCode;
            var cardErrorMessage = args.CardErrorMessage;

            db.shop.paymentLog.updatePaymentLogIsPay(userId, paymentCode, trackingId, isPaid, cardErrorCode, cardErrorMessage, os,
                (result) => {
                    processAfterPaySuccess(userId, os, result);
                    //notify hay gì đoá
                    return res.json(result);
                });
        }
    );

    app.post('/payment/info',
        [ensureLogin],
        function (req, res, next) {
            var user = req.session.passport.user;
            var userId = user.UserId;
            var os = user.OS;
            var args = req.body;

            var paymentCode = args.PaymentCode;

            db.shop.paymentLog.getPaymentLogInfo(userId, paymentCode, (result) => {
                return res.json(result);
            });
        }
    );

    app.post('/payment/systemInfo',
        function (req, res) {

            var args = req.body;
            var paymentCode = args.PaymentCode;
            var tk = args.Token;
            if (tk == token) {
                db.shop.paymentLog.getPaymentLogInfoForSystem(paymentCode, (result) => {
                    return res.json(result);
                });
            }
            else {
                return res.status(401).json(resultHelper.notAuthorize("Không có quyền truy cập! Vui lòng đăng nhập trước!!!"));
            }
        }
    );

    app.post('/payment/list',
        [ensureLogin],
        function (req, res, next) {
            var user = req.session.passport.user;
            var userId = user.UserId;
            var os = user.OS;
            var args = req.body;

            var isPaid = args.IsPaid;
            var methodId = args.MethodId;
            var lastDate = args.LastDate;
            var itemCount = args.ItemCount;

            db.shop.paymentLog.getPaymentLogs(userId, isPaid, methodId, lastDate, itemCount, (result) => {
                return res.json(result);
            });
        }
    );

    ////////
    //vip config
    app.post('/vippack/info',
        function (req, res, next) {
            var args = req.body;
            var configId = args.ConfigId;

            db.shop.vipConfig.getVipConfigInfo(configId, (result) => {
                return res.json(result);
            });
        }
    );

    app.post('/vippack/list',
        function (req, res, next) {
            var args = req.body;
            var active = args.Active;
            if (!active) {
                active = 1;
            }

            db.shop.vipConfig.getVipConfigs(active, (result) => {
                return res.json(result);
            });
        }
    );

    app.post('/vippack/inapp',
        function (req, res) {
            db.shop.vipConfig.getVipConfigsForInApp((result) => {
                return res.json(result);
            });
        });

    app.post('/user/getPromotion',
        [ensureLogin],
        function (req, res, next) {
            var user = req.session.passport.user;
            var userId = user.UserId;
            var os = user.OS;
            db.promotion.getPromotionForUser(userId, (err, lst) => {
                if (err) {
                    res.json(resultHelper.dataError(err));
                }
                else {
                    if (_.some(lst, (item) => {
                        return item.PromoType == db.promotion.promoType.VIPTRIAL && item.AutoApply == true;
                    })) {
                        lst = _.filter(lst, (item) => {
                            return item.PromoTargetUser != db.promotion.promoTargetUser.UNVIP;
                        });
                    }

                    var autoApplyList = _.filter(lst, (item) => {
                        return item.AutoApply == true;
                    });

                    if (autoApplyList && autoApplyList.length > 0) {
                        async.each(autoApplyList, (item, cb) => {
                            promotionHelper.applyPromotionToUser(item, userId, os, (result) => {
                                cb();
                            });
                        },
                            (errX) => {
                                var result = resultHelper.success(lst);
                                res.json(result);
                            });
                    }
                    else {
                        var result = resultHelper.success(lst);
                        res.json(result);
                    }
                }
            });
        }
    );

    app.post('/user/setPromotion',
        [ensureLogin],
        function (req, res, next) {
            var user = req.session.passport.user;
            var userId = user.UserId;
            var os = user.OS;
            var args = req.body;
            var promotionId = args.PromotionId;
            db.promotion.checkUserPromotion(userId, promotionId, (resX) => {
                var isExist = false;

                if (resX.ErrorCode == 0) {
                    if (resX.Data === true) {
                        isExist = true;
                    }
                }

                if (!isExist) {
                    db.promotion.getPromotion(promotionId, (err, promotion) => {
                        if (!err) {
                            promotionHelper.applyPromotionToUser(promotion, userId, os, (result) => {
                                res.json(result);
                            });
                        }
                        else {
                            res.json(resultHelper.notExists());
                        }
                    });
                }
                else {
                    res.json(resultHelper.exists());
                }
            });
        }
    );

    app.post('/user/refusePromotion',
        [ensureLogin],
        function (req, res, next) {
            var user = req.session.passport.user;
            var userId = user.UserId;
            var args = req.body;
            var promotionId = args.PromotionId;

            db.promotion.addUserPromotion(userId, promotionId, false, true, (err) => {
                if (err) {
                    res.json(resultHelper.dataError(err));
                }
                else {
                    res.json(resultHelper.success());
                }
            });
        }
    );
}

