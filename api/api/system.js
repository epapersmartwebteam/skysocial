﻿var db = require('../models/all');

var myConfig = require('../myConfig');

var ensureLogin = require('../helper/sessionLoginHelper').ensureLogin;
var osHelper = require('../helper/osHelper');

var errorCode = require('../errorCode');

module.exports = systemAPI;

function systemAPI(app, passport, result) {
    app.post('/system/getVersionInfo',
         function (req, res, next) {
             var os = osHelper.getOSFromRequest(req);

             db.system.getOSSetting(os, function (err, data) {
                 if (err) {
                     return res.json(result.create(errorCode.checkingError, 'Chưa cấu hình OS'));
                 } else {
                     return res.json(result.create(errorCode.success, '', data));
                 }
             });
        });

}