﻿var moment = require('moment');
var db = require('../models/all');
var ensureLogin = require('../helper/sessionLoginHelper').ensureLogin;
var errorCode = require('../errorCode');

var resultHelper = require('../models/result');

var config = require('../myConfig');

module.exports = (app, passport, result) => {
    //feature tab
    app.post('/sync/feature/getNewPostCount',
        [ensureLogin],
        (req, res) => {
            var data = req.body;

            var lastSyncDate = data.LastSyncDate;
            db.sync.featureTab.getNewPostCount(lastSyncDate, (result) => {
                res.json(result);
            });
        }
    );

    app.post('/sync/feature/getHasUpdatePostId',
        [ensureLogin],
        (req, res) => {
            var data = req.body;

            var lastSyncDate = data.LastSyncDate;
            db.sync.featureTab.getHasUpdatePostId(lastSyncDate, (result) => {
                res.json(result);
            });
        }
    );

    app.post('/sync/feature/getHasNewActivityPostId',
        [ensureLogin],
        (req, res) => {
            var data = req.body;

            var lastSyncDate = data.LastSyncDate;
            db.sync.featureTab.getHasNewActivityPostId(lastSyncDate, (result) => {
                res.json(result);
            });
        }
    );

    app.post('/sync/feature/getDeletedPostId',
        [ensureLogin],
        (req, res) => {
            var data = req.body;
            var lastSyncDate = data.LastSyncDate;
            db.sync.featureTab.getFeatureFeedHasDeletePostId(lastSyncDate, (result) => {
                res.json(result);
            });
        }
    );
};