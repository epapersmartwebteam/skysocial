﻿var db = require('../models/all');

var userHelper = require('../helper/userHelper');

var ensureLogin = require('../helper/sessionLoginHelper').ensureLogin;

var resultHelper = require('../models/result');

var errorCode = require('../errorCode');

var systemUserId = require('../myConfig').SystemUserId;
var roleAdmin = require('../myConfig').RoleAdmin;

module.exports = userSaveAPI;

function userSaveAPI(app, passport) {

    //Collections
    app.post('/user/collection/info',
        [ensureLogin],
        function (req, res, next) {
            var user = req.session.passport.user;
            var userId = user.UserId;

            var args = req.body;
            var collectionId = args.CollectionId;

            db.userSave.getCollection(userId, collectionId, (result) => {
                return res.json(result);
            });
        }
    );

    app.post('/user/collection/getList',
        [ensureLogin],
        function (req, res, next) {
            var user = req.session.passport.user;
            var userId = user.UserId;

            //var args = req.body;

            db.userSave.getCollections(userId, (result) => {
                return res.json(result);
            });
        }
    );

    app.post('/user/collection/add',
        [ensureLogin],
        function (req, res, next) {
            var user = req.session.passport.user;
            var userId = user.UserId;

            var args = req.body;
            var collectionName = args.CollectionName;

            db.userSave.createCollection(userId, collectionName, (result) => {
                if (result.ErrorCode == errorCode.success) {
                    var collectionId = result.Data;
                    db.userSave.getCollection(userId, collectionId, (resX) => {
                        return res.json(resX);
                    });
                }
                else {
                    return res.json(result);
                }
            });
        }
    );

    app.post('/user/collection/updateName',
        [ensureLogin],
        function (req, res, next) {
            var user = req.session.passport.user;
            var userId = user.UserId;

            var args = req.body;
            var collectionId = args.CollectionId;
            var collectionName = args.CollectionName;

            db.userSave.updateCollectionName(userId, collectionId, collectionName, (result) => {
                return res.json(result);
            });
        }
    );

    app.post('/user/collection/updateCover',
        [ensureLogin],
        function (req, res, next) {
            var user = req.session.passport.user;
            var userId = user.UserId;

            var args = req.body;
            var collectionId = args.CollectionId;
            var coverImageItemId = args.CoverImageItemId;

            db.userSave.updateCollectionCover(userId, collectionId, coverImageItemId, (result) => {
                return res.json(result);
            });
        }
    );

    app.post('/user/collection/delete',
        [ensureLogin],
        function (req, res, next) {
            var user = req.session.passport.user;
            var userId = user.UserId;

            var args = req.body;
            var collectionId = args.CollectionId;

            db.userSave.deleteCollection(userId, collectionId, (result) => {
                return res.json(result);
            });
        }
    );

    //Saved
    app.post('/user/saved/getCollectionIdList',
        [ensureLogin],
        function (req, res, next) {
            var user = req.session.passport.user;
            var userId = user.UserId;

            var args = req.body;
            var imageItemId = args.ImageItemId;
            db.userSave.getCollectionsOfSaveImage(userId, imageItemId, (result) => {
                return res.json(result);
            });
        }
    );

    app.post('/user/saved/add',
        [ensureLogin],
        function (req, res, next) {
            var user = req.session.passport.user;
            var userId = user.UserId;

            var args = req.body;
            var imageItemId = args.ImageItemId;
            var collectionIds = args.CollectionIds;

            db.userSave.addSaveImage(userId, imageItemId, collectionIds, (result) => {
                return res.json(result);
            });
        }
    );

    app.post('/user/saved/remove',
        [ensureLogin],
        function (req, res, next) {
            var user = req.session.passport.user;
            var userId = user.UserId;

            var args = req.body;
            var imageItemId = args.ImageItemId;

            db.userSave.removeSaveImage(userId, imageItemId, null, true, (result) => {
                return res.json(result);
            });
        }
    );

    app.post('/user/saved/removeFromCollection',
        [ensureLogin],
        function (req, res, next) {
            var user = req.session.passport.user;
            var userId = user.UserId;

            var args = req.body;
            var imageItemId = args.ImageItemId;
            var collectionIds = args.CollectionIds;

            db.userSave.removeSaveImage(userId, imageItemId, collectionIds, false, (result) => {
                return res.json(result);
            });

        }
    );
}