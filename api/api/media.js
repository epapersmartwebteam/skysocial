﻿var db = require('../models/all');
var storageHelper = require('../helper/storageHelper');
var ensureLogin = require('../helper/sessionLoginHelper').ensureLogin;
var moment = require('moment');
var errorCode = require('../errorCode');
//import các hàm notify
var notify = require('../notify/caller');

module.exports = mediaAPI;

function mediaAPI(app, passport, result) {
    //menu
    app.post('/menu/list',
        [ensureLogin],
        function (req, res, next) {
            var user = req.session.passport.user;
            var userId = user.UserId;
            var os = user.OS;
            var args = req.body;
            db.menu.getMenuList((err, lst) => {
                if (err) {
                    return res.json(result.dataError(err));
                }
                else {
                    return res.json(result.success(lst));
                }
            });
        }
    );

    /////////media
    app.post('/media/list',
        [ensureLogin],
        function (req, res, next) {
            var user = req.session.passport.user;
            var userId = user.UserId;
            var os = user.OS;
            var args = req.body;

            var tags = args.Tags;
            var isFree = args.IsFree;
            var pageIndex = args.PageIndex;
            var itemCount = args.ItemCount;
            var getPriceInfo = args.GetPriceInfo;

            var ast = getPriceInfo ? db.media.mediaSchema : db.media.mediaBasicSchema;
            db.media.getMediaList(userId, tags, isFree, pageIndex, itemCount, ast, (err, lst) => {
                if (err) {
                    return res.json(result.dataError(err));
                }
                else {
                    return res.json(result.success(lst));
                }
            });
        }
    );

    app.post('/media/audio/all',
        [ensureLogin],
        function (req, res, next) {
            var user = req.session.passport.user;
            var userId = user.UserId;
            var args = req.body;
            var pageIndex = args.PageIndex;
            var itemCount = args.ItemCount;
            db.mediaItem.getAllItemList(userId, ["AUDIO"], pageIndex, itemCount, null, (err, lst) => {
                if (err) {
                    return res.json(result.dataError(err));
                }
                else {
                    return res.json(result.success(lst));
                }
            });
        }
    );

    app.post('/media/video/all',
        [ensureLogin],
        function (req, res, next) {
            var user = req.session.passport.user;
            var userId = user.UserId;
            var args = req.body;
            var pageIndex = args.PageIndex;
            var itemCount = args.ItemCount;
            db.mediaItem.getAllItemList(userId, ["VIDEO"], pageIndex, itemCount, null, (err, lst) => {
                if (err) {
                    return res.json(result.dataError(err));
                }
                else {
                    return res.json(result.success(lst));
                }
            });
        }
    );

    app.post('/media/id',
        [ensureLogin],
        function (req, res, next) {
            var user = req.session.passport.user;
            var userId = user.UserId;

            var args = req.body;
            var mediaId = args.MediaId;
            var getPriceInfo = args.GetPriceInfo;

            var ast = getPriceInfo ? db.media.mediaSchema : db.media.mediaBasicSchema;
            db.media.getMediaById(userId, mediaId, ast, (err, media) => {
                if (err) {
                    return res.json(result.dataError(err));
                }
                else {
                    return res.json(result.success(media));
                }
            });
        }
    );

    //media item
    app.post('/media/item/list',
        [ensureLogin],
        function (req, res, next) {
            var user = req.session.passport.user;
            var userId = user.UserId;

            var args = req.body;
            var mediaId = args.MediaId;

            db.mediaItem.getMediaItemByMediaId(userId, mediaId, null, (err, lst) => {
                if (err) {
                    return res.json(result.dataError(err));
                }
                else {
                    return res.json(result.success(lst));
                }
            });
        }
    );

    app.post('/media/item/id',
        [ensureLogin],
        function (req, res, next) {
            var user = req.session.passport.user;
            var userId = user.UserId;

            var args = req.body;
            var itemId = args.ItemId;

            db.mediaItem.getMediaItemById(itemId, userId, null, (err, mediaItem) => {
                if (err) {
                    return res.json(result.dataError(err));
                }
                else {
                    return res.json(result.success(mediaItem));
                }
            });
        }
    );

    /////////like
    app.post('/media/getLikeList',
        [ensureLogin],
        function (req, res, next) {
            var user = req.session.passport.user;

            var args = req.body;
            var mediaId = args.MediaId;
            var lastDate = args.LastDate;
            var itemCount = args.ItemCount;

            db.mediaLike.getMediaLikeList(mediaId, lastDate, itemCount, (err, lst) => {
                if (err) {
                    return res.json(result.dataError(err));
                }
                else {
                    return res.json(result.success(lst));
                }
            });
        }
    );

    app.post('/media/like',
        [ensureLogin],
        function (req, res, next) {
            var user = req.session.passport.user;
            var userId = user.UserId;

            var args = req.body;
            var mediaId = args.MediaId;

            db.mediaLike.likeMedia(userId, mediaId, (err, dumb) => {
                if (err) {
                    return res.json(result.dataError(err));
                }
                else {
                    return res.json(result.success());
                }
            });
        }
    );

    app.post('/media/unlike',
        [ensureLogin],
        function (req, res, next) {
            var user = req.session.passport.user;
            var userId = user.UserId;

            var args = req.body;
            var mediaId = args.MediaId;

            db.mediaLike.unlikeMedia(userId, mediaId, (err, dumb) => {
                if (err) {
                    return res.json(result.dataError(err));
                }
                else {
                    return res.json(result.success());
                }
            });
        }
    );

    ////////comment
    app.post('/media/comment/list',
        [ensureLogin],
        function (req, res, next) {
            var user = req.session.passport.user;
            var userId = user.UserId;
            var os = user.OS;

            var args = req.body;
            var mediaId = args.MediaId;
            var lastId = args.LastId;
            var itemCount = args.ItemCount;

            db.mediaComment.getMediaCommentByMediaId(userId, mediaId, lastId, itemCount, null, (err, lst) => {
                if (err) {
                    return res.json(result.dataError(err));
                }
                else {
                    return res.json(result.success(lst));
                }
            });
        }
    );
    app.post('/media/comment/replyList',
        [ensureLogin],
        function (req, res, next) {
            var user = req.session.passport.user;
            var userId = user.UserId;
            var os = user.OS;

            var args = req.body;
            var mediaId = args.MediaId;
            var replyToId = args.ReplyToId;
            var lastId = args.LastId;
            var itemCount = args.ItemCount;

            db.mediaComment.getMediaCommentReplyList(userId, replyToId, lastId, itemCount, null, (err, item) => {
                if (err) {
                    return res.json(result.dataError(err));
                }
                else {
                    return res.json(result.success(item));
                }
            });
        }
    );

    app.post('/media/comment/id',
        [ensureLogin],
        function (req, res, next) {
            var user = req.session.passport.user;
            var userId = user.UserId;
            var os = user.OS;

            var args = req.body;
            var itemId = args.ItemId;
            db.mediaComment.getMediaCommentById(userId, itemId, null, (err, item) => {
                if (err) {
                    return res.json(result.dataError(err));
                }
                else {
                    return res.json(result.success(item));
                }
            });
        }
    );

    app.post('/media/comment/add',
        [ensureLogin],
        function (req, res, next) {
            var user = req.session.passport.user;
            var userId = user.UserId;
            var os = user.OS;

            var args = req.body;

            var mediaId = args.MediaId;
            var comment = args.Comment;
            var replyToId = args.ReplyToId;

            db.mediaComment.createMediaComment(userId, mediaId, comment, replyToId, os, function (err, commentId) {
                if (err) {
                    return res.json(result.dataError(err));
                }
                else {
                    db.mediaComment.getMediaCommentById(userId, commentId, null, function (err1, data) {
                        if (err1) {
                            return res.json(result.dataError(err1));
                        }
                        else {
                            return res.json(result.success(data));
                        }
                    });

                    notify.notifyNewMediaComment(userId, mediaId, commentId, (x) => {
                    });
                }
            });
        }
    );

    app.post('/media/comment/update',
        [ensureLogin],
        function (req, res, next) {
            var user = req.session.passport.user;
            var userId = user.UserId;
            var os = user.OS;
            var args = req.body;

            var itemId = args.ItemId;
            var comment = args.Comment;

            db.mediaComment.updateMediaComment(itemId, comment, os, function (err, dumb) {

                if (err) {
                    return res.json(result.dataError(err));
                }
                else {

                    db.mediaComment.getMediaCommentById(userId, itemId, null, function (err1, data) {

                        if (err1) {
                            return res.json(result.dataError(err1));
                        }
                        else {
                            return res.json(result.success(data));
                        }
                    });
                }
            });
        }
    );

    app.post('/media/comment/delete',
        [ensureLogin],
        function (req, res, next) {
            var user = req.session.passport.user;
            var userId = user.UserId;

            var args = req.body;

            var itemId = args.ItemId;

            db.mediaComment.deleteMediaComment(itemId, function (err, mediaId) {
                if (err) {
                    return res.json(result.dataError(err));
                }
                else {
                    return res.json(result.success());
                }
            });
        }
    );

    app.post('/media/comment/like',
        [ensureLogin],
        function (req, res, next) {
            var user = req.session.passport.user;
            var userId = user.UserId;
            var userGUID = user.UserGUID;

            var args = req.body;
            var commentId = args.CommentId;

            db.mediaComment.likeMediaComment(commentId, userId, (err, itemId) => {
                if (!err) {
                    notify.notifyLikeMediaComment(userId, commentId, (x) => {
                    });
                    return res.json(result.success());
                }
                else {
                    return res.json(result.dataError(err));
                }
            });
        }
    );
    app.post('/media/comment/unlike',
        [ensureLogin],
        function (req, res, next) {
            var user = req.session.passport.user;
            var userId = user.UserId;
            var userGUID = user.UserGUID;

            var args = req.body;
            var commentId = args.CommentId;

            db.mediaComment.unlikeMediaComment(commentId, userId, (err, itemId) => {
                if (!err) {
                    return res.json(result.success());
                }
                else {
                    return res.json(result.dataError(err));
                }
            });
        }
    );

    app.post('/media/comment/getLikeList',
        [ensureLogin],
        function (req, res, next) {
            var args = req.body;
            var commentId = args.CommentId;
            var lastId = args.LastId;
            var itemCount = args.ItemCount;

            db.mediaComment.getMediaCommentLikeList(commentId, lastId, itemCount, (err, lst) => {
                if (!err) {
                    return res.json(result.success(lst));
                }
                else {
                    return res.json(result.dataError(err));
                }
            });
        }
    );

    //upload
    app.post('/media/getUploadSAS',
        function (req, res, next) {
            var args = req.body;

            var mediaGUID = args.MediaGUID;
            var mediaType = args.MediaType;
            var coverExtension = args.CoverExtension;

            storageHelper.generateUploadMediaSAS(mediaGUID, mediaType, coverExtension, function (err, data) {
                if (err) {
                    return res.json(result.dataError(err));
                }
                else {
                    return res.json(result.success(data));
                }
            });
        });

    app.post('/media/item/getUploadSAS',
        function (req, res, next) {

            var args = req.body;
            var mediaGUID = args.MediaGUID;
            var itemGUID = args.ItemGUID;
            var extension = args.Extension;
            var coverExtension = args.CoverExtension;

            storageHelper.generateUploadMediaItemSAS(mediaGUID, itemGUID, extension, coverExtension, function (err, data) {
                if (err) {
                    return res.json(result.dataError(err));
                }
                else {
                    return res.json(result.success(data));
                }
            });
        });

    app.post('/media/deleteAllFiles',
        function (req, res, next) {
            var args = req.body;
            var mediaGUID = args.MediaGUID;

            storageHelper.deleteMediaBlobs(mediaGUID, (err, data) => {
                if (err) {
                    return res.json(result.dataError(err));
                }
                else {
                    return res.json(result.success(data));
                }
            });
        }
    );

    app.post('/media/item/deleteAllFiles',
        function (req, res, next) {
            var args = req.body;
            var mediaGUID = args.MediaGUID;
            var itemGUID = args.ItemGUID;

            storageHelper.deleteMediaItemBlobs(mediaGUID, itemGUID, (err, data) => {
                if (err) {
                    return res.json(result.dataError(err));
                }
                else {
                    return res.json(result.success(data));
                }
            });
        }
    );
}