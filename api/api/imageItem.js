﻿var db = require('../models/all');
var storageHelper = require('../helper/storageHelper');
var ensureLogin = require('../helper/sessionLoginHelper').ensureLogin;

var errorCode = require('../errorCode');
//import các hàm notify
var notify = require('../Notify/caller');
var cryptoEngine = require('../helper/cryptoEngine');

var config = require('../myConfig');

var redisCacheHelper = require('../models/redisCacheHelper');

module.exports = imageItemAPI;



function imageItemAPI(app, passport, result) {
    app.post('/image/getUploadSAS',
        [ensureLogin],
        function (req, res) {
            var itemGUID = req.body.ItemGUID;
            var imageExtensions = req.body.ImageExtensions;
            var userGUID = req.session.passport.user.UserGUID;
            storageHelper.generateUploadImageSAS(userGUID, itemGUID, imageExtensions, function (err, data) {
                if (err) {
                    return res.json(result.create(errorCode.dataError, err));
                }
                else {
                    return res.json(result.create(errorCode.success, '', data));
                }
            });
        });

    app.post('/image/addImageItem',
        [ensureLogin],
        function (req, res) {
            var userInfo = req.session.passport.user;
            var userId = userInfo.UserId;

            var args = req.body;
            var itemGUID = args.ItemGUID;
            var itemContent = args.ItemContent;
            var itemType = args.ItemType;
            var itemData = args.ItemDate;

            var createOS = userInfo.OS;
            var description = args.Description;
            var isPrivate = args.IsPrivate;
            var location = args.Location;
            var lat = args.Lat;
            var long = args.Long;
            var userTags = args.UserTags;
            var hashtags = args.Hashtags;

            itemContent = JSON.stringify(itemContent);

            db.imageItem.createImageItem(userId, itemGUID, itemContent, itemType, itemData, createOS, description, isPrivate, location, lat, long,
                function (err, imageItem) {
                    var imageItemId = imageItem.ImageItemId;
                    if (!err) {

                        if (hashtags) {

                            //todo: chưa xử lý lỗi nếu add hashtag bị lỗi
                            db.imageItem.createImageHashtags(imageItemId, hashtags, function (err1, data1) {

                            });
                        }

                        if (userTags) {
                            //todo: chưa xử lý lỗi nếu add user tags bị lỗi
                            db.imageItem.createImageUsertags(imageItemId, userTags, function (err2, data2) {

                            });
                        }

                        //sau khi thêm xong thì gán trở ngược vào biến imageItem để trả về
                        //var imageItem = {
                        //    ImageItemId: imageItemId,
                        //    ItemGUID: itemGUID,
                        //    UserId: userId,
                        //    ItemContent: itemContent,
                        //    CreateDate: moment().utc().format('X'),
                        //    CreateOS: createOS,
                        //    LastModifyDate: moment().utc().format('X'),
                        //    LastModifyOS: createOS,
                        //    Description: description,
                        //    Location: location,
                        //    Lat: lat,
                        //    Long: long,
                        //    IsPrivate: isPrivate,
                        //    LikeCount: 0,
                        //    CommentCount: 0,
                        //    ShareCount: 0,
                        //    ShareFromId: null,
                        //    WebLink: config.WebLink + cryptoEngine.encryptText(imageItemId)
                        //};
                        notify.notifyNewImageUpload(userId, imageItemId, function (error, dumb) {

                        });

                        imageItem.WebLink = config.WebLink + cryptoEngine.encryptText(imageItemId);

                        //kiểm tra có phải là feature user hay ko và add vào cache nếu có cấu hình
                        redisCacheHelper.addNewPostToFeatureTab(imageItem, (err) => {

                        });


                        //trả về
                        imageItem = db.imageItem.parseItemContent(imageItem, userId);
                        return res.json(result.create(errorCode.success, '', imageItem));
                    }
                    else {
                        return res.json(result.create(errorCode.dataError, err));
                    }
                });
        });
    app.post('/image/updateCommentingOff',
        [ensureLogin],
        function (req, res) {
            var user = req.session.passport.user;
            var userId = user.UserId;

            var args = req.query;
            var imageItemId = args.ImageItemId;
            var commentingOff = args.CommentingOff;

            db.imageItem.updateImageItemCommentingOff(userId, imageItemId, commentingOff, (err, data) => {
                if (err) {
                    res.json(result.create(errorCode.dataError, err));
                }
                else {
                    var resultData = result.notExists();
                    switch (data) {
                        case 0: {
                            resultData = result.success();
                            //update lên cache
                            redisCacheHelper.updatePostExtraToFeatureTab(userId, imageItemId, undefined, undefined, undefined, commentingOff, true, () => { });
                            break;
                        }
                        case 1: {
                            resultData = result.notExists();
                            break;
                        }
                        case 2: {
                            resultData = result.notPermission();
                            break;
                        }
                    }
                    res.json(resultData);
                }
            });
        }
    );

    app.post('/image/searchHashtag',
        [ensureLogin],
        function (req, res) {
            var keyword = req.query.Keyword;
            var userId = req.session.passport.user.UserId;

            db.hashtag.searchHashtag(userId, keyword, function (err, data) {
                if (err) {
                    res.json(result.create(errorCode.dataError, err));
                }
                else {
                    return res.json(result.create(errorCode.success, '', data));
                }
            });
        });

    app.post('/image/searchUserForMention',
        [ensureLogin],
        function (req, res) {
            var keyword = req.query.Keyword;
            var userId = req.session.passport.user.UserId;

            db.userMention.searchUserMention(userId, keyword, function (err, data) {
                if (err) {
                    res.json(result.create(errorCode.dataError, err));
                }
                else {
                    return res.json(result.create(errorCode.success, '', data));
                }
            });
        });

    app.post('/image/report',
        [ensureLogin],
        function (req, res) {
            var user = req.session.passport.user;
            var userId = user.UserId;
            var reportTypeId = req.query.ReportTypeId;
            var imageItemId = req.query.ImageItemId;
            var comment = '';
            var os = user.OS;

            db.imageReport.addImageReport(userId, reportTypeId, imageItemId, comment, os, function (err, reportId) {
                if (err) {
                    res.json(result.create(errorCode.dataError, err));
                }
                else {
                    //nếu tài khoản admin report hình thì sẽ xóa luôn hình
                    if (user.RoleId == config.RoleAdmin) {
                        db.imageItem.deleteImageItem(imageItemId, userId, function (err1, data1) {
                        });
                        db.imageReport.approveImageReport(reportId, true, true, true, userId, () => {
                        });
                    }
                    return res.json(result.create(errorCode.success, '', { Success: true }));
                }
            });
        });

    app.post('/image/delete',
        [ensureLogin],
        function (req, res) {
            var user = req.session.passport.user;
            var userId = user.UserId;
            var userGUID = user.UserGUID;
            var imageItemId = req.query.ImageItemId;

            db.imageItem.getImageItemById(userId, imageItemId, [], function (error, imageItem) {
                if (!error) {
                    db.imageItem.deleteImageItem(imageItemId, userId, function (err, data) {
                        if (err) {
                            if (err == 'NotPermission') {
                                res.json(result.create(errorCode.notPermission, { NotPermission: true }));
                            }
                            else {

                                //xóa hết notify liên quan tới hình bị xóa
                                //db.userNotify.deleteUserNotifyByImage(imageItemId, function (errDumb, dumb) {
                                //    //ko làm gì
                                //});
                                res.json(result.create(errorCode.dataError, err));
                            }
                        }
                        else {

                            //xóa trên Azure Storage
                            var imageItemGUID = imageItem.ItemGUID;
                            //var userGUID = data.UserGUID;
                            //đánh dấu xoá nên ko xoá hình trên cloud
                            //azureHelper.deleteImageBlobs(userGUID, imageItemGUID, function () {
                            //    //ko cần đợi kết quả
                            //});

                            //xóa hết notify liên quan tới hình bị xóa
                            //db.userNotify.deleteUserNotifyByImage(imageItemId, function (errDumb, dumb) {
                            //    //ko làm gì
                            //});
                            var ownerId = imageItem.UserId;
                            redisCacheHelper.deletePostFromFeatureTab(ownerId, imageItemId, () => { });

                            return res.json(result.create(errorCode.success, '', { Success: true }));
                        }
                    });
                }
            });
        });

    app.post('/image/comment/like',
        [ensureLogin],
        function (req, res) {
            var user = req.session.passport.user;
            var userId = user.UserId;
            var userGUID = user.UserGUID;

            var commentId = req.query.CommentId;
            db.imageComment.likeImageComment(commentId, userId, (err, itemId) => {
                if (!err) {
                    notify.notifyLikeComment(userId, commentId, (x) => {
                    });
                    return res.json(result.create(errorCode.success, '', { Success: true }));
                }
                else {
                    res.json(result.create(errorCode.dataError, err));
                }
            });
        }
    );
    app.post('/image/comment/unlike',
        [ensureLogin],
        function (req, res) {
            var user = req.session.passport.user;
            var userId = user.UserId;
            var userGUID = user.UserGUID;

            var commentId = req.query.CommentId;

            db.imageComment.unlikeImageComment(commentId, userId, (err, itemId) => {
                if (!err) {
                    return res.json(result.create(errorCode.success, '', { Success: true }));
                }
                else {
                    res.json(result.create(errorCode.dataError, err));
                }
            });
        }
    );

    app.post('/image/comment/getLikeList',
        [ensureLogin],
        function (req, res) {
            var commentId = req.query.CommentId;
            var lastId = req.query.LastId;
            var itemCount = req.query.ItemCount;

            db.imageComment.getImageCommentLikeList(commentId, lastId, itemCount, (err, lst) => {
                if (!err) {
                    return res.json(result.create(errorCode.success, '', lst));
                }
                else {
                    res.json(result.create(errorCode.dataError, err));
                }
            });
        }
    );
}
