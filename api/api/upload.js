﻿var storageHelper = require('../helper/storageHelper');

var token = 'airpodpro';
module.exports = uploadAPI;

function uploadAPI(app, passport, resultHelper) {
    app.post('/upload/getSAS',
        function (req, res) {
            var args = req.body;

            var container = args.container;
            var fileName = args.fileName;
            var tk = args.token;
            if (tk == token) {
                storageHelper.generateUploadSAS(container, fileName, (err, result) => {
                    res.json(result);
                });
            }
            else {
                res.json(resultHelper.notAuthorize());
            }
        });
}