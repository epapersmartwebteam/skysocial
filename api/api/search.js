﻿var db = require('../models/all');

var ensureLogin = require('../helper/sessionLoginHelper').ensureLogin;

module.exports = searchAPI;

var errorCode = require('../errorCode');

function searchAPI(app, passport, result) {

    //Tìm kiếm trong User và Hashtag theo từ khóa, kết quả trả về trộn giữa 2 cái
    app.post('/search/top',
        [ensureLogin],
        function (req, res, next) {
            var user = req.session.passport.user;
            var userId = user.UserId;

            var keyword = req.query.Keyword;
            var rowCount = req.query.RowCount;

            if (keyword == undefined) {
                return res.json(result.create(errorCode.parameterError, 'Keyword'));
            }
            else {
                if (rowCount == undefined) {
                    rowCount = 0;
                }

                db.search.searchTop(userId, keyword, rowCount, function (err, data) {
                    if (err) {
                        res.json(result.create(errorCode.dataError, err));
                    }
                    else {                        
                        return res.json(result.create(errorCode.success, '', data));
                    }
                });
            }
        });

    //Tìm kiếm Hashtag theo keyword
    app.post('/search/tags',
        [ensureLogin],
        function (req, res, next) {
            var user = req.session.passport.user;
            var userId = user.UserId;

            var keyword = req.query.Keyword;
            var rowCount = req.query.RowCount;

            if (keyword == undefined) {
                return res.json(result.create(errorCode.parameterError, 'Keyword'));
            }
            else {
                if (rowCount == undefined) {
                    rowCount = 0;
                }

                db.search.searchTags(userId, keyword, rowCount, function (err, data) {
                    if (err) {
                        res.json(result.create(errorCode.dataError, err));
                    }
                    else {
                        return res.json(result.create(errorCode.success, '', data));
                    }
                });
            }
        });

    //Tìm kiếm User theo Keyword
    app.post('/search/people',
        [ensureLogin],
        function (req, res, next) {
            var user = req.session.passport.user;
            var userId = user.UserId;

            var keyword = req.query.Keyword;
            var rowCount = req.query.RowCount;

            if (keyword == undefined) {
                return res.json(result.create(errorCode.parameterError, 'Keyword'));
            }
            else {
                if (rowCount == undefined) {
                    rowCount = 0;
                }

                db.search.searchPeople(userId, keyword, rowCount, function (err, data) {
                    if (err) {
                        res.json(result.create(errorCode.dataError, err));
                    }
                    else {
                        return res.json(result.create(errorCode.success, '', data));
                    }
                });
            }
        });

    app.post('/search/chatContact',
        [ensureLogin],
        function (req, res, next) {
            var user = req.session.passport.user;
            var userId = user.UserId;

            var data = req.body;

            var keyword = data.Keyword;
            var rowCount = data.RowCount;
            var followPriority = data.ByFollow;
            var groupPriority = data.ByGroup;
            

            if (!keyword) {
                return res.json(result.create(errorCode.parameterError, 'Keyword'));
            }
            else {
                if (!rowCount) {
                    rowCount = 20;
                }

                if (!followPriority || followPriority == 'false' || followPriority == 0) {
                    followPriority = false;
                }

                if (followPriority == 1 || followPriority == 'true') {
                    followPriority = true;
                }

                if (!groupPriority || groupPriority == 'false' || groupPriority == 0) {
                    groupPriority = false;
                }

                if (groupPriority == 1 || groupPriority == 'true') {
                    groupPriority = true;
                }

                console.log(keyword, followPriority, groupPriority);

                db.search.searchToChat(userId, followPriority, groupPriority, rowCount, keyword, (err, data) => {
                    if (err) {
                        res.json(result.create(errorCode.dataError, err));
                    }
                    else {
                        return res.json(result.create(errorCode.success, '', data));
                    }
                });
            }
        }
    );
}