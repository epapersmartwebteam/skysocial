﻿var async = require('async');

var logger = require('../helper/logHelper').logger;

var { URL, URLSearchParams } = require('url');

var db = require('../models/all');

var userHelper = require('../helper/userHelper');

var { ensureLogin, parseLoginResultToToken } = require('../helper/sessionLoginHelper');

var storageHelper = require('../helper/storageHelper');

var mailHelper = require('../helper/mailHelper');

var dateHelper = require('../helper/dateHelper');

var cryptoHelper = require('../helper/cryptoEngine');

var phoneHelper = require('../helper/phoneHelper');

var osHelper = require('../helper/osHelper');

var utilityHelper = require('../helper/utility');

var firebaseHelper = require('../helper/firebaseHelper');

var coreChatFirebase = require('../chat/firebaseHelper');
var corechatAPI = require('../chat/chatCore');

//var sns = require('../notify/sns');

var notify = require('../notify/caller');


module.exports = userAPI;

//var resultHelper = require('../models/result');

var errorCode = require('../errorCode');
var myConfig = require('../myConfig');
var systemUserId = myConfig.SystemUserId;
var roleAdmin = myConfig.RoleAdmin;
var roleMod = myConfig.RoleMod;

function userAPI(app, passport, resultHelper) {
    function checkLoginParameter(username, password) {
        var error;
        if (!username) {
            error = {};
            error.username = true;
        }
        if (!password) {
            if (!error) {
                error = {};
            }
            error.password = true;
        }
        return error;
    }
    //đăng nhập qua Email, UserName, Phone
    app.post('/user/login',
        function (req, res, next) {
            var params = req.body;

            var username = params.username;
            var password = params.password;

            var error = checkLoginParameter(username, password);
            if (error) {
                res.json(resultHelper.parameterError(error));
            }
            else {
                db.user.login(username, password, function (err, data) {
                    if (!err) {

                        //lấy os và ghi log
                        var os = osHelper.getOSFromRequest(req);
                        db.user.updateLoginLog(data.UserId, os, function (err1, data1) {
                        });
                        //try {
                        //    logger.debug('Login success:', data);
                        //}
                        //catch (ex) {
                        //}
                        //trả về ko cần chờ
                        if (data.Beta == true || data.RoleId == 1) {
                            data.ReleaseDate = 0;
                        }
                        else {
                            var now = dateHelper.getUnixTimeStamp();
                            data.ReleaseMessage = myConfig.releaseMessage;
                            if (now >= myConfig.releaseDate) {
                                data.ReleaseDate = 0;
                            }
                            else {
                                if (myConfig.releaseDate == -1) {
                                    data.ReleaseDate = myConfig.releaseDate;
                                }
                                else {
                                    var countDown = myConfig.releaseDate - dateHelper.getUnixTimeStamp();
                                    if (countDown < 0) {
                                        countDown = 0;
                                    }
                                    data.ReleaseDate = countDown;
                                }
                            }
                        }

                        var checkChatActive = (cb) => {
                            if (data.ChatActive) {
                                cb(true);
                            }
                            else {
                                logger.debug('need create chat');
                                if (!data.ChatCreateFailTime || data.ChatCreateFailTime < 10) {
                                    createCoreChatUser(data.UserId, data.UserName, data.Email, data.Phone, function (result) {
                                        if (result.ErrorCode == 0) {
                                            corechatAPI.updateUserAvatar(coreChatFirebase.getSkyUID(data.UserId), data.AvatarMedium, () => { });
                                            cb(true);
                                        }
                                        else {
                                            cb(false);
                                        }
                                    });
                                }
                                else {
                                    cb(false);
                                }
                            }
                        };

                        checkChatActive(function (checkResult) {
                            logger.debug('checkChatActive result', checkResult);
                            if (checkResult) {
                                //tạo custom token trên firebase corechat
                                coreChatFirebase.createCustomToken(coreChatFirebase.getSkyUID(data.UserId), (resultCT) => {
                                    logger.debug('createCustomToken');
                                    logger.debug(resultCT);
                                    if (resultCT.ErrorCode == errorCode.success) {
                                        data.CoreChatCustomToken = resultCT.Data;
                                    }
                                    var result = resultHelper.create(errorCode.success, 'Thành công', data);

                                    return res.json(parseLoginResultToToken(result, os));
                                });
                            }
                            else {
                                var result = resultHelper.create(errorCode.success, 'Thành công', data);
                                return res.json(parseLoginResultToToken(result, os));
                            }
                        });
                    }
                    else {
                        return res.json(err);
                    }
                });
            }
        });


    //đăng nhập thông qua Facebook
    app.post('/user/loginFacebook',
        passport.authenticate('localFacebook', { failWithError: true }),
        function (req, res, next) {

            res.json(resultHelper.create(errorCode.success, 'Thành công', req.session.passport.user));
        },
        function (err, req, res, next) {

            // handle error
            return res.json(err);
        });

    app.post('/user/setPassword',
        function (req, res, next) {
            //var args = req.body;
            //var password = args.Password;
            //var phone = args.Phone;
            //password = cryptoHelper.encryptText(password);
            //db.user.updateUserPasswordByPhone(phone, password, (err, result) => {
            //    if (err) {
            //        return res.json(resultHelper.dataError(err));
            //    }
            //    else {
            //        return res.json(resultHelper.success());
            //    }
            //});
        });

    function checkSignUpParameter(userName, password, email, phone, callback) {
        var hasMail = true, hasPhone = true;
        var userNameOK = true, passwordOK = true, emailOK = true, phoneOK = true;
        var userNameExist = false, emailExist = false, phoneExist = false;

        var checkPassword = (cb) => {
            if (password) {
                password = password.trim();
                if (password) {
                }
                else {
                    passwordOK = false;
                }
                cb();
            }
            else {
                passwordOK = false;
                cb();
            }
        };

        var checkUserName = (cb) => {
            if (userName) {
                if (userHelper.validateUserName(userName)) {
                    db.user.checkUserNameExists(userName, function (errCheckUserName, isUserNameExisted) {
                        if (errCheckUserName) {
                            userNameOK = false;
                            cb();
                        }
                        else {
                            userNameExist = (isUserNameExisted === 1);
                            cb();
                        }
                    });
                }
                else {
                    userNameOK = false;
                    cb();
                }
            }
            else {
                userNameOK = false;
                cb();
            }
        };

        var checkEmail = (cb) => {
            if (email) {
                if (utilityHelper.isEmail(email)) {
                    db.user.checkEmailExists(email, function (errCheck, isEmailExisted) {
                        if (errCheck) {
                            emailOK = false;
                            cb();
                        }
                        else {
                            emailExist = (isEmailExisted === 1);
                            cb();
                        }
                    });
                }
                else {
                    emailOK = false;
                    cb();
                }
            }
            else {
                hasMail = false;
                cb();
            }
        };

        var checkPhone = (cb) => {
            if (phone) {
                if (phoneHelper.isMobile(phone)) {
                    phone = phoneHelper.patchValidPhoneNumber(phone);
                    db.user.checkPhoneExists(phone, function (errCheck, isMobileExisted) {
                        if (errCheck) {
                            phoneOK = false;
                            cb();
                        }
                        else {
                            phoneExist = (isMobileExisted === 1);
                            cb();
                        }
                    });
                }
                else {
                    phoneOK = false;
                    cb();
                }
            }
            else {
                hasPhone = false;
                cb();
            }
        };

        async.parallel([checkPassword, checkUserName, checkEmail, checkPhone], (err) => {
            if ((passwordOK && userNameOK && emailOK && phoneOK && !userNameExist && !emailExist && !phoneExist) && (hasMail || hasPhone)) {
                callback(resultHelper.success({ password: password, userName: userName, email: email, phone: phone }));
            }
            else {
                var paraError = {
                    Invalid: [],
                    Existed: []
                };
                if (!passwordOK) {
                    paraError.Invalid.push('Password');
                }
                if (!userNameOK) {
                    paraError.Invalid.push('UserName');
                }
                if (!emailOK) {
                    paraError.Invalid.push('Email');
                }
                if (!phoneOK) {
                    paraError.Invalid.push('Phone');
                }

                if (userNameExist) {
                    paraError.Existed.push('UserName');
                }
                if (emailExist) {
                    paraError.Existed.push('Email');
                }
                if (phoneExist) {
                    paraError.Existed.push('Phone');
                }
                if (!hasMail && !hasPhone) {
                    paraError.AtleastOne = true;
                }

                callback(resultHelper.parameterError(paraError));
            }
        });
    }

    function createCoreChatUser(userId, userName, email, phone, callback) {
        coreChatFirebase.createUser(coreChatFirebase.getSkyUID(userId), userName, (resultCT) => {
            logger.debug('create user on firebase', resultCT);
            if (resultCT.ErrorCode == 0) {
                corechatAPI.createUser(coreChatFirebase.getSkyUID(userId), userName, email, phone, (resultCC) => {
                    logger.debug('create user chatcore', resultCC);
                    if (resultCC.ErrorCode == 0) {
                        //cập nhật DB
                        var chatUserId = resultCC.Data._id;
                        db.user.updateUserChatInfo(userId, chatUserId, (resUpdate) => {

                        });
                        callback(resultHelper.create(errorCode.success, ''));
                    }
                    else {
                        //cập nhật DB
                        db.user.updateUserChatCreateFailTime(userId, (resUpdate) => {

                        });
                        callback(resultHelper.create(errorCode.success, 'Chưa tạo được user bên ChatCore'));
                    }
                });
            }
            else {
                //cập nhật DB
                db.user.updateUserChatCreateFailTime(userId, (resUpdate) => {

                });
                callback(resultHelper.create(errorCode.success, 'Chưa tạo được user bên ChatCore'));
            }
        });
    }

    app.post('/user/signup',
        function (req, res, next) {
            var args = req.body;
            var userName = args.UserName;
            var email = args.Email;
            var phone = args.Phone;
            var password = args.Password;
            var gender = args.Gender;

            var os = args.OS;
            var countryId = args.CountryId;
            var countryName = args.CountryName;

            var facebookId = '';
            var fullName = '';
            var isActive = 1;

            checkSignUpParameter(userName, password, email, phone, (result) => {

                if (result.ErrorCode == errorCode.success) {
                    var data = result.Data;
                    email = data.email;
                    userName = data.userName;
                    password = data.password;
                    phone = data.phone;
                    db.user.signup(email, userName, password, phone, facebookId, fullName, gender, countryId, countryName, os, isActive, function (err, userId) {

                        if (err) {
                            return res.json(resultHelper.create(errorCode.dataError, ''));
                        }
                        else {
                            db.user.addFollower(userId, systemUserId, true, function (err0, dumbass) { });
                            if (email) {
                                firebaseHelper.sendVerifiedLinkEmail(userId, email, () => {

                                });
                            }

                            //tạo user bên core chat
                            createCoreChatUser(userId, userName, email, phone, function (result) {
                                res.json(result);
                            });

                        }
                    });
                }
                else {

                    return res.json(result);
                }
            });
        }
    );
    //gửi mail quên mật khẩu
    app.post('/user/forgetPassword',
        function (req, res, next) {

            var email = req.query.Email;
            db.user.checkEmailExists(email, function (errCheckEmail, isEmailExisted) {
                if (errCheckEmail) {
                    return res.json(resultHelper.create(errorCode.dataNotExisted, 'Email không tồn tại'));
                }
                else {
                    if (isEmailExisted) {
                        mailHelper.sendForgetPasswordMail(email, function (err, dumb) {
                            if (!err) {
                                return res.json(resultHelper.create(errorCode.success, ''));
                            }
                            else {
                                return res.json(resultHelper.create(errorCode.cloudError, ''));
                            }
                        });
                    }
                    else {
                        return res.json(resultHelper.create(errorCode.loginUserNameNotMatch, ''));
                    }
                }
            });
        });

    //Đăng ký thông qua Facebook
    app.post('/user/signupByFacebook',
        function (req, res, next) {
            var facebookId = req.query.FacebookId;
            var username = req.query.UserName;
            var password = req.query.Password;
            var os = req.query.OS;
            var countryId = req.query.CountryId;
            var countryName = req.query.CountryName;
            var email = req.query.Email;
            var phone = req.query.Phone;

            var gender = req.query.Gender;

            var isActive = 1;



            var fullName = req.query.FullName;

            if (fullName == undefined) {
                fullName = '';
            }

            //kiểm tra FacebookId tồn tại
            db.user.checkFacebookIdExists(facebookId, function (errCheckFacebookId, isFacebookIdExisted) {

                if (errCheckFacebookId) {
                    return res.json(resultHelper.create(errorCode.checkingError, 'FacebookId'));
                }
                else {

                    //nếu FacebookId không tồn tại thì tiếp tục
                    if (isFacebookIdExisted === 0) {

                        db.user.checkEmailExists(email, function (errCheckEmail, isEmailExisted) {

                            if (errCheckEmail) {
                                return res.json(resultHelper.create(errorCode.checkingError, 'Email'));
                            }
                            else {

                                //đã tồn tại email rồi thì update facebookId vào tài khoản của email đó
                                if (isEmailExisted) {
                                    db.user.updateUserFacebookId(email, facebookId, function (err1, dumb) {
                                        if (!err1) {
                                            return res.json(resultHelper.create(errorCode.success, ''));
                                        }
                                        else {
                                            return res.json(resultHelper.create(errorCode.dataError, err1));
                                        }
                                    });
                                }

                                //ngược lại thì mới tiếp tục tạo tài khoản mới
                                else {

                                    //kiểm tra username tồn tại
                                    db.user.checkUserNameExists(username, function (errCheckName, isNameExisted) {
                                        if (errCheckName) {
                                            return res.json(resultHelper.create(errorCode.checkingError, 'Username'));
                                        }
                                        else {

                                            //nếu UserName chưa tồn tại thì tiếp tục
                                            if (isNameExisted === 0) {
                                                db.user.signup(email, username, password, phone, facebookId, fullName, gender, countryId, countryName, os, isActive, function (err, userId) {
                                                    if (err) {
                                                        return res.json(resultHelper.create(errorCode.dataError, ''));
                                                    }
                                                    else {

                                                        //cập nhật User đã có trên Fiseo                                                        
                                                        db.facebook.updateFacebookFriendIsOnFiseo(facebookId, username, function (errX, fuckingdumbass) {
                                                            //todo: notify đến những User khác là bạn của User mới đăng kí trên Facebook
                                                        });

                                                        db.user.addFollower(userId, systemUserId, true, function (err0, dumbass) { });
                                                        return res.json(resultHelper.create(errorCode.success, ''));
                                                    }
                                                });
                                            }
                                            else {
                                                return res.json(resultHelper.create(errorCode.dataExisted, 'UserName'));
                                            }
                                        }
                                    });
                                }
                            }
                        });
                    }

                    //nếu tồn tại rồi thì trả về thành công, để client đăng nhập
                    else {
                        return res.json(resultHelper.create(errorCode.success, ''));
                    }
                }
            });
        });

    //đăng ký bằng Email
    app.post('/user/signupByEmail',
        function (req, res, next) {
            var email = req.query.Email;
            var username = req.query.UserName;
            var password = req.query.Password;
            var os = req.query.OS;
            var countryId = req.query.CountryId;
            var countryName = req.query.CountryName;

            var gender = req.query.Gender;

            var facebookId = '';
            var phone = '';
            var isActive = 1;

            var fullName = req.query.FullName;

            if (fullName == undefined) {
                fullName = '';
            }
            //kiểm tra email tồn tại
            db.user.checkEmailExists(email, function (errCheckEmail, isEmailExisted) {
                if (errCheckEmail) {
                    return res.json(resultHelper.create(errorCode.checkingError, 'Email!'));
                }
                else {

                    //nếu email không tồn tại thì tiếp tục
                    if (isEmailExisted === 0) {

                        //kiểm tra username tồn tại
                        db.user.checkUserNameExists(username, function (errCheckName, isNameExisted) {
                            if (errCheckName) {
                                return res.json(resultHelper.create(errorCode.checkingError, 'Username'));
                            }
                            else {

                                //nếu UserName chưa tồn tại thì tiếp tục
                                if (isNameExisted === 0) {
                                    db.user.signup(email, username, password, phone, facebookId, fullName, gender, countryId, countryName, os, isActive, function (err, userId) {
                                        if (err) {
                                            return res.json(resultHelper.create(errorCode.dataError, ''));
                                        }
                                        else {
                                            db.user.addFollower(userId, systemUserId, true, function (err0, dumbass) { });
                                            return res.json(resultHelper.create(errorCode.success, ''));
                                        }
                                    });
                                }
                                else {
                                    return res.json(resultHelper.create(errorCode.dataExisted, 'UserName'));
                                }
                            }
                        });
                    }
                    else {
                        return res.json(resultHelper.create(errorCode.dataExisted, 'Email'));
                    }
                }
            });
        });

    //đăng ký bằng số phone
    app.post('/user/signupByPhone',
        function (req, res, next) {
            var phone = req.query.Phone;
            var username = req.query.UserName;
            var password = req.query.Password;
            var os = req.query.OS;
            var countryId = req.query.CountryId;
            var countryName = req.query.countryName;

            var gender = req.query.Gender;

            var facebookId = '';
            var email = '';
            var isActive = 1;

            var fullName = req.query.FullName;

            if (fullName == undefined) {
                fullName = '';
            }

            //kiểm tra phone tồn tại
            db.user.checkPhoneExists(phone, function (errCheckPhone, isPhoneExisted) {
                if (errCheckPhone) {
                    return res.json(resultHelper.create(errorCode.checkingError, 'Phone'));
                }
                else {

                    //nếu phone không tồn tại thì tiếp tục
                    if (isPhoneExisted === 0) {

                        //kiểm tra username tồn tại
                        db.user.checkUserNameExists(username, function (errCheckName, isNameExisted) {
                            if (errCheckName) {
                                return res.json(resultHelper.create(errorCode.checkingError, 'Username'));
                            }
                            else {

                                //nếu UserName chưa tồn tại thì tiếp tục
                                if (isNameExisted === 0) {
                                    db.user.signup(email, username, password, phone, facebookId, fullName, gender, countryId, countryName, os, isActive, function (err, userId) {
                                        if (err) {
                                            return res.json(resultHelper.create(errorCode.dataError, ''));
                                        }
                                        else {
                                            db.user.addFollower(userId, systemUserId, true, function (err0, dumbass) { });
                                            return res.json(resultHelper.create(errorCode.success, ''));
                                        }
                                    });
                                }
                                else {
                                    return res.json(resultHelper.create(errorCode.dataExisted, 'UserName'));
                                }
                            }
                        });
                    }
                    else {
                        return res.json(resultHelper.create(errorCode.dataExisted, 'Phone'));
                    }
                }
            });
        });

    //kiểm tra email đã tồn tại chưa, trả về true / false
    app.post('/user/checkEmailExists',
        function (req, res, next) {

            var email = req.query.Email;

            db.user.checkEmailExists(email, function (errCheckEmail, isEmailExisted) {
                if (errCheckEmail) {
                    return res.json(resultHelper.create(errorCode.checkingError, 'Email!'));
                }
                else {
                    return res.json(resultHelper.create(errorCode.success, '', (isEmailExisted === 1)));
                }
            });
        });

    //Kiểm tra Username đã tồn tại chưa, trả về true / false
    app.post('/user/checkUserNameExists',
        function (req, res, next) {

            var userName = req.query.UserName;

            if (userHelper.validateUserName(userName)) {
                db.user.checkUserNameExists(userName, function (errCheckUserName, isUserNameExisted) {
                    if (errCheckUserName) {
                        return res.json(resultHelper.create(errorCode.checkingError, 'Username!'));
                    }
                    else {
                        return res.json(resultHelper.create(errorCode.success, 'Thành công', (isUserNameExisted === 1)));
                    }
                });
            }
            else {
                return res.json(resultHelper.create(errorCode.dataFormatError, 'UserName'));
            }
        }
    );

    //Kiểm tra số điên thoại đã tồn tại chưa, trả về true / false
    app.post('/user/checkPhoneExists',
        function (req, res, next) {
            var phone = req.query.Phone;

            if (phone && phone.indexOf(' ') == 0) {
                phone = '+' + phone.substring(1);
            }

            db.user.checkPhoneExists(phone, function (errCheckPhone, isPhoneExisted) {

                if (errCheckPhone) {
                    return res.json(resultHelper.create(errorCode.checkingError, 'Phone!'));
                }
                else {
                    return res.json(resultHelper.create(errorCode.success, '', (isPhoneExisted === 1)));
                }
            });
        });

    //kiểm tra FacebookId đã tồn tại chưa, trả về true / fasle
    app.post('/user/checkFacebookIdExists',
        function (req, res, next) {
            var facebookId = req.query.FacebookId;
            db.user.checkFacebookIdExists(facebookId, function (errCheckFacebookId, isFacebookIdExisted) {
                if (errCheckFacebookId) {
                    return res.json(resultHelper.create(errorCode.checkingError, 'FacebookId!'));
                }
                else {
                    return res.json(resultHelper.create(errorCode.success, '', (isFacebookIdExisted === 1)));
                }
            });
        });

    //Lấy đường dẫn SAS của Avatar để upload lên Azure
    app.post('/user/getAvatarUploadSAS',
        [ensureLogin],
        function (req, res, next) {

            var itemGUID = req.query.ItemGUID;
            var imageExtension = req.query.ImageExtension;
            var userGUID = req.session.passport.user.UserGUID;

            storageHelper.generateUploadAvatarSAS(userGUID, itemGUID, imageExtension, function (err, data) {
                if (err) {
                    return res.json(resultHelper.create(errorCode.checkingError, err));
                }
                else {
                    return res.json(resultHelper.create(errorCode.success, '', data));
                }
            });
        });

    /*********
     * 
     *****/
    app.post('/user/info',
        [ensureLogin],
        function (req, res, next) {
            var user = req.session.passport.user;
            return res.json(resultHelper.success(user));
        });


    app.post('/user/updateCommentingOff',
        [ensureLogin],
        function (req, res, next) {
            var user = req.session.passport.user;
            var userId = user.UserId;
            var args = req.query;

            var commentingOff = args.CommentingOff;

            db.user.updateUserCommentingOff(userId, commentingOff, (err, data) => {
                if (err) {
                    return res.json(resultHelper.create(errorCode.dataError, err));
                }
                else {
                    return res.json(resultHelper.success());
                }
            });
        }
    );
    /***
    User Report
    ***/

    app.post('/user/report',
        [ensureLogin],
        function (req, res, next) {
            var user = req.session.passport.user;
            var userId = user.UserId;
            var reportTypeId = req.query.ReportTypeId;
            var targetUserId = req.query.UserId;
            var comment = '';
            var os = user.OS;

            db.userReport.addUserReport(userId, reportTypeId, targetUserId, comment, os, function (err, reportId) {
                if (err) {
                    res.json(resultHelper.create(errorCode.dataError, err));
                }
                else {
                    //nếu người Report User là tài khoản Fiseo Admin thì Banned tài khoản bị report đồng thời xóa tất cả hình của User bị report
                    if (user.RoleId == roleAdmin && targetUserId != systemUserId) {


                        db.user.updateUserIsBanned(targetUserId, true, userId, function (err1, rec1) {
                        });

                        //khoan add, nhiều khi admin ngứa tay report gây lỗi ko đăng nhập lại đc
                        //db.userEndpoint.addUserDevicesToBlackList(targetUserId, function (err3, rec3) {
                        //});

                        db.imageItem.deleteUserImageItem(targetUserId, userId, function (err2, rec2) {
                        });

                        db.userReport.approveUserReport(reportId, true, true, true, userId, () => {
                        });
                    }
                    return res.json(resultHelper.create(errorCode.success, '', { Success: true }));
                }
            });
        });

    app.post('/user/setBan',
        [ensureLogin],
        function (req, res, next) {
            var user = req.session.passport.user;
            var userId = user.UserId;
            var targetUserId = req.body.UserId;

            if ((user.RoleId == roleAdmin || user.RoleId == roleMod) && targetUserId != systemUserId) {

                db.user.updateUserIsBanned(targetUserId, true, userId, function (err1, rec1) {
                });

                db.imageItem.deleteUserImageItem(targetUserId, userId, function (err2, rec2) {
                });
            }
        }
    );
    /**
    User Endpoint
    */

    /***
    tạm gán OS userId = 250077 thành iOSSandbox để test SNS khi develop trong các hàm addUserEndpoint, deleteUserEndpoint, getUserNotifySetting, setUserNotifySetting
    ****/

    //thêm user endpoint, nếu đã có thì thôi, nếu chưa có token thì xóa các endpoint có cùng deviceId của User và thêm mới lại
    app.post('/user/addUserEndpoint',
        [ensureLogin],
        function (req, res, next) {
            var user = req.session.passport.user;
            var userId = user.UserId;
            var os = user.OS;
            var deviceId = req.query.DeviceId;
            var onesignalId = req.query.OnesignalId;
            var onesignalAppId = req.query.OnesignalAppId;
            var languageCode = req.query.LanguageCode;

            //if (userId == 250077) {
            //    os = 'iOSSandbox';
            //}

            db.userEndpoint.checkUserEndpointExists(userId, os, deviceId, onesignalId, function (err, isExisted) {

                //cập nhật
                notify.updateUserTagLanguage(userId, onesignalId, languageCode, () => {
                });

                db.userEndpoint.deleteOtherUserEndpoint(userId, deviceId, onesignalId, () => {
                });

                if (isExisted) {
                    return res.json(resultHelper.create(errorCode.success, '', { Success: true }));
                }
                else {

                    //sns.createPlatformEndpointARN(os, token, function (error, endpoint) {
                    //    if (!error) {



                    db.userEndpoint.addUserEndpoint(userId, os, deviceId, onesignalAppId, onesignalId, languageCode, function (err1, dumb) {
                        if (!err1) {
                            return res.json(resultHelper.create(errorCode.success, '', { Success: true }));
                        }
                        else {
                            if (err1.ErrorCode == errorCode.notPermission) {
                                req.session.passport.user.IsBanned = true;
                            }

                            return res.json(resultHelper.create(errorCode.dataError, err1, { Success: false }));

                        }
                    });
                    //    }
                    //    else {
                    //        return res.json(result.create(errorCode.cloudError, error, { Success: false }));
                    //    }
                    //});
                }
            });
        });

    //xóa user endpoint
    app.post('/user/deleteUserEndpoint',
        [ensureLogin],
        function (req, res, next) {
            var user = req.session.passport.user;
            var userId = user.UserId;
            var os = user.OS;
            var deviceId = req.query.DeviceId;

            //if (userId == 250077) {
            //    os = 'iOSSandbox';
            //}

            db.userEndpoint.deleteUserEndpoint(userId, os, deviceId, function (err, dumb) {
                if (!err) {
                    return res.json(resultHelper.create(errorCode.success, '', { Success: true }));
                }
                else {
                    return res.json(resultHelper.create(errorCode.dataError, err, { Success: false }));
                }
            });
        }
    );

    /**
    Cập nhật ngôn ngữ của device, cập nhật toàn bộ endpoint của User trên Device
    */
    app.post('/user/updateDeviceLanguage',
        [ensureLogin],
        function (req, res, next) {
            var user = req.session.passport.user;
            var userId = user.UserId;
            var deviceId = req.query.DeviceId;
            var languageCode = req.query.LanguageCode;
            var onesignalId = req.query.OnesignalId;

            //cập nhật
            notify.updateUserTagLanguage(onesignalId, languageCode, () => {
            });

            db.userEndpoint.updateDeviceUserEnpointLanguage(userId, deviceId, languageCode, function (err, dumb) {
                if (!err) {
                    return res.json(resultHelper.create(errorCode.success, '', { Success: true }));
                }
                else {



                    return res.json(resultHelper.create(errorCode.dataError, err, { Success: false }));
                }
            });
        });

    /**
    User notify setting
    */
    //Lấy user notify setting
    app.post('/user/getUserNotifySetting',
        [ensureLogin],
        function (req, res, next) {
            var user = req.session.passport.user;
            var userId = user.UserId;
            var os = user.OS;

            if (userId == 250077) {
                os = 'iOSSandbox';
            }

            db.userNotify.getUserNotifySetting(userId, os, function (err, data) {
                if (!err) {
                    return res.json(resultHelper.create(errorCode.success, '', data));
                }
                else {
                    return res.json(resultHelper.create(errorCode.dataError, err, null));
                }
            });
        });

    //cập nhật cấu hình notify
    app.post('/user/setUserNotifySetting',
        [ensureLogin],
        function (req, res, next) {
            var user = req.session.passport.user;
            var userId = user.UserId;
            var os = user.OS;
            var newComment = utilityHelper.convertToBool(req.query.NewComment);
            var newLike = utilityHelper.convertToBool(req.query.NewLike);
            var newFollower = utilityHelper.convertToBool(req.query.NewFollower);
            var followAccepted = utilityHelper.convertToBool(req.query.FollowAccepted);
            var followUploadImage = utilityHelper.convertToBool(req.query.FollowUploadImage);
            var mentionInComment = utilityHelper.convertToBool(req.query.MentionInComment);
            var commentHasReply = utilityHelper.convertToBool(req.query.CommentHasReply);
            var commentHasLike = utilityHelper.convertToBool(req.query.CommentHasLike);
            var followingAction = utilityHelper.convertToBool(req.query.FollowingAction);

            var chat = utilityHelper.convertToBool(req.query.Chat);

            db.userNotify.setUserNotifySetting(userId, newComment, newLike, newFollower, followAccepted, followUploadImage,
                mentionInComment, commentHasReply, commentHasLike, followingAction, chat, os, function (err, data) {
                    if (!err) {
                        return res.json(resultHelper.create(errorCode.success, '', { Success: true }));
                    }
                    else {
                        return res.json(resultHelper.create(errorCode.dataError, err, { Success: false }));
                    }
                });
        });

    /**
    User notify
    */
    app.post('/user/getUserNotifyCount',
        [ensureLogin],
        function (req, res) {
            var user = req.session.passport.user;
            var userId = user.UserId;
            db.userNotify.getUserNotifyCount(userId, function (err, data) {
                if (!err) {
                    return res.json(resultHelper.create(errorCode.success, '', { Count: data }));
                }
                else {
                    return res.json(resultHelper.create(errorCode.dataError, err, null));
                }
            });
        }
    );
    //lấy danh sách user notify
    app.post('/user/getUserNotify',
        [ensureLogin],
        function (req, res, next) {

            var user = req.session.passport.user;
            var userId = user.UserId;
            var os = user.OS;
            var isViewed = req.query.IsViewed;
            var lastId = req.query.LastId;
            var itemCount = req.query.ItemCount;

            db.userNotify.getUserNotify(userId, isViewed, lastId, itemCount, function (err, data) {
                if (!err) {
                    return res.json(resultHelper.create(errorCode.success, '', data));
                }
                else {
                    return res.json(resultHelper.create(errorCode.dataError, err, null));
                }
            });
        });

    //cập nhật user notify đã xem
    app.post('/user/updateUserNotifyIsView',
        [ensureLogin],
        function (req, res, next) {
            var user = req.session.passport.user;
            var userId = user.UserId;
            var os = user.OS;
            var notifyId = req.query.NotifyId;

            db.userNotify.updateUserNotifyIsView(notifyId, function (err, data) {
                if (!err) {
                    return res.json(resultHelper.create(errorCode.success, '', { Success: true }));
                }
                else {
                    return res.json(resultHelper.create(errorCode.dataError, err, null));
                }
            });
        });

    //cập nhật danh sách user notify đã xem
    app.post('/user/updateUserNotifyListIsView',
        [ensureLogin],
        function (req, res, next) {
            var user = req.session.passport.user;
            var userId = user.UserId;
            var os = user.OS;
            var notifyIdList = req.query.NotifyIdList;

            if (notifyIdList) {
                notifyIdList = notifyIdList.split(',');
            }

            db.userNotify.updateUserNotifyListIsView(notifyIdList, function (err, data) {
                if (!err) {
                    return res.json(resultHelper.create(errorCode.success, '', { Success: true }));
                }
                else {
                    return res.json(resultHelper.create(errorCode.dataError, err, null));
                }
            });
        });

    //cập nhật tất cả user notify sang đã xem
    app.post('/user/updateAllUserNotifyIsView',
        [ensureLogin],
        function (req, res, next) {
            var user = req.session.passport.user;
            var userId = user.UserId;
            var os = user.OS;

            db.userNotify.updateAllUserNotifyIsView(userId, function (err, data) {
                if (!err) {
                    return res.json(resultHelper.create(errorCode.success, '', { Success: true }));
                }
                else {
                    return res.json(resultHelper.create(errorCode.dataError, err, null));
                }
            });
        });

    app.post('/user/updateFollowingPushNotifySetting',
        [ensureLogin],
        function (req, res, next) {
            var user = req.session.passport.user;
            var fromUserId = user.UserId;
            var toUserId = req.query.UserId;

            var newPost = utilityHelper.convertToBool(req.query.NewPost);
            var newActivity = utilityHelper.convertToBool(req.query.NewActivity);

            db.user.updateFollowerPushNotify(fromUserId, toUserId, newPost, newActivity, function (err, data) {
                if (!err) {
                    return res.json(resultHelper.create(errorCode.success, '', { Success: true }));
                }
                else {
                    return res.json(resultHelper.create(errorCode.dataError, err, null));
                }
            });
        });

    app.post('/user/getFollowingPushNotifySetting',
        [ensureLogin],
        function (req, res, next) {
            var user = req.session.passport.user;
            var fromUserId = user.UserId;
            var toUserId = req.query.UserId;

            db.user.getFollowerPushNotify(fromUserId, toUserId, function (err, data) {
                if (!err) {
                    return res.json(resultHelper.create(errorCode.success, '', data));
                }
                else {
                    return res.json(resultHelper.create(errorCode.dataError, err, null));
                }
            });
        });


    /**
    Facebook friends
    */
    //lấy danh sách friend trên facebook, phân trang trả về
    app.post('/user/getFacebookFriends',
        [ensureLogin],
        function (req, res, next) {
            var user = req.session.passport.user;
            var facebookId = user.FacebookId;

            var inApp = req.query.InApp;
            var isInvited = req.query.IsInvited;
            var isHide = req.query.IsHide;
            var lastId = req.query.LastId;
            var count = req.query.RowCount;

            db.facebook.getFacebookFriends(facebookId, inApp, isInvited, isHide, lastId, count, function (err, data) {
                if (err) {
                    res.json(resultHelper.create(errorCode.dataError, err, null));
                }
                else {
                    res.json(resultHelper.create(errorCode.success, null, data));
                }
            });
        });

    //cập nhật danh sách friend, tối đa nên để 100 / 1 request để tránh vượt quá giới hạn 1 lần request
    app.post('/user/updateFacebookFriends',
        [ensureLogin],
        function (req, res, next) {
            var user = req.session.passport.user;
            var facebookId = user.FacebookId;

            if (facebookId != undefined) {
                var friends = req.body.Friends;

                var len = 0;
                if (Array.isArray(friends)) {
                    len = friends.lenght;
                }

                for (var i = 0; i < len; i++) {
                    var friend = friends[i];
                    db.facebook.UpdateFacebookFriend(facebookId, friend.Id, friend.Name, friend.Avatar, function (err, data) {
                        //ko làm gì cả vì ko đợi cập nhật xong
                    });
                }

                return res.json(resultHelper.create(errorCode.success, '', null));
            }
            else {
                return res.json(resultHelper.create(errorCode.dataError, 'FacebookLogin', null));
            }
        });

    //cập nhật trạng thái đã mời tham gia App trên facebook
    app.post('/user/updateFacebookFriendIsInvited',
        [ensureLogin],
        function (req, res, next) {
            var user = req.session.passport.user;
            var facebookId = user.FacebookId;

            var friendFacebookId = req.query.FacebookId;

            db.facebook.updateFacebookFriendIsInvited(facebookId, friendFacebookId, function (err, data) {
                res.json(resultHelper.create(errorCode.success, null, { Success: err ? false : true }));
            });
        }
    );

    //cập nhật trạng thái ẩn user không hiển thị trong danh sách mời hoặc suggest
    app.post('/user/updateFacebookFriendIsHide',
        [ensureLogin],
        function (req, res, next) {
            var user = req.session.passport.user;
            var facebookId = user.FacebookId;

            var friendFacebookId = req.query.FacebookId;

            db.facebook.updateFacebookFriendIsHide(facebookId, friendFacebookId, function (err, data) {
                res.json(resultHelper.create(errorCode.success, null, { Success: err ? false : true }));
            });
        }
    );

    app.post('/user/media/checkCanPlay',
        [ensureLogin],
        function (req, res, next) {
            var user = req.session.passport.user;
            var userId = user.UserId;
            var os = user.OS;

            var args = req.body;
            var mediaId = args.MediaId;

            db.userLibrary.checkUserCanPlay(userId, mediaId, (result) => {
                return res.json(result);
            });
        }
    );

    /////////////////////////////////////////
    //user library
    app.post('/user/library/list',
        [ensureLogin],
        function (req, res, next) {
            var user = req.session.passport.user;
            var userId = user.UserId;
            var os = user.OS;

            var args = req.body;
            var isBuy = args.IsBuy;
            var isFree = args.IsFree;
            var lastDate = args.LastDate;
            var itemCount = args.ItemCount;

            db.userLibrary.getUserLibraries(userId, isBuy, isFree, lastDate, itemCount, (result) => {
                return res.json(result);
            });
        }
    );

    app.post('/user/library/checkExists',
        [ensureLogin],
        function (req, res, next) {
            var user = req.session.passport.user;
            var userId = user.UserId;
            var os = user.OS;
            var args = req.body;

            var mediaId = args.MediaId;

            db.userLibrary.getUserLibrary(userId, mediaId, (result) => {
                if (result.ErrorCode == errorCode.success) {
                    var item = result.Data;
                    if (item) {
                        return res.json(resultHelper.success(true));
                    }
                    else {
                        return res.json(resultHelper.success(false));
                    }
                }
                else {
                    return res.json(resultHelper.success(false));
                }
            });
        }
    );

    app.post('/user/library/add',
        [ensureLogin],
        function (req, res, next) {
            var user = req.session.passport.user;
            var userId = user.UserId;
            var os = user.OS;
            var args = req.body;

            var mediaId = args.MediaId;
            var isFree = args.IsFree;
            var isView = args.IsView;

            db.userLibrary.addUserLibrary(userId, mediaId, isFree, 0, isView, os, (result) => {
                return res.json(result);
            });
        }
    );

    app.post('/user/library/updateView',
        [ensureLogin],
        function (req, res, next) {
            var user = req.session.passport.user;
            var userId = user.UserId;
            var os = user.OS;
            var args = req.body;

            var itemId = args.ItemId;

            db.userLibrary.updateUserLibraryView(userId, itemId, (result) => {
                return res.json(result);
            });
        }
    );

    app.post('/user/library/updateFavorite',
        [ensureLogin],
        function (req, res, next) {
            var user = req.session.passport.user;
            var userId = user.UserId;
            var os = user.OS;
            var args = req.body;

            var itemId = args.ItemId;
            var isFavorite = args.IsFavorite;

            db.userLibrary.updateUserLibraryIsFavorite(userId, itemId, isFavorite, os, (result) => {
                return res.json(result);
            });
        }
    );

    app.post('/user/library/updateArchived',
        [ensureLogin],
        function (req, res, next) {
            var user = req.session.passport.user;
            var userId = user.UserId;
            var os = user.OS;
            var args = req.body;

            var itemId = args.ItemId;
            var isArchived = args.IsArchived;

            db.userLibrary.updateUserLibraryIsArchived(userId, itemId, isArchived, os, (result) => {
                return res.json(result);
            });
        }
    );

    app.post('/user/library/delete',
        [ensureLogin],
        function (req, res, next) {
            var user = req.session.passport.user;
            var userId = user.UserId;
            var os = user.OS;
            var args = req.body;

            var itemId = args.ItemId;

            db.userLibrary.deleteUserLibrary(userId, itemId, (result) => {
                return res.json(result);
            });
        }
    );

    app.post('/user/suggest/getHashtags',
        [ensureLogin],
        function (req, res, next) {
            var user = req.session.passport.user;
            var userId = user.UserId;
            var os = user.OS;
            var args = req.body;

            var categoryId = args.CategoryId;
            db.userHashtag.getSystemHashtags(userId, categoryId, (err, docs) => {
                if (err) {
                    res.json(resultHelper.dataError(err));
                }
                else {
                    res.json(resultHelper.success(docs));
                }
            });
        }
    );

    app.post('/user/suggest/getHashtagCategory',
        [ensureLogin],
        function (req, res, next) {
            var user = req.session.passport.user;
            var userId = user.UserId;
            var os = user.OS;
            var args = req.body;

            db.userHashtag.getSystemHashtagCategorys((err, docs) => {
                if (err) {
                    res.json(resultHelper.dataError(err));
                }
                else {
                    res.json(resultHelper.success(docs));
                }
            });
        }
    );

    app.post('/user/suggest/hashtagSelected',
        [ensureLogin],
        function (req, res) {
            var user = req.session.passport.user;
            var userId = user.UserId;

            db.user.updateUserHashtagSuggested(userId, (err) => {
                if (err) {
                    res.json(resultHelper.dataError(err));
                }
                else {
                    res.json(resultHelper.success());
                }
            });
        }
    );

    app.post('/user/suggest/setHashtags',
        [ensureLogin],
        function (req, res) {
            var user = req.session.passport.user;
            var userId = user.UserId;

            var args = req.body;
            var categoryId = args.CategoryId;
            var hashtags = args.Hashtags;

            if (categoryId && hashtags && typeof (hashtags) == 'object') {
                db.userHashtag.setUserSystemHashtags(userId, categoryId, hashtags, (err) => {
                    if (err) {
                        res.json(resultHelper.dataError(err));
                    }
                    else {
                        db.userHashtag.followHashtag(userId, hashtags, false, (errX) => {
                        });
                        res.json(resultHelper.success());
                    }
                });
            }
            else {
                var err = {
                    categoryId: categoryId ? false : true,
                    hashtags: (hashtags && typeof (hashtags) == 'object') ? false : true
                };
                res.json(resultHelper.parameterError(err));
            }
        }
    );

    app.post('/user/verification/sendEmail',
        [ensureLogin],
        function (req, res) {
            var user = req.session.passport.user;
            var userId = user.UserId;
            db.user.getUserById(userId, ["Email"], (err, userInfo) => {
                if (userInfo) {
                    var email = userInfo.Email;
                    if (email) {
                        firebaseHelper.sendVerifiedLinkEmail(userId, email, (result) => {
                            res.json(result);
                        });
                    }
                    else {
                        res.json(resultHelper.notExists('User not have email'));
                    }
                }
                else {
                    res.json(resultHelper.notExists('User not exists'));
                }
            });
        }
    );

    app.post('/user/verification/verifyEmail',
        function (req, res) {
            var args = req.body;
            var url = args.Url;

            if (url) {
                var urlX = new URL(url);

                var searchParams = new URLSearchParams(urlX.search);

                if (searchParams.has('continueUrl')) {
                    var urlC = new URL(searchParams.get('continueUrl'));

                    var searchParam2 = new URLSearchParams(urlC.search);

                    if (searchParam2.has('id')) {

                        var x = searchParam2.get('id');

                        x = x.replace(/\s/g, '+');

                        var query = cryptoHelper.decryptText(decodeURI(x));

                        if (query) {

                            var pp = new URLSearchParams(query);
                            if (pp.has('email') && pp.has('userId')) {
                                var email = pp.get('email');
                                var userId = pp.get('userId');

                                firebaseHelper.verifyLink(email, url, (result) => {
                                    if (result.ErrorCode == 0) {
                                        db.user.updateEmailVerified(userId, email, (err) => {
                                            if (!err) {
                                                res.json(resultHelper.success({}));
                                            }
                                            else {
                                                res.json(resultHelper.dataError('Lỗi khi cập nhật xác thực Email'));
                                            }
                                        });
                                    }
                                    else {
                                        res.json(resultHelper.dataError('Link không hợp lệ hoặc đã quá hạn'));
                                    }
                                });
                            }
                            else {
                                res.json(resultHelper.dataError('Link không hợp lệ'));
                            }
                        }
                        else {
                            res.json(resultHelper.dataError('Link không hợp lệ'));
                        }
                    }
                    else {
                        res.json(resultHelper.parameterError({ Url: true }));
                    }
                }
                else {
                    res.json(resultHelper.parameterError({ Url: true }));
                }
            }
            else {
                res.json(resultHelper.parameterError({ Url: true }));
            }

        }
    );

    app.post('/user/sendResetPassword',
        (req, res) => {
            var args = req.body;
            var userName = args.UserName;

            if (userName) {
                db.user.getUserByUserName(userName, [], (err, user) => {
                    if (err) {
                        res.json(resultHelper.dataError(err));
                    }
                    else {

                        if (user) {
                            var userId = user.UserId;
                            var email = user.Email;
                            if (email) {
                                firebaseHelper.sendResetPasswordEmail(userId, email, (result) => {
                                    res.json(result);
                                });
                            }
                            else {
                                res.json(resultHelper.notExists('User không có Email. Không thể gửi mail reset password'));
                            }
                        }
                        else {
                            res.json(resultHelper.notExists());
                        }
                    }
                });
            }
            else {
                res.json(resultHelper.parameterError({
                    UserName: !userName
                }));
            }
        }
    );

    function getUserIdAndEmailFromUrl(url) {
        var result = { email: '', userId: 0 };

        url = decodeURI(url);

        var urlX = new URL(url);
        var searchParams = new URLSearchParams(urlX.search);
        if (searchParams.has('id')) {
            var x = searchParams.get('id');
            var query = cryptoHelper.decryptText(decodeURI(x));
            if (query) {
                var pp = new URLSearchParams(query);
                if (pp.has('email') && pp.has('userId')) {
                    var email = pp.get('email');
                    var userId = pp.get('userId');
                    result.email = email;
                    result.userId = userId;

                }
            }
        }
        return result;
    }

    app.post('/user/verifyPasswordResetCode',
        function (req, res, next) {
            var args = req.body;
            var continueUrl = args.ContinueUrl;
            var actionCode = args.ActionCode;

            firebaseHelper.verifyPasswordResetCode(actionCode, (result) => {
                if (result.ErrorCode == 0) {
                    var email = result.Data.Email;
                    if (email) {
                        email = email.toLowerCase();
                    }
                    var data = getUserIdAndEmailFromUrl(continueUrl);
                    if (data.email && email == data.email.toLowerCase()) {
                        db.user.getUserById(data.userId, db.user.userSchema, (err, user) => {

                            //return done(null, data);
                            req.body.username = user.UserName;
                            req.body.password = cryptoHelper.decryptText(user.Password);
                            next();
                        });
                    }
                    else {
                        //return done("Email không trùng khớp", null);
                        res.json(resultHelper.notAuthorize("Email không trùng khớp"));
                    }
                }
                else {
                    //return done("Code reset không hợp lệ", null);
                    res.json(resultHelper.notAuthorize("Code reset không hợp lệ"));
                }
            });
        },
        passport.authenticate('local', { failWithError: true }),
        function (req, res, next) {
            res.json(resultHelper.create(errorCode.success, 'Thành công', req.session.passport.user));
        },
        function (err, req, res, next) {
            return res.json(err);
        });

    app.post('/user/updatePassword',
        [ensureLogin],
        (req, res) => {
            var user = req.session.passport.user;
            var userId = user.UserId;

            var args = req.body;

            var password = args.Password;
            if (password && password.trim()) {
                password = cryptoHelper.encryptText(password);
                db.user.updateUserPassword(userId, password, (err) => {
                    if (err) {
                        res.json(resultHelper.dataError(err));
                    }
                    else {
                        res.json(resultHelper.success());
                    }
                });
            }
            else {
                res.json(resultHelper.parameterError({ Password: true }));
            }
        }
    );

    app.post('/user/verification/verifyPhone',
        [ensureLogin],
        (req, res) => {
            var user = req.session.passport.user;
            var userId = user.UserId;

            var args = req.body;
            var authToken = args.AuthToken;

            firebaseHelper.verifyTokenAndGetData(authToken, (resultFB) => {
                if (resultFB.ErrorCode == 0) {
                    var phone = resultFB.Data;
                    db.user.updatePhoneVerified(userId, phone, (err) => {
                        if (err) {
                            res.json(resultHelper.dataError(err));
                        }
                        else {
                            res.json(resultHelper.success());
                        }
                    });
                }
                else {
                    res.json(resultFB);
                }
            });
        }
    );
    //phần user group (các nhóm hashtag do User lựa chọn)
    app.post('/user/group/getList',
        [ensureLogin],
        (req, res) => {
            var user = req.session.passport.user;
            var userId = user.UserId;
            var args = req.body;
            var targetUserId = args.UserId;
            if (targetUserId && targetUserId != 0) {
                try {
                    userId = parseInt(targetUserId);
                }
                catch (ex) {
                    userId = user.UserId;
                }
            }

            db.userHashtag.getUserGroupList(userId, (err, lst) => {
                if (err) {
                    res.json(resultHelper.dataError(err));
                }
                else {
                    res.json(resultHelper.success(lst));
                }
            });
        }
    );

    app.post('/user/group/getInfo',
        [ensureLogin],
        (req, res) => {
            var user = req.session.passport.user;
            var userId = user.UserId;
            var args = req.body;
            var hashtagId = args.HashtagId;
            var targetUserId = args.UserId;
            if (targetUserId && targetUserId != 0) {
                try {
                    userId = parseInt(targetUserId);
                }
                catch (ex) {
                    userId = user.UserId;
                }
            }

            if (hashtagId) {
                db.userHashtag.getUserGroupInfoByHashtagId(userId, hashtagId, (err, m) => {
                    if (err) {
                        res.json(resultHelper.dataError(err));
                    }
                    else {
                        res.json(resultHelper.success(m));
                    }
                });
            }
            else {
                res.json(resultHelper.parameterError({
                    hashtagId: true
                }));
            }
        }
    );

    app.post('/user/group/getMembers',
        [ensureLogin],
        (req, res) => {
            var user = req.session.passport.user;
            var userId = user.UserId;
            var args = req.body;
            var targetUserId = args.UserId;
            if (targetUserId && targetUserId != 0) {
                try {
                    userId = parseInt(targetUserId);
                }
                catch (ex) {
                    userId = user.UserId;
                }
            }
            var hashtagId = args.HashtagId;
            var lastJoinDate = args.LastJoinDate;
            if (lastJoinDate === 0 || lastJoinDate === '0' || lastJoinDate === 'null') {
                lastJoinDate = null;
            }

            var count = args.Count;
            if (hashtagId) {
                db.userHashtag.getUserGroupMemberListByHashtagId(userId, hashtagId, lastJoinDate, count, (err, lst) => {
                    if (err) {
                        res.json(resultHelper.dataError(err));
                    }
                    else {
                        res.json(resultHelper.success(lst));
                    }
                });
            }
            else {
                res.json(resultHelper.parameterError({
                    hashtagId: true
                }));
            }
        }
    );

    //trả về danh sách user follow id
    app.post('/user/following/getIdList',
        [ensureLogin],
        (req, res) => {
            var user = req.session.passport.user;
            var userId = user.UserId;

            db.user.getUserFollowingIdList(userId, (err, lst) => {
                if (err) {
                    res.json(resultHelper.dataError(err));
                }
                else {
                    res.json(resultHelper.success(lst));
                }
            });
        }
    );

}
