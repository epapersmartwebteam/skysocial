﻿var _ = require('lodash');
var db = require('../models/all');
var ensureLogin = require('../helper/sessionLoginHelper').ensureLogin;

var logger = require('../helper/logHelper').logger;

module.exports = insightAPI;

function insightAPI(app, passport, resultHelper) {
    app.post('/insight/addEngagements',
        [ensureLogin],
        function (req, res, next) {
            var user = req.session.passport.user;
            var userId = user.UserId;

            var args = req.body;
            logger.info(req.body);
            var engagements = args.Engagements;
            logger.info(engagements);

            try {
                _.each(engagements, (item, idx) => {
                    if (typeof item === 'string') {
                        item = JSON.parse(item);
                    }
                    item.UserId = userId;
                    engagements[idx] = item;
                });

                if (engagements && engagements.length > 0) {
                    db.insight.addPostEngagements(engagements, (result) => {
                        res.json(result);
                    });
                }
            }
            catch (ex) {
                
                res.json(resultHelper.parameterError(ex));
            }
        }
    );

    app.post('/insight/addClick',
        function (req, res, next) {
            var args = req.body;

            var userId = args.UserId;
            var postId = args.PostId;
            var engageType = db.insight.ENGAGETYPE.LINKCLICK;
            db.insight.addPostEngagement(userId, postId, engageType, (result) => {
                res.json(result);
            });
        }
    );

    app.post('/insight/getPostInsight',
        [ensureLogin],
        function (req, res, next) {
            var user = req.session.passport.user;
            var userId = user.UserId;

            var args = req.body;
            
            var lastPostId = args.LastPostId;
            var itemCount = args.ItemCount;
            
            db.insight.getPostInsight(userId, lastPostId, itemCount, (result) => {
                res.json(result);
            });
        }
    );

    app.post('/insight/getInsightByDate',
        [ensureLogin],
        function (req, res, next) {
            var user = req.session.passport.user;
            var userId = user.UserId;

            var args = req.body;
            var fromDate = args.FromDate;
            var toDate = args.ToDate;

            db.insight.getTotalInsightByDate(userId, fromDate, toDate, (result) => {
                res.json(result);
            });
        }
    );
}