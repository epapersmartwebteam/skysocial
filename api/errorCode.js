﻿module.exports = {
    loginUserNameNotMatch: 110,
    loginPasswordNotMatch: 111,
    notAuthorize: -1,
    success: 0,
    dataNotExisted: 100,
    dataExisted: 101,
    dataFormatError: 102,
    checkingError: 103,
    dataError: 104,
    cloudError: 105,
    notPermission: 106,
    parameterError: 107,
    serviceError: 109
};
