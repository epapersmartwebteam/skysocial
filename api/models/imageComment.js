﻿var _ = require('lodash');
var sql = require('mssql');
var config = require('../myConfig').dataConfig;
var result = require('./result');
var myConfig = require('../myConfig');
var CDNUrl = require('../myConfig').CDNUrl;
var cacheConfig = myConfig.cacheConfig;
var defaultRowCount = require('../myConfig').defaultRowCount;
var redisCacheHelper = require('./redisCacheHelper');

var logger = require('../helper/logHelper').logger;

var insight = require('./insight');

//var { checkToClearCacheMTP } = require('./imageItem');

var imageCommentMustHaveColumn = [
    'ItemId',
    'ImageItemId',
    'UserId',
    'ReplyToId',
    'Edited'
];

var imageCommentSchema = [
    'Comment',
    'CreateDate',
    'CreateOS',
    'LastModifyDate',
    'LastModifyOS',
    'LikeCount',
    'ReplyCount'
];

function getImageCommentSelectionField(projectionArray, prefix) {
    var result = imageCommentSchema.filter(function (n) {
        return projectionArray.indexOf(n) > -1;
    });
    result = result.concat(imageCommentMustHaveColumn);

    //tạo mảng clone để lưu tên cột trước khi Add Prefix vào
    var cloneResult = result.slice(0);

    if (prefix) {
        result.forEach(function (item, i) { result[i] = prefix + '.' + item; });
    }

    //duyệt xem nếu là kiểu Datetime thì format lại thành chuỗi yyyy-MM-dd HH:mm:ss.ms
    //result.forEach(function (item, i) {
    //    if (item.indexOf('Date') >= 0) {
    //        result[i] = 'convert(nvarchar, ' + result[i] + ', 120) as ' + cloneResult[i];
    //    }
    //});

    return result.join(',');
}

var connection = new sql.ConnectionPool(config);
connection.connect().catch((error) => {
    logger.error('database connection error', error);
});

connection.on('error', function (err) {
    logger.error('database connection error', err);
    connection = new sql.ConnectionPool(config);
    connection.connect();
});

function calculateCommentCountQuery() {
    var query = ' Update ImageItems ';
    query += ' Set CommentCount = (Select count(1) From ImageComments Where ImageItemId = @ImageItemId) ';
    query += ' Where ImageItemId = @ImageItemId; ';
    return query;
}

function calculateReplyCountQuery() {
    var query = ' Update ImageComments ';
    query += ' Set ReplyCount = (Select count(1) From ImageComments Where ReplyToId = @ReplyToId) ';
    query += ' Where ItemId = @ReplyToId; ';
    return query;
}

function calculateLikeCommentQuery() {
    var query = ' Update ImageComments ';
    query += ' Set LikeCount = (Select count(1) From ImageCommentLikes Where CommentId = @CommentId) ';
    query += ' Where ItemId = @CommentId; ';
    return query;
}

module.exports = {
    getImageCommentByImageId: function (imageItemId, replyToId, lastId, lastDate, count, ast, callback) {

        var request = new sql.Request(connection);

        //if (lastDate)
        //    lastDate = new Date(lastDate);

        request.input('ImageItemId', sql.BigInt, parseFloat(imageItemId));
        request.input('LastId', sql.BigInt, parseFloat(lastId));
        request.input('LastDate', sql.BigInt, lastDate);


        //lấy danh sách field
        var selectionField = getImageCommentSelectionField(ast, 'IC');
        if (!selectionField) {
            selectionField = '*';
        }

        if (!count) {
            count = defaultRowCount;
        }

        var query = 'Select top ' + count + ' ' + selectionField;
        query += ' From [ImageComments] IC';
        query += ' Where IC.ImageItemId = @ImageItemId';
        //ko lấy reply comment, lấy reply comment bằng hàm khác
        if (replyToId) {
            request.input('ReplyToId', sql.BigInt, parseFloat(replyToId));
            query += ' and ReplyToId = @ReplyToId ';
        }
        else {
            query += ' and ReplyToId is null ';
        }

        if (lastId && lastDate) {
            query += ' and (IC.CreateDate < @LastDate or (IC.CreateDate = @LastDate and IC.ItemId < @LastId))';
        }
        else if (lastId) {
            query += ' and IC.ItemId < @LastId ';
        }
        else if (lastDate) {
            query += ' and (IC.CreateDate < @LastDate)';
        }
        query += ' Order by IC.CreateDate desc, IC.ItemId desc';

        request.query(query,
            function (err, result) {
                if (err) {
                    logger.error('getImageCommentByImageId', err);
                    return callback(err, null);
                }
                else {
                    var recordset = result.recordset;
                    if (recordset) {
                        return callback(null, recordset);
                    }
                    else {
                        return callback(null, null);
                    }
                }
            });
    },

    //lấy thông tin 1 comment
    getImageCommentById: function (itemId, ast, callback) {
        var request = new sql.Request(connection);

        request.input('ItemId', sql.BigInt, parseFloat(itemId));

        //lấy danh sách field
        var selectionField = getImageCommentSelectionField(ast, 'IC');
        if (!selectionField) {
            selectionField = '*';
        }

        var query = 'Select ' + selectionField + ' From ImageComments IC Where ItemId = @ItemId';

        request.query(query,
            function (err, result) {
                if (err) {
                    logger.error('getImageCommentById', err);
                    return callback(err, null);
                }
                else {
                    var recordset = result.recordset;
                    if (recordset) {
                        return callback(null, recordset[0]);
                    }
                    else {
                        return callback(null, null);
                    }
                }
            });
    },

    getImageCommentReplyList: function (replyToId, lastId, count, ast, callback) {
        var request = new sql.Request(connection);
        request.input('ReplyToId', sql.BigInt, replyToId);
        request.input('LastId', sql.BigInt, lastId);

        //lấy danh sách field
        var selectionField = getImageCommentSelectionField(ast, 'IC');
        if (!selectionField) {
            selectionField = '*';
        }

        if (!count) {
            count = defaultRowCount;
        }

        var query = 'Select top ' + count + ' ' + selectionField;
        query += ' From [ImageComments] IC';
        query += ' Where ReplyToId = @ReplyToId ';
        if (lastId) {
            query += ' and IC.ItemId < @LastId ';
        }

        query += ' Order by IC.ItemId desc';

        request.query(query,
            function (err, result) {
                if (err) {
                    logger.error('getImageCommentReplyList', err);
                    return callback(err, null);
                }
                else {
                    var recordset = result.recordset;
                    if (recordset) {
                        return callback(null, recordset);
                    }
                    else {
                        return callback(null, null);
                    }
                }
            });
    },

    //thêm mới comment
    createImageComment: function (userId, imageItemId, comment, replyToId, os, callback) {
        var request = new sql.Request(connection);

        request.input('UserId', sql.BigInt, parseFloat(userId));
        request.input('ImageItemId', sql.BigInt, parseFloat(imageItemId));
        request.input('Comment', sql.NVarChar, comment);
        request.input('CreateOS', sql.NVarChar, os);
        request.input('ReplyToId', sql.BigInt, replyToId);

        var query = 'Insert into ImageComments(UserId, ImageItemId, Comment, CreateOS, ReplyToId, Edited, LikeCount, ReplyCount) ';
        query += ' Values(@UserId, @ImageItemId, @Comment, @CreateOS, @ReplyToId, 0, 0, 0); ';
        query += ' Declare @ItemId bigint = SCOPE_IDENTITY()';

        query += calculateCommentCountQuery();

        if (replyToId) {
            query += calculateReplyCountQuery();
        }

        //lấy comment Id mới
        query += 'Select @ItemId as ItemId;';

        request.query(query,
            function (err, result) {
                if (err) {
                    logger.error('createImageComment', err);
                    return callback(err, null);
                }
                else {
                    var recordset = result.recordset;

                    //kiểm tra để clear cache, trong hàm có check là có cấu hình cache hay ko rồi chạy tiếp
                    //checkToClearCacheMTP(imageItemId, () => {
                    //});
                    redisCacheHelper.checkToClearCacheMTP(imageItemId, () => {
                    });

                    if (recordset) {
                        insight.addPostEngagement(userId, imageItemId, insight.ENGAGETYPE.COMMENT, () => {
                        });
                        return callback(null, recordset[0].ItemId);
                    }
                    else {
                        return callback(null, null);
                    }
                }
            });
    },

    //cập nhật comment
    updateImageComment: function (itemId, comment, os, callback) {
        var request = new sql.Request(connection);

        request.input('ItemId', sql.BigInt, parseFloat(itemId));
        request.input('Comment', sql.NVarChar, comment);
        request.input('LastModifyOS', sql.NVarChar, os);

        var query = ' Insert Into ImageCommentHistory(CommentId, Comment, CreateDate) ';
        query += ' Select @ItemId, Comment, CreateDate ';
        query += ' From ImageComments ';
        query += ' Where ItemId = @ItemId; ';

        query += ' Update ImageComments ';
        query += ' Set Comment = @Comment, ';
        query += ' LastModifyOS = @LastModifyOS, ';
        query += ' LastModifyDate = dbo.UNIX_TIMESTAMP(getUTCDate()) ';
        query += ' Where ItemId = @ItemId';


        request.query(query,
            function (err, result) {
                if (err) {
                    logger.error('updateImageComment', err);
                    return callback(err, null);
                }
                else {
                    var recordset = result.recordset;
                    if (recordset) {
                        return callback(null, recordset);
                    }
                    else {
                        return callback(null, null);
                    }
                }
            });
    },

    //xóa comment
    deleteImageComment: function (itemId, ast, callback) {
        var request = new sql.Request(connection);

        request.input('ItemId', sql.BigInt, parseFloat(itemId));

        var query = ' declare @ImageItemId bigint = (select top 1 ImageItemId From ImageComments Where ItemId = @ItemId); ';
        query += ' declare @ReplyToId bigint = (Select top 1 ReplyToId From ImageComments Where ItemId = @ItemId); ';

        query += ' Delete From ImageCommentHistory ';
        query += ' Where CommentId = @ItemId; ';

        query += ' Delete From ImageCommentLikes ';
        query += ' Where CommentId = @ItemId; ';

        query += ' Delete From ImageComments ';
        query += ' Where ItemId = @ItemId or ReplyToId = @ItemId; ';

        //xoá khỏi user notify
        query += ' Delete From UserNotifies ';
        query += ' Where CommentId = @ItemId and ImageItemId is not null; ';

        query += calculateCommentCountQuery();

        query += calculateReplyCountQuery();

        //trả về ImageItemId
        query += ' Select @ImageItemId as ImageItemId';

        request.query(query,
            function (err, result) {
                if (err) {
                    logger.error('deleteImageComment', err);
                    return callback(err, null);
                }
                else {
                    var recordset = result.recordset;
                    //kiểm tra để clear cache, trong hàm có check là có cấu hình cache hay ko rồi chạy tiếp
                    //checkToClearCacheMTP(imageItemId, () => {
                    //});

                    if (recordset) {
                        var imageItemId = recordset[0].ImageItemId;
                        redisCacheHelper.checkToClearCacheMTP(imageItemId, () => {
                        });
                        return callback(null, imageItemId);
                    }
                    else {
                        return callback(null, null);
                    }
                }
            });
    },

    likeImageComment: function (commentId, userId, callback) {
        var request = new sql.Request(connection);

        request.input('CommentId', sql.BigInt, commentId);
        request.input('UserId', sql.BigInt, userId);

        var query = 'Declare @CommentOwnerId bigint = (Select top 1 UserId From ImageComments Where ItemId = @CommentId); ';
        query += '  Declare @ItemId bigint; ';
        query += ' if not exists(Select * From ImageCommentLikes Where CommentId = @CommentId and UserId = @UserId) ';
        query += ' begin ';

        //query += ' Update ImageComments ';
        //query += ' Set LikeCount = LikeCount + 1 ';
        //query += ' Where ItemId = @CommentId; ';        

        query += ' Insert into ImageCommentLikes(CommentId, CommentOwnerId, UserId) ';
        query += ' Values(@CommentId, @CommentOwnerId, @UserId); ';
        query += ' Set @ItemId = SCOPE_IDENTITY(); ';

        query += calculateLikeCommentQuery();

        query += ' end ';
        query += ' else ';
        query += ' begin ';
        query += ' Set @ItemId = (Select top 1 ItemId From ImageCommentLikes Where CommentId = @CommentId and UserId = @UserId); ';
        query += ' end ';



        query += ' Select @ItemId as ItemId;';

        request.query(query,
            function (err, result) {
                if (err) {
                    logger.error('likeImageComment', err);
                    return callback(err, null);
                }
                else {
                    var recordset = result.recordset;
                    if (recordset) {
                        return callback(null, recordset[0].ItemId);
                    }
                    else {
                        return callback(null, null);
                    }
                }
            });
    },

    unlikeImageComment: function (commentId, userId, callback) {
        var request = new sql.Request(connection);

        request.input('CommentId', sql.BigInt, commentId);
        request.input('UserId', sql.BigInt, userId);

        var query = '  Declare @ItemId bigint = 0; ';
        query += ' if exists(Select * From ImageCommentLikes Where CommentId = @CommentId and UserId = @UserId) ';
        query += ' begin ';

        query += ' Set @ItemId = (Select top 1 ItemId From ImageCommentLikes where CommentId = @CommentId and UserId = @UserId); ';

        //query += ' Update ImageComments ';
        //query += ' Set LikeCount = LikeCount - 1 ';
        //query += ' Where ItemId = @CommentId; ';

        query += ' Delete From ImageCommentLikes ';
        query += ' Where CommentId = @CommentId and UserId = @UserId; ';

        query += calculateLikeCommentQuery();

        query += ' end ';

        query += ' Select @ItemId as ItemId;';

        request.query(query,
            function (err, result) {

                if (err) {
                    logger.error('unlikeImageComment', err);
                    return callback(err, null);
                }
                else {
                    var recordset = result.recordset;
                    if (recordset) {
                        return callback(null, recordset[0].ItemId);
                    }
                    else {
                        return callback(null, null);
                    }
                }
            });
    },

    checkImageCommentIsLiked: function (userId, commentId, callback) {
        var request = new sql.Request(connection);
        request.input('CommentId', sql.BigInt, parseFloat(commentId));
        request.input('UserId', sql.BigInt, parseFloat(userId));

        var query = 'if exists(Select * From [ImageCommentLikes] Where UserId = @UserId and CommentId = @CommentId) ';
        query += ' Select 1 as Existed';
        query += ' else ';
        query += ' Select 0 as Existed';

        request.query(query, function (err, res) {

            if (err) {
                logger.error('checkImageCommentIsLiked', err);
                return callback(err);
            }
            else {
                var recordset = res.recordset;
                return callback(null, recordset[0].Existed);
            }
        });
    },

    getImageCommentLikeList: function (commentId, lastId, itemCount, callback) {
        var request = new sql.Request(connection);

        request.input('CommentId', sql.BigInt, commentId);

        if (!itemCount || itemCount == 0) {
            itemCount = defaultRowCount;
        }

        var query = ' Select top ' + itemCount + ' ICL.ItemId, ICL.CommentId, ICL.CreateDate as CreateDate, U.UserId, U.UserName, U.FullName, ';
        query += " case when (U.AvatarSmall is not null and U.AvatarSmall != '') then '" + CDNUrl + "' + U.AvatarSmall else '" + CDNUrl + myConfig.NoAvatarLink + "' end as AvatarSmall ";
        query += ', U.IsVip, ISNULL(U.VipLevelId, -1) as VipLevelId ';
        query += ' From ImageCommentLikes ICL inner join Users U on ICL.UserId = U.UserId ';
        query += ' Where ICL.CommentId = @CommentId ';
        if (lastId) {
            request.input('LastId', sql.BigInt, lastId);
            query += ' and ICL.ItemId < @LastId ';
        }

        query += ' Order by ICL.ItemId desc ';

        request.query(query,
            function (err, result) {
                if (err) {
                    logger.error('getImageCommentLikeList', err);
                    return callback(err, null);
                }
                else {
                    var recordset = result.recordset;
                    return callback(null, recordset);
                }
            });
    },

    getTopCommentOfFeaturePost: function (imageItemId, topN, callback) {
        var request = new sql.Request(connection);

        request.input('ImageItemId', sql.BigInt, imageItemId);
        request.input('TopN', sql.Int, topN);

        var query = 'getTopCommentOfFeaturePost';

        request.execute(query, function (err, result) {
            if (err) {
                logger.error('getTopCommentOfFeaturePost', err);
                return callback(err);
            }
            else {
                var recordset = result.recordset;
                var items = [];
                _.each(recordset, (item) => {
                    items.push({
                        ItemId: item.ItemId,
                        Comment: item.Comment,
                        CreateDate: item.CreateDate,
                        User: {
                            UserName: item.UserName,
                            AvatarSmall: CDNUrl + (item.AvatarSmall ? item.AvatarSmall : myConfig.NoAvatarLink)
                        }
                    });
                });
                return callback(null, items);
            }
        });
    }
};