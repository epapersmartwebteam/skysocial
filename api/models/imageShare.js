﻿var sql = require('mssql');
var config = require('../myConfig').dataConfig;
var result = require('./result');

var defaultRowCount = require('../myConfig').defaultRowCount;

var logger = require('../helper/logHelper').logger;

var imageShareMustHaveColumn = [
    'ItemId',
    'ImageItemId',
    'UserId'
];

var imageShareSchema = [
    'ShareTo',
    'CreateDate'
];

function getImageShareSelectionField(projectionArray, prefix) {
    var result = imageShareSchema.filter(function (n) {
        return projectionArray.indexOf(n) > -1;
    });
    result = result.concat(imageShareMustHaveColumn);

    //tạo mảng clone để lưu tên cột trước khi Add Prefix vào
    var cloneResult = result.slice(0);

    if (prefix) {
        result.forEach(function (item, i) { result[i] = prefix + '.' + item; });
    }

    //duyệt xem nếu là kiểu Datetime thì format lại thành chuỗi yyyy-MM-dd HH:mm:ss.ms
    result.forEach(function (item, i) {
        //if (item.indexOf('Date') >= 0) {
        //    result[i] = 'convert(nvarchar, ' + result[i] + ', 120) as ' + cloneResult[i];
        //}
    });

    return result.join(',');
}

var connection = new sql.ConnectionPool(config);
connection.connect().catch((error) => {
    logger.error('database connection error', error);
});

connection.on('error', function (err) {
    logger.error('database connection error', err);
    connection = new sql.ConnectionPool(config);
    connection.connect();
});


module.exports = {
    getImageShareByImageId: function (imageItemId, lastId, lastDate, count, ast, callback) {

        var request = new sql.Request(connection);

        //if (lastDate)
        //    lastDate = new Date(lastDate);

        request.input('ImageItemId', sql.BigInt, parseFloat(imageItemId));
        request.input('LastId', sql.BigInt, parseFloat(lastId));
        request.input('LastDate', sql.BigInt, lastDate);

        //lấy danh sách field
        var selectionField = getImageShareSelectionField(ast, 'IC');
        if (!selectionField) {
            selectionField = '*';
        }

        if (!count) {
            count = defaultRowCount;
        }

        var query = 'Select top ' + count + ' ' + selectionField;
        query += ' From [ImageShares] IC';
        query += ' Where IC.ImageItemId = @ImageItemId';
        if (lastId && lastDate) {
            query += ' and (IC.CreateDate < @LastDate or (IC.CreateDate = @LastDate and IC.UserId < @LastId))';
        }
        else if (lastDate) {
            query += ' and (IC.CreateDate < @LastDate)';
        }
        query += ' Order by IC.CreateDate desc, IC.UserId desc';

        request.query(query,
            function (err, result) {
                if (err) {
                    logger.error('getImageShareByImageId', err);
                    return callback(err, null);
                }
                else {
                    var recordset = result.recordset;
                    if (recordset) {
                        return callback(null, recordset);
                    }
                    else {
                        return callback(null, null);
                    }
                }
            });
    },

    //kiểm tra user đã share 1 hình chưa
    checkUserShared: function (userId, imageItemId, callback) {
        var request = new sql.Request(connection);
        request.input('ImageItemId', sql.BigInt, parseFloat(imageItemId));
        request.input('UserId', sql.BigInt, parseFloat(userId));

        var query = "if exists(Select * From [ImageItems] Where UserId = @UserId and ShareFromId = @ImageItemId) ";
        query += ' Select 1 as Existed';
        query += ' else ';
        query += ' Select 0 as Existed';

        request.query(query, function (err, result) {

            if (err) {
                logger.error('checkUserShared', err);
                return callback(err);
            }
            else {
                var recordset = result.recordset;
                return callback(null, recordset[0].Existed);
            }
        });
    }
};