﻿var sql = require('mssql');
var moment = require('moment');
var _ = require('lodash');
var resultHelper = require('../result');
//lấy config chuỗi kết nối
var config = require('../../myConfig').dataConfig;
var defaultRowCount = require('../../myConfig').defaultRowCount;

var CDNUrl = require('../../myConfig').CDNUrl;

var utility = require('../../helper/utility');

var logger = require('../../helper/logHelper').logger;
var loggerPayment = require('../../helper/logHelper').loggerPayment;
var loggerPaymentInfo = require('../../helper/logHelper').loggerPaymentInfo;
//NOTE: chỉ khi tạo hoặc update Payment thì mới ghi vào loggerPayment, khi get thông tin hoặc danh sách thì chỉ ghi lỗi vào log thông thường


var PAYMETHODS = {
    ATM: 1,
    BANKTRANS: 2,
    VISAMASTER: 3,
    PAYPAL: 4,
    PASSCODE: 5,
    MOBILECARD: 6,
    APPLEINAPP: 7,
    ACCOUNT: 8
};

var connection = new sql.ConnectionPool(config);
connection.connect().catch((error) => {
    logger.error('database connection error', error);
});

connection.on('error', function (err) {
    logger.error('database connection error', err);
    connection = new sql.ConnectionPool(config);
    connection.connect();
});

module.exports = {
    generatePaymentCode: function (userId) {
        return userId + '-' + moment().format('YYMMDDHHmmss');
    },

    createPaymentLog: function (userId, paymentCode, methodId, price, discountCode, discount, total,
        payTypeId, itemId, isDigital, isVipPurchase, vipMonth, os, description, callback) {
        var request = new sql.Request(connection);
        request.input('UserId', sql.BigInt, userId);

        request.input('PaymentCode', sql.NVarChar, paymentCode);
        request.input('MethodId', sql.Int, methodId);
        request.input('Price', sql.Int, price);
        request.input('DiscountCode', sql.NVarChar, discountCode);
        request.input('Discount', sql.Int, discount);
        request.input('Total', sql.Int, total);
        request.input('PayTypeId', sql.Int, payTypeId);
        request.input('ItemId', sql.BigInt, itemId);
        request.input('IsDigital', sql.Bit, isDigital);
        request.input('IsVipPurchase', sql.Bit, isVipPurchase);
        request.input('VipMonth', sql.Int, vipMonth);
        request.input('OS', sql.NVarChar, os);
        request.input('Description', sql.NVarChar, description);

        var query = 'createUserPaymentLogWithDes';

        request.execute(query,
            function (err, result) {

                if (err) {
                    loggerPayment.error('createPaymentLog', err);
                    callback(resultHelper.dataError(err));
                }
                else {
                    var recordset = result.recordset;
                    var item;
                    if (recordset && recordset.length > 0) {
                        item = recordset[0];
                    }
                    loggerPaymentInfo.info('createPaymentLog', item);
                    callback(resultHelper.success(item));
                }
            });
    },

    createPaymentLogFromMobileCard: function (userId, paymentCode, price, discountCode, discount, total,
        payTypeId, itemId, isDigital, isVipPurchase, vipMonth, cardType, cardAmount, cardPIN, cardSerial, os, callback) {
        var methodId = PAYMETHODS.MOBILECARD;

        var request = new sql.Request(connection);
        request.input('UserId', sql.BigInt, userId);
        request.input('PaymentCode', sql.NVarChar, paymentCode);
        request.input('MethodId', sql.Int, methodId);
        request.input('Price', sql.Int, price);
        request.input('DiscountCode', sql.NVarChar, discountCode);
        request.input('Discount', sql.Int, discount);
        request.input('Total', sql.Int, total);
        request.input('PayTypeId', sql.Int, payTypeId);
        request.input('ItemId', sql.BigInt, itemId);
        request.input('IsDigital', sql.Bit, isDigital);
        request.input('IsVipPurchase', sql.Bit, isVipPurchase);
        request.input('VipMonth', sql.Int, vipMonth);
        request.input('CardType', sql.NVarChar, cardType);
        request.input('CardAmount', sql.Int, cardAmount);
        request.input('CardPIN', sql.NVarChar, cardPIN);
        request.input('CardSerial', sql.NVarChar, cardSerial);
        request.input('OS', sql.NVarChar, os);

        var query = 'createUserPaymentLogFromMobileCard';

        request.execute(query,
            function (err, result) {
                if (err) {
                    loggerPayment.error('createPaymentLogFromMobileCard', err);
                    callback(resultHelper.dataError(err));
                }
                else {
                    var recordset = result.recordset;
                    var item;
                    if (recordset && recordset.length > 0) {
                        item = recordset[0];
                    }
                    loggerPaymentInfo.info('createPaymentLogFromMobileCard', item);
                    callback(resultHelper.success(item));
                }
            });
    },

    updatePaymentLogIsPay: function (userId, paymentCode, trackingId, isPaid, cardErrorCode, cardErrorMessage, os, callback) {
        var request = new sql.Request(connection);
        request.input('UserId', sql.BigInt, userId);
        request.input('PaymentCode', sql.NVarChar, paymentCode);
        request.input('TrackingId', sql.NVarChar, trackingId);
        request.input('IsPaid', sql.Bit, isPaid);
        request.input('CardErrorCode', sql.NVarChar, cardErrorCode);
        request.input('CardErrorMessage', sql.NVarChar, cardErrorMessage);
        request.input('OS', sql.NVarChar, os);

        var query = 'updatePaymentLogIsPay';

        request.execute(query,
            function (err, result) {

                if (err) {
                    loggerPayment.error('updatePaymentLogIsPay', err);
                    callback(resultHelper.dataError(err));
                }
                else {
                    var recordset = result.recordset;
                    var error = 0;
                    var item;
                    if (recordset && recordset.length > 0) {
                        item = recordset[0];
                    }
                    if (item.ErrorCode) {
                        error = item.ErrorCode;
                    }
                    if (error == 0) {
                        loggerPaymentInfo.info('updatePaymentLogIsPay', item);
                        callback(resultHelper.success(item));
                    }
                    else {
                        loggerPaymentInfo.warn('updatePaymentLogIsPay', "Payment not exists");
                        callback(resultHelper.notExists("PaymentCode không tồn tại"));
                    }
                }
            });
    },

    createPaymentLogFromPasscode: function (userId, paymentCode, passCode, os, callback) {
        var methodId = PAYMETHODS.PASSCODE;
        var request = new sql.Request(connection);
        request.input('UserId', sql.BigInt, userId);
        request.input('PaymentCode', sql.NVarChar, paymentCode);
        request.input('MethodId', sql.Int, methodId);
        request.input('Passcode', sql.VarChar, passCode);
        request.input('OS', sql.NVarChar, os);

        var query = 'createUserPaymentLogFromPassCode';

        request.execute(query,
            function (err, result) {
                if (err) {
                    loggerPayment.error('createPaymentLogFromPasscode', err);
                    callback(resultHelper.dataError(err));
                }
                else {
                    var recordset = result.recordset;
                    var item;
                    if (recordset && recordset.length > 0) {
                        item = recordset[0];
                    }
                    if (item) {
                        if (!item.ErrorCode) {
                            loggerPaymentInfo.info('createPaymentLogFromPasscode', item);
                            callback(resultHelper.success(item));
                        }
                        else {
                            switch (item.ErrorCode) {
                                case 0: {
                                    loggerPaymentInfo.warn('createPaymentLogFromPasscode', "Passcode không tồn tại.");
                                    callback(resultHelper.notExists('Passcode không tồn tại.'));
                                    break;
                                }
                                case -1: {
                                    loggerPaymentInfo.warn('createPaymentLogFromPasscode', "Passcode chưa được kích hoạt.");
                                    callback(resultHelper.notExists('Passcode chưa được kích hoạt.'));
                                    break;
                                }
                                case -2: {
                                    loggerPaymentInfo.warn('createPaymentLogFromPasscode', "Passcode đã được sử dụng.");
                                    callback(resultHelper.notExists('Passcode đã được sử dụng.'));
                                    break;
                                }
                            }
                        }
                    }

                }
            });
    },

    getPaymentLogInfo: function (userId, paymentCode, callback) {
        var request = new sql.Request(connection);
        request.input('UserId', sql.BigInt, userId);
        request.input('PaymentCode', sql.NVarChar, paymentCode);

        var query = 'Select * From UserPaymentLogs Where UserId = @UserId and PaymentCode = @PaymentCode';

        request.query(query,
            function (err, result) {
                if (err) {
                    logger.error('getPaymentLogInfo', err);
                    callback(resultHelper.dataError(err));
                }
                else {
                    var recordset = result.recordset;
                    var item;
                    if (recordset && recordset.length > 0) {
                        item = recordset[0];
                    }
                    callback(resultHelper.success(item));
                }
            }
        );
    },

    getPaymentLogInfoForSystem: function (paymentCode, callback) {
        var request = new sql.Request(connection);
        
        request.input('PaymentCode', sql.NVarChar, paymentCode);

        var query = 'Select * From UserPaymentLogs Where PaymentCode = @PaymentCode';

        request.query(query,
            function (err, result) {
                if (err) {
                    logger.error('getPaymentLogInfoForSystem', err);
                    callback(resultHelper.dataError(err));
                }
                else {
                    var recordset = result.recordset;
                    var item;
                    if (recordset && recordset.length > 0) {
                        item = recordset[0];
                    }
                    callback(resultHelper.success(item));
                }
            }
        );
    },

    getPaymentLogs: function (userId, isPaid, methodId, lastDate, itemCount, callback) {
        var request = new sql.Request(connection);
        request.input('UserId', sql.BigInt, userId);
        request.input('IsPaid', sql.Bit, isPaid);
        request.input('MethodId', sql.Bit, methodId);
        request.input('LastDate', sql.Bit, lastDate);

        var query = 'Select top ' + itemCount + ' * ';
        query += ' From UserPaymentLogs ';
        query += ' Where UserId = @UserId ';
        if (methodId > 0) {
            query += ' and MethodId = @MethodId ';
        }
        if (lastDate) {
            query += ' and CreateDate < @LastDate ';
        }

        query += ' Order by CreateDate desc ';

        request.query(query,
            function (err, result) {
                if (err) {
                    logger.error('getPaymentLogs', err);
                    callback(resultHelper.dataError(err));
                }
                else {
                    var recordset = result.recordset;
                    callback(resultHelper.success(recordset));
                }
            }
        );
    }
};