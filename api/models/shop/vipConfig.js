﻿var sql = require('mssql');
var _ = require('lodash');
var resultHelper = require('../result');
//lấy config chuỗi kết nối
var config = require('../../myConfig').dataConfig;
var defaultRowCount = require('../../myConfig').defaultRowCount;

var CDNUrl = require('../../myConfig').CDNUrl;

var logger = require('../../helper/logHelper').logger;
var loggerPayment = require('../../helper/logHelper').loggerPayment;
var loggerPaymentInfo = require('../../helper/logHelper').loggerPaymentInfo;
//NOTE: ghi phần cập nhật Vip vào Payment vì liên quan tới money

var { convertVipLevel } = require('../user');

var connection = new sql.ConnectionPool(config);
connection.connect().catch((error) => {
    logger.error('database connection error', error);
});

connection.on('error', function (err) {
    logger.error('database connection error', err);
    connection = new sql.ConnectionPool(config);
    connection.connect();
});

function parseVipConfigInfo(vipConfig) {
    vipConfig.VipLevel = convertVipLevel(vipConfig.VipLevelId, true);
    return vipConfig;
}

module.exports = {
    getVipConfigInfo: function (configId, callback) {
        var request = new sql.Request(connection);

        request.input('ConfigId', sql.Int, configId);
        var query = 'Select * From AppVipSellConfigs Where ConfigId = @ConfigId';

        request.query(query, (err, result) => {
            if (err) {
                logger.error('getVipConfigInfo', err);
                callback(resultHelper.dataError(err));
            }
            else {
                var recordset = result.recordset;
                var item;
                if (recordset && recordset.length > 0) {
                    item = parseVipConfigInfo(recordset[0]);
                }
                callback(resultHelper.success(item));
            }
        });
    },

    getVipConfigs: function (active, callback) {
        var request = new sql.Request(connection);

        var query = ' Select * ';
        query += ' From AppVipSellConfigs ';
        if (active > -1) {
            request.input('Active', sql.Bit, (active == 1));
            query += ' Where Active = @Active ';
        }
        query += ' Order by SortId, ConfigId ';

        request.query(query, (err, result) => {
            if (err) {
                logger.error('getVipConfigs', err);
                callback(resultHelper.dataError(err));
            }
            else {
                var recordset = result.recordset;
                for (var i = 0; i < recordset.length; i++) {
                    recordset[i] = parseVipConfigInfo(recordset[i]);
                }
                callback(resultHelper.success(recordset));
            }
        });
    },

    getVipConfigsForInApp: function (callback) {
        var request = new sql.Request(connection);

        var query = ' Select * ';
        query += ' From AppVipSellConfigs ';
        query += ' Where Active = 1 and EnableIOSInAppPaid = 1 ';
        query += ' Order by IOSInAppPrice ';

        request.query(query, (err, result) => {
            if (err) {
                logger.error('getVipConfigsForInApp', err);
                callback(resultHelper.dataError(err));
            }
            else {
                var recordset = result.recordset;
                for (var i = 0; i < recordset.length; i++) {
                    recordset[i] = parseVipConfigInfo(recordset[i]);
                }
                callback(resultHelper.success(recordset));
            }
        });
    },
    //updateUserVip: function (userId, isVip, month, callback) {
    //    var request = new sql.Request(connection);
    //    request.input('UserId', sql.BigInt, userId);
    //    request.input('IsVip', sql.Bit, isVip);
    //    request.input('VipMonth', sql.Int, month);

    //    var query = 'updateUserVip';
    //    request.execute(query, (err, result) => {
    //        if (err) {
    //            loggerPayment.error('getVipConfigs', err);
    //            callback(resultHelper.dataError(err));
    //        }
    //        else {
    //            loggerPaymentInfo.info('updateUserVip Success for user: ', userId);
    //            callback(resultHelper.success());
    //        }
    //    });
    //}
};