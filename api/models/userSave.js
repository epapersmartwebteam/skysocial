﻿var _ = require('lodash');
var sql = require('mssql');

var moment = require('moment');

var myConfig = require('../myConfig');

var config = myConfig.dataConfig;
var defaultRowCount = myConfig.defaultRowCount;

var utilityHelper = require('../helper/utility');

var CDNUrl = myConfig.CDNUrl;

var convertToSQLBitType = require('../helper/utility').convertToSQLBitType;

var resultHelper = require('./result');
var insight = require('./insight');

var logger = require('../helper/logHelper').logger;

var connection = new sql.ConnectionPool(config);
connection.connect().catch((error) => {
    logger.error('database connection error', error);
});

connection.on('error', function (err) {
    logger.error('database connection error', err);
    connection = new sql.ConnectionPool(config);
    connection.connect();
});

module.exports = {

    ///collections
    getCollections: function (userId, callback) {
        var request = new sql.Request(connection);
        request.input('UserId', sql.BigInt, userId);

        var query = " SELECT UC.*, JSON_QUERY(I.ItemContent, '$[0].Small') as CoverInfo ";
        query += ' FROM UserCollections UC Left Join ImageItems I on UC.CoverImageItemId = I.ImageItemId ';
        query += ' WHERE UC.UserId = @UserId ';

        request.query(query, (err, result) => {
            if (err) {
                logger.error('getCollections', err);
                callback(resultHelper.dataError(err));
            }
            else {
                var recordset = result.recordset;
                if (recordset) {
                    _.each(recordset, (item, idx) => {
                        if (item.CoverInfo) {
                            try {
                                recordset[idx].CoverInfo = JSON.parse(item.CoverInfo);
                            }
                            catch (e) {
                            }
                        }
                    });
                }
                callback(resultHelper.success(recordset));
            }
        });
    },

    getCollection: function (userId, collectionId, callback) {
        var request = new sql.Request(connection);
        request.input('UserId', sql.BigInt, userId);
        request.input('CollectionId', sql.Int, collectionId);

        var query = " SELECT UC.*, JSON_QUERY(I.ItemContent, '$[0].Small') as CoverInfo ";
        query += ' FROM UserCollections UC Left Join ImageItems I on UC.CoverImageItemId = I.ImageItemId ';
        query += ' WHERE UC.UserId = @UserId and UC.CollectionId = @CollectionId ';

        request.query(query, (err, result) => {
            if (err) {
                logger.error('getCollection', err);
                callback(resultHelper.dataError(err));
            }
            else {
                var recordset = result.recordset;
                var item;
                if (recordset) {
                    try {
                        item = recordset[0];
                        item.CoverInfo = JSON.parse(item.CoverInfo);
                    }
                    catch (e) {
                    }
                }
                callback(resultHelper.success(item));
            }
        });
    },

    createCollection: function (userId, collectionName, callback) {
        var request = new sql.Request(connection);
        request.input('UserId', sql.BigInt, userId);
        request.input('CollectionName', sql.NVarChar, collectionName);
        console.log(collectionName);
        var query = 'INSERT INTO UserCollections(UserId, CollectionName) ';
        query += ' VALUES(@UserId, @CollectionName) ';
        query += ' Select SCOPE_IDENTITY() as CollectionId ';
        request.query(query, (err, result) => {
            if (err) {
                logger.error('createCollection', err);
                callback(resultHelper.dataError(err));
            }
            else {
                console.log(result);
                var recordset = result.recordset;
                var collectionId;
                if (recordset && recordset.length > 0) {
                    collectionId = recordset[0].CollectionId;
                }
                return callback(resultHelper.success(collectionId));
                
            }
        });
    },
    updateCollectionName: function (userId, collectionId, collectionName, callback) {
        var request = new sql.Request(connection);
        request.input('UserId', sql.BigInt, userId);
        request.input('CollectionId', sql.Int, collectionId);
        request.input('CollectionName', sql.NVarChar, collectionName);

        var query = 'IF(EXISTS(SELECT * FROM UserCollections WHERE UserId = @UserId and CollectionId = @CollectionId)) ';
        query += ' BEGIN ';
        query += ' UPDATE UserCollections ';
        query += ' SET CollectionName = @CollectionName, ';
        query += ' LastUpdateDate = dbo.UNIX_TIMESTAMP(getUTCDate()) ';
        query += ' WHERE UserId = @UserId and CollectionId = @CollectionId; ';
        query += ' END ';
        query += ' ELSE ';
        query += ' BEGIN ';
        query += ' SELECT 1 as NotExists ';
        query += ' END ';

        request.query(query, (err, result) => {
            if (err) {
                logger.error('updateCollectionName', err);
                callback(resultHelper.dataError(err));
            }
            else {
                var recordset = result.recordset;
                if (recordset && recordset.length > 0 && recordset[0].NotExists) {
                    callback(resultHelper.notExists({ CollectionId: collectionId }));
                }
                else {
                    callback(resultHelper.success());
                }
            }
        });
    },
    updateCollectionCover: function (userId, collectionId, coverImageItemId, callback) {
        var request = new sql.Request(connection);
        request.input('UserId', sql.BigInt, userId);
        request.input('CollectionId', sql.Int, collectionId);
        request.input('CoverImageItemId', sql.BigInt, coverImageItemId);

        var query = 'IF(EXISTS(SELECT * FROM UserCollections WHERE UserId = @UserId and CollectionId = @CollectionId)) ';
        query += ' BEGIN ';
        query += ' UPDATE UserCollections ';
        query += ' SET CoverImageItemId = @CoverImageItemId, ';
        query += ' LastUpdateDate = dbo.UNIX_TIMESTAMP(getUTCDate()) ';
        query += ' WHERE UserId = @UserId and CollectionId = @CollectionId; ';
        query += ' END ';
        query += ' ELSE ';
        query += ' BEGIN ';
        query += ' SELECT 1 as NotExists ';
        query += ' END ';

        request.query(query, (err, result) => {
            if (err) {
                logger.error('updateCollectionCover', err);
                callback(resultHelper.dataError(err));
            }
            else {
                var recordset = result.recordset;
                if (recordset && recordset.length > 0 && recordset[0].NotExists) {
                    callback(resultHelper.notExists({ CollectionId: collectionId }));
                }
                else {
                    callback(resultHelper.success());
                }
            }
        });
    },

    deleteCollection: function (userId, collectionId, callback) {
        var request = new sql.Request(connection);
        request.input('UserId', sql.BigInt, userId);
        request.input('CollectionId', sql.Int, collectionId);

        var query = ' if(Exists(Select * From UserCollections Where CollectionId = @CollectionId and UserId = @UserId)) ';
        query += ' Begin ';
        query += ' Delete From UserCollectionSaves ';
        query += ' Where CollectionId = @CollectionId; ';
        query += ' Delete From UserCollections ';
        query += ' Where CollectionId = @CollectionId; ';
        query += ' End ';
        query += ' Else ';
        query += ' Begin ';
        query += ' Select 1 as NotPermission ';
        query += ' End ';

        request.query(query, (err, result) => {
            if (err) {
                logger.error('deleteCollection', err);
                callback(resultHelper.dataError(err));
            }
            else {
                var recordset = result.recordset;
                if (recordset && recordset.length > 0 && recordset[0].NotPermission) {
                    callback(resultHelper.notPermission({ CollectionId: collectionId }));
                }
                else {
                    callback(resultHelper.success());
                }
            }
        });
    },

    ///saved
    //vì dùng GraphQL nên để chung bên file imageItem 
    //getSaveImage: function (userId, collectionId, callback) {

    //},

    addSaveImage: function (userId, imageItemId, collectionIds, callback) {
        var request = new sql.Request(connection);
        request.input('UserId', sql.BigInt, userId);
        request.input('ImageItemId', sql.BigInt, imageItemId);
        request.input('CollectionIds', sql.NVarChar, collectionIds);
        
        request.execute('addSaveImage', (err, result) => {
            if (err) {
                logger.error('addSaveImage', err);
                callback(resultHelper.dataError(err));
            }
            else {
                var imageExisted = false;
                if (result && result.recordset && result.recordset.length > 0) {
                    imageExisted = (result.recordset[0].ImageExisted == true || result.recordset[0].ImageExisted == 1);
                }
                if (imageExisted) {
                    insight.addPostEngagement(userId, imageItemId, insight.ENGAGETYPE.SAVED, () => {
                    });
                    callback(resultHelper.success());
                }
                else {
                    callback(resultHelper.notExists());
                }
            }
        });
    },

    removeSaveImage: function (userId, imageItemId, collectionIds, isRemoveSave, callback) {
        var request = new sql.Request(connection);
        request.input('UserId', sql.BigInt, userId);
        request.input('ImageItemId', sql.BigInt, imageItemId);
        request.input('CollectionIds', sql.VarChar, collectionIds);
        request.input('IsRemoveSave', sql.Bit, isRemoveSave);

        request.execute('removeSaveImage', (err, result) => {
            if (err) {
                logger.error('removeSaveImage', err);
                callback(resultHelper.dataError(err));
            }
            else {
                callback(resultHelper.success());
            }
        });
    },

    getCollectionsOfSaveImage: function (userId, imageItemId, callback) {
        var request = new sql.Request(connection);
        request.input('ImageItemId', sql.BigInt, parseFloat(imageItemId));
        request.input('UserId', sql.BigInt, parseFloat(userId));

        var query = 'Select distinct CollectionId From UserCollectionSaves Where UserId = @UserId and ImageItemId = @ImageItemId ';
        request.query(query, function (err, res) {
            if (err) {
                logger.error('getCollectionsOfSaveImage', err);
                return callback(resultHelper.dataError(err));
            }
            else {
                var recordset = res.recordset;
                var items = _.map(recordset, (item) => {
                    return item.CollectionId;
                });
                return callback(resultHelper.success(items));
            }
        });
    },

    checkUserSaveImage: function (userId, imageItemId, callback) {
        var request = new sql.Request(connection);
        request.input('ImageItemId', sql.BigInt, parseFloat(imageItemId));
        request.input('UserId', sql.BigInt, parseFloat(userId));

        var query = 'if exists(Select * From [UserSaves] Where UserId = @UserId and ImageItemId = @ImageItemId) ';
        query += ' Select 1 as Existed';
        query += ' else ';
        query += ' Select 0 as Existed';

        request.query(query, function (err, res) {

            if (err) {
                logger.error('checkUserSaveImage', err);
                return callback(err);
            }
            else {
                var recordset = res.recordset;
                return callback(null, recordset[0].Existed);
            }
        });
    },
    getUserImageSavedDate: function (userId, imageItemId, callback) {
        var request = new sql.Request(connection);
        request.input('ImageItemId', sql.BigInt, parseFloat(imageItemId));
        request.input('UserId', sql.BigInt, parseFloat(userId));

        var query = 'Select CreateDate as SavedDate From [UserSaves] Where UserId = @UserId and ImageItemId = @ImageItemId ';        

        request.query(query, function (err, res) {

            if (err) {
                logger.error('getUserImageSavedDate', err);
                return callback(err);
            }
            else {
                var recordset = res.recordset;
                if (recordset && recordset.length > 0) {
                    return callback(null, recordset[0].SavedDate);
                }
                else {
                    return callback(null, null);
                }
            }
        });
    }
};