﻿var sql = require('mssql');
var config = require('../myConfig').dataConfig;
var result = require('./result');

var defaultRowCount = require('../myConfig').defaultRowCount;

var logger = require('../helper/logHelper').logger;

var connection = new sql.ConnectionPool(config);
connection.connect().catch((error) => {
    logger.error('database connection error', error);
});

connection.on('error', function (err) {
    logger.error('database connection error', err);
    connection = new sql.ConnectionPool(config);
    connection.connect();
});


module.exports = {
    updateFacebookFriend: function (facebookId, facebookFriendId, name, avatar, callback) {

        var request = new sql.Request(connection);

        request.input('FacebookId', sql.NVarChar, facebookId);
        request.input('FriendFacebookId', sql.NVarChar, facebookFriendId);
        request.input('Name', sql.NVarChar, name);
        request.input('Avatar', sql.NVarChar, avatar);

        var query = ' declare @AppUserName nvarchar(200) = (Select top 1 UserName From Users Where FacebookId = @FacebookFriendId); ';
        query += ' declare @InApp bit = (case when @AppUserName is null then 0 else 1 end); ';

        query += ' if(Exists(select * From FacebookFriends Where FacebookId = @FacebookId and FacebookFriendId = @FacebookFriendId)) ';
        query += ' begin ';
        query += ' Update FacebookFriends ';
        query += ' Set Name = @Name, ';
        query += ' AvatarUrl = @Avatar, ';
        query += ' InApp = @InApp, ';
        query += ' AppUserName = @AppUserName, ';
        query += ' LastModifyDate = GetDate() ';
        query += ' Where FacebookId = @FacebookId and FacebookFriendId = @FacebookFriendId ';
        query += ' end ';
        query += ' else ';
        query += ' begin ';
        query += ' Insert into FacebookFriends(FacebookId, FacebookFriendId, Name, AvatarUrl, InApp, AppUserName, IsInvited, IsHide, LastModifyDate) ';
        query += ' Values (@FacebookId, @FacebookFriendId, @Name, @Avatar, @InApp, @AppUserName, 0, 0, GetDate()) ';
        query += ' end ';

        request.query(query, function (err, result) {
            if (err) {
                logger.error('updateFacebookFriend', err);
                return callback(err);
            }
            else {
                var recordset = result.recordset;
                return callback(err, recordset);
            }
        });
    },

    updateFacebookFriendIsInvited: function (facebookId, friendFacebookId, callback){
        var request = new sql.Request(connection);

        request.input('FacebookId', sql.NVarChar, facebookId);
        request.input('FriendFacebookId', sql.NVarChar, friendFacebookId);

        var query = 'Update FacebookFriends ';
        query += ' Set IsInvited = 1 ';
        query += ' Where FacebookId = @FacebookId and FacebookFriendId = @FacebookFriendId ';

        request.query(query, function (err, result) {
            if (err) {
                logger.error('updateFacebookFriend', err);
                return callback(err);
            }
            else {
                var recordset = result.recordset;
                return callback(err, recordset);
            }
        });
    },

    updateFacebookFriendIsHide: function(facebookId, friendFacebookId, callback) {
        var request = new sql.Request(connection);

        request.input('FacebookId', sql.NVarChar, facebookId);
        request.input('FriendFacebookId', sql.NVarChar, friendFacebookId);

        var query = 'Update FacebookFriends ';
        query += ' Set IsHide = 1 ';
        query += ' Where FacebookId = @FacebookId and FacebookFriendId = @FacebookFriendId ';

        request.query(query, function (err, result) {
            if (err) {
                logger.error('updateFacebookFriendIsHide', err);
                return callback(err);
            }
            else {
                var recordset = result.recordset;
                return callback(err, recordset);
            }
        });
    },

    updateFacebookFriendIsInApp: function (facebookFriendId, AppUserName, callback) {
        var request = new sql.Request(connection);

        request.input('FacebookFriendId', sql.NVarChar, facebookFriendId);
        request.input('AppUserName', sql.NVarChar, AppUserName);

        var query = ' Update FacebookFriends ';
        query += ' Set InApp = 1, ';
        query += ' AppUserName = @AppUserName ';
        query += ' Where FacebookFriendId = @FacebookFriendId ';

        request.query(query, function (err, result) {
            if (err) {
                logger.error('updateFacebookFriendIsInApp', err);
                return callback(err);
            }
            else {
                var recordset = result.recordset;
                return callback(err, recordset);
            }
        });
    },

    getFacebookFriends: function (facebookId, inApp, isInvited, isHide, lastId, count, callback) {
        var request = new sql.Request(connection);
        request.input('FacebookId', sql.NVarChar, facebookId);        

        if (count == undefined || count == 0) {
            count = defaultRowCount;
        }

        var query = ' Select top ' + count + ' * ';
        query += ' From FacebookFriends ';
        query += ' Where FacebookId = @FacebookId ';
                
        if (inApp != undefined && inApp != null && inApp != '' && inApp >= 0) {

            request.input('InApp', sql.Bit, inApp);
            query += ' and InApp = @InApp ';
        }

        if (isInvited != undefined && isInvited != null && isInvited != '' && isInvited >= 0) {
            isInvited = (isInvited == 1);
            request.input('IsInvited', sql.Bit, isInvited);
            query += ' and IsInvited = @IsInvited ';
        }

        if (isHide != undefined && isHide != null && isHide != '' && isHide >= 0) {
            isHide = (isHide == 1);
            request.input('IsHide', sql.Bit, isHide);
            query += ' and IsHide = @IsHide ';
        }

        if (lastId != undefined && lastId != null && lastId != '' && lastId > 0) {
            request.input('LastId', sql.BigInt, lastId);
            query += ' and ItemId > @LastId ';
        }

        request.query(query, function (err, result) {
            if (err) {
                logger.error('getFacebookFriends', err);
                return callback(err);
            }
            else {
                var recordset = result.recordset;
                return callback(err, recordset);
            }
        });
    }
};