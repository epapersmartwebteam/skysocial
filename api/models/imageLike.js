﻿var sql = require('mssql');
var config = require('../myConfig').dataConfig;
var result = require('./result');

var defaultRowCount = require('../myConfig').defaultRowCount;

var logger = require('../helper/logHelper').logger;

var imageLikeMustHaveColumn = [
    'ItemId',
    'ImageItemId',
    'UserId'
];

var imageLikeSchema = [
    'LikeEmotionId',
    'CreateDate'
];

function getImageLikeSelectionField(projectionArray, prefix) {
    var result = imageLikeSchema.filter(function (n) {
        return projectionArray.indexOf(n) > -1;
    });
    result = result.concat(imageLikeMustHaveColumn);

    //tạo mảng clone để lưu tên cột trước khi Add Prefix vào
    var cloneResult = result.slice(0);

    if (prefix) {
        result.forEach(function (item, i) { result[i] = prefix + '.' + item; });
    }

    //duyệt xem nếu là kiểu Datetime thì format lại thành chuỗi yyyy-MM-dd HH:mm:ss.ms
    //result.forEach(function (item, i) {
    //    if (item.indexOf('Date') >= 0) {
    //        result[i] = 'convert(nvarchar, ' + result[i] + ', 120) as ' + cloneResult[i];
    //    }
    //});

    return result.join(',');
}

var connection = new sql.ConnectionPool(config);
connection.connect().catch((error) => {
    logger.error('database connection error', error);
});

connection.on('error', function (err) {
    logger.error('database connection error', err);
    connection = new sql.ConnectionPool(config);
    connection.connect();
});


module.exports = {
    getImageLikeByImageId: function (imageItemId, lastId, lastDate, count, ast, callback) {

        var request = new sql.Request(connection);

        //if (lastDate)
        //    lastDate = new Date(lastDate);

        request.input('ImageItemId', sql.BigInt, parseFloat(imageItemId));
        request.input('LastId', sql.BigInt, parseFloat(lastId));
        request.input('LastDate', sql.BigInt, lastDate);

        //lấy danh sách field
        var selectionField = getImageLikeSelectionField(ast, 'IC');
        if (!selectionField) {
            selectionField = '*';
        }

        if (!count) {
            count = defaultRowCount;
        }

        var query = 'Select top ' + count + ' ' + selectionField;
        query += ' From [ImageLikes] IC';
        query += ' Where IC.ImageItemId = @ImageItemId';

        if (lastId && lastDate) {
            query += ' and (IC.CreateDate < @LastDate or (IC.CreateDate = @LastDate and IC.UserId < @LastId))';
        }
        else if (lastDate) {
            query += ' and (IC.CreateDate < @LastDate)';
        }
        query += ' Order by IC.CreateDate desc, IC.UserId desc';

        request.query(query,
            function (err, res) {
                if (err) {
                    logger.error('getImageLikeByImageId', err);
                    return callback(err, null);
                }
                else {
                    var recordset = res.recordset;
                    if (recordset) {
                        return callback(null, recordset);
                    }
                    else {
                        return callback(null, null);
                    }

                }
            });
    },

    //kiểm tra user đã like 1 hình chưa
    checkUserLiked: function (userId, imageItemId, callback) {
        var request = new sql.Request(connection);
        request.input('ImageItemId', sql.BigInt, parseFloat(imageItemId));
        request.input('UserId', sql.BigInt, parseFloat(userId));

        var query = 'if exists(Select * From [ImageLikes] Where UserId = @UserId and ImageItemId = @ImageItemId) ';
        query += ' Select 1 as Existed';
        query += ' else ';
        query += ' Select 0 as Existed';

        request.query(query, function (err, res) {

            if (err) {
                logger.error('checkUserLiked', err);
                return callback(err);
            }
            else {
                var recordset = res.recordset;
                return callback(null, recordset[0].Existed);
            }
        });
    }
};