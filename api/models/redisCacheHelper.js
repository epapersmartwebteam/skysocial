﻿var _ = require('lodash');
var async = require('async');

var redis = require("redis");
var myConfig = require('../myConfig');
var cacheConfig = myConfig.cacheConfig;
var errorCode = require('../errorCode');

var logger = require('../helper/logHelper').logger;

var resultHelper = require('./result');
var dateHelper = require('../helper/dateHelper');
var utilityHelper = require('../helper/utility');
const { promisify } = require('util');

var db = require('./all');

var state = {
    clientRedis: null,
};

function connect(done) {
    if (state.clientRedis) return done();

    state.clientRedis = redis.createClient(myConfig.redisConfig);
    //var r = redis.createClient(myConfig.redisConfig);

    return done();
}

//////////////HASH/////////////////////
//lấy 1 field cụ thể trong hash
function hget(key, field, callback) {
    state.clientRedis.hget(key, field, (err, reply) => {
        if (err) {
            logger.error('hget', err, key, field);
        }
        callback(err, reply);
    });
}


//lấy tất cả field cụ thể trong hash
function hgetall(key, callback) {
    state.clientRedis.hgetall(key, (err, reply) => {
        if (err) {
            logger.error('hgetall', err, key);
        }
        callback(err, reply);
    });
}


//lấy nhiều field trong hash
function hmget(key, fields, callback) {
    state.clientRedis.hmget(key, fields, (err, reply) => {
        if (err) {
            logger.error('hmget', err, key, fields);
        }
        callback(err, reply);
    });
}


//set 1 field vào hash
function hset(key, field, value, callback) {
    state.clientRedis.hset(key, field, value, (err, reply) => {
        if (err) {
            logger.error('hset', err, key, field, value);
        }
        callback(err);
    });
}

function hmset(key, items, callback) {
    var args = [key];
    _.each(items, (item) => {
        args.push(item.field);
        args.push(JSON.stringify(item.value));
    });
    state.clientRedis.hmset(args, (err, reply) => {
        if (err) {
            logger.error('hmset', err, key, items);
        }
        callback(err);
    });
}

function hkeys(key, callback) {
    state.clientRedis.hkeys(key, (err, reply) => {
        if (err) {
            logger.error('hkeys', err, key);
        }
        callback(err, reply);
    });
}

function hdel(key, field, callback) {
    state.clientRedis.hdel(key, field, (err, reply) => {
        if (err) {
            logger.error('hdel', err, key, field);
        }
        callback(err);
    });
}


///////////SORTEDSET//////////////
//
function zadd(key, items, callback) {
    var args = [key];
    _.each(items, (item) => {
        args.push(item.score);
        args.push(item.member);
    });
    state.clientRedis.zadd(args, (err, response) => {
        if (err) {

            logger.error('zadd', err, key, items);

            callback(err);
        }
        else {

            callback(null);
        }
    });
}

function zrevrangebyscore(key, maxScore, minScore, withScore, offset, count, callback) {
    var args = [key, maxScore, minScore];
    if (withScore === true) {
        args.push('WITHSCORES');
    }
    if ((offset && offset > 0) || (count && count > 0)) {
        args.push('LIMIT');
        args.push((offset ? offset : 0));
        args.push((count ? (count > 0 ? count : Number.MAX_SAFE_INTEGER) : Number.MAX_SAFE_INTEGER));
    }

    state.clientRedis.zrevrangebyscore(args, function (err, response) {
        if (err) {
            logger.error('zrevrangebyscore', err, key, maxScore, minScore, offset, count);
        }
        callback(err, response);
    });
}


function zrangebyscore(key, minScore, maxScore, withScore, offset, count, callback) {
    var args = [key, minScore, maxScore];
    if (withScore === true) {
        args.push('WITHSCORES');
    }
    if ((offset && offset > 0) || (count && count > 0)) {
        args.push('LIMIT');
        args.push((offset ? offset : 0));
        args.push((count ? (count > 0 ? count : Number.MAX_SAFE_INTEGER) : Number.MAX_SAFE_INTEGER));
    }
    state.clientRedis.zrangebyscore(args, function (err, response) {
        if (err) {
            logger.error('zrangebyscore', err, key, maxScore, minScore, offset, count);
        }
        callback(err, response);
    });
}


function zrem(key, member, callback) {
    state.clientRedis.zrem(key, member, (err, response) => {
        if (err) {
            logger.error('zrem', err, key, member);
        }
        callback(err);
    });
}

function zscore(key, member, callback) {
    state.clientRedis.zscore(key, member, (err, score) => {
        if (err) {
            logger.error('zscore', err, key, member);
            callback(null);
        }
        else {
            callback(score);
        }
    });
}

/////////////////
function get(key, callback) {
    state.clientRedis.get(key, (err, reply) => {
        if (err) {
            logger.error('get', err, key);
        }
        callback(err, reply);
    });
}


function set(key, value) {
    
    state.clientRedis.set(key, value);
}


function del(key, callback) {
    state.clientRedis.del(key, (err, reply) => {
        if (err) {
            logger.error('del', err, key);
        }
        callback();
    });
}

function checkToClearCacheMTP(imageItemId, callback) {
    if (cacheConfig.enableCache) {
        var getAsync = promisify(state.clientRedis.get).bind(state.clientRedis);

        getAsync(cacheConfig.MTPTabKey).then(function (res) {
            var items = utilityHelper.checkAndParseToJson(res);
            var x = _.find(items, (item) => {
                return item.ImageItemId == imageItemId;
            });

            if (x) {
                del(cacheConfig.MTPTabKey, () => { });
            }
            callback();
        })
            .catch(function (err) {
                callback();
            });
    }
}

function checkUserIsFeatureUser(userId, needCheck, callback) {
    if (needCheck) {
        db.user.checkUserIsFeatureUser(userId, (resX) => {
            if (resX.ErrorCode == errorCode.success) {
                callback(true);
            }
            else {
                callback(false);
            }
        });
    }
    else {
        callback(true);
    }
}

function addNewPostToFeatureTab(imageItem, callback) {
    if (cacheConfig.enableCache) {
        checkUserIsFeatureUser(imageItem.UserId, true, (resX) => {
            if (resX) {

                var items = [{ member: imageItem.ImageItemId, score: imageItem.CreateDate }];

                var addToSet = (cb) => {
                    zadd(cacheConfig.FeatureTabSetKey, items, (err) => {
                        cb(err);
                    });
                };

                var addToHash = (cb) => {
                    hset(cacheConfig.FeatureTabHashKey, imageItem.ImageItemId, JSON.stringify(imageItem), (err) => {
                        cb(err);
                    });
                };

                async.parallel([addToSet, addToHash],
                    (err) => {
                        if (err) {
                            callback(resultHelper.dataError(err));
                        }
                        else {
                            callback(resultHelper.success());
                        }
                    }
                );
            }
        });
    }
    else {
        callback(resultHelper.success());
    }
}

function syncUpdateDatePostToSet(imageItemId, callback) {
    hset(cacheConfig.FeatureTabUpdatedSetKey, imageItemId, dateHelper.getUnixTimeStamp(), (err, reply) => {
        callback();
    });
}

function syncNewActivityPostToSet(imageItemId, callback) {
    hset(cacheConfig.FeatureTabActivitySetKey, imageItemId, dateHelper.getUnixTimeStamp(), (err, reply) => {
        callback();
    });
}

function updatePostInfoToFeatureTab(userId, imageItemId, description, isPrivate, location, lat, long, callback) {
    if (cacheConfig.enableCache) {
        checkUserIsFeatureUser(userId, true, (resX) => {
            if (resX) {
                if (isPrivate === true) {
                    //xoá khỏi cache vì là private
                    deletePostFromFeatureTab(imageItemId, callback);
                }
                else {

                    //sync vào set ngày update, để truy vấn khi cần tìm xem những post có ngày update mới hơn
                    syncUpdateDatePostToSet(imageItemId, () => {
                    });

                    var getImageItem = (cb) => {
                        db.imageItem.getImageItemById(userId, imageItemId, db.imageItem.imageItemSchema, (err, data) => {
                            cb(data);
                        });
                    };

                    var addNewPost = (cb) => {
                        getImageItem((data) => {
                            if (data) {
                                addNewPostToFeatureTab(data, () => {

                                });
                            }
                        });
                    };



                    hget(cacheConfig.FeatureTabHashKey, imageItemId, (err, imageItem) => {
                        if (err) {
                            addNewPost(() => { callback(resultHelper.success()); });
                        }
                        else {
                            if (imageItem) {

                                try {
                                    imageItem = utilityHelper.checkAndParseToJson(imageItem);
                                }
                                catch (ex) {
                                }


                                if (description != undefined) {
                                    imageItem.Description = description;
                                }
                                if (location != undefined) {
                                    imageItem.Location = location;
                                }
                                if (lat != undefined) {
                                    imageItem.Lat = lat;
                                }
                                if (long != undefined) {
                                    imageItem.Long = long;
                                }

                                hset(cacheConfig.FeatureTabHashKey, imageItemId, JSON.stringify(imageItem), () => {
                                    callback(resultHelper.success());
                                });
                            }
                            else {
                                addNewPost(() => { callback(resultHelper.success()); });
                            }
                        }
                    });
                }
            }
        });
    }
    else {
        callback(resultHelper.success());
    }
}

function updatePostExtraToFeatureTab(userId, imageItemId, likeCount, shareCount, commentCount, commentingOff, needCheckIsFeature, callback) {
    if (cacheConfig.enableCache) {

        checkUserIsFeatureUser(userId, needCheckIsFeature, (resX) => {
            if (resX) {

                hget(cacheConfig.FeatureTabHashKey, imageItemId, (err, imageItem) => {
                    if (err) {
                        callback(resultHelper.dataError());
                    }
                    else {
                        //sync ngày có activity cuối vào set để truy vấn kiểm tra có activity mới hơn
                        syncNewActivityPostToSet(imageItemId, () => {

                        });

                        if (imageItem) {

                            try {
                                imageItem = utilityHelper.checkAndParseToJson(imageItem);
                            }
                            catch (ex) {
                            }


                            if (likeCount != undefined) {
                                imageItem.LikeCount = likeCount;
                            }

                            if (shareCount != undefined) {
                                imageItem.ShareCount = shareCount;
                            }

                            if (commentCount != undefined) {
                                imageItem.CommentCount = commentCount;
                            }

                            if (commentingOff != undefined) {
                                imageItem.CommentingOff = commentingOff;
                            }

                            hset(cacheConfig.FeatureTabHashKey, imageItemId, JSON.stringify(imageItem), () => {
                                callback(resultHelper.success());
                            });
                        }
                        else {
                            callback(resultHelper.success());
                        }
                    }
                });
            }
            else {
                callback(resultHelper.success());
            }
        });
    }
    else {
        callback(resultHelper.success());
    }
}

function updatePostCommentCountToFeatureTab(imageItemId, isAdd, callback) {
    if (cacheConfig.enableCache) {
        hget(cacheConfig.FeatureTabHashKey, imageItemId, (err, imageItem) => {
            if (err) {
                callback(resultHelper.success());
            }
            else {

                if (imageItem) {
                    //sync ngày có activity cuối vào set để truy vấn kiểm tra có activity mới hơn
                    syncNewActivityPostToSet(imageItemId, () => {

                    });

                    try {
                        imageItem = utilityHelper.checkAndParseToJson(imageItem);
                    }
                    catch (ex) {
                    }

                    if (!imageItem.CommentCount) {
                        imageItem.CommentCount = 0;
                    }

                    if (isAdd) {
                        imageItem.CommentCount++;
                    }
                    else {
                        imageItem.CommentCount--;
                    }
                    hset(cacheConfig.FeatureTabHashKey, imageItemId, JSON.stringify(imageItem), (err, reply) => {
                        callback(resultHelper.success());
                    });
                }
                else {
                    callback(resultHelper.success());
                }
            }
        });
    }
    else {
        callback(resultHelper.success());
    }
}

function deletePostFromFeatureTab(userId, imageItemId, callback) {
    if (cacheConfig.enableCache) {
        checkUserIsFeatureUser(userId, true, (resX) => {
            if (resX) {
                //xoá khỏi set feature
                zrem(cacheConfig.FeatureTabSetKey, imageItemId, () => { });
                //xoá khỏi set ngày update cuối
                zrem(cacheConfig.FeatureTabUpdatedSetKey, imageItemId, () => { });
                //xoá khỏi set ngày hoạt động cuối
                zrem(cacheConfig.FeatureTabActivitySetKey, imageItemId, () => { });



                //xoá khỏi list feature
                hdel(cacheConfig.FeatureTabHashKey, imageItemId, () => { });
            }
            callback(resX);
        });
    }
    else {
        callback(resultHelper.success());
    }
}

function checkPostIsFeatureCache(imageItemId, callback) {
    hget(cacheConfig.FeatureTabHashKey, imageItemId, (err, imageItem) => {
        if (err) {
            callback(resultHelper.dataError(err));
        }
        else {
            if (imageItem) {
                callback(resultHelper.success(imageItem));
            }
            else {
                callback(resultHelper.notExists());
            }
        }
    });
}

function getPostFromFeatureCache(max, min, count, getNewPost, callback) {
    var getHashs = (err, fields, cb) => {
        if (err) {
            cb(resultHelper.dataError(err));
        }
        else {
            if (fields && fields.length > 0) {
                hmget(cacheConfig.FeatureTabHashKey, fields, (errX, lst) => {
                    if (errX) {
                        cb(resultHelper.dataError(errX));
                    }
                    else {
                        try {
                            _.each(lst, (item, idx) => {

                                lst[idx] = utilityHelper.checkAndParseToJson(lst[idx]);
                            });
                        }
                        catch (ex) {

                        }

                        cb(resultHelper.success(lst));
                    }
                });
            }
            else {
                cb(resultHelper.success([]));
            }
        }
    };

    if (getNewPost) {
        zrangebyscore(cacheConfig.FeatureTabSetKey, min, max, false, 0, count, (err, fields) => {

            getHashs(err, fields, callback);
        });
    }
    else {
        zrevrangebyscore(cacheConfig.FeatureTabSetKey, max, min, false, 0, count, (err, fields) => {

            getHashs(err, fields, callback);
        });
    }
}

function getPostsFromFeatureTab(lastDate, lastId, count, getNewPost, callback) {
    var max, min;
    if (getNewPost) {
        max = '+inf';
        if (lastDate) {

            min = lastDate + 1;
            getPostFromFeatureCache(max, min, count, getNewPost, callback);
        }
        else if (lastId) {
            zscore(cacheConfig.FeatureTabSetKey, lastId, (score) => {

                if (!score) {
                    min = '+inf';
                }
                else {
                    min = parseInt(score) + 1;
                }

                getPostFromFeatureCache(max, min, count, getNewPost, callback);
            });
        }
        else {
            min = '-inf';
            getPostFromFeatureCache(max, min, count, getNewPost, callback);
        }
    }
    else {
        min = '-inf';
        if (lastDate) {
            max = lastDate - 1;

            getPostFromFeatureCache(max, min, count, getNewPost, callback);
        }
        else if (lastId) {
            zscore(cacheConfig.FeatureTabSetKey, lastId, (score) => {
                if (!score) {
                    max = '+inf';
                }
                else {
                    max = parseInt(score) - 1;
                }

                getPostFromFeatureCache(max, min, count, getNewPost, callback);
            });

        }
        else {
            max = '+inf';
            getPostFromFeatureCache(max, min, count, getNewPost, callback);
        }
    }
}

function initSyncPostToFeatureTab(callback) {
    var lastId = null, lastDate = null, count = cacheConfig.itemPerSync;

    var deleteHash = (cb) => {
        del(cacheConfig.FeatureTabHashKey, () => {

            cb();
        });
    };

    var deleteKey = (cb) => {
        del(cacheConfig.FeatureTabSetKey, () => {

            cb();
        });
    };

    var getAndSync = (cb) => {
        getAndSyncToFeatureTab(lastId, lastDate, count, () => {
            cb();
        });
    };

    var syncDeletePost = (cb) => {
        getAndSyncDeletePostToFeatureTab(() => {
            cb();
        });
    };

    async.series([deleteHash, deleteKey, getAndSync, syncDeletePost], (err) => {
        callback();
    });
}

function getAndSyncDeletePostToFeatureTab(callback) {
    db.imageItem.getDeletedFeaturefeedsFromDB((err, lst) => {
        if (err) {
            callback(resultHelper.dataError(err));
        }
        else {
            addDeletePostToFeatureTab(lst, () => {
                callback(resultHelper.success());
            });
        }
    });
}

function getAndSyncToFeatureTab(lastId, lastDate, count, callback) {
    db.imageItem.getFeaturefeedsFromDB(lastId, lastDate, count, (err, lst) => {
        if (err) {
            callback(resultHelper.dataError(err));
        }
        else {
            var commentOfItems = [];
            async.eachLimit(lst, myConfig.cacheConfig.parallelItemMax, (item, cb) => {
                db.imageComment.getTopCommentOfFeaturePost(item.ImageItemId, myConfig.cacheConfig.commentPerPost, (err, items) => {
                    commentOfItems.push({ ImageItemId: item.ImageItemId, Comments: items });
                    cb();
                });
            }, (errX) => {

                syncPostToFeatureTab(lst, commentOfItems, () => {

                    if (lst.length == count) {
                        lastId = lst[lst.length - 1].ImageItemId;
                        lastDate = lst[lst.length - 1].CreateDate;
                        getAndSyncToFeatureTab(lastId, lastDate, count, callback);
                    }
                    else {
                        callback(resultHelper.success());
                    }
                });
            });
        }
    });
}

function syncPostCommentToFeatureTab(imageItemIds, commentOfItems, callback) {
    var commentHashes = [];
    _.each(imageItemIds, (imageItemId) => {
        var commentItem = _.find(commentOfItems, (x) => {
            return x.ImageItemId == imageItemId;
        });

        if (commentItem) {
            commentHashes.push({
                field: commentItem.ImageItemId,
                value: commentItem.Comments
            });
        }
    });

    hmset(cacheConfig.FeatureTabPostCommentHashKey, commentHashes, () => {
        callback();
    });
}

function syncPostToFeatureTab(items, commentOfItems, callback) {
    var hashes = [];
    var sets = [];

    _.each(items, (item) => {
        hashes.push({
            field: item.ImageItemId,
            value: item
        });

        sets.push({
            member: item.ImageItemId,
            score: item.CreateDate
        });
    });


    var addHash = (cb) => {
        hmset(cacheConfig.FeatureTabHashKey, hashes, () => {
            cb();
        });
    };

    var addSet = (cb) => {
        zadd(cacheConfig.FeatureTabSetKey, sets, () => {
            cb();
        });
    };

    var addCommentHash = (cb) => {
        var imageItemIds = _.map(items, (item) => {
            return item.ImageItemId;
        });
        syncPostCommentToFeatureTab(imageItemIds, commentOfItems, () => {
            cb();
        });
    };

    async.parallel([addHash, addSet, addCommentHash], (err) => {
        callback();
    });
}

function getFeatureFeedNewPostCount(lastSyncDate, callback) {
    var max = '+inf', min = parseInt(lastSyncDate) + 1;
    zrangebyscore(cacheConfig.FeatureTabSetKey, min, max, false, 0, 0, (err, reply) => {
        if (err) {
            callback(resultHelper.dataError(err));
        }
        else {
            var count = 0;
            if (reply) {
                count = reply.length;
            }

            callback(resultHelper.success(count));
        }
    });
}

function getFeatureFeedUpdatePostId(lastSyncDate, callback) {
    var max = '+inf', min = parseInt(lastSyncDate) + 1;
    zrangebyscore(cacheConfig.FeatureTabUpdatedSetKey, min, max, false, 0, 0, (err, reply) => {
        if (err) {
            callback(resultHelper.dataError(err));
        }
        else {
            var lst = [];
            if (reply) {
                _.each(reply, (item) => {
                    lst.push(parseInt(item));
                });
            }

            callback(resultHelper.success(lst));
        }
    });
}

function getFeatureFeedNewActivityPostId(lastSyncDate, callback) {
    var max = '+inf', min = parseInt(lastSyncDate) + 1;
    zrangebyscore(cacheConfig.FeatureTabActivitySetKey, min, max, false, 0, 0, (err, reply) => {
        if (err) {
            callback(resultHelper.dataError(err));
        }
        else {
            var lst = [];
            if (reply) {
                _.each(reply, (item) => {
                    lst.push(parseInt(item));
                });
            }

            callback(resultHelper.success(lst));
        }
    });
}

function getFeatureFeedDeletePostId(lastSyncDate, callback) {
    var max = '+inf', min = parseInt(lastSyncDate) + 1;
    zrangebyscore(cacheConfig.FeatureTabDeleteSetKey, min, max, false, 0, 0, (err, reply) => {
        if (err) {
            callback(resultHelper.dataError(err));
        }
        else {
            var lst = [];
            if (reply) {
                _.each(reply, (item) => {
                    lst.push(parseInt(item));
                });
            }

            callback(resultHelper.success(lst));
        }
    });
}

function addDeletePostToFeatureTab(items, callback) {
    var sets = [];
    _.each(items, (item) => {
        sets.push({
            member: item.ImageItemId, score: item.DeleteDate
        });
    });

    zadd(cacheConfig.FeatureTabDeleteSetKey, sets, () => {
        callback();
    });
}

function getFeaturePostComment(imageItemId, topN, callback) {
    hget(cacheConfig.FeatureTabPostCommentHashKey, imageItemId, (err, comments) => {
        if (err) {
            callback(resultHelper.dataError(err));
        }
        else {
            var lst = [];
            if (comments && comments.length > 0) {
                comments = utilityHelper.checkAndParseToJson(comments);
                lst = _.take(comments, topN);
            }
            callback(resultHelper.success(lst));
        }
    });
}

function updateFeaturePostComment(imageItemId, comment, callback) {
    //kiểm tra đang nằm trong cache Feature thì mới update
    checkPostIsFeatureCache(imageItemId, (result) => {
        if (result.ErrorCode == 0) {
            //lấy thông tin user, vì bên hàm kia ko có lấy mà chỉ có UserId
            db.user.getUserById(comment.UserId, ["AvatarSmall"], (resU) => {
                if (resU.ErrorCode == 0) {
                    var user = resU.Data;
                    //gán thêm thông tin User vào
                    comment.User = {
                        UserName: user.UserName, AvatarSmall: user.AvatarSmall
                    };
                    //lấy item chưa danh sách comment ra
                    hget(cacheConfig.FeatureTabPostCommentHashKey, imageItemId, (err, comments) => {
                        if (err) {
                            callback(resultHelper.dataError(err));
                        }
                        else {
                            if (!comments) {
                                comments = [];
                            }
                            else {
                                comments = utilityHelper.checkAndParseToJson(comments);
                            }
                            //thêm item mới vào
                            comments.push(comment);
                            //xoá item đầu đi nếu lớn hơn so với số comment trên post
                            while (comments.length > cacheConfig.commentPerPost) {
                                comments.shift();
                            }
                            //gán lại vào hash
                            hset(cacheConfig.FeatureTabPostCommentHashKey, imageItemId, comments, () => {
                                callback(resultHelper.success());
                            });
                        }
                    });
                }
                else {
                    callback(resultHelper.success());
                }
            });
        }
        else {
            callback(resultHelper.success());
        }
    });
}

function deleteFeaturePostComment(imageItemId, commentId, callback) {
    //kiểm tra đang nằm trong cache Feature thì mới update
    checkPostIsFeatureCache(imageItemId, (result) => {
        if (result.ErrorCode == 0) {
            hget(cacheConfig.FeatureTabPostCommentHashKey, imageItemId, (err, comments) => {
                if (err) {
                    callback(resultHelper.dataError(err));
                }
                else {
                    if (!comments) {
                        comments = [];
                    }
                    else {
                        comments = utilityHelper.checkAndParseToJson(comments);
                    }
                    _.remove(comments, (comment) => {
                        return comment.ItemId == commentId;
                    });
                    //gán lại vào hash
                    hset(cacheConfig.FeatureTabPostCommentHashKey, imageItemId, comments, () => {
                        callback(resultHelper.success());
                    });
                    callback(resultHelper.success());
                }
            });
        }
        else {
            callback(resultHelper.success());
        }
    });
}

module.exports = {
    connect: connect,
    checkToClearCacheMTP: checkToClearCacheMTP,
    addNewPostToFeatureTab: addNewPostToFeatureTab,
    updatePostInfoToFeatureTab: updatePostInfoToFeatureTab,
    updatePostExtraToFeatureTab: updatePostExtraToFeatureTab,
    updatePostCommentCountToFeatureTab: updatePostCommentCountToFeatureTab,
    deletePostFromFeatureTab: deletePostFromFeatureTab,
    getPostsFromFeatureTab: getPostsFromFeatureTab,
    initSyncPostToFeatureTab: initSyncPostToFeatureTab,
    getFeatureFeedNewPostCount: getFeatureFeedNewPostCount,
    getFeatureFeedUpdatePostId: getFeatureFeedUpdatePostId,
    getFeatureFeedNewActivityPostId: getFeatureFeedNewActivityPostId,
    getFeatureFeedDeletePostId: getFeatureFeedDeletePostId,
    addDeletePostToFeatureTab: addDeletePostToFeatureTab,
    getFeaturePostComment: getFeaturePostComment,
    updateFeaturePostComment: updateFeaturePostComment,
    deleteFeaturePostComment: deleteFeaturePostComment,
    syncPostCommentToFeatureTab: syncPostCommentToFeatureTab
};