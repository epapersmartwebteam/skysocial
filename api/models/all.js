﻿exports.user = require('./user');
exports.userEndpoint = require('./userEndpoint');
exports.userNotify = require('./userNotify');
exports.userMention = require('./userMention');
exports.userReport = require('./userReport');
exports.userSave = require('./userSave');
exports.userHashtag = require('./userHashtag');

///user library
exports.userLibrary = require('./userLibrary');

exports.imageItem = require('./imageItem');
exports.imageComment = require('./imageComment');
exports.imageLike = require('./imageLike');
exports.imageShare = require('./imageShare');
exports.hashtag = require('./hashtag');
exports.imageReport = require('./imageReport');

exports.search = require('./search');

exports.chat = require('./chat');
exports.facebook = require('./facebook');
exports.system = require('./system');

///////////////////
//media
exports.media = require('./hub/media');
exports.mediaLike = require('./hub/mediaLike');
exports.mediaItem = require('./hub/mediaItem');
exports.mediaComment = require('./hub/mediaComment');
exports.menu = require('./hub/menu');

////////////////////////////
//shop
exports.shop = {
    paymentLog: require('./shop/paymentLog'),
    vipConfig: require('./shop/vipConfig'),
};

exports.promotion = require('./promotion');

//insight
exports.insight = require('./insight');

exports.sync = require('./sync');