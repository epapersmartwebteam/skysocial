﻿var sql = require('mssql');

var moment = require('moment');
var momentDateTimeFormat = require('../myConfig').momentDateTimeFormat;

//lấy config chuỗi kết nối
var config = require('../myConfig').dataConfig;
var defaultRowCount = require('../myConfig').defaultRowCount;

var utilityHelper = require('../helper/utility');

var CDNUrl = require('../myConfig').CDNUrl;

var convertToSQLBitType = require('../helper/utility').convertToSQLBitType;

var logger = require('../helper/logHelper').logger;

var connection = new sql.ConnectionPool(config);
connection.connect().catch((error) => {
    logger.error('database connection error', error);
});

connection.on('error', function (err) {
    logger.error('database connection error', err);
    connection = new sql.ConnectionPool(config);
    connection.connect();
});


module.exports = {
    getUserNotifyCount: function (userId, callback) {
        var request = new sql.Request(connection);
        request.input('UserId', sql.BigInt, userId);
        var query = 'Select count(*) as UnreadCount ';
        query += ' From UserNotifies ';
        query += ' Where UserId = @UserId and (IsDelete = 0 or IsDelete is null) ';
        query += ' and IsViewed = 0 ';
        request.query(query, function (err, result) {
            if (err) {
                logger.error('getUserNotifyCount', err);
                callback(err);
            }
            else {
                var recordset = result.recordset;
                var count = recordset[0].UnreadCount;
                callback(null, count);
            }
        });
    },
    //lấy notify của user
    getUserNotify: function (userId, isViewed, lastId, itemCount, callback) {
        var request = new sql.Request(connection);
        if (!itemCount || itemCount == 0) {
            itemCount = defaultRowCount;
        }
        request.input('UserId', sql.BigInt, userId);
        request.input('IsViewed', sql.Int, isViewed);
        request.input('LastId', sql.BigInt, lastId);
        request.input('ItemCount', sql.BigInt, itemCount);

        var query = " Select top (@ItemCount) uf.NotifyId, uf.UserId, uf.NotifyTypeId, uf.NotifyType, uf.Message, uf.MessageEN, ";
        query += " uf.ActionUserId, uf.TargetUserId, uf.ImageItemId, uf.CommentId, uf.Comment, uf.ChatRoomId, uf.ChatLogId, uf.MediaId, ";
        query += " uf.CreateDate, uf.IsViewed, uf.ViewDate, ";
        query += " u.UserName as ActionUserName, u.AvatarSmall as ActionUserAvatar, ";
        query += " case when uf.ImageItemId is not null then (";
        query += " case when (ISJSON(I.ItemContent) > 0) then ";
        query += " ('" + CDNUrl + "' + (JSON_Value(I.ItemContent, '$[0].Small.Link'))) else '' end) ";
        query += " when uf.MediaId is not null then ( ";
        query += "'" + CDNUrl + "' + M.Thumbnail) ";
        query += " else '' end  as ImageUrl";
        query += " From UserNotifies uf inner join Users u on uf.ActionUserId = u.UserId ";
        query += " left join ImageItems I on uf.ImageItemId = I.ImageItemId ";
        query += " left join Medias M on uf.MediaId = M.MediaId ";
        query += " Where uf.UserId = @UserId and (uf.IsDelete = 0 or uf.IsDelete is null)";
        query += ' and (I.ImageItemId is null or I.IsDelete != 1) ';
        if (isViewed != -1) {
            query += ' and IsViewed = @IsViewed ';
        }

        if (lastId && lastId > 0) {
            query += ' and NotifyId < @LastId ';
        }
        query += ' Order by uf.NotifyId desc';
        
        request.query(query, function (err, result) {
            var recordset = [];
            if (!err) {
                recordset = result.recordset;
                //parse lại sang kiểu Int do các field này kiểu BigInt bị biến thành Kiểu String
                recordset.forEach(function (item, i) {
                    recordset[i].NotifyId = parseInt(item.NotifyId, 10);
                    recordset[i].UserId = parseInt(item.UserId, 10);
                    recordset[i].ActionUserId = parseInt(item.ActionUserId, 10);
                    recordset[i].TargetUserId = parseInt(item.TargetUserId, 10);
                    recordset[i].ImageItemId = parseInt(item.ImageItemId, 10);
                    recordset[i].CommentId = parseInt(item.CommentId, 10);
                    recordset[i].ChatRoomId = parseInt(item.ChatRoomId, 10);
                    recordset[i].ChatLogId = parseInt(item.ChatLogId, 10);
                    recordset[i].MediaId = parseInt(item.MediaId, 10);
                    recordset[i].CreateDate = parseInt(item.CreateDate, 10);
                    //gán luôn đường dẫn hình
                    recordset[i].ActionUserAvatar = CDNUrl + item.ActionUserAvatar;
                });
            }
            else {
                logger.error('getUserNotify', err);
            }
            return callback(err, recordset);
        });
    },

    //thêm user notify
    addUserNotify: function (userId, notifyTypeId, notifyType, message, actionUserId, targetUserId, imageItemId, commentId, chatRoomId, chatLogId, callback) {
        var request = new sql.Request(connection);

        request.input('UserId', sql.BigInt, userId);
        request.input('NotifyTypeId', sql.Int, notifyTypeId);
        request.input('NotifyType', sql.NVarChar, notifyType);
        request.input('Message', sql.NVarChar, message);
        request.input('ActionUserId', sql.BigInt, actionUserId);
        request.input('TargetUserId', sql.BigInt, targetUserId);
        request.input('ImageItemId', sql.BigInt, imageItemId);
        request.input('CommentId', sql.BigInt, commentId);
        request.input('ChatRoomId', sql.BigInt, chatRoomId);
        request.input('ChatLogId', sql.BigInt, chatLogId);

        var query = ' Insert into UserNotifies(UserId, NotifyTypeId, NotifyType, Message, ActionUserId, TargetUserId, ImageItemId, CommentId, ChatRoomId, ChatLogId, CreateDate, IsViewed) ';
        query += ' Values(@UserId, @NotifyTypeId, @NotifyType, @Message, @ActionUserId, @TargetUserId, @ImageItemId, @CommentId, @ChatRoomId, @ChatLogId, ([dbo].[UNIX_TIMESTAMP](getutcdate())), 0)';

        request.query(query, function (err, result) {
            if (err) {
                logger.error('addUserNotify', err);
                return callback(err);
            }
            else {
                var recordset = result.recordset;
                return callback(err, recordset);
            }
        });
    },

    updateUserNotifyLikeIsDelete: function (actionUserId, imageItemId, callback) {
        var request = new sql.Request(connection);
        request.input('ActionUserId', sql.BigInt, actionUserId);
        request.input('ImageItemId', sql.BigInt, imageItemId);

        var query = " Update UserNotifies ";
        query += " Set IsDelete = 1 ";
        query += " Where ActionUserId = @ActionUserId and ImageItemId = @ImageItemId and (NotifyType = 'NewLike' or NotifyType = 'FollowingLike') ";

        request.query(query, function (err, result) {
            if (err) {
                logger.error('updateUserNotifyLikeIsDelete', err);
                return callback(err);
            }
            else {
                var recordset = result.recordset;
                return callback(err, recordset);
            }
        });
    },

    deleteUserNotifyCommentWhenDeleteComment(commentId, callback) {
        var request = new sql.Request(connection);
        request.input('CommentId', sql.BigInt, commentId);

        var query = 'Delete From UserNotifies ';
        query += ' Where CommentId = @CommentId and ImageItemId is not null';

        request.query(query, function (err, result) {
            if (err) {
                logger.error('deleteUserNotifyCommentWhenDeleteComment', err);
                return callback(err);
            }
            else {
                var recordset = result.recordset;
                return callback(err, recordset);
            }
        });
    },

    //cập nhật user notify đã xem cho tới notifyId truyền vào
    updateUserNotifyIsView: function (notifyId, callback) {
        var request = new sql.Request(connection);

        request.input('NotifyId', sql.BigInt, notifyId);

        var query = ' Update UserNotifies ';
        query += ' Set IsViewed =  1, ';
        query += ' ViewDate = ([dbo].[UNIX_TIMESTAMP](getutcdate())) ';
        query += ' Where NotifyId = @NotifyId and IsViewed = 0';

        request.query(query, function (err, result) {
            if (err) {
                logger.error('updateUserNotifyIsView', err);
            }
            
            return callback(err);
        });
    },

    //cập nhật danh sách user notify đã xem (mảng Id đc truyền vào)
    updateUserNotifyListIsView: function (notifyIdList, callback) {
        var request = new sql.Request(connection);

        notifyIdList.forEach(function (notifyId) {
            request.input('NotifyId', sql.BigInt, notifyId);

            var query = ' Update UserNotifies ';
            query += ' Set IsViewed =  1, ';
            query += ' ViewDate = ([dbo].[UNIX_TIMESTAMP](getutcdate())) ';
            query += ' Where NotifyId = @NotifyId ';

            request.query(query, function (err, recordset) {
            });
        });
        callback(null, { Success: true });
    },

    //cập nhật tất cả user notify đã xem
    updateAllUserNotifyIsView: function (userId, callback) {
        var request = new sql.Request(connection);

        request.input('UserId', sql.BigInt, userId);

        var query = ' Update UserNotifies ';
        query += ' Set IsViewed =  1, ';
        query += ' ViewDate = ([dbo].[UNIX_TIMESTAMP](getutcdate())) ';
        query += ' Where UserId = @UserId and IsViewed = 0';

        request.query(query, function (err, result) {
            if (err) {
                logger.error('updateAllUserNotifyIsView', err);
                return callback(err);
            }
            else {
                var recordset = result.recordset;
                return callback(err, recordset);
            }
        });
    },

    //xóa các notify liên quan tới 1 hình
    deleteUserNotifyByImage: function (imageItemId, callback) {
        var request = new sql.Request(connection);
        request.input('ImageItemId', sql.BigInt, imageItemId);

        var query = 'Delete From UserNotifies Where ImageItemId = @ImageItemId';
        request.query(query, function (err, result) {
            if (err) {
                logger.error('deleteUserNotifyByImage', err);
                return callback(err);
            }
            else {
                var recordset = result.recordset;
                return callback(err, recordset);
            }
        });
    },

    //lấy cấu hình notify
    getUserNotifySetting: function (userId, os, callback) {
        var request = new sql.Request(connection);

        request.input('UserId', sql.BigInt, userId);

        var query = 'Select * From UserNotifySettings Where UserId = @UserID';

        request.query(query,
            function (err, result) {
                if (!err) {
                    var res;
                    var recordset = result.recordset;
                    if (recordset == undefined || recordset.length == 0) {

                        res = {
                            UserId: userId,
                            NewComment: true,
                            NewLike: true,
                            NewFollower: true,
                            FollowAccepted: true,
                            FollowUploadImage: true,
                            MentionInComment: true,
                            CommentHasReply: true,
                            CommentHasLike: true,
                            FollowingAction: true,
                            Chat: true,
                            LastModifyDate: moment().format(momentDateTimeFormat),
                            LastModifyOS: os
                        };
                    }
                    else {
                        //parse lại UserId do kiểu BigInt mặc định sẽ thành kiểu chuỗi
                        recordset[0].UserId = userId;  //userId truyền vào là kiểu Int nên chỉ cần gán lại
                        recordset[0].NewComment = utilityHelper.convertToBool(recordset[0].NewComment);
                        recordset[0].NewLike = utilityHelper.convertToBool(recordset[0].NewLike);
                        recordset[0].NewFollower = utilityHelper.convertToBool(recordset[0].NewFollower);
                        recordset[0].FollowAccepted = utilityHelper.convertToBool(recordset[0].FollowAccepted);
                        recordset[0].FollowUploadImage = utilityHelper.convertToBool(recordset[0].FollowUploadImage);
                        recordset[0].MentionInComment = utilityHelper.convertToBool(recordset[0].MentionInComment);
                        recordset[0].CommentHasReply = utilityHelper.convertToBool(recordset[0].CommentHasReply);
                        recordset[0].CommentHasLike = utilityHelper.convertToBool(recordset[0].CommentHasLike);
                        recordset[0].FollowingAction = utilityHelper.convertToBool(recordset[0].FollowingAction);
                        recordset[0].Chat = utilityHelper.convertToBool(recordset[0].Chat);
                        res = recordset[0];
                    }
                    return callback(err, res);
                }
                else {
                    logger.error('getUserNotifySetting', err);
                    return callback(err);
                }
            });
    },

    //set cấu hình notify
    setUserNotifySetting: function (userId, newComment, newLike, newFollower, followAccepted, followUploadImage,
        mentionInComment, commentHasReply, commentHasLike, followingAction, chat, os, callback) {
        var request = new sql.Request(connection);
        console.log(followUploadImage);
        var test = convertToSQLBitType(followUploadImage);
        console.log(test);

        request.input('UserId', sql.BigInt, userId);
        request.input('NewComment', sql.Bit, convertToSQLBitType(newComment));
        request.input('NewLike', sql.Bit, convertToSQLBitType(newLike));
        request.input('NewFollower', sql.Bit, convertToSQLBitType(newFollower));
        request.input('FollowAccepted', sql.Bit, convertToSQLBitType(followAccepted));
        request.input('FollowUploadImage', sql.Bit, convertToSQLBitType(followUploadImage));
        request.input('MentionInComment', sql.Bit, convertToSQLBitType(mentionInComment));
        request.input('CommentHasReply', sql.Bit, convertToSQLBitType(commentHasReply));
        request.input('CommentHasLike', sql.Bit, convertToSQLBitType(commentHasLike));
        request.input('FollowingAction', sql.Bit, convertToSQLBitType(followingAction));
        request.input('Chat', sql.Bit, convertToSQLBitType(chat));
        request.input('OS', sql.VarChar, os);

        var query = 'If (exists(Select * From UserNotifySettings Where UserId = @UserId)) ';
        query += ' Begin ';
        query += ' Update UserNotifySettings ';
        query += ' Set NewComment = @NewComment, ';
        query += ' NewLike = @NewLike, ';
        query += ' NewFollower = @NewFollower, ';
        query += ' FollowAccepted = @FollowAccepted, ';
        query += ' FollowUploadImage = @FollowUploadImage, ';
        query += ' MentionInComment = @MentionInComment, ';
        query += ' CommentHasReply = @CommentHasReply, ';
        query += ' CommentHasLike = @CommentHasLike, ';
        query += ' FollowingAction = @FollowingAction, ';
        query += ' Chat = @Chat, ';
        query += ' LastModifyDate = ([dbo].[UNIX_TIMESTAMP](getutcdate())), ';
        query += ' LastModifyOS = @OS ';
        query += ' Where UserId = @UserId ';
        query += ' End ';
        query += ' Else ';
        query += ' Begin ';
        query += ' Insert into UserNotifySettings(UserId, NewComment, NewLike, NewFollower, FollowAccepted, FollowUploadImage, MentionInComment, CommentHasReply, CommentHasLike, FollowingAction, Chat, LastModifyDate, LastModifyOS) ';
        query += ' Values(@UserId, @NewComment, @NewLike, @NewFollower, @FollowAccepted, @FollowUploadImage, @MentionInComment, @CommentHasReply, @CommentHasLike, @FollowingAction, @Chat,([dbo].[UNIX_TIMESTAMP](getutcdate())), @OS) ';
        query += ' End';

        request.query(query,
            function (err, result) {
                if (err) {
                    logger.error('setUserNotifySetting', err);
                    return callback(err);
                }
                else {
                    var recordset = result.recordset;
                    return callback(err, recordset);
                }
            });
    }
};
