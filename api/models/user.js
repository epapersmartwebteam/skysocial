﻿var _ = require('lodash');
var sql = require('mssql');

var NodeCache = require('node-cache');
const myCache = new NodeCache();

//import lớp encrypt data
var cryptoEngine = require('../helper/cryptoEngine');

//lấy config chuỗi kết nối
var config = require('../myConfig').dataConfig;
var defaultRowCount = require('../myConfig').defaultRowCount;

var storageHelper = require('../helper/storageHelper');


var CDNUrl = require('../myConfig').CDNUrl;
var myConfig = require('../myConfig');

var resultHelper = require('./result');
var utilityHelper = require('../helper/utility');

var errorCode = require('../errorCode');
var dateHelper = require('../helper/dateHelper');

var logger = require('../helper/logHelper').logger;

var VIPLEVEL = {
    SKY: "SKY",
    VIP: "VIP",
    VVIP: "VVIP"
};

var userMustHaveColumn = [
    'UserId',
    'UserGUID',
    'Email',
    'UserName',
    'Password',
    'Phone',
    'FacebookId',
    'CreateDate',
    'RoleId',
    'CommentingOff',
    'EmailVerified',
    'PhoneVerified',
    'ImageCount',
    'ChatUserId',
    'ChatActive',
    'ChatCreateFailTime',
    'StorageProvider'
];

var userSchema = [
    'FullName',
    'Gender',
    'Birthday',
    'Zodiac',
    'MaritalStatus',
    'AvatarOriginal',
    'AvatarSmall',
    'AvatarMedium',
    'Job',
    'Address',
    'Favorite',
    'CountryId',
    'CountryName',
    'CreateOS',
    'LastLoginDate',
    'LastLoginOS',
    'IsPublic',
    'IsBanned',
    'IsActive',
    'IsVip',
    'VipLevelId',
    'IsVipTrial',
    'VipStartDate',
    'VipEndDate',
    'FollowingCount',
    'FollowerCount',
    'GroupCount',
    'CommentingOff',
    'SocialInfo',
    'HashtagSuggested',
    'Beta'
];
function convertVipLevel(vipLevelId, isVip) {
    var value;
    if (isVip) {
        switch (vipLevelId) {
            case 1: {
                value = VIPLEVEL.VVIP;
                break;
            }
            case 0:
            default: {
                value = VIPLEVEL.VIP;
                break;
            }
        }
    }
    else {
        value = VIPLEVEL.SKY;
    }
    return value;
}
function getUserSelectionField(projectionArray, prefix, notConvert) {
    var result = userSchema.filter(function (n) {
        return projectionArray.indexOf(n) > -1;
    });

    result = result.concat(userMustHaveColumn);

    //tạo mảng clone để lưu tên cột trước khi Add Prefix vào
    var cloneResult = result.slice(0);

    if (prefix) {
        result.forEach(function (item, i) { result[i] = prefix + '.' + item; });
    }

    //duyệt xem nếu là kiểu Datetime thì format lại thành chuỗi yyyy-MM-dd HH:mm:ss.ms
    if (notConvert == null) {
        cloneResult.forEach(function (item, i) {
            //if (item.indexOf('Date') >= 0 || item == 'Birthday') {
            //    //result[i] = 'convert(nvarchar, ' + result[i] + ', 120) as ' + cloneResult[i];
            //}
            //else
            //if (item.startsWith('Avatar')) {
            //    result[i] = 'case when ' + result[i] + ' is not null then ' + "'" + CDNUrl + "'" + ' + ' + result[i] + ' else ' + "'" + CDNUrl + myConfig.NoAvatarLink + "' end " + ' as ' + cloneResult[i];
            //}
            if (item == 'GroupCount') {
                var categoryId = myConfig.suggestConfigs.categoryForSuggestGroup;
                result[i] = '(Select Count(1) From UserSystemHashtags USH  inner join SystemHashtags SH on USH.HashtagId = SH.HashtagId ';
                result[i] += 'Where USH.CategoryId = ' + categoryId + ' and USH.UserId = ' + (prefix ? prefix : 'Users') + '.UserId and SH.IsShow = 1 ) as ' + cloneResult[i];
            }
        });
    }
    return result.join(',');
}

function checkIsUndefined(obj) {
    if (typeof obj != "undefined") {
        return false;
    }
    else {
        return true;
    }
}

var connection = new sql.ConnectionPool(config);
connection.connect().catch((error) => {
    logger.error('database connection error', error);
});

connection.on('error', function (err) {
    logger.error('database connection error', err);
    connection = new sql.ConnectionPool(config);
    connection.connect();
});

//parse lại UserId do kiểu BigInt mặc định sẽ thành kiểu chuỗi
function parseUserInfo(userInfo) {
    if (userInfo) {
        userInfo.UserId = parseInt(userInfo.UserId, 10);
        userInfo.CanPost = true;
        userInfo.HashtagSuggested = userInfo.HashtagSuggested ? true : false;

        userInfo.AvatarOriginal = storageHelper.fixAvatarLink(userInfo.AvatarOriginal, userInfo.StorageProvider);
        userInfo.AvatarMedium = storageHelper.fixAvatarLink(userInfo.AvatarMedium, userInfo.StorageProvider);
        userInfo.AvatarSmall = storageHelper.fixAvatarLink(userInfo.AvatarSmall, userInfo.StorageProvider);

        if (userInfo.SocialInfo) {
            try {
                userInfo.SocialInfo = JSON.parse(userInfo.SocialInfo);
            }
            catch (e) {
            }
        }
        if (!userInfo.VipLevelId && userInfo.IsVip) {
            userInfo.VipLevelId = 0;
        }
        userInfo.VipLevel = convertVipLevel(userInfo.VipLevelId, userInfo.IsVip);
    }
    return userInfo;
}

function getFeatureUsers(callback) {
    var cacheKey = 'featureUsers';
    var data = myCache.get(cacheKey);
    if (data) {
        callback(null, data);
    }
    else {
        var request = new sql.Request(connection);
        var query = 'Select * From FeatureUsers';
        request.query(query, (err, result) => {
            if (err) {
                logger.error('getFeatureUsers', err);
                callback(err);
            }
            else {
                data = result.recordset;
                myCache.set(cacheKey, data, 600);
                callback(null, data);
            }
        });
    }
}

function checkUserIsFeatureUser(userId, callback) {
    getFeatureUsers((err, lst) => {
        if (err) {
            callback(resultHelper.dataError());
        }
        else {
            if (lst && lst.length > 0) {
                if (_.some(lst, (item) => {
                    return item.UserId == userId;
                })) {
                    callback(resultHelper.success());
                }
                else {
                    callback(resultHelper.notExists());
                }
            }
            else {
                callback(resultHelper.notExists());
            }
        }
    });
}

function updateFollowCount() {
    var query = '';

    //tăng số following của User thực hiện
    query += ' Update Users ';
    query += ' Set FollowingCount = (Select Count(1) From UserFollowers Where FromUserId = @FromUserId and IsAccepted = 1) ';
    query += ' Where UserId = @FromUserId; ';

    //tăng số follwer của User đc follow
    query += ' Update Users ';
    query += ' Set FollowerCount = (Select Count(1) From UserFollowers Where ToUserId = @ToUserId and IsAccepted = 1) ';
    query += ' Where UserId = @ToUserId; ';

    return query;
}

module.exports = {
    VIPLEVEL: VIPLEVEL,
    userSchema: userSchema,
    convertVipLevel: convertVipLevel,
    //Đăng nhập bằng Email, UserName, Phone
    login: function (usernameOrEmail, password, callback) {
        var that = this;
        var request = new sql.Request(connection);

        //cắt khoảng trắng
        if (usernameOrEmail) {
            usernameOrEmail = usernameOrEmail.trim();
        }

        if (password) {
            password = password.trim();
        }

        request.input('UserName', sql.NVarChar, usernameOrEmail);

        var selectionField = getUserSelectionField(userSchema);
        if (!selectionField) {
            selectionField = '*';
        }

        var query = 'select ' + selectionField + ' From [Users] Where Email = @UserName or UserName = @UserName or Phone = @UserName';

        logger.debug(query);
        request.query(query, function (err, res) {
            if (err) {
                logger.error('login', err);
                return callback(err, null);
            }
            else {

                //Mã hóa password để so sánh với pass trong db
                var encryptedPass = cryptoEngine.encryptText(password);
                var isAuth = false;

                /**duyệt từng record trả về vì đăng nhập theo Email hoặc UserName nên có khả năng nhiều hơn 1 record 
                do bên App ko kiểm tra User có thể là Email của User khác*/
                /**
                Vì không thể break khỏi forEach nên khi return rồi thì forEach vẫn tiếp tục chạy và chạy tiếp xuống dòng dưới
                Nên phải thêm biến IsAuth vào để kiểm tra và ngăn không callback tiếp bên dưới
                Return trong trường hợp này không có tác dụng thoát khỏi hàm (đã test)
                */
                var recordset = res.recordset;

                recordset.forEach(function (item) {

                    //chưa mã hóa password để kiểm tra cho đúng
                    if (item.Password === encryptedPass && !isAuth) {
                        isAuth = true;

                        //parse lại UserId do kiểu BigInt mặc định sẽ thành kiểu chuỗi
                        item = parseUserInfo(item);

                        if (item.IsVip == true) {
                            if (item.VipEndDate) {
                                if (item.VipEndDate < dateHelper.getUnixTimeStamp()) {
                                    item.IsVip = false;
                                    that.updateUserIsVip(item.UserId, false, null, null, null, null, (err, result) => {

                                    });
                                }
                            }
                        }

                        //kiểm tra ngăn ko cho đăng nhập đối với tài khoản bị banned
                        if (item.IsBanned == 1 || item.IsBanned == true) {
                            return callback(resultHelper.create(errorCode.notPermission, "Banned"));
                        }
                        return callback(null, item);
                    }
                });

                if (!isAuth) {

                    if (recordset && recordset.length > 0) {
                        return callback(resultHelper.create(errorCode.loginPasswordNotMatch, 'Password'));
                    }
                    else {
                        return callback(resultHelper.create(errorCode.loginUserNameNotMatch, 'UsernameOrPhone'));
                    }
                }
            }
        });
    },

    //Đăng nhập qua FacebookId
    loginFacebook: function (facebookId, callback) {
        var request = new sql.Request(connection);

        //cắt khoảng trắng
        if (facebookId) {
            facebookId = facebookId.trim();
        }

        request.input('FacebookId', sql.NVarChar, facebookId);

        var selectionField = getUserSelectionField(userSchema);
        if (!selectionField) {
            selectionField = '*';
        }

        var query = 'select ' + selectionField + ' From [Users] Where FacebookId = @FacebookId';

        request.query(query, function (err, res) {
            if (err) {
                logger.error('loginFacebook', err);
                return callback(err, null);
            }
            else {
                var recordset = res.recordset;
                if (recordset) {

                    var userInfo = recordset[0];
                    if (userInfo != undefined) {
                        //parse lại UserId do kiểu BigInt mặc định sẽ thành kiểu chuỗi
                        userInfo = parseUserInfo(userInfo);

                        //kiểm tra ngăn ko cho đăng nhập đối với tài khoản bị banned
                        if (userInfo.IsBanned == 1 || userInfo.IsBanned == true) {
                            return callback(resultHelper.create(errorCode.notPermission, "Banned"));
                        }
                    }

                    return callback(null, userInfo);
                }
                else {
                    return callback(resultHelper.create(errorCode.loginUserNameNotMatch, 'FacebookId'));
                }
            }
        });
    },

    //cập nhật lần đăng nhập cuối vào bảng User và ghi log
    updateLoginLog: function (userId, os, callback) {
        var request = new sql.Request(connection);

        request.input('UserId', sql.Int, userId);
        request.input('LoginOS', sql.NVarChar, os);

        var query = 'Update [Users] Set LastLoginOS = @LoginOS, LastLoginDate = ([dbo].[UNIX_TIMESTAMP](getutcdate())) Where UserId = @UserId; ';

        request.query(query, function (err, recordset) {
            if (err) {
                logger.error('updateLoginLog', err);
                return callback(err, null);
            }
            else {
                query = 'Insert Into UserLoginLogs(UserId, LoginOS) Values(@UserId, @LoginOS) ';
                request.query(query, function (errInsert, recordsetInsert) {
                    if (errInsert) {
                        logger.error('updateLoginLog', errInsert);
                        return callback(errInsert, null);
                    }
                    else {
                        return callback(null, resultHelper.create(errorCode.success, 'Thành công'));
                    }
                });
            }
        });
    },

    //kiểm tra Email tồn tại
    checkEmailExists: function (email, callback) {
        var request = new sql.Request(connection);


        //cắt khoảng trắng
        if (email) {
            email = email.trim();
        }

        request.input('Email', sql.NVarChar, email);

        var query = 'if exists(Select * From [Users] Where Email = @Email and EmailVerified = 1) ';
        query += ' Select 1 as Existed';
        query += ' else ';
        query += ' Select 0 as Existed';

        request.query(query, function (err, result) {
            if (err) {
                logger.error('checkEmailExists', err);
                return callback(err);
            }
            else {

                var recordset = result.recordset;
                return callback(null, recordset[0].Existed);
            }
        });
    },

    //kiểm tra UserName tồn tại
    checkUserNameExists: function (username, callback) {
        var request = new sql.Request(connection);

        //cắt khoảng trắng
        if (username) {
            username = username.trim();
        }

        request.input('UserName', sql.NVarChar, username);

        var query = 'if exists(Select * From [Users] Where UserName = @UserName) ';
        query += ' Select 1 as Existed';
        query += ' else ';
        query += ' Select 0 as Existed';

        request.query(query, function (err, res) {
            if (err) {
                logger.error('checkUserNameExists', err);
                return callback(err);
            }
            else {
                var recordset = res.recordset;
                return callback(null, recordset[0].Existed);
            }
        });
    },

    //kiểm tra Phone tồn tại
    checkPhoneExists: function (phone, callback) {
        var request = new sql.Request(connection);

        //cắt khoảng trắng
        if (phone) {
            phone = phone.trim();
        }

        request.input('Phone', sql.NVarChar, phone);

        var query = 'if exists(Select * From [Users] Where Phone = @Phone and PhoneVerified = 1) ';
        query += ' Select 1 as Existed';
        query += ' else ';
        query += ' Select 0 as Existed';

        request.query(query, function (err, res) {

            if (err) {
                logger.error('checkPhoneExists', err);
                return callback(err);
            }
            else {
                var recordset = res.recordset;
                return callback(null, recordset[0].Existed);
            }
        });
    },

    //kiểm tra FacebookId tồn tại
    checkFacebookIdExists: function (facebookId, callback) {
        var request = new sql.Request(connection);

        //cắt khoảng trắng
        if (facebookId) {
            facebookId = facebookId.trim();
        }

        request.input('FacebookId', sql.NVarChar, facebookId);

        var query = 'if exists(Select * From [Users] Where FacebookId = @FacebookId) ';
        query += ' Select 1 as Existed';
        query += ' else ';
        query += ' Select 0 as Existed';

        request.query(query, function (err, res) {
            if (err) {
                logger.error('checkFacebookIdExists', err);
                return callback(err);
            }
            else {
                var recordset = res.recordset;
                return callback(null, recordset[0].Existed);
            }
        });
    },

    checkUserIsBanned: function (userInfo, callback) {
        if (userInfo.IsBanned == 1 || userInfo.IsBanned == true) {
            return callback(true);
        }
        else {
            var request = new sql.Request(connection);
            request.input('UserId', sql.BigInt, userInfo.UserId);
            var query = 'if(exists(Select * From Users Where UserId = @UserId and IsBanned = 1)) ';
            query += 'Select 1 as IsExists';
            query += ' else ';
            query += ' Select 0 as IsExists';

            request.query(query, function (err, res) {
                if (!err) {
                    var recordset = res.recordset;
                    //là chủ hình mới đc xóa
                    if (recordset[0].IsExists == 1) {
                        return callback(true);
                    }
                    return callback(false);
                }
                else {
                    logger.error('checkUserIsBanned', err);
                    return callback(true);
                }
            });
        }
    },

    //đăng ký
    signup: function (email, username, password, phone, facebookId, fullName, gender, countryId, countryName, os, isActive, callback) {
        var request = new sql.Request(connection);

        //cắt khoảng trắng
        if (email) {
            email = email.trim();
        }

        if (username) {
            username = username.trim();
        }

        if (password) {
            password = password.trim();
        }

        if (phone) {
            phone = phone.trim();
        }

        if (facebookId) {
            facebookId = facebookId.trim();
        }

        if (countryId) {
            countryId = countryId.trim();
        }

        if (countryName) {
            countryName = countryName.trim();
        }

        if (os) {
            os = os.trim();
        }


        if (gender == null || gender == undefined) {
            gender = -1;
        }

        var encryptedPass;

        if (password == '' || password == null || password == undefined) {
            encryptedPass = '';
        }
        else {
            encryptedPass = cryptoEngine.encryptText(password);
        }
        request.input('Email', sql.NVarChar, email);
        request.input('UserName', sql.NVarChar, username);
        request.input('Password', sql.NVarChar, encryptedPass);
        request.input('Phone', sql.NVarChar, phone);
        request.input('FacebookId', sql.NVarChar, facebookId);
        request.input('CountryId', sql.VarChar, countryId);
        request.input('CountryName', sql.NVarChar, countryName);
        request.input('CreateOS', sql.NVarChar, os);
        request.input('IsActive', sql.Bit, isActive);
        request.input('FullName', sql.NVarChar, fullName);
        request.input('Gender', sql.Int, gender);

        var query = 'Insert into Users(UserGUID, Email, UserName, Password, Phone, FacebookId, CountryId, CountryName, EmailVerified, PhoneVerified,  ';
        query += ' CreateOS, CreateDate, IsPublic, IsBanned, IsActive,FollowingCount, FollowerCount, ImageCount, FullName, Gender)';
        query += ' Values(Lower(NEWID()), @Email, @UserName, @Password, @Phone, @FacebookId, @CountryId, @CountryName, 0, 0, ';
        query += ' @CreateOS, dbo.UNIX_TIMESTAMP(getUTCDate()), 1, 0, @IsActive, 0, 0, 0, @FullName, @Gender); ';
        query += ' Select SCOPE_IDENTITY() as UserId ';

        request.query(query, function (err, res) {

            if (err) {
                logger.error('signup', err);
                return callback(err, null);
            }
            else {
                var recordset = res.recordset;
                return callback(null, recordset[0].UserId);
            }
        });
    },

    //lấy thông tin user bằng UserId, có truyền các field cần lấy, nếu truyền rỗng là lấy tất cả *
    getUserById: function (id, ast, callback) {
        var request = new sql.Request(connection);
        request.input('UserId', sql.BigInt, parseFloat(id));

        var selectionField = getUserSelectionField(ast);
        if (!selectionField) {
            selectionField = '*';
        }

        var query = 'select ' + selectionField + ' From [Users] Where UserId = @UserId';

        request.query(query, function (err, res) {
            if (err) {
                logger.error('getUserById', err);
                return callback(err, null);
            }
            else {
                var recordset = res.recordset;

                //nếu tồn tại User thì trả về giá trị đầu tiên trong list
                if (recordset) {
                    var item = parseUserInfo(recordset[0]);
                    return callback(null, item);
                }
                else {
                    return callback(null, null);
                }
            }
        });
    },

    //lấy thông tin user bằng UserId, có truyền các field cần lấy, nếu truyền rỗng là lấy tất cả *
    getUserByUserName: function (userName, ast, callback) {
        var request = new sql.Request(connection);
        request.input('UserName', sql.NVarChar, userName);

        var selectionField = getUserSelectionField(ast);
        if (!selectionField) {
            selectionField = '*';
        }

        var query = 'select ' + selectionField + ' From [Users] Where UserName = @UserName';

        request.query(query, function (err, res) {
            if (err) {
                logger.error('getUserByUserName', err);
                return callback(err, null);
            }
            else {
                var recordset = res.recordset;
                //nếu tồn tại User thì trả về giá trị đầu tiên trong list
                if (recordset) {
                    var item = parseUserInfo(recordset[0]);
                    return callback(null, item);
                }
                else {
                    return callback(null, null);
                }
            }
        });
    },

    getUserByEmail: function (email, ast, callback) {
        var request = new sql.Request(connection);
        request.input('Email', sql.VarChar, email);

        var selectionField = getUserSelectionField(ast);
        if (!selectionField) {
            selectionField = '*';
        }

        var query = 'select ' + selectionField + ' From [Users] Where Email = @Email';

        request.query(query, function (err, res) {
            if (err) {
                logger.error('getUserByEmail', err);
                return callback(err, null);
            }
            else {
                var recordset = res.recordset;
                //nếu tồn tại User thì trả về giá trị đầu tiên trong list
                if (recordset) {
                    var item = parseUserInfo(recordset[0]);
                    return callback(null, item);
                }
                else {
                    return callback(null, null);
                }
            }
        });
    },

    getUserByPhone: function (phone, ast, callback) {
        var request = new sql.Request(connection);
        request.input('Phone', sql.VarChar, phone);

        var selectionField = getUserSelectionField(ast);
        if (!selectionField) {
            selectionField = '*';
        }

        var query = 'select ' + selectionField + ' From [Users] Where Phone = @Phone';

        request.query(query, function (err, res) {
            if (err) {
                logger.error('getUserByPhone', err);
                return callback(err, null);
            }
            else {
                var recordset = res.recordset;
                //nếu tồn tại User thì trả về giá trị đầu tiên trong list
                if (recordset) {
                    var item = parseUserInfo(recordset[0]);
                    return callback(null, item);
                }
                else {
                    return callback(null, null);
                }
            }
        });
    },

    /**
    User Follow
    */
    //lấy danh sách bạn đang follow sắp theo ngày Follow
    getUserFollowingIdList(userId, callback) {
        var request = new sql.Request(connection);

        request.input('UserId', sql.BigInt, parseInt(userId));
        var query = ' Select ToUserId as UserId ';
        query += ' From UserFollowers ';
        query += ' Where FromUserId = @UserId ';

        request.query(query, (err, result) => {
            if (err) {
                logger.error(err);
                callback(err);
            }
            else {
                var lst = [];
                if (result && result.recordset) {
                    lst = result.recordset;
                    lst = _.map(lst, (item) => {
                        return parseInt(item.UserId);
                    });
                }
                callback(null, lst);
            }
        });
    },

    //lấy danh sách bạn đang Follow
    getUserFollowingList: function (id, lastId, lastDate, count, ast, callback) {
        var request = new sql.Request(connection);

        //if (lastDate) {
        //    lastDate = new Date(lastDate);
        //}

        request.input('UserID', sql.BigInt, parseFloat(id));


        var selectionField = getUserSelectionField(ast, 'U');
        if (!selectionField) {
            selectionField = '*';
        }

        if (!count) {
            count = defaultRowCount;
        }

        //tạo query dựa trên tham số
        var query = 'Select top ' + count + ' ' + selectionField;
        query += ' From [Users] U Inner Join UserFollowers F on U.UserId = F.ToUserId ';
        query += ' Where F.FromUserId = @UserID and F.IsAccepted = 1 ';
        //if (lastId && lastDate) {
        //    request.input('LastId', sql.BigInt, parseFloat(lastId));
        //    request.input('LastDate', sql.BigInt, lastDate);
        //    query += ' and (U.CreateDate < @LastDate or (U.CreateDate = @LastDate and UserId < @LastId)) ';
        //}
        if (lastId) {
            request.input('LastId', sql.BigInt, parseFloat(lastId));
            query += ' and (UserId < @LastId) ';
            query += ' Order by U.UserId desc ';
        }
        else if (lastDate) {
            request.input('LastDate', sql.BigInt, lastDate);
            query += ' and (U.CreateDate < @LastDate) ';
            query += ' Order by U.CreateDate desc ';
        }
        else {
            query += ' Order by U.UserId desc ';
        }

        request.query(query, function (err, res) {

            if (err) {
                logger.error('getUserFollowingList', err);
                return callback(err, null);
            }
            else {
                var recordset = res.recordset;
                //nếu tồn tại User thì trả về giá trị đầu tiên trong list
                if (recordset) {
                    var items = _.map(recordset, (item) => { return parseUserInfo(item); });

                    return callback(null, items);
                }
                else {
                    return callback(null, null);
                }
            }
        });
    },

    //lấy danh sách đang Follow bạn
    getUserFollowerList: function (id, lastId, lastDate, count, ast, callback) {
        var request = new sql.Request(connection);

        //if (lastDate)
        //    lastDate = new Date(lastDate);

        request.input('UserID', sql.BigInt, parseFloat(id));

        var selectionField = getUserSelectionField(ast, 'U');
        if (!selectionField) {
            selectionField = '*';
        }

        if (!count) {
            count = defaultRowCount;
        }

        //tạo query dựa trên tham số
        var query = 'Select top ' + count + ' ' + selectionField;
        query += ' From [Users] U Inner Join UserFollowers F on U.UserId = F.FromUserId ';
        query += ' Where F.ToUserId = @UserID and F.IsAccepted = 1 ';

        //if (lastId && lastDate) {
        //    request.input('LastId', sql.BigInt, parseFloat(lastId));
        //    request.input('LastDate', sql.BigInt, lastDate);
        //    query += ' and (U.CreateDate < @LastDate or (U.CreateDate = @LastDate and UserId < @LastId)) ';
        //}
        if (lastId) {
            request.input('LastId', sql.BigInt, parseFloat(lastId));
            query += ' and (UserId < @LastId) ';
            query += ' Order by U.UserId desc ';
        }
        else if (lastDate) {
            request.input('LastDate', sql.BigInt, lastDate);
            query += ' and (U.CreateDate < @LastDate) ';
            query += ' Order by U.CreateDate desc ';
        }
        else {
            query += ' Order by U.UserId desc ';
        }

        request.query(query, function (err, res) {
            if (err) {
                logger.error('getUserFollowerList', err);
                return callback(err, null);
            }
            else {
                var recordset = res.recordset;
                //nếu tồn tại User thì trả về giá trị đầu tiên trong list
                if (recordset) {
                    var items = _.map(recordset, (item) => { return parseUserInfo(item); });

                    return callback(null, items);
                }
                else {
                    return callback(null, null);
                }
            }
        });
    },

    //thêm follower, nếu người đc follow đang ở public thì set accept luôn, ngược lại chỉ set vào mà ko accept
    addFollower: function (fromUserId, toUserId, pushNotifyDefault, callback) {
        var request = new sql.Request(connection);

        request.input('FromUserId', sql.BigInt, fromUserId);
        request.input('ToUserId', sql.BigInt, toUserId);
        request.input('PushNotifyDefault', sql.Bit, pushNotifyDefault);

        //nếu chưa có mới tiếp tục
        var query = 'if(not exists(Select * From UserFollowers Where FromUserId = @FromUserId and ToUserId = @ToUserId)) ';
        query += ' Begin ';

        //nếu User đc follow đang public thì cho thêm vào và accept luôn
        query += ' if(exists(Select * From Users Where UserId = @ToUserId and IsPublic = 1)) ';
        query += ' begin ';
        query += ' Insert into UserFollowers(FromUserId, ToUserId, CreateDate, IsAccepted, AcceptedDate, PushNotifyNewPost, PushNotifyNewActivity) ';
        query += ' Values(@FromUserId, @ToUserId, dbo.UNIX_TIMESTAMP(getUTCDate()), 1, dbo.UNIX_TIMESTAMP(getUTCDate()), @PushNotifyDefault, @PushNotifyDefault); ';

        //tăng số following của User thực hiện
        //tăng số follwer của User đc follow
        query += updateFollowCount();

        query += ' end ';

        //ngược lại nếu đang ở chế độ private thì chỉ thêm vào và gán là chưa accepted
        query += ' else ';
        query += ' Begin ';
        query += ' Insert into UserFollowers(FromUserId, ToUserId, CreateDate, IsAccepted, PushNotifyNewPost, PushNotifyNewActivity) ';
        query += ' Values(@FromUserId, @ToUserId, dbo.UNIX_TIMESTAMP(getUTCDate()), 0, @PushNotifyDefault, @PushNotifyDefault) ';
        query += ' end ';
        query += ' end ';

        request.query(query, function (err, res) {
            if (err) {
                logger.error('addFollower', err);
                return callback(err, null);
            }
            else {
                var recordset = res.recordset;
                return callback(err, recordset);
            }
        });
    },

    updateFollowerPushNotify(fromUserId, toUserId, newPost, newActivity, callback) {
        var request = new sql.Request(connection);

        request.input('FromUserId', sql.BigInt, fromUserId);
        request.input('ToUserId', sql.BigInt, toUserId);
        request.input('PushNotifyNewPost', sql.Bit, newPost);
        request.input('PushNotifyNewActivity', sql.Bit, newActivity);

        var query = 'Update UserFollowers ';
        query += ' Set PushNotifyNewPost = @PushNotifyNewPost, ';
        query += ' PushNotifyNewActivity = @PushNotifyNewActivity ';
        query += ' Where FromUserId = @FromUserId and ToUserId = @ToUserId ';

        request.query(query, function (err, res) {
            if (err) {
                logger.error('updateFollowerPushNotify', err);
                return callback(err, null);
            }
            else {
                var recordset = res.recordset;
                return callback(err, recordset);
            }
        });
    },

    getFollowerPushNotify(fromUserId, toUserId, callback) {
        var request = new sql.Request(connection);
        request.input('FromUserId', sql.BigInt, fromUserId);
        request.input('ToUserId', sql.BigInt, toUserId);

        var query = 'Select top 1 PushNotifyNewPost, PushNotifyNewActivity ';
        query += 'From UserFollowers ';
        query += ' Where FromUserId = @FromUserId and ToUserId = @ToUserId ';

        request.query(query, function (err, result) {
            if (err) {
                logger.error('getFollowerPushNotify', err);
                callback(err);
            }
            else {
                var recordset = result.recordset;
                var PushNotifyNewPost = (recordset[0] != undefined) ? recordset[0].PushNotifyNewPost : false;
                var PushNotifyNewActivity = (recordset[0] != undefined) ? recordset[0].PushNotifyNewActivity : false;

                PushNotifyNewPost = utilityHelper.convertToBool(PushNotifyNewPost);
                PushNotifyNewActivity = utilityHelper.convertToBool(PushNotifyNewActivity);

                callback(err, { NewPost: PushNotifyNewPost, NewActivity: PushNotifyNewActivity });
            }
        });
    },

    //bỏ follower
    removeFollower: function (fromUserId, toUserId, callback) {
        var request = new sql.Request(connection);

        request.input('FromUserId', sql.BigInt, fromUserId);
        request.input('ToUserId', sql.BigInt, toUserId);

        var query = 'if(Exists(Select * From UserFollowers Where FromUserId = @FromUserId and ToUserId = @ToUserId)) ';
        query += ' Begin ';
        query += ' Declare @IsAccepted bit = (Select top 1 IsAccepted From UserFollowers Where FromUserId = @FromUserId and ToUserId = @ToUserId); ';
        query += ' Delete From UserFollowers ';
        query += ' Where FromUserID = @FromUserId and ToUserId = @ToUserId; ';
        query += ' if(@IsAccepted = 1) ';
        query += ' Begin ';

        query += updateFollowCount();

        query += ' end ';
        query += ' end ';

        request.query(query, function (err, result) {
            if (err) {
                logger.error('removeFollower', err);
                callback(err);
            }
            else {
                var recordset = result.recordset;
                return callback(err, recordset);
            }
        });
    },

    acceptUserFollow: function (fromUserId, toUserId, callback) {
        var request = new sql.Request(connection);

        request.input('FromUserId', sql.BigInt, fromUserId);
        request.input('ToUserId', sql.BigInt, toUserId);

        var query = 'if(Exists(Select * From UserFollowers Where FromUserId = @FromUserId and ToUserId = @ToUserId)) ';
        query += ' Begin ';
        query += ' Declare @IsAccepted bit = (Select top 1 IsAccepted From UserFollowers Where FromUserId = @FromUserId and ToUserId = @ToUserId); ';
        query += ' if(@IsAccepted = 0) ';
        query += ' Begin ';
        query += ' Update UserFollowers ';
        query += ' Set IsAccepted = 1, ';
        query += ' AcceptedDate = dbo.UNIX_TIMESTAMP(getUTCDate()) ';
        query += ' Where FromUserId = @FromUserId and ToUserId = @ToUserId; ';
        query += updateFollowCount();
        query += ' End ';
        query += ' End ';

        request.query(query, function (err, result) {
            if (err) {
                logger.error('acceptUserFollow', err);
                callback(err);
            }
            else {
                var recordset = result.recordset;
                return callback(err, recordset);
            }
        });
    },

    /**
    Các hàm cập nhật data
    */
    updateEmailVerified: function (userId, email, callback) {
        var request = new sql.Request(connection);

        request.input('UserId', sql.BigInt, userId);
        request.input('Email', sql.NVarChar, email);

        var query = 'Update Users ';
        query += ' Set EmailVerified = 1, ';
        query += '      Email = @Email ';
        query += ' Where UserId = @UserId; ';

        //cập nhật những tài khoản khác ko đc phép dùng email này nữa, xoá rỗng
        query += ' Update Users ';
        query += " Set Email = '', EmailVerified = 0 ";
        query += ' Where Email = @Email and UserId != @UserId ';

        request.query(query, function (err, result) {
            if (err) {
                logger.error('updateEmailVerified', err);
                callback(err);
            }
            else {
                callback(null);
            }
        });
    },

    updatePhoneVerified: function (userId, phone, callback) {
        var request = new sql.Request(connection);
        request.input('UserId', sql.BigInt, userId);
        request.input('Phone', sql.NVarChar, phone);

        var query = 'Update Users ';
        query += ' Set PhoneVerified = 1, ';
        query += '      Phone = @Phone ';
        query += ' Where UserId = @UserId; ';

        query += ' Update Users ';
        query += " Set Phone = '', PhoneVerified = 0 ";
        query += ' Where Phone = @Phone and UserId != @UserId ';

        request.query(query, function (err, result) {
            if (err) {
                logger.error('updatePhoneVerified', err);
                callback(err);
            }
            else {
                callback(null);
            }
        });
    },

    updateUserIsBanned: function (userId, isBanned, banAdminId, callback) {
        var request = new sql.Request(connection);

        request.input('UserId', sql.BigInt, userId);
        request.input('IsBanned', sql.Bit, isBanned);
        request.input('BanAdminId', sql.BigInt, banAdminId);

        var query = 'Update Users ';
        query += ' Set IsBanned = @IsBanned, IsActive = !@IsBanned, BanAdminId = @BanAdminId ';
        query += ' Where UserId = @UserId ';

        request.query(query, function (err, result) {
            if (err) {
                logger.error('updateUserIsBanned', err);
                return callback(err);
            }
            else {
                var recordset = result.recordset;
                return callback(err, recordset);
            }
        });
    },

    updateUserIsVip: function (userId, isVip, vipLevelId, vipStartDate, vipEndDate, isTrial, callback) {
        var request = new sql.Request(connection);

        request.input('UserId', sql.BigInt, userId);
        request.input('IsVip', sql.Bit, isVip);
        request.input('VipStartDate', sql.BigInt, vipStartDate);
        request.input('VipEndDate', sql.BigInt, vipEndDate);
        request.input('VipLevelId', sql.Int, vipLevelId);

        var query = 'Update Users ';
        query += ' Set IsVip = @IsVip, VipStartDate = @VipStartDate, VipEndDate = @VipEndDate, VipLevelId = @VipLevelId ';


        if (isTrial != null) {
            request.input('IsVipTrial', sql.Bit, isTrial);
            query += ', IsVipTrial = @IsVipTrial ';
        }

        query += ' Where UserId = @UserId ';

        request.query(query, function (err, result) {
            if (err) {
                logger.error('updateUserIsVip', err);
                return callback(err);
            }
            else {
                var recordset = result;
                return callback(err, recordset);
            }
        });
    },

    //cập nhật avatar
    updateUserAvatar: function (userId, avatarOriginal, avatarMedium, avatarSmall, callback) {
        var request = new sql.Request(connection);

        request.input('UserId', sql.BigInt, userId);
        request.input('AvatarOriginal', sql.NVarChar, avatarOriginal);
        request.input('AvatarMedium', sql.NVarChar, avatarMedium);
        request.input('AvatarSmall', sql.NVarChar, avatarSmall);

        var query = 'Update Users ';
        query += ' Set AvatarOriginal = @AvatarOriginal, ';
        query += ' AvatarMedium = @AvatarMedium, ';
        query += ' AvatarSmall = @AvatarSmall ';
        query += ' Where UserId = @UserId ';

        request.query(query, function (err, result) {
            if (err) {
                logger.error('updateUserAvatar', err);
                return callback(err);
            }
            else {
                var recordset = result.recordset;

                return callback(err, recordset);
            }
        });
    },

    //for core chat
    updateUserChatInfo: function (userId, chatUserId, callback) {
        var request = new sql.Request(connection);

        request.input('UserId', sql.BigInt, userId);
        request.input('ChatUserId', sql.VarChar, chatUserId);

        var query = 'Update Users ';
        query += ' Set ChatUserId = @ChatUserId, ChatActive = 1 ';
        query += ' Where UserId = @UserId ';


        request.query(query, function (err, result) {
            if (err) {
                logger.error('updateUserChatInfo', err);
                return callback(err);
            }
            else {
                var recordset = result.recordset;

                return callback(err, recordset);
            }
        });
    },

    updateUserChatCreateFailTime: function (userId, callback) {
        var request = new sql.Request(connection);

        request.input('UserId', sql.BigInt, userId);

        var query = 'Update Users ';
        query += ' Set ChatCreateFailTime = COALESCE(ChatCreateFailTime, 0) + 1 ';
        query += ' Where UserId = @UserId ';

        request.query(query, function (err, result) {
            if (err) {
                logger.error('updateUserChatCreateFailTime', err);
                return callback(err);
            }
            else {
                var recordset = result.recordset;

                return callback(err, recordset);
            }
        });
    },

    //cập nhật facebookId
    updateUserFacebookId: function (email, facebookId, callback) {
        var request = new sql.Request(connection);

        request.input('Email', sql.NVarChar, email);
        request.input('FacebookId', sql.NVarChar, facebookId);

        var query = ' Update Users ';
        query += ' Set FacebookId = @FacebookId ';
        query += ' Where Email = @Email ';

        request.query(query, function (err, result) {
            if (err) {
                logger.error('updateUserFacebookId', err);
                return callback(err);
            }
            else {
                var recordset = result.recordset;
                return callback(err, recordset);
            }
        });
    },

    checkUserProfileExistBeforeUpdate(userId, userName, email, phone, callback) {
        var request = new sql.Request(connection);

        request.input('UserId', sql.BigInt, userId);

        var isFirst = true;

        var query = 'declare @Existed nvarchar(200) = \'\'; ';

        if (!checkIsUndefined(userName)) {
            request.input('UserName', sql.NVarChar, userName);
            query += ' if(exists(select * From Users Where UserId != @UserId and UserName = @UserName)) ';
            query += ' begin ';
            query += ' Set @Existed = @Existed + \'UserName,\'';
            query += ' end ';
        }

        if (!checkIsUndefined(email)) {
            request.input('Email', sql.NVarChar, email);
            query += ' if(exists(select * From Users Where UserId != @UserId and Email = @Email and EmailVerified = 1)) ';
            query += ' begin ';
            query += ' Set @Existed = @Existed + \'Email,\'';
            query += ' end ';
        }

        if (!checkIsUndefined(phone)) {
            request.input('Phone', sql.NVarChar, phone);
            query += ' if(exists(select * From Users Where UserId != @UserId and Phone = @Phone  and PhoneVerified = 1)) ';
            query += ' begin ';
            query += ' Set @Existed = @Existed + \'Phone,\'';
            query += ' end ';
        }

        query += ' SET @Existed = ';
        query += ' CASE @Existed WHEN null THEN null  ';
        query += ' ELSE ( ';
        query += ' CASE LEN(@Existed) WHEN 0 THEN @Existed ';
        query += ' ELSE LEFT(@Existed, LEN(@Existed) - 1)  ';
        query += ' END  ';
        query += ' ) END';
        query += ' select @Existed as ExistedField';

        request.query(query, function (err, result) {
            if (err) {
                logger.error('checkUserProfileExistBeforeUpdate', err);
                return callback(err);
            }
            else {
                var recordset = result.recordset;
                var res = recordset[0].ExistedField;
                return callback(err, res);
            }
        });
    },

    updateUserHashtagSuggested: function (userId, callback) {
        var request = new sql.Request(connection);
        request.input('UserId', sql.BigInt, userId);
        var query = 'Update Users Set HashtagSuggested = 1 Where UserId = @UserId';
        request.query(query, function (err, result) {
            if (err) {
                logger.error('updateUserHashtagSuggested', err);
            }
            callback(err);
        });
    },

    //cập nhật thông tin usedr
    updateUserProfile: function (userId, userName, email, phone, fullName, gender, birthDay, favorite, socialInfo, zodiac, maritalStatus, address, callback) {
        var request = new sql.Request(connection);
        var query = 'Update Users ';
        var isSet = false;
        request.input('UserId', sql.BigInt, userId);
        if (!checkIsUndefined(userName)) {
            request.input('UserName', sql.NVarChar, userName);
            if (!isSet) {
                query += ' Set ';
                isSet = true;
            }
            else {
                query += ', ';
            }
            query += ' UserName = @UserName ';
        }
        if (!checkIsUndefined(email)) {
            request.input('Email', sql.NVarChar, email);
            if (!isSet) {
                query += ' Set ';
                isSet = true;
            }
            else {
                query += ', ';
            }
            query += ' Email = @Email ';
            query += ', EmailVerified = 0 ';
        }

        if (!checkIsUndefined(phone)) {
            request.input('Phone', sql.NVarChar, phone);
            if (!isSet) {
                query += ' Set ';
                isSet = true;
            }
            else {
                query += ', ';
            }
            query += ' Phone = @Phone ';
            query += ', PhoneVerified = 0 ';
        }

        if (!checkIsUndefined(fullName)) {
            request.input('FullName', sql.NVarChar, fullName);
            if (!isSet) {
                query += ' Set ';
                isSet = true;
            }
            else {
                query += ', ';
            }
            query += ' FullName = @FullName ';
        }

        if (!checkIsUndefined(gender)) {
            request.input('Gender', sql.Int, gender);
            if (!isSet) {
                query += ' Set ';
                isSet = true;
            }
            else {
                query += ', ';
            }
            query += ' Gender = @Gender ';
        }
        if (!checkIsUndefined(birthDay)) {
            request.input('BirthDay', sql.NVarChar, birthDay);
            if (!isSet) {
                query += ' Set ';
                isSet = true;
            }
            else {
                query += ', ';
            }
            query += ' BirthDay = @BirthDay ';
        }
        if (!checkIsUndefined(favorite)) {
            request.input('Favorite', sql.NVarChar, favorite);
            if (!isSet) {
                query += ' Set ';
                isSet = true;
            }
            else {
                query += ', ';
            }
            query += ' Favorite = @Favorite ';
        }

        if (!checkIsUndefined(zodiac)) {
            request.input('Zodiac', sql.NVarChar, zodiac);
            if (!isSet) {
                query += ' Set ';
                isSet = true;
            }
            else {
                query += ', ';
            }
            query += ' Zodiac = @Zodiac ';
        }

        if (!checkIsUndefined(maritalStatus)) {
            request.input('MaritalStatus', sql.NVarChar, maritalStatus);
            if (!isSet) {
                query += ' Set ';
                isSet = true;
            }
            else {
                query += ', ';
            }
            query += ' MaritalStatus = @MaritalStatus ';
        }

        if (!checkIsUndefined(address)) {
            request.input('Address', sql.NVarChar, address);
            if (!isSet) {
                query += ' Set ';
                isSet = true;
            }
            else {
                query += ', ';
            }
            query += ' Address = @Address ';
        }

        if (!checkIsUndefined(socialInfo)) {
            request.input('SocialInfo', sql.NVarChar, socialInfo);
            if (!isSet) {
                query += ' Set ';
                isSet = true;
            }
            else {
                query += ', ';
            }
            query += ' SocialInfo = @SocialInfo ';
        }

        query += ' Where UserId = @UserId ';

        request.query(query, function (err, result) {
            if (err) {
                logger.error('updateUserProfile', err);
                return callback(err);
            }
            else {
                var recordset = result.recordset;
                return callback(err, recordset);
            }
        });
    },

    updateUserPasswordByPhone: function (phone, password, callback) {
        var request = new sql.Request(connection);

        request.input('Phone', sql.NVarChar, phone);
        request.input('Password', sql.NVarChar, password);

        var query = 'Update Users ';
        query += ' Set Password = @Password ';
        query += ' Where Phone = @Phone';

        request.query(query, function (err, result) {
            if (err) {
                logger.error('updateUserPasswordByPhone', err);
                return callback(err);
            }
            else {
                var recordset = result.recordset;

                return callback(err, recordset);
            }
        });
    },

    //cập nhật User password
    updateUserPassword: function (userId, password, callback) {
        var request = new sql.Request(connection);

        request.input('UserId', sql.BigInt, userId);
        request.input('Password', sql.NVarChar, password);

        var query = 'Update Users ';
        query += ' Set Password = @Password ';
        query += ' Where UserId = @UserId';

        request.query(query, function (err, result) {
            if (err) {
                logger.error('updateUserPassword', err);
                return callback(err);
            }
            else {
                var recordset = result.recordset;

                return callback(err, recordset);
            }
        });
    },

    //update user commenting off, ko cho phép comment vào tất cả hình của user 
    updateUserCommentingOff: function (userId, commentingOff, callback) {
        var request = new sql.Request(connection);

        request.input('UserId', sql.BigInt, userId);
        request.input('CommentingOff', sql.Bit, commentingOff);

        var query = ' Update Users ';
        query += ' Set CommentingOff = @CommentingOff ';
        query += ' Where UserId = @UserId ';

        request.query(query, function (err, result) {
            if (err) {
                logger.error('updateUserCommentingOff', err);
                return callback(err);
            }
            else {
                var recordset = result.recordset;

                return callback(err, recordset);
            }
        });
    },

    suggestUserByUserInfo: function (userId, count, ast, callback) {
        var request = new sql.Request(connection);

        request.input('UserId', sql.BigInt, userId);
        request.input('ItemCount', sql.Int, count);

        var selectionField = getUserSelectionField(ast);
        if (!selectionField) {
            selectionField = '*';
        }

        var query = 'declare @temp Table(FollowingId bigint);';
        query += ' insert into @temp(FollowingId) ';
        query += ' Select ToUserId ';
        query += ' From UserFollowers ';
        query += ' Where FromUserId = @UserId ';

        query += ' declare @topN bigint = (Select count(*) From @temp); ';
        query += ' set @topN = @topN + @ItemCount; ';

        query += " declare @PreferGender int = (Select case Hashtag when 'female' then 0 when 'male' then 1 else -1 end ";
        query += ' From UserSystemHashtags ';
        query += ' Where UserId = @UserId and CategoryId = 6); ';

        query += ' Select distinct top (@ItemCount) ' + selectionField;
        selectionField = getUserSelectionField(ast, null, true);
        query += ' From ( select top ' + myConfig.SystemSuggestNumber + ' ' + selectionField + ', 0 as SOrder ';
        query += ' From Users U ';
        query += ' Where U.UserId in (Select UserId From SystemSuggestUsers) ';
        query += ' and U.UserId not in (Select FollowingId From @temp) and U.UserId != @UserId and (IsBanned is null or IsBanned = 0) and (IsActive = 1) ';
        query += ' Order by NEWID()';

        query += ' union ';

        query += ' select top (@ItemCount) ' + selectionField + ', 1 as SOrder ';
        query += ' From ( ';
        selectionField = getUserSelectionField(ast, 'U', true);
        query += ' select distinct top (@topN) ' + selectionField;
        query += ' From UserSystemHashtags USH inner join Users U on USH.UserId = U.UserId ';
        query += ' Where USH.UserId != @UserId and HashtagId in ';
        query += ' (Select HashtagId From UserSystemHashtags Where UserId = @UserId and CategoryId in (5, 1)) ';
        query += ' and (@PreferGender = -1 or Gender = @PreferGender) and (U.IsBanned is null or U.IsBanned = 0) and (U.IsActive = 1) ';
        query += ') as t ';

        query += ' Where UserId not in (Select FollowingId From @temp) and UserId != @UserId ';
        query += ' Order by NewID() ';
        query += ' ) as tt ';
        query += ' Where (RoleId is null or RoleId != -1)';

        query += ' Order by ImageCount desc ';

        request.query(query, function (err, result) {
            if (err) {
                logger.error('suggestUserByUserInfo', err);
                return callback(err);
            }
            else {
                var recordset = result.recordset;
                return callback(err, recordset);
            }
        });
    },

    //suggest user có lượng post cao
    suggestUserHighPost: function (userId, count, ast, callback) {

        var minPost = myConfig.MinPostForSuggest;

        if (!count && count <= 0) {
            count = defaultRowCount;
        }

        var request = new sql.Request(connection);

        request.input('UserId', sql.BigInt, userId);
        request.input('ItemCount', sql.Int, count);
        request.input('MinPost', sql.Int, minPost);

        var selectionField = getUserSelectionField(ast);
        if (!selectionField) {
            selectionField = '*';
        }

        var query = 'declare @temp Table(FollowingId bigint);';
        query += ' insert into @temp(FollowingId) ';
        query += ' Select ToUserId ';
        query += ' From UserFollowers ';
        query += ' Where FromUserId = @UserId ';

        query += ' declare @topN bigint = (Select count(*) From @temp); ';
        query += ' set @topN = @topN + @ItemCount; ';

        query += ' Select distinct top (@ItemCount) ' + selectionField;
        selectionField = getUserSelectionField(ast, null, true);
        query += ' From ( select top ' + myConfig.SystemSuggestNumber + ' ' + selectionField + ', 0 as SOrder ';
        query += ' From Users U ';
        query += ' Where U.UserId in (Select UserId From SystemSuggestUsers) ';
        query += ' and U.UserId not in (Select FollowingId From @temp) and U.UserId != @UserId and (IsBanned is null or IsBanned = 0) and (IsActive = 1) ';
        query += ' Order by NEWID()';

        query += ' union ';

        query += ' select top (@ItemCount) ' + selectionField + ', 1 as SOrder ';
        query += ' From ( ';

        //selectionField = getUserSelectionField(ast, null, true);

        query += ' select top (@topN) ' + selectionField;
        query += ' From Users ';
        query += ' Where ImageCount >= @MinPost and (IsBanned is null or IsBanned = 0) and (IsActive = 1)';
        query += ' Order by ImageCount desc';
        query += ' ) as t ';
        query += ' Where UserId not in (Select FollowingId From @temp) and UserId != @UserId ';
        query += ' Order by NewID() ';
        query += ' ) as tt ';
        query += ' Where (RoleId is null or RoleId != -1)';

        //lỗi khi dùng distinct mà sort
        query += ' Order by ImageCount desc ';


        request.query(query, function (err, result) {
            if (err) {
                logger.error('suggestUserHighPost', err);
                return callback(err);
            }
            else {
                var recordset = result.recordset;
                return callback(err, recordset);
            }
        });
    },

    //suggest user có lượng follow lớn
    suggestUserHighFollower: function (userId, count, ast, callback) {

        var minFollow = myConfig.MinFollowForSuggest;

        if (!count && count <= 0) {
            count = defaultRowCount;
        }

        var request = new sql.Request(connection);

        request.input('UserId', sql.BigInt, userId);
        request.input('ItemCount', sql.Int, count);
        request.input('MinFollow', sql.Int, minFollow);

        var selectionField = getUserSelectionField(ast);
        if (!selectionField) {
            selectionField = '*';
        }

        var query = 'declare @temp Table(FollowingId bigint);';
        query += ' insert into @temp(FollowingId) ';
        query += ' Select ToUserId ';
        query += ' From UserFollowers ';
        query += ' Where FromUserId = @UserId ';

        query += ' declare @topN bigint = (Select count(*) From @temp); ';
        query += ' set @topN = @topN + @ItemCount; ';

        query += ' Select distinct top (@ItemCount) ' + selectionField;
        selectionField = getUserSelectionField(ast, null, true);
        query += ' From ( select top ' + myConfig.SystemSuggestNumber + ' ' + selectionField + ', 0 as SOrder ';
        query += ' From Users U ';
        query += ' Where U.UserId in (Select UserId From SystemSuggestUsers) ';
        query += ' and U.UserId not in (Select FollowingId From @temp) and U.UserId != @UserId and (IsBanned is null or IsBanned = 0) and (IsActive = 1) ';
        query += ' Order by NEWID()';

        query += ' union ';

        query += ' select top (@ItemCount) ' + selectionField + ', 1 as SOrder ';
        query += ' From ( ';

        //selectionField = getUserSelectionField(ast, null, true);

        query += ' select top (@topN) ' + selectionField;
        query += ' From Users ';
        query += ' Where FollowerCount >= @MinFollow and (IsBanned is null or IsBanned = 0) and (IsActive = 1) ';
        query += ' Order by FollowerCount desc ';
        query += ' ) as t ';
        query += ' Where UserId not in (Select FollowingId From @temp) and UserId != @UserId ';
        query += ' Order by NewID() ';
        query += ' ) as tt ';
        query += ' Where (RoleId is null or RoleId != -1) ';
        //lỗi khi dùng distinct mà sort
        query += ' Order by ImageCount desc ';

        request.query(query, function (err, result) {
            if (err) {
                logger.error('suggestUserHighFollower', err);
                return callback(err);
            }
            else {
                var recordset = result.recordset;
                return callback(err, recordset);
            }

        });
    },

    suggestUserRelative: function (userId, count, ast, callback) {

        var minPost = myConfig.MinPostForFollowingSuggest;

        if (!count && count <= 0) {
            count = defaultRowCount;
        }

        var request = new sql.Request(connection);

        request.input('UserId', sql.BigInt, userId);
        request.input('ItemCount', sql.Int, count);
        request.input('MinPost', sql.Int, minPost);

        var selectionField = getUserSelectionField(ast, 'tt');
        if (!selectionField) {
            selectionField = '*';
        }

        var query = 'declare @temp Table(FollowingId bigint);';
        query += ' insert into @temp(FollowingId) ';
        query += ' Select ToUserId ';
        query += ' From UserFollowers ';
        query += ' Where FromUserId = @UserId ';

        query += ' Select distinct top (@ItemCount) ' + selectionField;
        selectionField = getUserSelectionField(ast, null, true);
        query += ' From ( select top ' + myConfig.SystemSuggestNumber + ' ' + selectionField + ', 0 as SOrder ';
        query += ' From Users U ';
        query += ' Where U.UserId in (Select UserId From SystemSuggestUsers) ';
        query += ' and U.UserId not in (Select FollowingId From @temp)  and U.UserId != @UserId and (IsBanned is null or IsBanned = 0) and (IsActive = 1) ';
        query += ' Order by NEWID()';

        query += ' union ';
        selectionField = getUserSelectionField(ast, 'U', true);
        query += ' select top (@ItemCount) ' + selectionField + ', 1 as SOrder ';
        query += ' From Users U inner join UserFollowers UF on U.UserId = UF.ToUserId ';
        query += ' Where U.ImageCount > @MinPost and  UF.FromUserId in (Select FollowingId From @temp) and U.UserId not in (Select FollowingId From @temp) and U.UserId != @UserId and (IsBanned is null or IsBanned = 0) and (IsActive = 1) ';
        query += ' Order by NewID() ';
        query += ' ) as tt ';
        query += ' Where (RoleId is null or RoleId != -1) ';
        //query += ' Order by SOrder, NEWID() ';


        request.query(query, function (err, result) {
            if (err) {
                logger.error('suggestUserRelative', err);
                return callback(err);
            }
            else {
                var recordset = result.recordset;
                return callback(err, recordset);
            }
        });
    },

    suggestFacebookFriends: function (userId, facebookId, count, ast, callback) {
        if (!count && count <= 0) {
            count = defaultRowCount;
        }

        var request = new sql.Request(connection);

        request.input('UserId', sql.BigInt, userId);
        request.input('FacebookId', sql.VarChar, facebookId);
        request.input('ItemCount', sql.Int, count);

        var selectionField = getUserSelectionField(ast, 'U');
        if (!selectionField) {
            selectionField = '*';
        }

        var query = 'declare @temp Table(FollowingId bigint);';
        query += ' insert into @temp(FollowingId) ';
        query += ' Select ToUserId ';
        query += ' From UserFollowers ';
        query += ' Where FromUserId = @UserId ';

        //đổ danh sách bạn từ facebook vào bảng tạm
        query += ' Declare @tempFB Table(FacebookId varchar(50)); ';
        query += ' insert into @tempFB(FacebookId) ';
        query += ' Select FacebookFriendId ';
        query += ' From FacebookFriends ';
        query += ' Where FacebookId = @FacebookId and OnFiseo = 1; ';

        //đổ tiếp danh sách những user có bạn là bạn trên facebook vào bảng tạm (phòng trường hợp lấy ko đc friendlist của user)
        query += ' Insert into @tempFB(FacebookId) ';
        query += ' Select Distinct FacebookId ';
        query += ' From FacebookFriends ';
        query += ' Where FacebookFriendId = @FacebookId; ';

        //xóa những record bị trùng trong bảng tạm
        query += ' WITH CTE AS( ';
        query += ' SELECT FacebookId, RN = ROW_NUMBER() OVER(PARTITION BY FacebookId Order by FacebookId) ';
        query += ' FROM @tempFB ';
        query += ' ) ';
        query += ' DELETE FROM CTE WHERE RN > 1 ';

        //lấy danh sách top ngẫu nhiên
        query += ' select top (@ItemCount) ' + selectionField;
        query += ' From Users U inner join @tempFB UF on U.FacebookId = UF.FacebookId ';
        query += ' Where U.UserId not in (Select FollowingId From @temp) and U.UserId != @UserId and (U.IsBanned is null or U.IsBanned = 0) and (U.IsActive = 1) ';
        query += ' Order by NewID() ';

        request.query(query, function (err, result) {
            if (err) {
                logger.error('suggestFacebookFriends', err);
                return callback(err);
            }
            else {
                var recordset = result.recordset;
                return callback(err, recordset);
            }
        });
    },

    getFeatureUsers: getFeatureUsers,
    checkUserIsFeatureUser: checkUserIsFeatureUser
};