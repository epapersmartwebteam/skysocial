﻿var sql = require('mssql');
var _ = require('lodash');

//lấy config chuỗi kết nối
var config = require('../myConfig').dataConfig;
var defaultRowCount = require('../myConfig').defaultRowCount;
var myConfig = require('../myConfig');
var systemUserId = myConfig.SystemUserId;

var storageHelper = require('../helper/storageHelper');

var insight = require('./insight');

//var CDNUrl = require('../myConfig').CDNUrl;
var RedirectLink = require('../myConfig').RedirectLink;

var cacheConfig = myConfig.cacheConfig;
var redisCacheHelper = require('./redisCacheHelper');
var utilityHelper = require('../helper/utility');

var logger = require('../helper/logHelper').logger;

String.prototype.capitalizeFirstLetter = function () {
    return this.charAt(0).toUpperCase() + this.slice(1);
};
function correctShareTo(shareTo) {
    return shareTo.toLowerCase().capitalizeFirstLetter();
}

var imageItemTypes = {
    INSTA: "INSTA",
    FB: "FB"
};

var imageItemMustHaveColumn = [
    'ImageItemId',
    'ItemGUID',
    'UserId',
    'IsPrivate',
    'CommentingOff',
    'IsDelete',
    'StorageProvider'
];

var imageItemSchema = [
    'ItemContent',
    'ItemType',
    'ItemData',
    'CreateDate',
    'CreateOS',
    'LastModifyDate',
    'LastModifyOS',
    'Description',
    'Location',
    'Lat',
    'Long',
    'IsPrivate',
    'LikeCount',
    'CommentCount',
    'ShareCount',
    'ShareFromId',
    'CommentingOff',
    'IsDelete'
];

function getImageItemSelectionField(projectionArray, prefix, isNotReplace) {
    var result = imageItemSchema.filter(function (n) {
        return projectionArray.indexOf(n) > -1;
    });
    result = result.concat(imageItemMustHaveColumn);

    //tạo mảng clone để lưu tên cột trước khi Add Prefix vào
    var cloneResult = result.slice(0);

    if (prefix) {
        result.forEach(function (item, i) { result[i] = prefix + '.' + item; });
    }

    if (isNotReplace) {
    }
    else {
        //duyệt xem nếu là kiểu Datetime thì format lại thành chuỗi yyyy-MM-dd HH:mm:ss.ms
        //cloneResult.forEach(function (item, i) {
        //if (item.indexOf('Date') >= 0) {
        //    result[i] = 'convert(nvarchar, ' + result[i] + ', 120) as ' + cloneResult[i];
        //}
        //else if (item == 'ImageOriginal' || item == 'ImageLarge' || item == 'ImageMedium' || item == 'ImageSmall') {
        //    result[i] = "'" + CDNUrl + "'" + ' + ' + result[i] + ' as ' + cloneResult[i];
        //}
        //if (item == 'ItemContent') {

        //}
        //});
    }
    return result.join(',');
}

function parseItemContent(imageItem, viewUserId) {
    if (imageItem) {
        if (imageItem.ItemContent) {
            imageItem.ItemContent = utilityHelper.checkAndParseToJson(imageItem.ItemContent);

            _.each(imageItem.ItemContent, (item, index) => {
                if (item.Large && item.Large.Link) {
                    imageItem.ItemContent[index].Large.Link = storageHelper.fixStorageLink(item.Large.Link, imageItem.StorageProvider);
                }
                if (item.Medium && item.Medium.Link) {
                    imageItem.ItemContent[index].Medium.Link = storageHelper.fixStorageLink(item.Medium.Link, imageItem.StorageProvider);
                }
                if (item.Small && item.Small.Link) {
                    imageItem.ItemContent[index].Small.Link = storageHelper.fixStorageLink(item.Small.Link, imageItem.StorageProvider);
                }
                if (viewUserId) {
                    if (item.Info) {
                        if (item.Info.Link && !item.Info.Link.startsWith('http')) {
                            imageItem.ItemContent[index].Info.Link = RedirectLink + '?l=' + encodeURIComponent(item.Info.Link) + '&u=' + viewUserId + '&p=' + imageItem.ImageItemId;
                        }
                    }
                }
            });
        }
        if (imageItem.ItemData) {
            imageItem.ItemData = utilityHelper.checkAndParseToJson(imageItem.ItemData);

            if (imageItem.ItemData.PreviewLink) {
                imageItem.ItemData.PreviewLink.Image = storageHelper.fixStorageLink(imageItem.ItemData.PreviewLink.Image, imageItem.StorageProvider);
            }
        }
    }

    return imageItem;
}

function checkIsUndefined(obj) {
    if (typeof obj != "undefined") {
        return false;
    }
    else {
        return true;
    }
}

var connection = new sql.ConnectionPool(config);
connection.connect().catch((error) => {
    logger.error('database connection error', error);
});

connection.on('error', function (err) {
    logger.error('database connection error', err);
    connection = new sql.ConnectionPool(config);
    connection.connect();
});


function createImageUsertags(imageItemId, usertags, callback) {

    usertags.forEach(function (item) {

        //chuyển sang viết thường cho hashtag
        item = parseFloat(item);

        var request = new sql.Request(connection);
        request.input('ImageItemId', sql.BigInt, imageItemId);
        request.input('UserName', sql.NVarChar, item);

        var query = ' Insert into ImageUserTags(ImageItemId, UserId, IsAccepted, AcceptDate) ';
        query += ' Select top 1 @ImageItemId, UserId, 1, dbo.UNIX_TIMESTAMP(getUTCDate()) ';
        query += ' From Users ';
        query += ' Where UserName = @UserName ';

        request.query(query,
            function (err, result) {
                if (err) {
                    logger.error('createImageUsertags', err);
                    return callback(err, null);
                }
                else {
                    var recordset = result.recordset;
                    return callback(null, recordset);
                }
            });
    });
}

function createImageHashtags(imageItemId, hashtags, callback) {

    hashtags.forEach(function (item) {

        //chuyển sang viết thường cho hashtag
        item = item.toLowerCase();

        var request = new sql.Request(connection);
        request.input('ImageItemId', sql.BigInt, imageItemId);
        request.input('HashTag', sql.NVarChar, item);

        var query = 'declare @HashtagId bigint;';
        query += ' if(exists(select * From Hashtags Where Hashtag = @Hashtag))';
        query += ' begin ';
        query += ' set @HashtagId = (Select top 1 HashtagId From Hashtags Where Hashtag = @Hashtag); ';
        query += ' end ';
        query += ' else ';
        query += ' begin ';
        query += ' Insert into Hashtags(Hashtag) ';
        query += ' Values(@Hashtag); ';
        query += ' set @HashtagId = (Select SCOPE_IDENTITY()); ';
        query += ' end ';
        query += ' Insert into ImageHashtags(ImageItemId, HashtagId, Hashtag) ';
        query += ' Values(@ImageItemId, @HashtagId, @Hashtag)';

        request.query(query,
            function (err, result) {
                if (err) {
                    logger.error('createImageHashtags', err);
                    return callback(err, null);
                }
                else {
                    var recordset = result.recordset;
                    return callback(null, recordset);
                }
            });
    });
}

function getAllFeaturefeedsFromDB(viewUserId, callback) {
    var request = new sql.Request(connection);
    var ast = imageItemSchema;
    var selectionField = getImageItemSelectionField(ast, 'I');

    var query = ' Select ' + selectionField;
    query += ' From ImageItems I ';
    query += ' Where I.UserId in (Select UserId From FeatureUsers) ';
    query += ' and I.IsPrivate = 0 and I.ShareFromId is null ';
    query += ' and IsDelete != 1';
    query += ' Order by I.CreateDate desc';

    request.query(query,
        function (err, result) {

            if (err) {
                logger.error('getAllFeaturefeedsFromDB', err);
                return callback(err, null);
            }
            else {
                var recordset = result.recordset;

                if (recordset) {
                    for (var i = 0; i < recordset.length; i++) {
                        recordset[i] = parseItemContent(recordset[i], viewUserId);
                    }
                    if (cacheConfig.enableCache) {

                        //clientRedis.set(cacheConfig.MTPTabKey, JSON.stringify(recordset));
                        //redisCacheHelper.set(cacheConfig.MTPTabKey, JSON.stringify(recordset));
                    }
                    return callback(null, recordset);
                }
                else {
                    return callback(null, null);
                }
            }
        });
}

function getDeletedFeaturefeedsFromDB(callback) {
    var query = 'Select ImageItemId, DeleteDate ';
    query += ' From ImageItems ';
    query += ' Where UserId in (Select UserId From FeatureUsers) ';
    query += ' and IsPrivate = 0 and ShareFromId is null ';
    query += ' and IsDelete = 1';

    var request = new sql.Request(connection);

    request.query(query,
        function (err, result) {

            if (err) {
                logger.error('getDeletedFeaturefeedsFromDB', err);
                return callback(err, null);
            }
            else {
                var recordset = result.recordset;

                if (recordset) {
                    //for (var i = 0; i < recordset.length; i++) {
                    //    recordset[i] = parseItemContent(recordset[i]);
                    //}

                    return callback(null, recordset);
                }
                else {
                    return callback(null, null);
                }
            }
        });
}

function getFeaturefeedsFromDB(lastId, lastDate, count, callback) {

    var ast = imageItemSchema;
    var selectionField = getImageItemSelectionField(ast, 'I');

    var query = ' Select top ' + count + ' ' + selectionField;
    query += ' From ImageItems I ';
    query += ' Where I.UserId in (Select UserId From FeatureUsers) ';
    query += ' and I.IsPrivate = 0 and I.ShareFromId is null ';
    query += ' and IsDelete != 1';

    //if (lastId && lastDate) {
    //    query += ' and (I.CreateDate < @LastDate or (I.CreateDate = @LastDate and I.ImageItemId < @LastId)) ';
    //}
    if (lastId) {
        query += ' and (I.ImageItemId < @LastId) ';
    }
    else if (lastDate) {
        query += ' and (I.CreateDate < @LastDate) ';
    }

    query += ' Order by I.CreateDate desc';

    var request = new sql.Request(connection);
    request.input('LastId', sql.BigInt, parseFloat(lastId));
    request.input('LastDate', sql.BigInt, lastDate);

    request.query(query,
        function (err, result) {

            if (err) {
                logger.error('getFeaturefeedsFromDB', err);
                return callback(err, null);
            }
            else {
                var recordset = result.recordset;

                if (recordset) {
                    for (var i = 0; i < recordset.length; i++) {
                        recordset[i] = parseItemContent(recordset[i]);
                    }

                    return callback(null, recordset);
                }
                else {
                    return callback(null, null);
                }
            }
        });
}

function calculateUserImageCountQuery() {
    var query = ' Update Users ';
    query += ' Set ImageCount = (Select Count(1) From ImageItems Where UserId = @UserId and IsDelete != 1) ';
    query += ' Where UserId = @UserId; ';
    return query;
}

function calculateImageLikeQuery() {
    var query = ' Update ImageItems ';
    query += ' Set LikeCount = (Select Count(1) From ImageLikes Where ImageItemId = @ImageItemId) ';
    query += ' Where ImageItemId = @ImageItemId ';
    return query;
}

//lấy thông tin hình theo Id
function getImageItemById(viewUserId, id, ast, callback) {
    var request = new sql.Request(connection);
    request.input('ImageItemId', sql.BigInt, parseFloat(id));

    var selectionField = getImageItemSelectionField(ast);
    if (!selectionField) {
        selectionField = '*';
    }

    var query = 'select ' + selectionField + ' From [ImageItems] Where ImageItemId = @ImageItemId';

    request.query(query,
        function (err, result) {
            if (err) {
                logger.error('getImageItemById', err);
                return callback(err, null);
            }
            else {
                var recordset = result.recordset;
                if (recordset) {
                    var item = parseItemContent(recordset[0], viewUserId);
                    if (!item || item.IsDelete) {
                        return callback(null, null);
                    }
                    else {
                        return callback(null, item);
                    }
                }
                else {
                    return callback(null, null);
                }
            }
        });
}

/**
       lấy danh sách hình theo User có phân trang
       các hình do User share lại thì sẽ reference tới hình gốc thay vì hiển thị thông tin hình share lại
   */
function getImageListByUserId(viewUserId, userId, lastId, lastDate, count, ast, callback) {
    var request = new sql.Request(connection);

    //if (lastDate)
    //    lastDate = new Date(lastDate);

    request.input('UserId', sql.BigInt, parseFloat(userId));
    request.input('LastId', sql.BigInt, parseFloat(lastId));
    request.input('LastDate', sql.BigInt, lastDate);

    var selectionField = getImageItemSelectionField(ast, 'I');
    if (!selectionField) {
        selectionField = '*';
    }

    if (!count) {
        count = defaultRowCount;
    }

    //var query = 'declare @temp table(ImageItemId bigint);';
    //query += ' Insert into @temp(ImageItemId)';
    //query += ' Select top ' + count + ' ShareFromId From ImageItems Where UserId = @UserId and ShareFromId is not null ';
    //query += ' and IsDelete != 1 ';

    //if (lastId && lastDate) {
    //    query += ' and (CreateDate < @LastDate or (CreateDate = @LastDate and ImageItemId < @LastId)) ';
    //}
    //else if (lastDate) {
    //    query += ' and (CreateDate < @LastDate) ';
    //}
    //else if (lastId) {
    //    query += ' and (ImageItemId < @LastId) ';
    //}
    //query += ' Order by CreateDate desc';

    var query = ' Select top ' + count + ' ' + selectionField;
    query += ' From [ImageItems] I';
    //query += ' Where ((I.UserId = @UserId and I.ShareFromId is null) or I.ImageItemId in (Select ImageItemId From @temp)) ';
    query += ' Where (I.UserId = @UserId and I.ShareFromId is null) ';
    query += ' and IsDelete != 1 ';

    //nếu không phải tự xem thì ko show hình riêng tư
    if (viewUserId != userId) {
        query += ' and IsPrivate != 1';
    }

    if (lastId) {
        query += ' and (I.ImageItemId < @LastId) ';
        query += ' Order by I.ImageItemId desc';
    }
    else if (lastDate) {
        query += ' and (I.CreateDate < @LastDate) ';
        query += ' Order by I.CreateDate desc';
    }
    else {
        query += ' Order by I.ImageItemId desc';
    }
    request.query(query,
        function (err, result) {

            if (err) {
                logger.error('getImageListByUserId', err);
                return callback(err, null);
            }
            else {
                var recordset = result.recordset;
                if (recordset) {
                    for (var i = 0; i < recordset.length; i++) {
                        recordset[i] = parseItemContent(recordset[i], viewUserId);
                    }
                    return callback(null, recordset);
                }
                else {
                    return callback(null, null);
                }
            }
        });
}

function getImageListByHashtag(userId, hashtag, lastId, lastDate, count, ast, callback) {
    var request = new sql.Request(connection);

    //if (lastDate)
    //    lastDate = new Date(lastDate);

    request.input('UserId', sql.BigInt, parseFloat(userId));
    request.input('Hashtag', sql.NVarChar, hashtag);
    request.input('LastId', sql.BigInt, parseFloat(lastId));
    request.input('LastDate', sql.BigInt, lastDate);

    var selectionField = getImageItemSelectionField(ast, 'I');
    if (!selectionField) {
        selectionField = '*';
    }

    if (!count) {
        count = defaultRowCount;
    }

    var query = 'declare @HashtagId bigint = (select top 1 HashtagId from Hashtags Where Hashtag = @Hashtag); ';
    query += ' declare @temp Table(ImageItemId bigint); ';
    query += ' insert into @temp (ImageItemId) ';
    query += ' Select ImageItemId ';
    query += ' From ImageHashtags ';
    query += ' Where HashtagId = @HashtagId; ';

    query += ' Select top ' + count + ' ' + selectionField;
    query += ' From ImageItems I';
    query += ' Where I.ImageItemId in (Select ImageItemId From @temp) ';
    query += ' and IsDelete != 1 ';

    //nếu không phải tự xem thì ko show hình riêng tư
    query += ' and (I.UserId = @UserId or I.IsPrivate != 1) ';

    //if (lastId && lastDate) {
    //    query += ' and (I.CreateDate < @LastDate or (I.CreateDate = @LastDate and I.ImageItemId < @LastId)) ';
    //}
    if (lastId) {
        query += ' and (I.ImageItemId < @LastId) ';
        query += ' Order by I.ImageItemId desc';
    }
    else if (lastDate) {
        query += ' and (I.CreateDate < @LastDate) ';
        query += ' Order by I.CreateDate desc';
    }
    else {
        query += ' Order by I.ImageItemId desc';
    }

    request.query(query,
        function (err, result) {

            if (err) {
                logger.error('getImageListByHashtag', err);
                return callback(err, null);
            }
            else {
                var recordset = result.recordset;
                if (recordset) {
                    for (var i = 0; i < recordset.length; i++) {
                        recordset[i] = parseItemContent(recordset[i], userId);
                    }
                    return callback(null, recordset);
                }
                else {
                    return callback(null, null);
                }
            }
        });
}

function getNewsfeedByUserIdSP(userId, lastId, lastDate, count, ast, callback) {
    var request = new sql.Request(connection);

    request.input('UserID', sql.BigInt, parseFloat(userId));
    request.input('Count', sql.BigInt, parseFloat(count));
    request.input('LastDate', sql.BigInt, lastDate);

    var selectionField = getImageItemSelectionField(ast);
    if (!selectionField) {
        selectionField = '*';
    }

    if (!count) {
        count = defaultRowCount;
    }

    /** dùng store nên chưa chuyền được SelectionField
        buộc phải dùng store do dùng Temp table để tăng tốc câu query lấy Danh sách người đang follow
        trung bình chạy khoảng 400-600ms nhanh hơn phân nửa so với viết thẳng query bên dưới
    */
    var query = 'getUserNewsfeed';
    request.execute(query,
        function (err, result) {
            if (err) {
                logger.error('getNewsfeedByUserIdSP', err);
                return callback(err, null);
            }
            else {
                var recordset = result.recordset;
                if (recordset) {

                    //dùng Store Procedure thì có trả về thêm returnValue nên phải lấy vế đầu [0] trước khi trả về
                    for (var i = 0; i < recordset.length; i++) {
                        recordset[i] = parseItemContent(recordset[i], userId);
                    }
                    return callback(null, recordset);
                }
                else {
                    return callback(null, null);
                }
            }
        });
}

function getNewsfeedByUserId(userId, lastId, lastDate, count, ast, callback) {
    var request = new sql.Request(connection);

    //if (lastDate)
    //    lastDate = new Date(lastDate);
    request.input('UserId', sql.BigInt, parseFloat(userId));
    request.input('LastId', sql.BigInt, parseFloat(lastId));
    request.input('LastDate', sql.BigInt, lastDate);

    var selectionField = getImageItemSelectionField(ast, 'I');
    if (!selectionField) {
        selectionField = '*';
    }

    if (!count || count <= 0) {
        count = defaultRowCount;
    }

    /**
        Không dùng StoreProcedure chạy nhanh hơn do chỉ select ra những field cần. Nhưng phải Insert Follower vào temp table trước
    */
    var query = 'declare @temp table(UserId bigint);';
    query += 'declare @tempHash table (HashtagId bigint); ';

    query += 'Insert Into @temp(UserId) Select ToUserId From UserFollowers Where FromUserId = @UserId and IsAccepted = 1;';
    query += 'Insert into @tempHash(HashtagId) Select HashtagId From UserHashtags Where UserId = @UserId; ';

    query += ' WITH MyRowSet as (';
    query += ' Select ' + selectionField;
    //query += ', Rank() over(Partition By I.UserId, CAST(dbo.UNIXToDateTime(I.CreateDate) as Date) Order by NewId()) as RowNum ';
    query += ' From ImageItems I ';
    query += ' Where (I.UserId in (Select UserId From @temp) or I.UserId = @UserId or (ImageItemId in (Select ImageItemId From ImageHashtags Where HashtagId in (Select HashtagId From @tempHash)))) ';
    query += ' and I.IsPrivate = 0 and I.ShareFromId is null ';
    query += ' and I.IsDelete != 1 ';
    query += ' and I.UserId != ' + systemUserId;

    //if (lastId && lastDate) {
    //    query += ' and (I.CreateDate < @LastDate or (I.CreateDate = @LastDate and I.ImageItemId < @LastId))';
    //}
    if (lastId) {
        query += ' and (I.ImageItemId < @LastId)';
    }
    else if (lastDate) {
        query += ' and (I.CreateDate < @LastDate)';
    }

    query += ') ';
    query += ' Select top ' + count + ' * ';
    query += ' From MyRowSet ';
    //query += ' Where RowNum <= ' + myConfig.MaxImagePerUserPerDay;
    if (lastId) {
        query += ' Order by ImageItemId desc';
    }
    else if (lastDate) {
        query += ' Order by CreateDate desc';
    }
    else {
        query += ' Order by CreateDate desc';
    }

    request.query(query,
        function (err, result) {

            if (err) {
                logger.error('getNewsfeedByUserId', err);
                return callback(err, null);
            }
            else {
                var recordset = result.recordset;

                if (recordset) {
                    for (var i = 0; i < recordset.length; i++) {
                        recordset[i] = parseItemContent(recordset[i], userId);
                    }
                    if (recordset.length > 2) {
                        var temp = _.slice(recordset, 1, recordset.length - 1);
                        temp = _.shuffle(temp);
                        temp.unshift(recordset[0]);
                        temp.push(recordset[recordset.length - 1]);
                        recordset = temp;
                    }
                    return callback(null, recordset);
                }
                else {
                    return callback(null, null);
                }
            }
        });
}
function getAllFeaturefeeds(viewUserId, callback) {
    //if (cacheConfig.enableCache) {
    //    redisCacheHelper.get(cacheConfig.MTPTabKey, (err, reply) => {
    //        console.log('MTP');
    //        if (!reply) {

    //            getAllFeaturefeedsFromDB(viewUserId, callback);
    //        }
    //        else {

    //            console.log('Get FeatureFeed From Redis');
    //            var recordset = JSON.parse(reply);
    //            callback(null, recordset);
    //        }
    //    });
    //}
    //else {

    //    getAllFeaturefeedsFromDB(viewUserId, callback);
    //}
}

function getFeatureFeedsFromCache(lastId, lastDate, count, getNewPost, callback) {
    redisCacheHelper.getPostsFromFeatureTab(lastDate, lastId, count, getNewPost, callback);
}

function getFeaturefeeds(viewUserId, lastId, lastDate, count, ast, callback) {

    var request = new sql.Request(connection);

    //if (lastDate)
    //    lastDate = new Date(lastDate);

    request.input('LastId', sql.BigInt, parseFloat(lastId));
    request.input('LastDate', sql.BigInt, lastDate);

    var selectionField = getImageItemSelectionField(ast, 'I');
    if (!selectionField) {
        selectionField = '*';
    }

    if (!count || count <= 0) {
        count = defaultRowCount;
    }

    /**
        Không dùng StoreProcedure chạy nhanh hơn do chỉ select ra những field cần. Nhưng phải Insert Follower vào temp table trước
    */
    var query = 'declare @temp table(UserId bigint);';
    query += 'Insert Into @temp(UserId) ';
    //query += ' Select ToUserId From UserFollowers Where FromUserId = @UserId and IsAccepted = 1; '
    query += ' Select 1 ;';
    query += ' Select top ' + count + ' ' + selectionField;
    query += ' From ImageItems I ';
    query += ' Where (I.UserId in (Select UserId From @temp)) ';
    query += ' and I.IsPrivate = 0 and I.ShareFromId is null';
    query += ' and IsDelete != 1 ';

    //if (lastId && lastDate) {
    //    query += ' and (I.CreateDate < @LastDate or (I.CreateDate = @LastDate and I.ImageItemId < @LastId))';
    //}
    if (lastId) {
        query += ' and (I.ImageItemId < @LastId)';
        query += ' Order by I.ImageItemId desc';
    }
    else if (lastDate) {
        query += ' and (I.CreateDate < @LastDate)';
        query += ' Order by I.CreateDate desc, I.ImageItemId desc';
    }
    else {
        query += ' Order by I.ImageItemId desc';
    }

    request.query(query,
        function (err, result) {

            if (err) {
                logger.error('getFeaturefeeds', err);
                return callback(err, null);
            }
            else {
                var recordset = result.recordset;

                if (recordset) {
                    for (var i = 0; i < recordset.length; i++) {
                        recordset[i] = parseItemContent(recordset[i], viewUserId);
                    }
                    return callback(null, recordset);
                }
                else {
                    return callback(null, null);
                }
            }
        });
}

function getExploreByUserId(userId, lastId, count, ast, callback) {
    var request = new sql.Request(connection);

    //lastDate tạm không cần dùng vì xét theo Id là đủ
    //if (lastDate)
    //    lastDate = new Date(lastDate);

    request.input('UserId', sql.BigInt, parseFloat(userId));

    if (!count || count <= 0) {
        count = defaultRowCount;
    }

    var choosenTotal = (count * 3) + 10;

    var selectionField = getImageItemSelectionField(ast, 'I1');
    if (!selectionField) {
        selectionField = '*';
    }

    var query = 'Select ' + selectionField;
    query += ' From ( ';

    selectionField = getImageItemSelectionField(ast, 'I2', true);
    if (!selectionField) {
        selectionField = '*';
    }

    query += ' Select top ' + count + ' ' + selectionField;
    query += ' From ( ';

    selectionField = getImageItemSelectionField(ast, 'I', true);
    if (!selectionField) {
        selectionField = '*';
    }

    query += ' Select top ' + choosenTotal + ' ' + selectionField;
    query += ' From ImageItems I ';

    //có truyền LastId vào mới thêm
    if (lastId != undefined && lastId != null && lastId > 0) {

        request.input('LastId', sql.BigInt, parseFloat(lastId));
        query += ' Where ImageItemId < @LastId and (IsPrivate = null or IsPrivate = 0) and UserId != @UserId and ShareFromId is null ';
    }
    else {
        query += ' Where (IsPrivate = null or IsPrivate = 0) and UserId != @UserId and ShareFromId is null ';
    }
    query += ' and IsDelete != 1 ';

    query += ' Order by ImageItemId desc ';
    query += ' ) as I2 ';
    query += ' Order by NEWID() ';
    query += ' ) as I1 ';
    query += ' Order by ImageItemId desc ';

    request.query(query,
        function (err, result) {

            if (err) {
                logger.error('getExploreByUserId', err);
                return callback(err, null);
            }
            else {
                var recordset = result.recordset;
                if (recordset) {
                    for (var i = 0; i < recordset.length; i++) {
                        recordset[i] = parseItemContent(recordset[i], userId);
                    }
                    return callback(null, recordset);
                }
                else {
                    return callback(null, null);
                }
            }
        });
}

function createImageItem(userId, itemGUID, itemContent, itemType, itemData,
    createOS, description, isPrivate, location, lat, long, callback) {
    var request = new sql.Request(connection);

    request.input('UserId', sql.BigInt, parseFloat(userId));
    request.input('ItemGUID', sql.NVarChar, (itemGUID ? itemGUID : ''));
    request.input('ItemContent', sql.NVarChar, (itemContent ? itemContent : ''));
    request.input('ItemType', sql.NVarChar, itemType ? itemType : imageItemTypes.INSTA);
    request.input('ItemData', sql.NVarChar, (itemData ? itemData : ''));
    request.input('CreateOS', sql.NVarChar, (createOS ? createOS : 'iOS'));
    request.input('Description', sql.NVarChar, (description ? description : ''));
    request.input('IsPrivate', sql.Bit, isPrivate);
    request.input('Location', sql.NVarChar, (location ? location : ''));
    request.input('Lat', sql.Float, (lat ? lat : 0));
    request.input('Long', sql.Float, (long ? long : 0));
    request.input('StorageProvider', sql.NVarChar, storageHelper.provider);

    var query = ' Insert into ImageItems(ItemGUID, UserId, ItemContent, ItemType, ItemData, StorageProvider, ';
    query += ' CreateOS, LastModifyOS, Description, IsPrivate, Location, Lat, Long, LikeCount, CommentCount, ShareCount) ';
    query += ' Values(@ItemGUID, @UserId, @ItemContent,  @ItemType, @ItemData, @StorageProvider, ';
    query += ' @CreateOS, @CreateOS, @Description, @IsPrivate, @Location, @Lat, @Long, 0, 0, 0); ';
    query += ' Declare @ImageItemId bigint = SCOPE_IDENTITY(); ';

    //query += ' Update Users ';
    //query += ' Set ImageCount = ImageCount + 1 ';
    //query += ' Where UserId = @UserId; ';
    query += calculateUserImageCountQuery();

    query += ' Select * From ImageItems Where ImageItemId = @ImageItemId ';

    request.query(query,
        function (err, result) {

            if (err) {
                logger.error('createImageItem', err);
                return callback(err, null);
            }
            else {
                //if (cacheConfig.enableCache) {
                //    if (userId == systemUserId) {
                //        clientRedis.del(cacheConfig.MTPTabKey);
                //    }
                //}
                var recordset = result.recordset;
                return callback(null, recordset[0]);
            }
        });
}

function updateImageItem(userId, imageItemId, os, description, itemData, isPrivate, location, lat, long, callback) {

    console.log(imageItemId, os, description, isPrivate, location, lat, long);

    var request = new sql.Request(connection);

    request.input('ImageItemId', sql.BigInt, imageItemId);
    request.input('OS', sql.VarChar, os);


    var query = 'Update ImageItems ';
    query += ' Set LastModifyOS = @OS, LastModifyDate = dbo.UNIX_TIMESTAMP(getUTCDate()) ';

    if (!checkIsUndefined(description)) {
        request.input('Description', sql.NVarChar, description);
        query += ', Description = @Description ';
    }

    if (!checkIsUndefined(isPrivate)) {
        request.input('IsPrivate', sql.Bit, isPrivate);
        query += ', IsPrivate = @IsPrivate ';
    }

    if (!checkIsUndefined(location)) {
        request.input('Location', sql.NVarChar, location);
        query += ', Location = @Location ';
    }

    if (!checkIsUndefined(lat)) {
        request.input('Lat', sql.Float, lat);
        request.input('Long', sql.Float, long);
        query += ', Lat = @Lat, Long = @Long ';
    }

    if (!checkIsUndefined(itemData)) {
        request.input('ItemData', sql.NVarChar, itemData);
        query += ', ItemData = @ItemData ';
    }

    query += ' Where ImageItemId = @ImageItemId ';

    request.query(query, function (err, result) {
        var recordset = result.recordset;
        if (err) {
            //if (cacheConfig.enableCache) {
            //    if (userId == systemUserId) {
            //        clientRedis.del(cacheConfig.MTPTabKey);
            //    }
            //}
            logger.error('updateImageItem', err);
        }
        return callback(err, recordset);
    });
}

function deleteImageItem(imageItemId, userId, callback) {
    var request = new sql.Request(connection);

    request.input('ImageItemId', sql.BigInt, imageItemId);
    request.input('UserId', sql.BigInt, userId);

    var query = 'if(exists(Select * From ImageItems Where ImageItemId = @ImageItemId and IsDelete != 1 and (UserId = @UserId or exists(select * From Users Where UserId = @UserId and RoleId in (' + myConfig.RoleAdmin + ',' + myConfig.RoleMod + '))))) ';
    query += ' Select 1 as IsExists ';
    query += ' else ';
    query += ' Select 0 as IsExists ';

    request.query(query, function (err, result) {
        if (!err) {
            var recordset = result.recordset;
            //là chủ hình mới đc xóa
            if (recordset[0].IsExists == 1) {

                var request1 = new sql.Request(connection);
                request1.input('ImageItemId', sql.BigInt, imageItemId);

                var query1 = ' Declare @OwnerGUID nvarchar(max) = (Select top 1 UserGUID From Users Where UserId in (Select UserId From ImageItems Where ImageItemId = @ImageItemId)); ';
                query1 += ' Declare @UserId bigint = (Select top 1 UserId From ImageItems Where ImageItemId = @ImageItemId); ';
                query1 += ' Update ImageItems ';
                query1 += ' Set IsDelete = 1, DeleteDate = dbo.UNIX_TIMESTAMP(getUTCDate()), DeleteUser = @UserId ';
                query1 += ' Where ImageItemId = @ImageItemId; ';
                query1 += calculateUserImageCountQuery();
                query1 += ' Select @OwnerGUID as UserGUID ';

                request1.query(query1, function (error, data) {

                    if (!error) {
                        deleteImageRelative(imageItemId, () => { });
                        return callback(error, data.recordset[0]);
                    }
                    else {
                        logger.error('deleteImageItem', err);
                        return callback(error);
                    }

                });
            }
            else {
                return callback('NotPermission');
            }
        }
        else {
            logger.error('deleteImageItem', err);
            return callback(err);
        }
    });
}

function deleteImageRelative(imageItemId, callback) {
    var request = new sql.Request(connection);

    request.input('ImageItemId', sql.BigInt, imageItemId);
    var query = 'deleteImageRelative';
    request.execute(query, function (err, result) {
        if (err) {
            logger.error('deleteImageRelative', err);
            callback(false);
        }
        else {
            callback(true);
        }
    });
}

function deleteUserAllImageRelative(userId, callback) {
    var request = new sql.Request(connection);

    request.input('UserId', sql.BigInt, userId);
    var query = 'deleteUserAllImageRelative';
    request.execute(query, function (err, result) {
        if (err) {
            logger.error('deleteUserAllImageRelative', err);
            callback(false);
        }
        else {
            callback(true);
        }
    });
}

function deleteUserImageItem(userId, deleteUserId, callback) {

    var request1 = new sql.Request(connection);
    request1.input('UserId', sql.BigInt, userId);
    request1.input('DeleteUserId', sql.BigInt, deleteUserId);

    //var query1 = ' Delete From ImageLikes Where ImageItemId in (Select ImageItemId From ImageItems Where UserId = @UserId); ';
    //query1 += ' Delete From ImageComments Where ImageItemId in (Select ImageItemId From ImageItems Where UserId = @UserId);';
    //query1 += ' Delete From ImageHashtags Where ImageItemId in (Select ImageItemId From ImageItems Where UserId = @UserId); ';
    //query1 += ' Delete From ImageReports Where ImageItemId in (Select ImageItemId From ImageItems Where UserId = @UserId); ';
    ////query1 += ' Delete From ImageSubscribes Where ImageItemId in (Select ImageItemId From ImageItems Where UserId = @UserId); ';
    ////query1 += ' Delete From UserLibraries Where ImageItemId in (Select ImageItemId From ImageItems Where UserId = @UserId); ';
    //query1 += ' Delete From ImageItems Where UserId = @UserId; ';

    var query1 = ' Update ImageItems ';
    query1 += ' Set IsDelete = 1, DeleteDate = dbo.UNIX_TIMESTAMP(getUTCDate()), DeleteUserId = @DeleteUserId ';
    query1 += ' Where UserId = @UserId';
    //query1 += ' Update Users Set ImageCount = 0 Where UserId = @UserId';
    query1 += calculateUserImageCountQuery();

    request1.query(query1, function (error, result) {
        var recordset = null;
        if (!error) {
            recordset = result.recordset;
            deleteUserAllImageRelative(userId, () => { });
        }
        else {
            logger.error('deleteUserImageItem', error);
        }
        return callback(error, recordset);
    });
}

function updateImageHashtags(imageItemId, hashtags, callback) {

    var request = new sql.Request(connection);
    request.input('ImageItemId', sql.BigInt, imageItemId);

    //xóa trước rồi insert lại vào sau
    var query = 'Delete From ImageHashtags Where ImageItemId = @ImageItemId';
    request.query(query,
        function (err, recordset) {
            if (!err) {

                //nếu xóa ko lỗi thì gọi hàm thêm vào
                createImageHashtags(imageItemId, hashtags, function (err1, dumb) {
                });
                return callback(err, { Success: true });
            }
            else {
                logger.error('updateImageHashtags', err);
                return callback(err);
            }
        });
}

function updateImageUsertags(imageItemId, usertags, callback) {
    var request = new sql.Request(connection);
    request.input('ImageItemId', sql.BigInt, imageItemId);

    //xóa trước rồi insert lại vào sau
    var query = 'Delete From ImageUserTags Where ImageItemId = @ImageItemId';
    request.query(query,
        function (err, recordset) {
            if (!err) {
                //nếu xóa ko lỗi thì gọi hàm thêm vào
                createImageUsertags(imageItemId, usertags, function (err1, dumb) {
                });
            }
            else {
                logger.error('updateImageUsertags', err);
                return callback(err);
            }
        });
}

function updateImageItemCommentingOff(userId, imageItemId, commentingOff, callback) {
    var request = new sql.Request(connection);

    request.input('ImageItemId', sql.BigInt, imageItemId);
    request.input('UserId', sql.BigInt, userId);
    request.input('CommentingOff', sql.Bit, commentingOff);

    var query = 'declare @OwnerId bigint = (Select top 1 UserId From ImageItems Where ImageItemId = @ImageItemId); ';
    query += ' if(@OwnerId is null) ';
    query += ' begin ';
    query += ' Select 1 as Error; ';
    query += ' end ';
    query += ' else ';
    query += ' begin ';
    query += ' if(@OwnerId != @UserId) ';
    query += ' begin ';
    query += ' Select 2 as Error; ';
    query += ' end';
    query += ' else ';
    query += ' begin ';
    query += ' Update ImageItems ';
    query += ' Set CommentingOff = @CommentingOff ';
    query += ' Where ImageItemId = @ImageItemId ';
    query += ' Select 0 as Error; ';
    query += ' end';
    query += ' end';

    request.query(query,
        function (err, result) {
            if (err) {
                logger.error('updateImageItemCommentingOff', err);
                return callback(err, null);
            }
            else {
                var recordset = result.recordset;
                if (recordset && recordset.length > 0) {
                    var error = recordset[0].Error;
                    if (error == 0) {
                        //if (userId == systemUserId) {
                        //    clientRedis.del(cacheConfig.MTPTabKey);
                        //}
                    }
                    callback(null, error);
                }
                else {
                    callback(null, 1);
                }
            }
        }
    );
}

function updateImageItemLiked(userId, imageItemId, isLiked, likeEmotionId, callback) {
    var request = new sql.Request(connection);

    request.input('ImageItemId', sql.BigInt, imageItemId);
    request.input('UserId', sql.BigInt, userId);
    request.input('IsLiked', sql.Bit, isLiked);
    request.input('LikeEmotionId', sql.Int, likeEmotionId);

    //nếu đã like trước đó
    var query = 'if(exists(Select * From ImageLikes Where UserId = @UserId and ImageItemId = @ImageItemId)) ';
    query += ' begin ';
    //set lại là ko like
    query += ' if(@IsLiked = 0) ';
    query += ' begin ';
    //xóa ImageLike đi
    query += ' Delete from ImageLikes where UserId = @UserId and ImageItemId = @ImageItemId; ';
    //cập nhật giảm số lượng like đi 1
    //query += ' Update ImageItems ';
    //query += ' Set LikeCount = LikeCount - 1 ';
    //query += ' Where ImageItemId = @ImageItemId ';
    query += calculateImageLikeQuery();
    query += ' end ';
    //ngược lại nếu vẫn set like
    query += ' else ';
    query += ' begin ';
    //cập nhật lại LikeEmotionId
    query += ' Update ImageLikes ';
    query += ' Set LikeEmotionId = @LikeEmotionId ';
    query += ' Where UserId = @UserId and ImageItemId = @ImageItemId ';
    query += ' end ';
    query += ' end ';
    //nếu ngược lại ko tồn tại đã like
    query += ' else ';
    query += ' begin ';
    //nếu set là like
    query += ' if(@IsLiked = 1) ';
    query += ' begin ';
    //thêm dòng trong ImageLike
    query += ' Insert into ImageLikes(ImageItemId, UserId, LikeEmotionId) ';
    query += ' Values(@ImageItemId, @UserId, @LikeEmotionId); ';
    //cập nhật thêm số lượng like thêm 1
    //query += ' Update ImageItems ';
    //query += ' Set LikeCount = LikeCount + 1 ';
    //query += ' Where ImageItemId = @ImageItemId ';
    query += calculateImageLikeQuery();
    query += ' end ';
    //ngược lại thì ko cần làm gì
    query += ' end';
    query += ' Select UserId From ImageItems Where ImageItemId = @ImageItemId ';

    request.query(query,
        function (err, result) {
            if (err) {
                logger.error('updateImageItemLiked', err);
                return callback(err, null);
            }
            else {
                var recordset = result.recordset;
                //if (cacheConfig.enableCache) {
                //    var item = recordset[0];
                //    if (item && item.UserId) {
                //        if (item.UserId == systemUserId) {
                //            redisCacheHelper.del(cacheConfig.MTPTabKey);
                //        }
                //    }
                //}
                if (isLiked) {
                    insight.addPostEngagement(userId, imageItemId, insight.ENGAGETYPE.LIKED, () => {
                    });
                }
                return callback(null, recordset);
            }
        });
}

function reshareImageItem(userId, imageItemId, shareTo, callback) {
    var request = new sql.Request(connection);

    shareTo = correctShareTo(shareTo);

    request.input('ImageItemId', sql.BigInt, imageItemId);
    request.input('UserId', sql.BigInt, userId);
    request.input('ShareTo', sql.NVarChar, shareTo);

    var query = ' if(not exists(Select * From ImageShares Where UserId = @UserId and ImageItemId = @ImageItemId and ShareTo = @ShareTo)) ';
    query += ' begin ';
    query += ' Insert into ImageShares(ImageItemId, UserId, ShareTo) ';
    query += ' Values(@ImageItemId, @UserId, @ShareTo); ';

    //cập nhật tăng số share của ImageItem
    query += ' Update ImageItems ';
    query += ' Set ShareCount = ShareCount + 1 ';
    query += ' Where ImageItemId = @ImageItemId; ';

    //nếu share lên wall
    query += " if(@ShareTo = 'Wall') ";
    query += ' begin ';
    query += ' Insert Into ImageItems(ItemGUID, UserId, ItemContent, CreateOS,  ';
    query += ' Description, Location, Lat, Long, IsPrivate, LikeCount, CommentCount, ShareCount, ShareFromId) ';
    query += ' Select ItemGUID, @UserId, ItemContent, CreateOS, ';
    query += ' Description, Location, Lat, Long, IsPrivate, 0, 0, 0, @ImageItemId ';
    query += ' From ImageItems ';
    query += ' Where ImageItemId = @ImageItemId ';
    query += ' end ';
    query += ' end ';

    request.query(query,
        function (err, result) {
            if (err) {
                logger.error('reshareImageItem', err);
                return callback(err, null);
            }
            else {
                var recordset = result.recordset;
                return callback(null, recordset);
            }
        });
}

function getSaveImages(viewUserId, userId, collectionId, lastDate, count, ast, callback) {
    var request = new sql.Request(connection);

    request.input('UserId', sql.BigInt, parseInt(userId));
    request.input('LastDate', sql.BigInt, parseFloat(lastDate));
    try {
        request.input('CollectionId', sql.Int, parseInt(collectionId));
    }
    catch (ex) {
    }

    var selectionField = getImageItemSelectionField(ast, 'I');
    if (!selectionField) {
        selectionField = '*';
    }

    if (!count) {
        count = defaultRowCount;
    }

    var query = ' Select top ' + count + ' ' + selectionField;
    query += ' , US.CreateDate as SavedDate ';
    query += ' From [ImageItems] I';

    if (collectionId) {
        query += ' Inner Join UserCollectionSaves US on I.ImageItemId = US.ImageItemId ';
        query += ' Where US.CollectionId = @CollectionId ';

    }
    else {
        query += ' Inner Join UserSaves US ON I.ImageItemId = US.ImageItemId ';
        query += ' Where US.UserId = @UserId ';
    }

    query += ' And IsDelete != 1 ';

    if (lastDate) {
        query += ' and US.CreateDate < @LastDate ';
    }

    query += ' Order By US.CreateDate desc ';

    request.query(query, (err, result) => {
        if (err) {
            logger.error('getSaveImages', err);
            return callback(err, null);
        }
        else {
            var recordset = result.recordset;

            if (recordset) {
                for (var i = 0; i < recordset.length; i++) {
                    recordset[i] = parseItemContent(recordset[i], viewUserId);
                }
                return callback(null, recordset);
            }
            else {
                return callback(null, null);
            }
        }
    });
}

function getMostXPosts(tableName, userId, topN, fromDate, toDate, ast, callback) {
    var selectionField = getImageItemSelectionField(ast, '');
    if (!selectionField) {
        selectionField = '*';
    }

    if (!topN || topN == 0) {
        topN = 1;
    }

    var query = 'Select ' + selectionField;
    query += ' From ImageItems ';
    query += ' Where UserId not in (Select UserId From FeatureUsers ) and ImageItemId in ';
    query += ' (Select top ' + topN + ' ImageItemId ';
    query += ' From ' + tableName + ' ';
    query += ' Where CreateDate >= @FromDate and CreateDate <= @ToDate ';
    query += ' Group by ImageItemId ';
    query += ' ORDER BY COUNT(*) DESC ';
    query += ' )';

    var request = new sql.Request(connection);
    request.input('FromDate', sql.BigInt, fromDate);
    request.input('ToDate', sql.BigInt, toDate);

    request.query(query, (err, result) => {
        //console.log(err);
        if (err) {
            logger.error('getMostXPosts ' + tableName, err);
            return callback(err, null);
        }
        else {
            var recordset = result.recordset;
            if (recordset) {
                for (var i = 0; i < recordset.length; i++) {
                    recordset[i] = parseItemContent(recordset[i], userId);
                }
                return callback(null, recordset);
            }
            else {
                return callback(null, null);
            }
        }
    });
}

function getMostLikePosts(userId, topN, fromDate, toDate, ast, callback) {
    getMostXPosts('ImageLikes', userId, topN, fromDate, toDate, ast, callback);
}

function getMostCommentPosts(userId, topN, fromDate, toDate, ast, callback) {
    getMostXPosts('ImageComments', userId, topN, fromDate, toDate, ast, callback);
}

function getMostSharePosts(userId, topN, fromDate, toDate, ast, callback) {
    getMostXPosts('ImageShares', userId, topN, fromDate, toDate, ast, callback);
}

function getLastMostPosts(userId, ast, callback) {
    var query = 'GetPreviousRunAdminInterReportHasData';

    var request = new sql.Request(connection);

    request.execute(query, (err, result) => {
        if (err) {
            callback(err);
        }
        else {
            var recordset = result.recordset;
            if (recordset && recordset.length > 0) {
                var likes = recordset[0].Likes;
                var shares = recordset[0].Shares;
                var comments = recordset[0].Comments;

                if (likes) {
                    likes = JSON.parse(likes);
                }
                else {
                    likes = [];
                }

                if (shares) {
                    shares = JSON.parse(shares);
                }
                else {
                    shares = [];
                }
                if (comments) {
                    comments = JSON.parse(comments);
                }
                else {
                    comments = [];
                }

                var imageItemIds = [];
                if (likes.length > 0) {
                    imageItemIds = [...imageItemIds, ...likes.map((item) => {
                        return item.ImageItemId;
                    })];
                }

                if (shares.length > 0) {
                    imageItemIds = [...imageItemIds, ...shares.map((item) => {
                        return item.ImageItemId;
                    })];
                }

                if (comments.length > 0) {
                    imageItemIds = [...imageItemIds, ...comments.map((item) => {
                        return item.ImageItemId;
                    })];
                }
                console.log(imageItemIds);
                imageItemIds = Array.from(new Set(imageItemIds));


                var selectionField = getImageItemSelectionField(ast, '');
                if (!selectionField) {
                    selectionField = '*';
                }

                query = 'Select ' + selectionField;
                query += ' From ImageItems ';
                query += ' Where ImageItemId in ';
                query += ' (' + imageItemIds.join(',') + ')';


                request.query(query, (errX, resultX) => {

                    if (errX) {
                        logger.error('getLastMostPosts', err);
                        return callback(errX, null);
                    }
                    else {
                        var recordsetX = resultX.recordset;
                        if (recordsetX) {
                            for (var i = 0; i < recordsetX.length; i++) {
                                recordsetX[i] = parseItemContent(recordsetX[i], userId);
                            }
                            return callback(null, recordsetX);
                        }
                        else {
                            return callback(null, null);
                        }
                    }
                });
            }
        }
    });
}

function checkFeatureFeedNewPosts(lastSyncDate, callback) {
    var query = 'Select Count(*) as NewPost ';
    query += ' From ImageItems ';
    query += ' Where (IsDelete is null or IsDelete = 0) and (IsPrivate is null or IsPrivate = 0) and ShareFromId is null ';
    query += ' and UserId in (Select UserId From FeatureUsers) ';
    query += ' and CreateDate > @LastSyncDate ';

    var request = new sql.Request(connection);
    request.input("LastSyncDate", sql.BigInt, lastSyncDate);
    request.query(query, (err, result) => {
        if (err) {
            logger.error('checkFeatureFeedNewPosts', err);
            callback(err);
        }
        else {
            var recordset = result.recordset;
            if (recordset && recordset.length > 0) {
                callback(null, recordset[0].NewPost);
            }
            else {
                callback(null, 0);
            }
        }
    });
}

function checkFeatureFeedUpdatedPosts(lastSyncDate, callback) {
    var query = 'Select ImageItemId ';
    query += ' From ImageItems ';
    query += ' Where (IsDelete is null or IsDelete = 0) and (IsPrivate is null or IsPrivate = 0) and ShareFromId is null ';
    query += ' and UserId in (Select UserId From FeatureUsers) ';
    query += ' and LastModifyDate > @LastSyncDate and CreateDate != LastModifyDate '; //chỉ những post có thay đổi, còn những post mới trả về trong hàm khác rồi

    var request = new sql.Request(connection);
    request.input("LastSyncDate", sql.BigInt, lastSyncDate);
    request.query(query, (err, result) => {
        if (err) {
            logger.error('checkFeatureFeedUpdatedPosts', err);
            callback(err);
        }
        else {
            var recordset = result.recordset;
            callback(null, recordset);
        }
    });
}

module.exports = {
    imageItemSchema: imageItemSchema,
    imageItemTypes: imageItemTypes,
    parseItemContent: parseItemContent,
    //lấy thông tin hình theo Id
    getImageItemById: getImageItemById,

    /**
        lấy danh sách hình theo User có phân trang
        các hình do User share lại thì sẽ reference tới hình gốc thay vì hiển thị thông tin hình share lại
    */
    getImageListByUserId: getImageListByUserId,

    getImageListByHashtag: getImageListByHashtag,

    //lấy danh sách newsfeed của User từ SP
    getNewsfeedByUserIdSP: getNewsfeedByUserIdSP,


    getNewsfeedByUserId: getNewsfeedByUserId,
    getAllFeaturefeeds: getAllFeaturefeeds,

    //lấy newsfeed ko dùng SP
    getFeaturefeeds: getFeaturefeeds,

    //lấy danh sách hình random cho User (UserId hiện tại chưa có tác dụng, chỉ truyền phòng hờ trước)
    getExploreByUserId: getExploreByUserId,

    //tạo Image Item
    createImageItem: createImageItem,

    updateImageItem: updateImageItem,

    //xóa hình
    deleteImageItem: deleteImageItem,

    deleteUserImageItem: deleteUserImageItem,

    //tạo image hashtags
    createImageHashtags: createImageHashtags,

    //cập nhật hashtags
    updateImageHashtags: updateImageHashtags,

    //tạo image Usertags
    createImageUsertags: createImageUsertags,

    //cập nhật user mention
    updateImageUsertags: updateImageUsertags,

    //update image commenting off
    //trả về 0 nếu update thành công
    //trả về 1 nếu image ko tồn tại
    //trả về 2 nếu không có quyền (ko là chủ hình)
    updateImageItemCommentingOff: updateImageItemCommentingOff,

    //update ImageLiked
    updateImageItemLiked: updateImageItemLiked,

    //share lại hình
    reshareImageItem: reshareImageItem,

    //image saved
    getSaveImages: getSaveImages,


    //thống kê
    getMostLikePosts: getMostLikePosts,
    getMostCommentPosts: getMostCommentPosts,
    getMostSharePosts: getMostSharePosts,
    getLastMostPosts: getLastMostPosts,
    //cache
    getFeatureFeedsFromCache: getFeatureFeedsFromCache,

    //sync
    checkFeatureFeedNewPosts: checkFeatureFeedNewPosts,
    checkFeatureFeedUpdatedPosts: checkFeatureFeedUpdatedPosts,
    getFeaturefeedsFromDB: getFeaturefeedsFromDB,
    getDeletedFeaturefeedsFromDB: getDeletedFeaturefeedsFromDB
};