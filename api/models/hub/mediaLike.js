﻿var sql = require('mssql');
var _ = require('lodash');

var logger = require('../../helper/logHelper').logger;

//lấy config chuỗi kết nối
var config = require('../../myConfig').dataConfig;
var defaultRowCount = require('../../myConfig').defaultRowCount;

var CDNUrl = require('../../myConfig').CDNUrl;

var connection = new sql.ConnectionPool(config);
connection.connect().catch((error) => {
    logger.error('database connection error', error);
});

connection.on('error', function (err) {
    logger.error('database connection error', err);
    connection = new sql.ConnectionPool(config);
    connection.connect();
});

function calculateMediaLikeQuery() {
    var query = ' Update Medias ';
    query += ' Set LikeCount = (Select Count(1) From MediaLikes Where MediaId = @MediaId) ';
    query += ' Where MediaId = @MediaId; ';
    return query;
}

module.exports = {
    getMediaLikeList: function (mediaId, lastDate, itemCount, callback) {
        var request = new sql.Request(connection);
        request.input('MediaId', sql.BigInt, mediaId);

        var query = "Select top " + itemCount + " ML.UserId, ML.CreateDate, U.UserName, ('" + CDNUrl + "' + U.AvatarSmall) as AvatarSmall, U.FullName, U.IsVip ";
        query += ' From MediaLikes ML inner join Users U on ML.UserId = U.UserId ';
        query += ' Where ML.MediaId = @MediaId ';
        if (lastDate) {
            request.input('LastDate', sql.BigInt, lastDate);
            query += ' and ML.CreateDate < @LastDate ';
        }
        query += ' Order by ML.CreateDate desc ';

        request.query(query,
            function (err, result) {
                if (err) {
                    logger.error('getMediaLikeList', err);
                    callback(err, null);
                }
                else {
                    var recordset = result.recordset;
                    callback(null, recordset);
                }
            });
    },

    likeMedia: function (userId, mediaId, callback) {
        var request = new sql.Request(connection);
        request.input('UserId', sql.BigInt, userId);
        request.input('MediaId', sql.BigInt, mediaId);

        var query = 'if(not exists(Select UserId From MediaLikes Where UserId = @UserId and MediaId = @MediaId)) ';
        query += ' begin ';
        query += ' Insert into MediaLikes(UserId, MediaId) ';
        query += ' Values(@UserId, @MediaId); ';

        //query += ' Update Medias ';
        //query += ' Set LikeCount = LikeCount + 1 ';
        //query += ' Where MediaId = @MediaId; ';
        query += calculateMediaLikeQuery();
        query += ' end ';

        request.query(query,
            function (err, result) {
                
                if (err) {
                    logger.error('likeMedia', err);
                    callback(err, null);
                }
                else {
                    var recordset = result.recordset;
                    callback(null, recordset);
                }
            });
    },

    unlikeMedia: function (userId, mediaId, callback) {
        var request = new sql.Request(connection);
        request.input('UserId', sql.BigInt, userId);
        request.input('MediaId', sql.BigInt, mediaId);

        var query = 'if(exists(Select UserId From MediaLikes Where UserId = @UserId and MediaId = @MediaId)) ';
        query += ' Begin ';
        query += ' Delete From MediaLikes Where UserId = @UserId and MediaId = @MediaId; ';
        //query += ' Update Medias Set LikeCount = LikeCount - 1 Where MediaId = @MediaId; ';
        query += calculateMediaLikeQuery();
        query += ' end ';

        request.query(query,
            function (err, result) {
                if (err) {
                    logger.error('unlikeMedia', err);
                    callback(err, null);
                }
                else {
                    var recordset = result.recordset;
                    callback(null, recordset);
                }
            });
    }
};
