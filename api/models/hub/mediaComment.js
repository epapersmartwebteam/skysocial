﻿var sql = require('mssql');

var logger = require('../../helper/logHelper').logger;
var myConfig = require('../../myConfig');

var config = myConfig.dataConfig;
var result = require('../result');

var defaultRowCount = myConfig.defaultRowCount;

var CDNUrl = myConfig.CDNUrl;


var mediaCommentMustHaveColumn = [
    'ItemId',
    'MediaId',
    'UserId',
    'ReplyToId',
    'Edited'
];

var mediaCommentSchema = [
    'Comment',
    'CreateDate',
    'CreateOS',
    'LastModifyDate',
    'LastModifyOS',
    'LikeCount',
    'ReplyCount'
];

function getMediaCommentSelectionField(projectionArray, prefix) {
    var result = mediaCommentSchema.filter(function (n) {
        return projectionArray.indexOf(n) > -1;
    });
    result = result.concat(mediaCommentMustHaveColumn);

    //tạo mảng clone để lưu tên cột trước khi Add Prefix vào
    var cloneResult = result.slice(0);

    if (prefix) {
        result.forEach(function (item, i) { result[i] = prefix + '.' + item; });
    }

    //duyệt xem nếu là kiểu Datetime thì format lại thành chuỗi yyyy-MM-dd HH:mm:ss.ms
    //result.forEach(function (item, i) {
    //    if (item.indexOf('Date') >= 0) {
    //        result[i] = 'convert(nvarchar, ' + result[i] + ', 120) as ' + cloneResult[i];
    //    }
    //});

    return result.join(',');
}

var connection = new sql.ConnectionPool(config);
connection.connect().catch((error) => {
    logger.error('database connection error', error);
});

connection.on('error', function (err) {
    logger.error('database connection error', err);
    connection = new sql.ConnectionPool(config);
    connection.connect();
});

function generateAvatarField(field, resultField) {
    return ' case when ' + field + ' is not null then ' + "'" + CDNUrl + "'" + ' + ' + field + ' else ' + "'" + CDNUrl + myConfig.NoAvatarLink + "' end " + ' as ' + resultField;
}
function generateIsLikedQuery(prefix) {
    var query = '';
    query += ' , cast( case when exists(Select 1 From MediaCommentLikes Where CommentId =  ' + (prefix ? prefix + '.' : '') + 'ItemId and UserId = @UserId) then 1 else 0 end as bit) as IsLiked ';
    return query;
}

function calculateMediaCommentQuery() {
    var query = ' Update Medias ';
    query += ' Set CommentCount = (Select count(1) From MediaComments Where MediaId = @MediaId) ';
    query += ' Where MediaId = @MediaId; ';
    return query;
}

function calculateCommentReplyCountQuery() {
    var query = ' Update MediaComments ';
    query += ' Set ReplyCount = (Select Count(1) From MediaComments Where ReplyToId = @ReplyToId) ';
    query += ' Where ItemId = @ReplyToId; ';
    return query;
}



function calculateMediaCommentLikeQuery() {
    var query = ' Update MediaComments ';
    query += ' Set LikeCount = (Select Count(1) From MediaCommentLikes Where CommentId = @CommentId) ';
    query += ' Where ItemId = @CommentId; ';
    return query;
}

module.exports = {
    getMediaCommentByMediaId: function (userId, mediaId, lastId, count, ast, callback) {

        var request = new sql.Request(connection);

        request.input('MediaId', sql.BigInt, parseFloat(mediaId));
        request.input('LastId', sql.BigInt, parseFloat(lastId));
        request.input('UserId', sql.BigInt, parseFloat(userId));

        //lấy danh sách field
        if (!ast) {
            ast = mediaCommentSchema;
        }
        var selectionField = getMediaCommentSelectionField(ast, 'IC');
        if (!selectionField) {
            selectionField = '*';
        }

        if (!count) {
            count = defaultRowCount;
        }

        var query = "Select top " + count + " " + selectionField + ", U.UserName, " + generateAvatarField('U.AvatarSmall', 'AvatarSmall') + ", U.FullName, U.IsVip ";
        query += ', ISNULL(U.VipLevelId, -1) as VipLevelId ';
        query += generateIsLikedQuery('IC');
        query += ' From [MediaComments] IC inner join Users U on IC.UserId = U.UserId ';
        query += ' Where IC.MediaId = @MediaId';
        //ko lấy reply comment, lấy reply comment bằng hàm khác
        query += ' and ReplyToId is null ';
        if (lastId) {
            query += ' and  IC.ItemId < @LastId ';
        }

        query += ' Order by IC.ItemId desc';

        request.query(query,
            function (err, result) {
                if (err) {
                    logger.error('getMediaCommentByMediaId', err);
                    callback(err, null);
                }
                else {
                    var recordset = result.recordset;
                    if (recordset) {
                        callback(null, recordset);
                    }
                    else {
                        callback(null, null);
                    }
                }
            });
    },

    //lấy thông tin 1 comment
    getMediaCommentById: function (userId, itemId, ast, callback) {
        var request = new sql.Request(connection);

        request.input('ItemId', sql.BigInt, parseFloat(itemId));
        request.input('UserId', sql.BigInt, parseFloat(userId));


        //lấy danh sách field
        if (!ast) {
            ast = mediaCommentSchema;
        }
        var selectionField = getMediaCommentSelectionField(ast, 'IC');

        if (!selectionField) {
            selectionField = '*';
        }

        var query = "Select " + selectionField + ", U.UserName, " + generateAvatarField('U.AvatarSmall', 'AvatarSmall') + ", U.FullName, U.IsVip ";
        query += ', ISNULL(U.VipLevelId, -1) as VipLevelId ';
        query += generateIsLikedQuery('IC');
        query += ' From MediaComments IC inner join Users U on IC.UserId = U.UserId ';
        query += ' Where ItemId = @ItemId';

        request.query(query,
            function (err, result) {
                if (err) {
                    logger.error('getMediaCommentById', err);
                    callback(err, null);
                }
                else {
                    var recordset = result.recordset;
                    if (recordset) {
                        callback(null, recordset[0]);
                    }
                    else {
                        callback(null, null);
                    }
                }
            }
        );
    },

    getMediaCommentReplyList: function (userId, replyToId, lastId, count, ast, callback) {
        var request = new sql.Request(connection);
        request.input('ReplyToId', sql.BigInt, replyToId);
        request.input('LastId', sql.BigInt, lastId);
        request.input('UserId', sql.BigInt, parseFloat(userId));

        if (!ast) {
            ast = mediaCommentSchema;
        }
        //lấy danh sách field
        var selectionField = getMediaCommentSelectionField(ast, 'IC');
        if (!selectionField) {
            selectionField = '*';
        }

        if (!count) {
            count = defaultRowCount;
        }

        var query = 'Select top ' + count + ' ' + selectionField + ", U.UserName, " + generateAvatarField('U.AvatarSmall', 'AvatarSmall') + ", U.FullName, U.IsVip ";
        query += ', ISNULL(U.VipLevelId, -1) as VipLevelId ';
        query += generateIsLikedQuery('IC');
        query += ' From [MediaComments] IC inner join Users U on IC.UserId = U.UserId ';
        query += ' Where ReplyToId = @ReplyToId ';
        if (lastId) {
            query += ' and IC.ItemId < @LastId ';
        }

        query += ' Order by IC.ItemId desc';

        request.query(query,
            function (err, result) {
                if (err) {
                    logger.error('getMediaCommentReplyList', err);
                    callback(err, null);
                }
                else {
                    var recordset = result.recordset;
                    if (recordset) {
                        callback(null, recordset);
                    }
                    else {
                        callback(null, null);
                    }
                }
            });
    },

    //thêm mới comment
    createMediaComment: function (userId, mediaId, comment, replyToId, os, callback) {
        var request = new sql.Request(connection);

        request.input('UserId', sql.BigInt, parseFloat(userId));
        request.input('MediaId', sql.BigInt, parseFloat(mediaId));
        request.input('Comment', sql.NVarChar, comment);
        request.input('CreateOS', sql.NVarChar, os);
        request.input('ReplyToId', sql.BigInt, replyToId);

        var query = 'Insert into MediaComments(UserId, MediaId, Comment, CreateOS, ReplyToId, Edited, LikeCount, ReplyCount) ';
        query += ' Values(@UserId, @MediaId, @Comment, @CreateOS, @ReplyToId, 0, 0, 0); ';
        query += ' Declare @ItemId bigint = SCOPE_IDENTITY()';

        //cập nhật thêm 1 comment vào ImageItem
        //query += 'Update Medias ';
        //query += ' Set CommentCount = CommentCount + 1 '
        //query += ' Where MediaId = @MediaId; '

        query += calculateMediaCommentQuery();

        if (replyToId) {
            //query += ' Update MediaComments ';
            //query += ' Set ReplyCount = ReplyCount + 1 ';
            //query += ' Where ItemId = @ReplyToId; ';
            query += calculateCommentReplyCountQuery();
        }

        //lấy comment Id mới
        query += 'Select @ItemId as ItemId;';

        request.query(query,
            function (err, result) {
                if (err) {
                    logger.error('createMediaComment', err);
                    callback(err, null);
                }
                else {
                    var recordset = result.recordset;
                    if (recordset) {
                        callback(null, recordset[0].ItemId);
                    }
                    else {
                        callback(null, null);
                    }
                }
            });
    },

    //cập nhật comment
    updateMediaComment: function (itemId, comment, os, callback) {
        var request = new sql.Request(connection);

        request.input('ItemId', sql.BigInt, parseFloat(itemId));
        request.input('Comment', sql.NVarChar, comment);
        request.input('LastModifyOS', sql.NVarChar, os);

        var query = ' Insert Into MediaCommentHistory(CommentId, Comment, CreateDate) ';
        query += ' Select @ItemId, Comment, CreateDate ';
        query += ' From MediaComments ';
        query += ' Where ItemId = @ItemId; ';

        query += ' Update MediaComments ';
        query += ' Set Comment = @Comment, ';
        query += ' LastModifyOS = @LastModifyOS, ';
        query += ' LastModifyDate = dbo.UNIX_TIMESTAMP(getUTCDate()) ';
        query += ' Where ItemId = @ItemId';


        request.query(query,
            function (err, result) {
                if (err) {
                    logger.error('updateMediaComment', err);
                    callback(err, null);
                }
                else {
                    var recordset = result.recordset;
                    if (recordset) {
                        callback(null, recordset);
                    }
                    else {
                        callback(null, null);
                    }
                }
            });
    },

    //xóa comment
    deleteMediaComment: function (itemId, callback) {
        var request = new sql.Request(connection);

        request.input('ItemId', sql.BigInt, parseFloat(itemId));

        var query = ' declare @MediaId bigint = (select top 1 MediaId From MediaComments Where ItemId = @ItemId); ';
        query += ' declare @ReplyToId bigint = (Select top 1 ReplyToId From MediaComments Where ItemId = @ItemId); ';

        query += ' Delete From MediaCommentHistory ';
        query += ' Where CommentId = @ItemId; ';

        query += ' Delete From MediaCommentLikes ';
        query += ' Where CommentId = @ItemId; ';

        query += ' Delete From MediaComments ';
        query += ' Where ItemId = @ItemId or ReplyToId = @ItemId; ';

        //xoá khỏi user notify
        query += ' Delete From UserNotifies ';
        query += ' Where CommentId = @ItemId and MediaId is not null; ';

        //cập nhật giảm số comment reply đi 1 (nếu có)
        //query += ' Update MediaComments ';
        //query += ' Set ReplyCount = ReplyCount - 1 ';
        //query += ' Where ItemId = @ReplyToId; ';

        ////cập nhật giảm số comment đi 1
        //query += ' Update Medias ';
        //query += ' Set CommentCount = CommentCount - 1 ';
        //query += ' Where MediaId = @MediaId; ';

        query += calculateCommentReplyCountQuery();
        query += calculateMediaCommentQuery();

        //trả về ImageItemId
        query += ' Select @MediaId as MediaId';

        request.query(query,
            function (err, result) {
                if (err) {
                    logger.error('deleteMediaComment', err);
                    callback(err, null);
                }
                else {
                    var recordset = result.recordset;
                    if (recordset) {
                        callback(null, recordset[0].MediaId);
                    }
                    else {
                        callback(null, null);
                    }
                }
            });
    },

    likeMediaComment: function (commentId, userId, callback) {
        var request = new sql.Request(connection);

        request.input('CommentId', sql.BigInt, commentId);
        request.input('UserId', sql.BigInt, userId);

        var query = 'Declare @CommentOwnerId bigint = (Select top 1 UserId From MediaComments Where ItemId = @CommentId); ';
        query += '  Declare @ItemId bigint; ';
        query += ' if not exists(Select * From MediaCommentLikes Where CommentId = @CommentId and UserId = @UserId) ';
        query += ' begin ';

        //query += ' Update MediaComments ';
        //query += ' Set LikeCount = LikeCount + 1 ';
        //query += ' Where ItemId = @CommentId; ';


        query += ' Insert into MediaCommentLikes(CommentId, CommentOwnerId, UserId) ';
        query += ' Values(@CommentId, @CommentOwnerId, @UserId); ';
        query += ' Set @ItemId = SCOPE_IDENTITY(); ';

        query += calculateMediaCommentLikeQuery();

        query += ' end ';
        query += ' else ';
        query += ' begin ';
        query += ' Set @ItemId = (Select top 1 ItemId From MediaCommentLikes Where CommentId = @CommentId and UserId = @UserId); ';
        query += ' end ';

        query += ' Select @ItemId as ItemId;';

        request.query(query,
            function (err, result) {
                if (err) {
                    logger.error('likeMediaComment', err);
                    callback(err, null);
                }
                else {
                    var recordset = result.recordset;
                    if (recordset) {
                        callback(null, recordset[0].ItemId);
                    }
                    else {
                        callback(null, null);
                    }
                }
            });
    },

    unlikeMediaComment: function (commentId, userId, callback) {
        var request = new sql.Request(connection);

        request.input('CommentId', sql.BigInt, commentId);
        request.input('UserId', sql.BigInt, userId);

        var query = '  Declare @ItemId bigint = 0; ';
        query += ' if exists(Select * From MediaCommentLikes Where CommentId = @CommentId and UserId = @UserId) ';
        query += ' begin ';

        query += ' Set @ItemId = (Select top 1 ItemId From MediaCommentLikes where CommentId = @CommentId and UserId = @UserId); ';

        //query += ' Update MediaComments ';
        //query += ' Set LikeCount = LikeCount - 1 ';
        //query += ' Where ItemId = @CommentId; ';        

        query += ' Delete From MediaCommentLikes ';
        query += ' Where CommentId = @CommentId and UserId = @UserId; ';

        query += calculateMediaCommentLikeQuery();

        query += ' end ';

        query += ' Select @ItemId as ItemId;';

        request.query(query,
            function (err, result) {
                if (err) {
                    logger.error('unlikeMediaComment', err);
                    callback(err, null);
                }
                else {
                    var recordset = result.recordset;
                    if (recordset) {
                        callback(null, recordset[0].ItemId);
                    }
                    else {
                        callback(null, null);
                    }
                }
            });
    },

    getMediaCommentLikeList: function (commentId, lastId, itemCount, callback) {
        var request = new sql.Request(connection);

        request.input('CommentId', sql.BigInt, commentId);

        if (!itemCount || itemCount == 0) {
            itemCount = defaultRowCount;
        }

        var query = ' Select top ' + itemCount + " ICL.ItemId, ICL.CommentId, ICL.CreateDate as CreateDate, U.UserId, U.UserName, ('" + CDNUrl + "' + U.AvatarSmall) as AvatarSmall, U.FullName, U.IsVip ";
        query += ', ISNULL(U.VipLevelId, -1) as VipLevelId ';
        query += ' From MediaCommentLikes ICL inner join Users U on ICL.UserId = U.UserId ';
        query += ' Where ICL.CommentId = @CommentId ';
        if (lastId) {
            request.input('LastId', sql.BigInt, lastId);
            query += ' and ICL.ItemId < @lastId ';
        }

        query += ' Order by ICL.ItemId desc ';

        request.query(query,
            function (err, result) {
                if (err) {
                    logger.error('getMediaCommentLikeList', err);
                    callback(err, null);
                }
                else {
                    var recordset = result.recordset;
                    callback(null, recordset);
                }
            });
    }
};