﻿var sql = require('mssql');
var _ = require('lodash');

var logger = require('../../helper/logHelper').logger;

//lấy config chuỗi kết nối
var config = require('../../myConfig').dataConfig;
var defaultRowCount = require('../../myConfig').defaultRowCount;

var storageHelper = require('../../helper/storageHelper');

var connection = new sql.ConnectionPool(config);
connection.connect().catch((error) => {
    logger.error('database connection error', error);
});

connection.on('error', function (err) {
    logger.error('database connection error', err);
    connection = new sql.ConnectionPool(config);
    connection.connect();
});

var mediaItemMustHaveColumn = [
    'ItemId',
    'ItemGUID',
    'MediaId',
    'ItemType',
    'StorageProvider'
];

var mediaItemSchema = [
    'Title',
    'Description',
    'SortId',
    'Author',
    'ReleaseYear',
    'FileUrl',
    'FileSizeInKb',
    'Width',
    'Height',
    'FileHDUrl',
    'FileHDSizeInKb',
    'HDWidth',
    'HDHeight',
    'LengthInSecond',
    'CoverUrl',
    'CoverWidth',
    'CoverHeight',
    'ThumbnailUrl',
    'ThumbWidth',
    'ThumbHeight',
    'LyricOrSubtitle',
    'CreateDate',
    'LastModifyDate',
    'IsPublish',
    'Version'
];

function addQueryStringLastModifyDate(val, media) {
    return val + "?" + media.LastModifyDate;
}

function parseMediaItem(media) {
    if (media.FileUrl) {
        media.FileUrl = storageHelper.fixStorageLink(media.FileUrl, media.StorageProvider);
        media.FileUrl = addQueryStringLastModifyDate(media.FileUrl, media);
    }
    if (media.FileHDUrl) {
        media.FileHDUrl = storageHelper.fixStorageLink(media.FileHDUrl, media.StorageProvider);
        media.FileHDUrl = addQueryStringLastModifyDate(media.FileHDUrl, media);
    }
    if (media.CoverUrl) {
        media.CoverUrl = storageHelper.fixStorageLink(media.CoverUrl, media.StorageProvider);
        media.CoverUrl = addQueryStringLastModifyDate(media.CoverUrl, media);
    }
    if (media.ThumbnailUrl) {
        media.ThumbnailUrl = storageHelper.fixStorageLink(media.ThumbnailUrl, media.StorageProvider);
        media.ThumbnailUrl = addQueryStringLastModifyDate(media.ThumbnailUrl, media);
    }
    return media;
}

function getMediaItemSelectionField(projectionArray, prefix, isNotReplace) {
    var result = mediaItemSchema.filter(function (n) {
        return projectionArray.indexOf(n) > -1;
    });
    result = result.concat(mediaItemMustHaveColumn);

    //tạo mảng clone để lưu tên cột trước khi Add Prefix vào
    var cloneResult = result.slice(0);

    if (prefix) {
        result.forEach(function (item, i) { result[i] = prefix + '.' + item; });
    }

    if (isNotReplace) {
    }
    else {
        //duyệt xem nếu là kiểu Datetime thì format lại thành chuỗi yyyy-MM-dd HH:mm:ss.ms
        //cloneResult.forEach(function (item, i) {
        //if (item.indexOf('FileUrl') >= 0 || item.indexOf('FileHDUrl') >= 0 || item.indexOf('CoverUrl') >= 0 || item.indexOf('ThumbnailUrl') >= 0) {
        //    result[i] = "'" + CDNUrl + "'" + ' + ' + result[i] + ' as ' + cloneResult[i];
        //}
        //if (item.indexOf('Date') >= 0) {
        //    result[i] = 'convert(nvarchar, ' + result[i] + ', 120) as ' + cloneResult[i];
        //}
        //else if (item == 'ImageOriginal' || item == 'ImageLarge' || item == 'ImageMedium' || item == 'ImageSmall') {
        //    result[i] = "'" + CDNUrl + "'" + ' + ' + result[i] + ' as ' + cloneResult[i];
        //}
        //if (item == 'ItemContent') {

        //}
        //});
    }
    return result.join(',');
}


module.exports = {
    getAllItemList: function (userId, itemTypes, pageIndex, itemCount, ast, callback) {
        var request = new sql.Request(connection);
        request.input('UserId', sql.BigInt, userId);
        if (!ast) {
            ast = mediaItemSchema;
        }
        var selectionField = getMediaItemSelectionField(ast, 'MI');
        if (!selectionField) {
            selectionField = 'MI.*';
        }
        var query = "Declare @IsVip bit = dbo.CheckUserIsVip(@UserId); ";
        query += " Select " + selectionField + ", ";
        query += " M.IsFree, M.Price, M.VipPrice, M.Title as MediaTitle, ";
        query += " cast(case when M.IsFree = 1 or (UL.IsBuy = 1) or (@IsVip = 1 and (M.VipNeedBuy is null or M.VipNeedBuy = 0)) then 1 else 0 end as bit) as CanPlay ";
        query += " From MediaItems MI inner join Medias M on MI.MediaId = M.MediaId ";
        query += " left join UserLibraries UL on UL.MediaId = MI.MediaId and UL.UserId = @UserId ";
        var strTypes = '';
        for (var i = 0; i < itemTypes.length; i++) {
            strTypes += " '" + itemTypes[i] + "'";
            if (i < itemTypes.length - 1) {
                strTypes += ", ";
            }
        }
        if (strTypes != '') {
            query += " Where MI.ItemType In (" + strTypes + ") ";
        }
        query += " Order by MI.CreateDate desc ";
        query += " OFFSET " + ((pageIndex - 1) * itemCount) + " ROWS ";
        query += " FETCH NEXT " + itemCount + " ROWS ONLY;";

        request.query(query,
            function (err, result) {
                if (err) {
                    logger.error('getAllItemList', err);
                    callback(err, null);
                }
                else {
                    var recordset = result.recordset;
                    for (var i = 0; i < recordset.length; i++) {
                        recordset[i] = parseMediaItem(recordset[i]);
                    }
                    callback(null, recordset);
                }
            });
    },

    getMediaItemByMediaId: function (userId, mediaId, ast, callback) {
        var request = new sql.Request(connection);
        request.input('MediaId', sql.BigInt, mediaId);
        request.input('UserId', sql.BigInt, userId);

        if (!ast) {
            ast = mediaItemSchema;
        }
        var selectionField = getMediaItemSelectionField(ast, 'MI');
        if (!selectionField) {
            selectionField = '*';
        }

        var query = "Declare @IsVip bit = dbo.CheckUserIsVip(@UserId); ";
        query += " Select " + selectionField + ", ";
        query += " M.IsFree, M.Price, M.VipPrice, M.Title as MediaTitle, ";
        query += " cast(case when M.IsFree = 1 or (UL.IsBuy = 1) or (@IsVip = 1 and (M.VipNeedBuy is null or M.VipNeedBuy = 0)) then 1 else 0 end as bit) as CanPlay ";
        query += " From MediaItems MI inner join Medias M on MI.MediaId = M.MediaId ";
        query += " left join UserLibraries UL on UL.MediaId = MI.MediaId and UL.UserId = @UserId ";
        query += ' Where MI.MediaId = @MediaId and MI.IsPublish = 1';

        query += ' Order by SortId, ItemId desc ';

        request.query(query,
            function (err, result) {
                if (err) {
                    logger.error('getMediaItemByMediaId', err);
                    callback(err, null);
                }
                else {
                    var recordset = result.recordset;
                    for (var i = 0; i < recordset.length; i++) {
                        recordset[i] = parseMediaItem(recordset[i]);
                    }
                    callback(null, recordset);
                }
            }
        );
    },

    getMediaItemById: function (itemId, userId, ast, callback) {
        var request = new sql.Request(connection);
        request.input('ItemId', sql.BigInt, itemId);
        request.input('UserId', sql.BigInt, userId);

        if (!ast) {
            ast = mediaItemSchema;
        }
        var selectionField = getMediaItemSelectionField(ast, 'MI');
        if (!selectionField) {
            selectionField = '*';
        }

        var query = "Declare @IsVip bit = dbo.CheckUserIsVip(@UserId); ";
        query += " Select " + selectionField + ", ";
        query += " M.IsFree, M.Price, M.VipPrice, M.Title as MediaTitle, ";
        query += " cast(case when M.IsFree = 1 or (UL.IsBuy = 1) or (@IsVip = 1 and (M.VipNeedBuy is null or M.VipNeedBuy = 0)) then 1 else 0 end as bit) as CanPlay ";
        query += " From MediaItems MI inner join Medias M on MI.MediaId = M.MediaId ";
        query += " left join UserLibraries UL on UL.MediaId = MI.MediaId and UL.UserId = @UserId ";
        query += ' Where MI.ItemId = @ItemId';

        request.query(query,
            function (err, result) {
                if (err) {
                    logger.error('getMediaItemById', err);
                    callback(err, null);
                }
                else {
                    var recordset = result.recordset;
                    if (recordset && recordset.length > 0) {

                        recordset[0] = parseMediaItem(recordset[0]);

                        callback(null, recordset[0]);
                    }
                    else {
                        callback(null, null);
                    }
                }
            }
        );
    }
};