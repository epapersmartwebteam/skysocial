﻿var sql = require('mssql');
var _ = require('lodash');

var logger = require('../../helper/logHelper').logger;

//lấy config chuỗi kết nối
var config = require('../../myConfig').dataConfig;
var defaultRowCount = require('../../myConfig').defaultRowCount;

var CDNUrl = require('../../myConfig').CDNUrl;

var connection = new sql.ConnectionPool(config);
connection.connect().catch((error) => {
    logger.error('database connection error', error);
});

connection.on('error', function (err) {
    logger.error('database connection error', err);
    connection = new sql.ConnectionPool(config);
    connection.connect();
});

var menuSchema = [
    'MenuId',
    'MenuName',
    'MenuIcon',
    'Position',
    'TargetType',
    'TargetContent'
];

function getMenuSelectionField() {
    return menuSchema.join(',');
}

function parseMenuInfo(menu) {
    try {
        menu.TargetContent = JSON.parse(menu.TargetContent);
    }
    catch (ex) {
    }
    finally {
        return menu;
    }
}

function getMenuList(callback) {
    var request = new sql.Request(connection);
    var query = 'Select ' + getMenuSelectionField() + ' ';
    query += ' From Menus ';
    query += ' Where IsActive = 1 and (IsDeleted is null or IsDeleted = 0) ';
    query += ' Order by Position ';
    request.query(query, (err, result) => {
        if (err) {
            logger.error('getMenuList', err);
            callback(err);
        }
        else {
            var recordset = result.recordset;
            _.each(recordset, (item, idx) => {
                recordset[idx] = parseMenuInfo(item);
            });
            callback(null, recordset);
        }
    });
}

module.exports = {
    getMenuList: getMenuList
};