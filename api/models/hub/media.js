﻿var sql = require('mssql');
var _ = require('lodash');

var logger = require('../../helper/logHelper').logger;

//lấy config chuỗi kết nối
var config = require('../../myConfig').dataConfig;
var defaultRowCount = require('../../myConfig').defaultRowCount;

var dateHelper = require('../../helper/dateHelper');

var storageHelper = require('../../helper/storageHelper');

var connection = new sql.ConnectionPool(config);
connection.connect().catch((error) => {
    logger.error('database connection error', error);
});

connection.on('error', function (err) {
    logger.error('database connection error', err);

    connection = new sql.ConnectionPool(config);
    connection.connect();
});

var MEDIATYPE = {
    PHOTO: 'PHOTO',
    AUDIO: 'AUDIO',
    VIDEO: 'VIDEO',
    PDF: 'PDF',
    EPUB: 'EPUB'
};

var mediaMustHaveColumn = [
    'MediaId',
    'MediaGUID',
    'MediaType',
    'StorageProvider'
];

var mediaBasicSchema = [
    'Title',
    'Description',
    'Link',
    'Cover',
    'Thumbnail',
    'SortId',
    'IsFree',
    'VipNeedBuy',
    'Version',
    'Author',
    'ReleaseYear',
    'CreateDate',
    'LikeCount',
    'CommentCount',
    'LastModifyDate',
    'CommentingOff',
    'IsLock'
];

var mediaSchema = [
    'Title',
    'Description',
    'Link',
    'Cover',
    'Thumbnail',
    'SortId',
    'IsPublish',
    'PublishAfter',
    'IsFree',
    'Price',
    'VipPrice',
    'EnableMobileCardPaid',
    'MobileCardPrice',
    'EnableIOSInAppPaid',
    'IOSInAppTier',
    'IOSInAppPrice',
    'IOSInAppProceeds',
    'VipNeedBuy',
    'Version',
    'Author',
    'ReleaseYear',
    'CreateDate',
    'LastModifyDate',
    'LikeCount',
    'CommentCount',
    'CommentingOff',
    'IsLock'
];

function getMediaSelectionField(projectionArray, prefix, isNotReplace) {
    var result = mediaSchema.filter(function (n) {
        return projectionArray.indexOf(n) > -1;
    });
    result = result.concat(mediaMustHaveColumn);

    //tạo mảng clone để lưu tên cột trước khi Add Prefix vào
    var cloneResult = result.slice(0);

    if (prefix) {
        result.forEach(function (item, i) { result[i] = prefix + '.' + item; });
    }

    if (isNotReplace) {
    }
    else {
        //duyệt xem nếu là kiểu Datetime thì format lại thành chuỗi yyyy-MM-dd HH:mm:ss.ms
        cloneResult.forEach(function (item, i) {
            //if (item.indexOf('Cover') >= 0 || item.indexOf('Thumbnail') >= 0 || item.indexOf('Link') >= 0) {
            //    result[i] = "'" + CDNUrl + "'" + ' + ' + result[i] + ' as ' + cloneResult[i];
            //}
            //if (item.indexOf('Date') >= 0) {
            //    result[i] = 'convert(nvarchar, ' + result[i] + ', 120) as ' + cloneResult[i];
            //}
            //else if (item == 'ImageOriginal' || item == 'ImageLarge' || item == 'ImageMedium' || item == 'ImageSmall') {
            //    result[i] = "'" + CDNUrl + "'" + ' + ' + result[i] + ' as ' + cloneResult[i];
            //}
            //if (item == 'ItemContent') {

            //}
            if (item == 'IsLock' || item == 'CommentingOff') {
                result[i] = "IsNull(" + result[i] + ", 0) as " + cloneResult[i];
            }
        });
    }
    return result.join(',');
}
function addQueryStringLastModifyDate(val, media) {
    return val + "?" + media.LastModifyDate;
}

function parseMedia(media) {

    if (media.Link) {
        media.Link = storageHelper.fixStorageLink(media.Link, media.StorageProvider);
        media.Link = addQueryStringLastModifyDate(media.Link, media);
    }
    if (media.Cover) {
        media.Cover = storageHelper.fixStorageLink(media.Cover, media.StorageProvider);
        media.Cover = addQueryStringLastModifyDate(media.Cover, media);
    }
    if (media.Thumbnail) {
        media.Thumbnail = storageHelper.fixStorageLink(media.Thumbnail, media.StorageProvider);
        media.Thumbnail = addQueryStringLastModifyDate(media.Thumbnail, media);
    }
    return media;
}
function generateIsLikedQuery(prefix) {
    var query = '';
    query += ' , cast( case when exists(Select 1 From MediaLikes Where MediaId = ' + (prefix ? prefix + '.' : '') + 'MediaId and UserId = @UserId) then 1 else 0 end as bit) as IsLiked ';
    return query;
}
module.exports = {
    MEDIATYPE: MEDIATYPE,
    mediaBasicSchema: mediaBasicSchema,
    mediaSchema: mediaSchema,
    parseMedia: parseMedia,
    getMediaById: function (userId, mediaId, ast, callback) {
        var request = new sql.Request(connection);
        request.input('MediaId', sql.BigInt, mediaId);
        request.input('UserId', sql.BigInt, userId);

        if (!ast) {
            ast = mediaSchema;
        }
        var selectionField = getMediaSelectionField(ast, 'M');
        if (!selectionField) {
            selectionField = '*';
        }

        var query = 'select ' + selectionField;
        query += generateIsLikedQuery('M');
        query += ' From [Medias] M Where M.MediaId = @MediaId';

        request.query(query,
            function (err, result) {
                if (err) {
                    logger.error('getMediaById', err);
                    callback(err, null);
                }
                else {
                    var recordset = result.recordset;
                    if (recordset && recordset.length > 0) {
                        var doc = parseMedia(recordset[0]);

                        callback(null, doc);
                    }
                    else {
                        callback(null, null);
                    }
                }
            });
    },
    getMediaList: function (userId, tags, isFree, pageIndex, itemCount, ast, callback) {
        var request = new sql.Request(connection);

        request.input('UserId', sql.BigInt, userId);

        if (!ast) {
            ast = mediaBasicSchema;
        }
        var selectionField = getMediaSelectionField(ast, 'M');
        if (!selectionField) {
            selectionField = '*';
        }

        if (!itemCount || itemCount == 0) {
            itemCount = defaultRowCount;
        }
        var query = 'Select distinct ' + selectionField;
        query += generateIsLikedQuery('M');
        query += ' From [Medias] M ';

        if (tags && tags.length > 0) {
            query += ' inner join MediaHashtags MH on M.MediaId = MH.MediaId ';
            query += ' inner join MediaTags MT on MH.HashtagId = MT.HashtagId ';
        }

        //var hasWhere = false;
        query += ' Where IsPublish = 1 and (PublishAfter is null or PublishAfter < ' + dateHelper.getUnixTimeStamp() + ') ';

        if (isFree && isFree != -1) {
            query += ' and IsFree = ' + isFree;
            //hasWhere = true;
        }

        if (tags && tags.length > 0) {
            //if (!hasWhere) {
            //    query += ' Where ';
            //}
            //else {
            query += ' and ';
            //}

            var strTags = '';
            for (var i = 0; i < tags.length; i++) {
                strTags += " '" + tags[i] + "'";
                if (i < tags.length - 1) {
                    strTags += ", ";
                }
            }

            query += " MT.Hashtag in (" + strTags + ") ";
        }

        query += ' Order by SortId, MediaId desc ';
        query += ' OFFSET ' + ((pageIndex - 1) * itemCount) + ' ROWS ';
        query += ' FETCH NEXT ' + itemCount + ' ROWS ONLY;';

        logger.debug('getMediaList query', query);

        request.query(query,
            function (err, result) {
                if (err) {
                    logger.error('getMediaList', err);
                    callback(err, null);
                }
                else {
                    var recordset = result.recordset;
                    for (var i = 0; i < recordset.length; i++) {
                        recordset[i] = parseMedia(recordset[i]);
                    }
                    callback(null, recordset);
                }
            });
    },

};