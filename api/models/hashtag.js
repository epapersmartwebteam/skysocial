﻿var sql = require('mssql');

//lấy config chuỗi kết nối
var config = require('../myConfig').dataConfig;
var defaultRowCount = require('../myConfig').defaultRowCount;

var logger = require('../helper/logHelper').logger;

var connection = new sql.ConnectionPool(config);
connection.connect().catch((error) => {
    logger.error('database connection error', error);
});

connection.on('error', function (err) {
    logger.error('database connection error', err);
    connection = new sql.ConnectionPool(config);
    connection.connect();
});


module.exports = {
    searchHashtag: function (userId, hashtag, callback) {
        var request = new sql.Request(connection);

        hashtag = hashtag.replace(/_/g, '[_]');
        request.input('Hashtag', sql.VarChar, hashtag);



        var query = 'Select top ' + defaultRowCount + ' h.Hashtag, (select count(*) From ImageHashtags ih Where ih.HashtagId = h.HashtagId) as PostCount ';
        query += ' From Hashtags h ';
        //if (hashtag.length == 1) {
        //    query += ' Where h.Hashtag = @Hashtag';
        //}
        //else {
            query += " Where h.Hashtag like (@Hashtag + '%') ";
            query += ' Order by len(h.Hashtag) ';
        //}

        request.query(query,
            function (err, result) {
                if (err) {
                    logger.error('searchHashtag', err);
                    return callback(err);
                }
                else {
                    var recordset = result.recordset;
                    return callback(err, recordset);
                }
            });
    }
};