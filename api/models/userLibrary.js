﻿var sql = require('mssql');
var _ = require('lodash');
var resultHelper = require('./result');
var mediaData = require('./hub/media');
//lấy config chuỗi kết nối
var config = require('../myConfig').dataConfig;
var defaultRowCount = require('../myConfig').defaultRowCount;

var CDNUrl = require('../myConfig').CDNUrl;

var logger = require('../helper/logHelper').logger;

var connection = new sql.ConnectionPool(config);
connection.connect().catch((error) => {
    logger.error('database connection error', error);
});

connection.on('error', function (err) {
    logger.error('database connection error', err);
    connection = new sql.ConnectionPool(config);
    connection.connect();
});

module.exports = {
    getUserLibraries: function (userId, isBuy, isFree, lastDate, itemCount, callback) {
        var request = new sql.Request(connection);

        request.input('UserId', sql.BigInt, userId);
        if (!itemCount) {
            itemCount = 100;
        }
        var query = ' Select top ' + itemCount + ' UL.*, ';
        query += ' M.MediaType, M.MediaGUID, M.Title, M.Description, M.Link, M.Cover, M.Thumbnail, M.Author, M.ReleaseYear, ';
        query += ' M.LikeCount, M.CommentCount, M.CommentingOff, M.IsLock, M.LastModifyDate ';
        query += ' From UserLibraries UL inner join Medias M on UL.MediaId = M.MediaId ';
        query += ' Where UL.UserId = @UserId ';

        if (isBuy > -1) {
            request.input('IsBuy', sql.Bit, isBuy);
            query += ' and UL.IsBuy = @IsBuy ';
        }

        if (isFree > -1) {
            request.input('IsFree', sql.Bit, isFree);
            query += ' and UL.IsFree = @IsFree ';
        }

        if (lastDate) {
            request.input('LastDate', sql.BigInt, lastDate);
            query += ' and UL.CreateDate < @LastDate ';
        }
        query += ' Order by UL.CreateDate desc ';
        
        request.query(query, (err, result) => {
            if (err) {
                logger.error('getUserLibraries', err);
                callback(resultHelper.dataError(err));
            }
            else {
                var recordset = result.recordset;
                
                for (var i = 0; i < recordset.length; i++) {
                    recordset[i] = mediaData.parseMedia(recordset[i]);
                }
                callback(resultHelper.success(recordset));
            }
        });
    },
    checkUserCanPlay: function (userId, mediaId, callback) {
        var request = new sql.Request(connection);
        request.input('UserId', sql.BigInt, userId);
        request.input('MediaId', sql.Int, mediaId);

        var query = 'checkMediaCanPlay';
        request.execute(query, (err, result) => {
            if (err) {
                logger.error('checkUserCanPlay', err);
                callback(resultHelper.dataError(err));
            }
            else {
                var recordset = result.recordset;
                var item = recordset ? recordset[0] : null;
                if (item) {
                    if (item.CanPlay === 1) {
                        item.CanPlay = true;
                    }
                    else {
                        item.CanPlay = false;
                    }
                }
                callback(resultHelper.success(item));
            }
        });
    },
    getUserLibrary: function (userId, mediaId, callback) {
        var request = new sql.Request(connection);

        request.input('UserId', sql.BigInt, userId);
        request.input('MediaId', sql.Int, mediaId);

        console.log(userId, mediaId);
        var query = ' Select * From UserLibraries Where UserId = @UserId and MediaId = @MediaId ';

        request.query(query, (err, result) => {
            
            if (err) {
                logger.error('getUserLibrary', err);
                callback(resultHelper.dataError(err));
            }
            else {
                var recordset = result.recordset;
                var item = recordset ? recordset[0] : null;
        
                callback(resultHelper.success(item));
            }
        });
    },

    addUserLibrary: function (userId, mediaId, isFree, isBuy, isView, os, callback) {
        var request = new sql.Request(connection);

        request.input('UserId', sql.BigInt, userId);
        request.input('MediaId', sql.Int, mediaId);
        request.input('IsFree', sql.Bit, isFree);
        request.input('IsBuy', sql.Bit, isBuy);
        request.input('IsFavorite', sql.Bit, 0);
        request.input('IsView', sql.Bit, isView);
        request.input('OS', sql.NVarChar, os);

        var query = 'addUserLibrary';

        request.execute(query, (err, result) => {
            if (err) {
                logger.error('addUserLibrary', err);
                callback(resultHelper.dataError(err));
            }
            else {
                callback(resultHelper.success({}));
            }
        });
    },

    updateUserLibraryView(userId, itemId, callback) {
        var request = new sql.Request(connection);
        request.input('UserId', sql.BigInt, userId);
        request.input('ItemId', sql.BigInt, itemId);

        var query = ' Update UserLibraries ';
        query += ' Set [Views] += 1, LastAccessDate = dbo.UNIX_TIMESTAMP(getUTCDate()) ';
        query += ' Where ItemId = @ItemId and UserId = @UserId';

        request.query(query, (err, result) => {
            if (err) {
                logger.error('updateUserLibraryView', err);
                callback(resultHelper.dataError(err));
            }
            else {
                callback(resultHelper.success({}));
            }
        });
    },

    updateUserLibraryIsFavorite(userId, itemId, isFavorite, os, callback) {
        var request = new sql.Request(connection);
        request.input('UserId', sql.BigInt, userId);
        request.input('ItemId', sql.BigInt, itemId);
        request.input('IsFavorite', sql.Bit, isFavorite);
        request.input('OS', sql.NVarChar, os);

        var query = ' Update UserLibraries ';
        query += ' Set IsFavorite = @IsFavorite, LastUpdateOS = @OS, LastUpdateDate = dbo.UNIX_TIMESTAMP(getUTCDate()) ';
        query += ' Where ItemId = @ItemId and UserId = @UserId ';

        request.query(query, (err, result) => {
            if (err) {
                logger.error('updateUserLibraryIsFavorite', err);
                callback(resultHelper.dataError(err));
            }
            else {
                callback(resultHelper.success({}));
            }
        });
    },

    updateUserLibraryIsArchived(userId, itemId, isArchived, os, callback) {
        var request = new sql.Request(connection);
        request.input('UserId', sql.BigInt, userId);
        request.input('ItemId', sql.BigInt, itemId);
        request.input('IsArchived', sql.Bit, isArchived);
        request.input('OS', sql.NVarChar, os);

        var query = ' Update UserLibraries ';
        query += ' Set IsArchived = @IsArchived, LastUpdateOS = @OS, LastUpdateDate = dbo.UNIX_TIMESTAMP(getUTCDate()) ';
        query += ' Where ItemId = @ItemId  and UserId = @UserId ';

        request.query(query, (err, result) => {
            if (err) {
                logger.error('updateUserLibraryIsArchived', err);
                callback(resultHelper.dataError(err));
            }
            else {
                callback(resultHelper.success({}));
            }
        });
    },

    deleteUserLibrary(userId, itemId, callback) {
        var request = new sql.Request(connection);
        request.input('UserId', sql.BigInt, userId);
        request.input('ItemId', sql.BigInt, itemId);

        var query = 'Delete From UserLibraries Where UserId = @UserId and ItemId = @ItemId';

        request.query(query, (err, result) => {
            if (err) {
                logger.error('deleteUserLibrary', err);
                callback(resultHelper.dataError(err));
            }
            else {
                callback(resultHelper.success({}));
            }
        });
    }
};