﻿var sql = require('mssql');

//lấy config chuỗi kết nối
var config = require('../myConfig').dataConfig;
var defaultRowCount = require('../myConfig').defaultRowCount;

var logger = require('../helper/logHelper').logger;

var connection = new sql.ConnectionPool(config);
connection.connect().catch((error) => {
    logger.error('database connection error', error);
});

connection.on('error', function (err) {
    logger.error('database connection error', err);
    connection = new sql.ConnectionPool(config);
    connection.connect();
});


module.exports = {
    searchUserMention: function (userId, keyword, callback) {
        var request = new sql.Request(connection);

        keyword = keyword.replace(/_/g, '[_]');

        request.input('UserId', sql.BigInt, userId);
        request.input('Keyword', sql.VarChar, keyword);

        request.execute('searchUserForMention',
            function (err, result) {
                if (err) {
                    logger.error('searchUserMention', err);
                    callback(err);
                }
                else {
                    var recordset = result.recordset;
                    callback(null, recordset);
                }
            });
    }
};