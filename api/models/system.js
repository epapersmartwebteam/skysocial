﻿var sql = require('mssql');
var config = require('../myConfig').dataConfig;
var result = require('./result');

var defaultChatRowCount = require('../myConfig').defaultChatRowCount;

var logger = require('../helper/logHelper').logger;

var connection = new sql.ConnectionPool(config);
connection.connect().catch((error) => {
    logger.error('database connection error', error);
});

connection.on('error', function (err) {
    logger.error('database connection error', err);
    connection = new sql.ConnectionPool(config);
    connection.connect();
});


var appId = 1;

module.exports = {
    getOSSetting: function (os, callback) {
        var request = new sql.Request(connection);
        request.input('OS', sql.VarChar, os);
        request.input('AppId', sql.Int, appId);
        console.log(os);
        console.log(appId);
        var query = 'Select OS, Version, ForceUpdate, ForceUpdateVersion, UpdateMessage, AppUrl ';
        query += ' From OSSettings ';
        query += ' Where OS = @OS and AppId = @AppId ';

        request.query(query, function (err, result) {

            if (!err) {
                var recordset = result.recordset;
                callback(err, recordset[0]);
            }
            else {
                logger.error('getOSSetting', err);
                callback(err);
            }
        });
    }
};