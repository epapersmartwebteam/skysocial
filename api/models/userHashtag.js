﻿var sql = require('mssql');

var _ = require('lodash');

var logger = require('../helper/logHelper').logger;

//lấy config chuỗi kết nối
var myConfig = require('../myConfig');
var config = myConfig.dataConfig;
var defaultRowCount = myConfig.defaultRowCount;

var dateHelper = require('../helper/dateHelper');

var { convertVipLevel }= require('./user');

var connection = new sql.ConnectionPool(config);
connection.connect().catch((error) => {
    logger.error('database connection error', error);
});

connection.on('error', function (err) {
    logger.error('database connection error', err);
    connection = new sql.ConnectionPool(config);
    connection.connect();
});

function parseHashTagInfo(doc) {
    if (doc) {
        if (doc.HashtagId) {
            doc.HashtagId = parseInt(doc.HashtagId);
        }
        if (doc.ImageUrl && !doc.ImageUrl.startsWith('http')) {
            doc.ImageUrl = myConfig.CDNUrl + doc.ImageUrl;
        }
    }
    return doc;
}

function parseGroupInfo(doc) {
    var result = {};
    if (doc) {
        result = {
            UserId: doc.UserId,
            ItemId: doc.ItemId,
            HashtagId: doc.HashtagId,
            Hashtag: doc.Hashtag,
            Name: doc.Name,
            NameEN: doc.NameEN,
            ImageUrl: myConfig.CDNUrl + doc.ImageUrl,
            Order: doc.Order,
            CategoryId: doc.CategoryId,
            JoinDate: doc.Createdate,
            MemberCount: doc.MemberCount
        };
    }
    return result;
}

module.exports = {
    followHashtag: function followHashtag(userId, hashtags, isFavorite, callback) {
        var createDate = dateHelper.getUnixTimeStamp();
        const table = new sql.Table('UserHashtags');
        table.create = true;
        table.columns.add('UserId', sql.BigInt, {
            nullable: false, primary: true
        });
        table.columns.add('HashtagId', sql.BigInt, {
            nullable: false, primary: true
        });
        table.columns.add('Hashtag', sql.NVarChar, {
            nullable: true
        });
        table.columns.add('CreateDate', sql.BigInt, {
            nullable: true
        });
        table.columns.add('IsFavorite', sql.Bit, {
            nullable: true
        });

        hashtags.forEach(function (item) {
            table.rows.add(userId, parseInt(item.HashtagId), item.Hashtag, createDate, isFavorite);
        });

        var request = new sql.Request(connection);
        request.bulk(table, (err, result) => {
            if (err) {
                logger.error('followHashtag', err);
            }
            callback(err);
        });
    },

    unfollowHashtag: (userId, hashtags, callback) => {
        var request = new sql.Request(connection);
        var query = 'Delete From UserHashtags ';
        query += ' Where UserId = @UserId ';
        if (hashtags.length > 0) {
            var hashtagList = _.map(hashtags, (item) => {
                return item.HashtagId;
            }).toString();
            query += ' and HashtagId in (' + hashtagList + ') ';
        }

        request.input('UserId', sql.BigInt, userId);

        request.query(query, (err, result) => {
            if (err) {
                logger.error('unfollowHashtag', err);
            }
            callback(err);
        });
    },

    getSystemHashtags: (userId, categoryId, callback) => {
        var request = new sql.Request(connection);
        request.input('UserId', sql.BigInt, userId);
        var query = '';
        query += 'Select *, ';
        query += ' cast(case when (select HashtagId From UserSystemHashtags Where UserId = @UserId and HashtagId = SystemHashtags.HashtagId)  is NULL then 0 else 1 end as bit) as IsSelected ';
        query += ' From SystemHashtags ';

        query += ' Where IsShow = 1 ';

        if (categoryId) {
            request.input('CategoryId', sql.BigInt, categoryId);
            query += ' and CategoryId = @CategoryId';
        }

        if (categoryId == myConfig.suggestConfigs.categoryForSuggestGroup) {
            query += ' and HashtagId in ';
            query += ' (Select SRH.HashtagId  ';
            query += ' From SystemRelativeHashtags SRH inner join SystemHashtags SH on SRH.HashtagId = SH.HashtagId ';
            query += ' Where CategoryId = @CategoryId and RelateHashtagId in ';
            query += '(Select HashtagId From UserSystemHashtags Where UserId = @UserId and CategoryId in (' + myConfig.suggestConfigs.categoryForRegion + ',' + myConfig.suggestConfigs.categoryForZodiac + ')) ';
            query += ' Group by SRH.HashtagId ';
            query += ' Having Count(SRH.HashtagId) >= ' + myConfig.suggestConfigs.havingCountForSuggestGroup;
            query += ') ';
        }
        query += ' Order By [Order], [Name] ';
        
        request.query(query, (err, result) => {
            if (err) {
                logger.error('getSystemHashtags', err);
                callback(err);
            }
            else {
                var docs = result.recordset;
                
                if (docs) {
                    for (var i = 0; i < docs.length; i++) {
                        docs[i] = parseHashTagInfo(docs[i]);
                    }
                }
                callback(null, docs);
            }
        });
    },

    getSystemHashtagCategorys: function getSystemHashtagCategorys(callback) {
        var request = new sql.Request(connection);
        var query = 'Select * From SystemHashtagCategory Where IsShow = 1 Order By [Order]';

        request.query(query, (err, result) => {
            if (err) {
                logger.error('getSystemHashtagCategorys', err);
                callback(err);
            }
            else {
                var docs = result.recordset;
                callback(null, docs);
            }
        });
    },

    getSystemHashtagCategory: function getSystemHashtagCategory(categoryId, callback) {
        var request = new sql.Request(connection);
        var query = 'Select * From SystemHashtagCategory Where CategoryId = @CategoryId';

        request.input('CategoryId', sql.Int, categoryId);
        request.query(query, (err, result) => {
            if (err) {
                logger.error('getSystemHashtagCategory', err);
                callback(err);
            }
            else {
                var docs = result.recordset;
                var doc;
                if (docs && docs.length > 0) {
                    doc = docs[0];
                }
                callback(null, doc);
            }
        });
    },

    removeUserSystemHashtags: function removeUserSystemHashtags(userId, categoryId, callback) {
        var request = new sql.Request(connection);

        request.input('UserId', sql.BigInt, userId);
        request.input('CategoryId', sql.Int, categoryId);

        var query = 'Delete From UserSystemHashtags Where UserId = @UserId and CategoryId = @CategoryId';

        request.query(query, (err, result) => {
            if (err) {
                logger.error('removeUserSystemHashtags', err);
            }
            callback(err);
        });
    },

    setUserSystemHashtags: function setUserSystemHashtags(userId, categoryId, hashtags, callback) {
        var that = this;
        this.removeUserSystemHashtags(userId, categoryId, (errX) => {
            if (!errX) {

                that.getSystemHashtagCategory(categoryId, (errC, category) => {
                    if (errC) {
                        callback(errC);
                    }
                    else {

                        var forSuggest = category.ForSuggest ? false : true;

                        var request = new sql.Request(connection);

                        var createDate = dateHelper.getUnixTimeStamp();
                        const table = new sql.Table('UserSystemHashtags');
                        table.create = true;
                        table.columns.add('UserId', sql.BigInt, {
                            nullable: false, primary: true
                        });
                        table.columns.add('HashtagId', sql.BigInt, {
                            nullable: false, primary: true
                        });
                        table.columns.add('Hashtag', sql.NVarChar, {
                            nullable: true
                        });
                        table.columns.add('CategoryId', sql.Int, {
                            nullable: true
                        });
                        table.columns.add('CreateDate', sql.BigInt, {
                            nullable: true
                        });
                        table.columns.add('ForSuggest', sql.Bit, {
                            nullable: true
                        });

                        hashtags.forEach(function (item) {
                            table.rows.add(userId, parseInt(item.HashtagId), item.Hashtag, categoryId, createDate, forSuggest);
                        });

                        request.bulk(table, (err, result) => {
                            if (err) {
                                logger.error('setUserSystemHashtags', err);
                            }
                            callback(err);
                        });
                    }
                });
            }
            else {
                callback(errX);
            }
        });
    },

    getUserGroupList: function getUserGroupList(userId, callback) {
        var request = new sql.Request(connection);

        var categoryId = myConfig.suggestConfigs.categoryForSuggestGroup;
        request.input('UserId', sql.BigInt, userId);
        request.input('CategoryId', sql.Int, categoryId);

        var query = 'Select UserId, SH.ItemId, SH.HashtagId, SH.Hashtag, SH.[Name], SH.NameEN, ';
        query += ' SH.ImageUrl, SH.[Order], USH.CategoryId, USH.CreateDate, ';
        query += ' (Select count(*) From UserSystemHashtags Where HashtagId = USH.HashtagId) as MemberCount ';
        query += ' From UserSystemHashtags USH  inner join SystemHashtags SH on USH.HashtagId = SH.HashtagId ';
        query += ' Where USH.CategoryId = @CategoryId and USH.UserId = @UserId and SH.IsShow = 1 ';

        request.query(query, (err, result) => {
            if (err) {
                logger.error('getUserGroupList', err);
                callback(err);
            }
            else {
                var recordset = result.recordset;
                var lst = [];
                if (recordset && recordset.length) {
                    _.each(recordset, (item) => {
                        lst.push(parseGroupInfo(item));
                    });
                }
                callback(null, lst);
            }
        });
    },

    getUserGroupInfo: function getUserGroupInfo(userId, itemId, callback) {
        var request = new sql.Request(connection);
        
        request.input('UserId', sql.BigInt, userId);
        request.input('ItemId', sql.Int, itemId);

        var query = 'Select UserId, SH.ItemId, SH.HashtagId, SH.Hashtag, SH.[Name], SH.NameEN, ';
        query += ' SH.ImageUrl, SH.[Order], USH.CategoryId, USH.CreateDate, ';
        query += ' (Select count(*) From UserSystemHashtags Where HashtagId = USH.HashtagId) as MemberCount ';
        query += ' From UserSystemHashtags USH  inner join SystemHashtags SH on USH.HashtagId = SH.HashtagId ';
        query += ' Where SH.ItemId = @ItemId and USH.UserId = @UserId and SH.IsShow = 1 ';

        request.query(query, (err, result) => {
            if (err) {
                logger.error('getUserGroupInfo', err);
                callback(err);
            }
            else {
                var recordset = result.recordset;
                var m = null;
                if (recordset && recordset.length) {
                    m = parseGroupInfo(recordset[0]);
                }
                callback(null, m);
            }
        });
    },

    getUserGroupInfoByHashtagId: function getUserGroupInfoByHashtagId(userId, hashtagId, callback) {
        var request = new sql.Request(connection);
                
        request.input('UserId', sql.BigInt, userId);
        request.input('HashtagId', sql.Int, hashtagId);

        var query = 'Select UserId, SH.ItemId, SH.HashtagId, SH.Hashtag, SH.[Name], SH.NameEN, ';
        query += ' SH.ImageUrl, SH.[Order], USH.CategoryId, USH.CreateDate, ';
        query += ' (Select count(*) From UserSystemHashtags Where HashtagId = USH.HashtagId) as MemberCount ';
        query += ' From UserSystemHashtags USH  inner join SystemHashtags SH on USH.HashtagId = SH.HashtagId ';
        query += ' Where USH.HashtagId = @HashtagId and USH.UserId = @UserId and SH.IsShow = 1 ';

        request.query(query, (err, result) => {
            if (err) {
                logger.error('getUserGroupInfoByHashtagId', err);
                callback(err);
            }
            else {
                var recordset = result.recordset;
                var m = null;
                if (recordset && recordset.length) {
                    m = parseGroupInfo(recordset[0]);
                }
                callback(null, m);
            }
        });
    },

    getUserGroupMemberListByHashtagId: function getUserGroupMemberByHashtagId(userId, hashtagId, lastJoinDate, topN, callback) {
        var request = new sql.Request(connection);
        if (!topN) {
            topN = defaultRowCount;
        }

        request.input('UserId', sql.BigInt, userId);
        request.input('HashtagId', sql.Int, hashtagId);
        request.input('TopN', sql.Int, topN);

        var query = 'Select top (@TopN) U.UserId, U.UserName, U.AvatarSmall, U.FullName, U.IsVip, U.VipLevelId, USH.CreateDate JoinDate, ';
        query += ' (Case When (Exists(Select * From UserFollowers Where FromUserId = @UserId and ToUserId = U.UserId)) then 1 else 0 end) as IsFollowed ';
        query += ' From UserSystemHashtags USH inner join Users U on USH.UserId = U.UserId ';
        query += ' Where USH.HashtagId =  @HashtagId ';
        console.log(lastJoinDate);
        if (lastJoinDate) {
            request.input('LastJoinDate', sql.BigInt, lastJoinDate);
            query += ' and USH.CreateDate < @LastJoinDate ';
        }
        query += ' Order by USH.CreateDate desc ';

        request.query(query, (err, result) => {
            if (err) {
                logger.error('getUserGroupMemberListByHashtagId', err);
                callback(err);
            }
            else {
                var lst = result.recordset;
                
                if (lst && lst.length > 0) {
                    for (var i = 0; i < lst.length; i++) {
                        lst[i].AvatarSmall = myConfig.CDNUrl + (lst[i].AvatarSmall ? lst[i].AvatarSmall : myConfig.NoAvatarLink);
                        lst[i].VipLevel = convertVipLevel(lst[i].VipLevelId, lst[i].IsVip);
                    }
                }
                callback(null, lst);
            }
        });
    }
};