﻿var sql = require('mssql');

//lấy config chuỗi kết nối
var config = require('../myConfig').dataConfig;
var defaultRowCount = require('../myConfig').defaultRowCount;

var logger = require('../helper/logHelper').logger;

var connection = new sql.ConnectionPool(config);
connection.connect().catch((error) => {
    logger.error('database connection error', error);
});

connection.on('error', function (err) {
    logger.error('database connection error', err);
    connection = new sql.ConnectionPool(config);
    connection.connect();
});


module.exports = {
    addUserReport: function (userId, reportTypeId, targetUserId, comment, os, callback) {
        var request = new sql.Request(connection);

        request.input('ReportUserId', sql.BigInt, userId);
        request.input('ReportTypeId', sql.Int, reportTypeId);
        request.input('UserId', sql.BigInt, targetUserId);
        request.input('Comment', sql.NVarChar, comment);
        request.input('CreateOS', sql.VarChar, os);

        var query = ' Insert Into UserReports(ReportTypeId, UserId, ReportUserId, Comment, CreateOS, IsChecked, IsCorrected, IsSolved) ';
        query += ' Values(@ReportTypeId, @UserId, @ReportUserId, @Comment, @CreateOS, 0, 0, 0); ';
        query += ' Select SCOPE_IDENTITY() as ReportId;';
        request.query(query,
            function (err, result) {
                if (err) {
                    logger.error('addUserReport', err);
                    callback(err);
                }
                else {
                    var recordset = result.recordset;
                    var reportId;
                    if (recordset) {
                        reportId = recordset[0].ReportId;
                    }
                    callback(err, reportId);
                }
            });
    },
    approveUserReport: function (reportId, isChecked, isSolved, isCorrected, userId, callback) {
        var request = new sql.Request(connection);

        request.input('ReportId', sql.BigInt, reportId);
        request.input('IsChecked', sql.Bit, isChecked);
        request.input('IsSolved', sql.Bit, isSolved);
        request.input('IsCorrected', sql.Bit, isCorrected);
        request.input('UserId', sql.BigInt, userId);

        var query = 'Update UserReports ';
        query += ' Set IsChecked = @IsChecked, IsSolved = @IsSolved, IsCorrected = @IsCorrected, ';
        query += ' ApproveUserId = @UserId, ApproveDate = dbo.UNIX_TIMESTAMP(getUTCDate()) ';
        query += ' Where ReportId = @ReportId ';

        request.query(query, function (err, result) {
            if (err) {
                logger.error('approveUserReport', err);
                callback(err);
            }
            else {
                callback(err, result);
            }
        });
    }
};