﻿var sql = require('mssql');
var config = require('../myConfig').dataConfig;

var _ = require('lodash');
var CDNUrl = require('../myConfig').CDNUrl;
var myConfig = require('../myConfig');

var resultHelper = require('./result');
var utilityHelper = require('../helper/utility');

var errorCode = require('../errorCode');
var dateHelper = require('../helper/dateHelper');

var logger = require('../helper/logHelper').logger;

var connection = new sql.ConnectionPool(config);
connection.connect().catch((error) => {
    logger.error('database connection error', error);
});

connection.on('error', function (err) {
    logger.error('database connection error', err);
    connection = new sql.ConnectionPool(config);
    connection.connect();
});

function parseItemContent(imageItem) {
    if (imageItem && imageItem.ItemContent) {
        imageItem.ItemContent = JSON.parse(imageItem.ItemContent);

        _.each(imageItem.ItemContent, (item, index) => {
            if (item.Large) {
                imageItem.ItemContent[index].Large.Link = CDNUrl + item.Large.Link;
            }
            if (item.Medium) {
                imageItem.ItemContent[index].Medium.Link = CDNUrl + item.Medium.Link;
            }
            if (item.Small) {
                imageItem.ItemContent[index].Small.Link = CDNUrl + item.Small.Link;
            }
        });
        //imageItem.ItemContent = JSON.stringify(imageItem.ItemContent);
    }

    return imageItem;
}

var ENGAGETYPE = {
    VIEW: 'VIEW',
    LINKCLICK: 'LINKCLICK',
    LIKED: 'LIKED',
    COMMENT: 'COMMENT',
    SHARED: 'SHARED',
    SAVED: 'SAVED'
};

module.exports = {
    ENGAGETYPE: ENGAGETYPE,
    addPostEngagements: function (engagements, callback) {
        var request = new sql.Request(connection);

        const table = new sql.Table('PostEngagements');
        table.create = true;
        
        table.columns.add('UserId', sql.BigInt);
        table.columns.add('PostId', sql.BigInt);
        table.columns.add('EngageType', sql.VarChar(20));
        table.columns.add('CreateDate', sql.BigInt);
        _.each(engagements, (item, idx) => {
            table.rows.add(item.UserId, item.PostId, item.EngageType, item.CreateDate);
        });

        request.bulk(table, (err, result) => {
            // ... error checks
            if (err) {
                logger.error('addPostEngagements', err);
                callback(resultHelper.dataError(err));
            }
            else {
                callback(resultHelper.success());
            }
        });
    },
    addPostEngagement: function (userId, postId, engageType, callback) {
        var request = new sql.Request(connection);

        request.input('UserId', sql.BigInt, userId);
        request.input('PostId', sql.BigInt, postId);
        request.input('EngageType', sql.NVarChar(20), engageType);

        var query = ' INSERT INTO PostEngagements(UserId, PostId, EngageType) ';
        query += ' VALUES(@UserId, @PostId, @EngageType) ';

        request.query(query, (err, result) => {
            if (err) {
                logger.error('addPostEngagement', err);
                callback(resultHelper.dataError(err));
            }
            else {
                callback(resultHelper.success());
            }
        });
    },
    getPostInsight: function (userId, lastPostId, itemCount, callback) {
        var request = new sql.Request(connection);

        request.input('UserId', sql.BigInt, userId);
        request.input('LastPostId', sql.BigInt, lastPostId);
        request.input('ItemCount', sql.Int, itemCount);        

        var query = ' Select top (@ItemCount) II.ImageItemId as PostId, II.ItemContent, II.CreateDate as PublishDate, ';
        query += " Sum(Case when EngageType = 'LIKED' then 1 else 0 end) as Liked, ";
        query += " Sum(Case when EngageType = 'COMMENT' then 1 else 0 end) as Comment, ";
        query += " Sum(Case when EngageType = 'SAVED' then 1 else 0 end) as Saved, ";
        query += " Sum(Case when EngageType = 'SHARED' then 1 else 0 end) as Shared, ";
        query += " Sum(Case when EngageType = 'LINKCLICK' then 1 else 0 end) as LinkClick, ";
        query += " Sum(Case when EngageType = 'VIEW' then 1 else 0 end) as [View], ";
        query += " (Select count(distinct UserId) ";
        query += " From PostEngagements ";
        query += " Where PostId = ImageItemId ";
        query += " and EngageType = 'VIEW') as Reach ";

        query += ' From ImageItems II inner join PostEngagements PE on II.ImageItemId = PE.PostId ';
        query += ' Where II.UserId = @UserId and (II.IsDelete is null or II.IsDelete = 0) ';

        if (lastPostId) {
            query += ' and II.ImageItemId < @LastPostId ';
        }
        query += ' Group by II.ImageItemId, II.ItemContent, II.CreateDate ';
        query += ' Order by II.ImageItemId desc ';

        //console.log(query);

        request.query(query, (err, result) => {
            if (err) {
                logger.error('getPostInsight', err);
                callback(resultHelper.dataError(err));
            }
            else {
                var recordset = result.recordset;
                _.each(recordset, (record, idx) => {
                    recordset[idx] = parseItemContent(record);
                });
                callback(resultHelper.success(recordset));
            }
        });
    },
    getTotalInsightByDate: function (userId, fromDate, toDate, callback) {
        var request = new sql.Request(connection);

        request.input('UserId', sql.BigInt, userId);
        request.input('FromDate', sql.BigInt, fromDate);
        request.input('ToDate', sql.BigInt, toDate);

        var query = " Select (Select count(ImageItemId) From ImageItems Where UserId = @UserId and CreateDate >= @FromDate and CreateDate <= @ToDate) as PostCount,  ";
        query += " Sum(Liked) as Liked, Sum(Comment) as Comment, Sum(Saved) as Saved, ";
        query += " Sum(Shared) as Shared, Sum(LinkClick) as LinkClick, Sum([View]) as [View], Sum(Reach) as Reach ";
        query += " From ( ";
        query += " Select II.ImageItemId, Sum(Case when EngageType = 'LIKED' then 1 else 0 end) as Liked, ";
        query += " Sum(Case when EngageType = 'COMMENT' then 1 else 0 end) as Comment, ";
        query += " Sum(Case when EngageType = 'SAVED' then 1 else 0 end) as Saved, ";
        query += " Sum(Case when EngageType = 'SHARED' then 1 else 0 end) as Shared, ";
        query += " Sum(Case when EngageType = 'LINKCLICK' then 1 else 0 end) as LinkClick, ";
        query += " Sum(Case when EngageType = 'VIEW' then 1 else 0 end) as [View], ";
        query += " (Select count(distinct UserId)  ";
        query += " From PostEngagements ";
        query += " Where PostId = ImageItemId and EngageType = 'VIEW' ";
        query += " and CreateDate >= @FromDate and CreateDate <= @ToDate) as Reach ";
        query += " From ImageItems II left join PostEngagements PE on II.ImageItemId = PE.PostId ";
        query += " Where (II.IsDelete is null or II.IsDelete = 0) and II.UserId = @UserId and PE.CreateDate >= @FromDate and PE.CreateDate <= @ToDate ";
        query += " Group By II.ImageItemId) as x ";
                

        request.query(query, (err, result) => {
            if (err) {
                logger.error('getTotalInsightByDate', err);
                callback(resultHelper.dataError(err));
            }
            else {
                var recordset = result.recordset;
                var item;
                if (recordset) {
                    item = recordset[0];
                }
                callback(resultHelper.success(item));
            }
        });        
    }
};