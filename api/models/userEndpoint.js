﻿var sql = require('mssql');

//lấy config chuỗi kết nối
var config = require('../myConfig').dataConfig;
var defaultRowCount = require('../myConfig').defaultRowCount;

var dbUser = require('./user');

var logger = require('../helper/logHelper').logger;

var errorCode = require('../errorCode');

var connection = new sql.ConnectionPool(config);
connection.connect().catch((error) => {
    logger.error('database connection error', error);
});

connection.on('error', function (err) {
    logger.error('database connection error', err);
    connection = new sql.ConnectionPool(config);
    connection.connect();
});

module.exports = {

    //kiểm tra xem endpoint tồn tại chưa
    checkUserEndpointExists: function (userId, os, deviceId, oneSignalId, callback) {
        var request = new sql.Request(connection);

        request.input('UserId', sql.BigInt, userId);
        request.input('OS', sql.VarChar, os);
        request.input('DeviceId', sql.VarChar, deviceId);
        request.input('OneSignalId', sql.VarChar, oneSignalId);

        var query = 'if(exists(Select * From UserDevices Where UserId = @UserId and OS = @OS and DeviceId = @DeviceId and OneSignalId = @OneSignalId)) ';
        query += ' Select 1 as Existed ';
        query += ' else ';
        query += ' Select 0 as Existed ';

        request.query(query,
            function (err, result) {
                if (err) {
                    logger.error('checkUserEndpointExists', err);
                    callback(err);
                }
                else {
                    var recordset = result.recordset;
                    callback(null, recordset[0].Existed);
                }
            });
    },

    //cập nhật user endpoint
    addUserEndpoint: function (userId, os, deviceId, oneSignalAppId, oneSignalId, languageCode, callback) {
        var request = new sql.Request(connection);

        request.input('UserId', sql.BigInt, userId);
        request.input('OS', sql.VarChar, os);
        request.input('DeviceId', sql.VarChar, deviceId);
        request.input('OneSignalAppId', sql.VarChar, oneSignalAppId);
        request.input('OneSignalId', sql.VarChar, oneSignalId);
        request.input('LanguageCode', sql.VarChar, languageCode);

        var requestCheck = new sql.Request(connection);
        requestCheck.input('DeviceId', sql.VarChar, deviceId);
        var checkQuery = 'if(exists(Select * From DeviceBlacklists Where DeviceId = @DeviceId)) ';
        checkQuery += ' Select 1 as IsExists ';
        checkQuery += ' else ';
        checkQuery += ' Select 0 as IsExists ';

        requestCheck.query(checkQuery, function (err, result) {
            if (!err) {
                var recordset = result.recordset;
                //nằm trong blacklist thì khóa tài khoản user
                if (recordset[0].IsExists == 1) {
                    dbUser.updateUserIsBanned(userId, 1, 0, function (err, recordset) {
                        callback(result.create(errorCode.notPermission, "Banned"));
                    });
                }
                else {
                    var query = 'Delete From UserDevices Where DeviceId = @DeviceId and UserId = @UserId; ';
                    query += 'Insert into UserDevices(UserId, OS, DeviceId, OneSignalAppId, OneSignalId, ErrorTime, LanguageCode) ';
                    query += ' Values(@UserId, @OS, @DeviceId, @OneSignalAppId, @OneSignalId, 0, @LanguageCode) ';

                    request.query(query,
                        function (err, resultX) {
                            var recordsetX = null;
                            if (err) {
                                logger.error('addUserEndpoint', err);
                            }
                            else {
                                recordsetX = resultX.recordset;
                            }
                            
                            callback(err, recordsetX);
                        });
                }
            }
            else {
                logger.error('checkUserEndpointExists', err);
                callback(err);
            }
        });
    },

    //cập nhật ngôn ngữ của các endpoint của 1 device khi user đổi ngôn ngữ hiển thị trên app
    updateDeviceUserEnpointLanguage: function (userId, deviceId, languageCode, callback) {
        var request = new sql.Request(connection);
        request.input('UserId', sql.BigInt, userId);
        request.input('DeviceId', sql.VarChar, deviceId);
        request.input('LanguageCode', sql.VarChar, languageCode);

        var query = 'Update UserDevices ';
        query += ' Set LanguageCode = @LanguageCode ';
        query += ' Where DeviceId = @DeviceId and UserId = @UserId ';

        request.query(query,
            function (err, result) {
                var recordset = null;
                if (err) {
                    logger.error('updateDeviceUserEnpointLanguage', err);
                }
                else {
                    recordset = result.recordset;
                }                
                callback(err, recordset);
            });
    },

    deleteOtherUserEndpoint: function (userId, deviceId, oneSignalId, callback) {
        var request = new sql.Request(connection);

        request.input('UserId', sql.BigInt, userId);
        request.input('DeviceId', sql.VarChar, deviceId);
        request.input('OneSignalId', sql.VarChar, oneSignalId);

        var query = 'Delete From UserDevices Where DeviceId = @DeviceId and UserId != @UserId; ';
        query += 'Delete From UserDevices Where OneSignalId = @OneSignalId and UserId != @UserId; ';
        
        request.query(query,
            function (err, result) {
                var recordset = null;
                if (err) {
                    logger.error('deleteOtherUserEndpoint', err);
                }
                else {
                    recordset = result.recordset;
                }
                callback(err, recordset);
            });
    },

    //xóa endpoint
    deleteUserEndpoint: function (userId, os, deviceId, callback) {
        var request = new sql.Request(connection);

        request.input('UserId', sql.BigInt, userId);
        request.input('OS', sql.VarChar, os);
        request.input('DeviceId', sql.VarChar, deviceId);

        var query = 'Delete From UserDevices Where DeviceId = @DeviceId and UserId = @UserId and OS = @OS; ';

        request.query(query,
            function (err, result) {
                var recordset = null;
                if (err) {
                    logger.error('deleteUserEndpoint', err);
                }
                else {
                    recordset = result.recordset;
                }
                callback(err, recordset);
            });
    },

    //lấy danh sách endpoint của user
    getUserEndpoint: function (userId, callback) {
        var request = new sql.Request(connection);

        request.input('UserId', sql.BigInt, userId);

        var query = 'Select * From UserDevices Where UserId = @UserId';

        request.query(query,
            function (err, result) {
                var recordset = null;
                if (err) {
                    logger.error('getUserEndpoint', err);
                }
                else {
                    recordset = result.recordset;
                }
                callback(err, recordset);
            });
    },

    addUserDevicesToBlackList: function (userId, callback) {
        var request = new sql.Request(connection);

        request.input('UserId', sql.BigInt, userId);

        var query = 'Insert into DeviceBlacklists(DeviceId, UserId) ';
        query += ' Select distinct DeviceId, UserId';
        query += ' From UserDevices ';
        query += ' Where UserId = @UserId';

        request.query(query,
            function (err, result) {
                var recordset = null;
                if (err) {
                    logger.error('addUserDevicesToBlackList', err);
                }
                else {
                    recordset = result.recordset;
                }
                callback(err, recordset);
            });
    }
};