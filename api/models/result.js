﻿var errorCode = require('../errorCode');
module.exports = {
    create: function (code, mess, data, sessionId) {
        if (data == undefined) {
            data = {};
        }

        return {
            ErrorCode: code,
            Message: mess,
            Data: data,
            SessionId: sessionId
        };
    },
    parameterError: function (err) {
        return {
            ErrorCode: errorCode.parameterError,
            Message: err
        };
    },
    success: function (data) {
        //if (data == null || data == undefined) {
        //    data = {};
        //}
        return {
            ErrorCode: errorCode.success,
            Message: '',
            Data: data
        };
    },
    notExists: function (err) {
        return {
            ErrorCode: errorCode.dataNotExisted,
            Message: err,
            Data: {}
        };
    },
    exists: function (err) {
        return {
            ErrorCode: errorCode.dataExisted,
            Message: err
        };
    },
    dataError: function (err) {
        return {
            ErrorCode: errorCode.dataError,
            Message: err,
            Data: {}
        };
    },
    notAuthorize: function (err) {
        return {
            ErrorCode: errorCode.notAuthorize,
            Message: err,
            Data: {}
        };
    },
    notPermission: function (err) {
        return {
            ErrorCode: errorCode.notPermission,
            Message: err,
            Data: {}
        };
    },
    serviceError: function (err) {
        return {
            ErrorCode: errorCode.serviceError,
            Message: err,
            Data: {}
        };
    }
};