﻿var _ = require('lodash');
var sql = require('mssql');
//lấy config chuỗi kết nối
var myConfig = require('../myConfig');
var resultHelper = require('./result');
var utilityHelper = require('../helper/utility');

var errorCode = require('../errorCode');
var dateHelper = require('../helper/dateHelper');

var redisCacheHelper = require('./redisCacheHelper');

var logger = require('../helper/logHelper').logger;

var connection = new sql.ConnectionPool(myConfig.dataConfig);
connection.connect().catch((error) => {
    logger.error('database connection error', error);
});

connection.on('error', function (err) {
    logger.error('database connection error', err);
    connection = new sql.ConnectionPool(myConfig.dataConfig);
    connection.connect();
});

function getFeatureFeeadNewPostCount(lastSyncDate, callback) {
    var request = new sql.Request(connection);
    request.input('LastSyncDate', sql.BigInt, lastSyncDate);
    var query = 'Select Count(1) as NewPostCount ';
    query += ' From ImageItems ';
    query += ' Where UserId in (Select UserId From FeatureUsers) and CreateDate > @LastSyncDate ';
    request.query(query, (err, result) => {
        if (err) {
            logger.error('getFeatureFeeadNewPostCount', err);
            callback(resultHelper.dataError(err));
        }
        else {
            var count = 0;
            if (result && result.recordset && result.recordset.length > 0) {
                var recordset = result.recordset;
                count = parseInt(recordset[0].NewPostCount);
            }
            callback(resultHelper.success(count));
        }
    });
}
function getFeatureFeedUpdatePostId(lastSyncDate, callback) {
    var request = new sql.Request(connection);
    request.input('LastSyncDate', sql.BigInt, lastSyncDate);

    var query = 'Select ImageItemId ';
    query += ' From ImageItems ';
    query += ' Where UserId in (Select UserId From FeatureUsers) and LastModifyDate != CreateDate and LastModifyDate > @LastSyncDate ';
    request.query(query, (err, result) => {
        if (err) {
            logger.error('getFeatureFeedUpdatePostId', err);
            callback(resultHelper.dataError(err));
        }
        else {
            var recordset = result.recordset;
            var lst = [];
            _.each(recordset, (item) => {
                lst.push(parseInt(item.ImageItemId));
            });
            callback(resultHelper.success(lst));
        }
    });
}
function getFeatureFeedNewActivityPostId(lastSyncDate, callback) {
    var request = new sql.Request(connection);

    var query = 'getFeatureFeedNewActivityPostId';

    request.input('LastSyncDate', sql.BigInt, lastSyncDate);

    request.execute(query, (err, result) => {
        if (err) {
            logger.error('getFeatureFeedNewActivityPostId', err);
            callback(resultHelper.dataError(err));
        }
        else {
            var recordset = result.recordset;
            var lst = [];
            _.each(recordset, (item) => {
                lst.push(parseInt(item.ImageItemId));
            });
            callback(resultHelper.success(lst));
        }
    });
}
function getFeatureFeedHasDeletePostId(lastSyncDate, callback) {
    var request = new sql.Request(connection);

    request.input('LastSyncDate', sql.BigInt, lastSyncDate);

    var query = 'Select ImageItemId ';
    query += ' From ImageItems ';
    query += ' Where UserId in (Select UserId From FeatureUsers) and IsDelete = 1 and DeleteDate > @LastSyncDate ';

    request.query(query, (err, result) => {
        if (err) {
            logger.error('getFeatureFeedHasDeletePostId', err);
            callback(resultHelper.dataError(err));
        }
        else {
            var recordset = result.recordset;
            var lst = [];
            _.each(recordset, (item) => {
                lst.push(parseInt(item.ImageItemId));
            });
            callback(resultHelper.success(lst));
        }
    });
}

function getFeatureFeedPostActivityCount(lastItemId, count, callback) {
    var request = new sql.Request(connection);
    request.input('TopN', sql.Int, count);    

    //bỏ share đi vì app ko gọi update
    var query = 'Select top (@TopN) ImageItemId, UserId, LikeCount, CommentCount ';
    query += ' From ImageItems ';
    query += ' Where IsDelete != 1 and UserId in (Select UserId From FeatureUsers) ';
    if (lastItemId) {
        request.input('LastItemId', sql.BigInt, lastItemId);
        query += ' and ImageItemId < @LastItemId';
    }
    query += ' Order by ImageItemId desc ';
    request.query(query, (err, result) => {
        if (err) {
            logger.error('getFeatureFeedHasDeletePostId', err);
            callback(resultHelper.dataError(err));
        }
        else {
            var lst = result.recordset;
            if (lst && lst.length > 0) {
                for (var i = 0; i < lst.length; i++) {
                    lst[i].UserId = parseInt(lst[i].UserId);
                    lst[i].ImageItemId = parseInt(lst[i].ImageItemId);
                }
            }
            callback(resultHelper.success(lst));
        }
    });
}

module.exports = {
    featureTab: {
        getNewPostCount: getFeatureFeeadNewPostCount,
        getHasUpdatePostId: getFeatureFeedUpdatePostId,
        getHasNewActivityPostId: getFeatureFeedNewActivityPostId,
        getFeatureFeedHasDeletePostId: getFeatureFeedHasDeletePostId,
        getFeatureFeedPostActivityCount: getFeatureFeedPostActivityCount
    }
};