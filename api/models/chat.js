﻿var sql = require('mssql');
var config = require('../myConfig').dataConfig;
var result = require('./result');

var defaultChatRowCount = require('../myConfig').defaultChatRowCount;

var logger = require('../helper/logHelper').logger;

var connection = new sql.ConnectionPool(config);
connection.connect().catch((error) => {
    logger.error('database connection error', error);
});

connection.on('error', function (err) {
    logger.error('database connection error', err);
    connection = new sql.ConnectionPool(config);
    connection.connect();
});


module.exports = {

    /**
    Các hàm về chat gần đây
    */
    //lấy danh sách chat gần đây của user
    getRecentChatRoom: function (userId, lastReceived, callback) {

        var request = new sql.Request(connection);
        request.input('UserId', sql.BigInt, userId);
        request.input('LastReceived', sql.DateTime, lastReceived);
        request.input('RowCount', sql.BigInt, defaultChatRowCount);
        request.execute('getRecentChatRoom', function (err, result) {
            if (err) {
                logger.error('getRecentChatRoom', err);
                callback(err);
            }
            else {
                var recordset = result.recordset;
                callback(null, recordset);
            }
        });
    },

    //tìm kiếm trong danh sách chat gần đây của user
    getRecentChatRoomByKeyword: function (userId, keyword, callback) {
        var request = new sql.Request(connection);
        request.input('UserId', sql.BigInt, userId);
        request.input('Keyword', sql.NVarChar, keyword);
        request.input('RowCount', sql.BigInt, defaultChatRowCount);
        request.execute('getRecentChatRoomByKeyword', function (err, recordSet) {
            if (err) {
                logger.error('getRecentChatRoomByKeyword', err);
                callback(err);
            }
            else {
                callback(null, recordSet[0]);
            }
        });
    },

    //lấy số lượng log chưa đọc của từng room của user
    getUserUnreadLog: function (userId, callback) {

        var request = new sql.Request(connection);
        request.input('UserId', sql.BigInt, userId);

        request.execute('getUserUnreadLog', function (err, recordSet) {
            if (err) {
                logger.error('getUserUnreadLog', err);
                callback(err);
            }
            else {
                callback(null, recordSet[0]);
            }
        });
    },


    /**
    Các hàm liên quan chat room
    */
    //lấy thông tin chat room đơn hoặc tạo nếu chưa có
    getPrivateChatRoom: function (userId1, userId2, os, callback) {

        if (userId1 > userId2) { //lưu theo quy tắc userid nhỏ hơn đứng trước để query ra cho dễ
            var temp = userId1;
            userId1 = userId2;
            userId2 = temp;
        }

        var request = new sql.Request(connection);

        request.input('UserId1', sql.BigInt, userId1);
        request.input('UserId2', sql.BigInt, userId2);
        request.input('OS', sql.VarChar, os);

        request.execute('getPrivateChatRoom', function (err, recordSet) {
            if (err) {
                logger.error('getPrivateChatRoom', err);
                callback(err);
            }
            else {
                callback(null, recordSet[0]);
            }
        });
    },

    //tạo group chat room
    createGroupChatRoom: function (userId, roomTypeId, chatRoomName, members, os, callback) {
        var request = new sql.Request(connection);

        request.input('UserId', sql.BigInt, userId);
        request.input('RoomTypeId', sql.Int, roomTypeId);
        request.input('ChatRoomName', sql.NVarChar, chatRoomName);
        request.input('Members', sql.NVarChar, members);
        request.input('OS', sql.VarChar, os);

        request.execute('createGroupChatRoom', function (err, recordSet) {
            if (err) {
                logger.error('createGroupChatRoom', err);
                callback(err);
            }
            else {
                callback(null, recordSet[0]);
            }
        });
    },

    //lấy chat room theo Id
    getChatRoom: function (chatRoomId, callback) {
        var request = new sql.Request(connection);

        request.input('ChatRoomId', sql.BigInt, chatRoomId);
        request.execute('getChatRoom', function (err, recordSet) {
            if (err) {
                logger.error('getChatRoom', err);
                callback(err);
            }
            else {
                callback(null, recordSet[0]);
            }
        });
    },

    //lấy danh sách member trong room
    getChatRoomMember: function (chatRoomId, callback) {
        var request = new sql.Request(connection);

        request.input('ChatRoomId', sql.BigInt, chatRoomId);
        request.execute('getChatRoomMember', function (err, recordSet) {
            if (err) {
                logger.error('getChatRoomMember', err);
                callback(err);
            }
            else {
                callback(null, recordSet[0]);
            }
        });

    },

    //lấy cấu hình chat room của user
    getUserChatRoomConfig: function (chatRoomId, userId, callback) {
        var request = new sql.Request(connection);

        request.input('ChatRoomId', sql.BigInt, chatRoomId);
        request.input('UserId', sql.BigInt, userId);

        request.execute('getUserChatRoomConfigs', function (err, recordSet) {
            if (err) {
                logger.error('getUserChatRoomConfig', err);
                callback(err);
            }
            else {
                callback(null, recordSet[0]);
            }
        });
    },

    //cập nhật cấu hình notify trong chat room của user
    setUserChatRoomConfig: function (userId, chatRoomId, isPushNotify, callback) {
        var request = new sql.Request(connection);

        request.input('ChatRoomId', sql.BigInt, chatRoomId);
        request.input('UserId', sql.BigInt, userId);
        request.input('IsPushNotify', sql.Bit, isPushNotify);

        request.execute('updateUserChatRoomConfig', function (err, recordSet) {
            if (err) {
                logger.error('setUserChatRoomConfig', err);
                callback(err);
            }
            else {
                callback(null, recordSet[0]);
            }
        });
    },

    //cập nhật tên chat room
    updateChatRoomName: function (userId, chatRoomId, chatRoomName, os, callback) {
        var request = new sql.Request(connection);

        request.input('ChatRoomId', sql.BigInt, chatRoomId);
        request.input('UserId', sql.BigInt, userId);
        request.input('ChatRoomName', sql.NVarChar, chatRoomName);
        request.input('OS', sql.VarChar, os);

        request.execute('updateChatRoomName', function (err, recordSet) {
            if (err) {
                logger.error('updateChatRoomName', err);
                callback(err);
            }
            else {

                //todo: cần kiểm tra kết quả trả về từ SP
                console.log(recordSet[0]);

                callback(null, recordSet[0].Success);
            }
        });
    },

    //thêm member vào room
    addChatRoomMember: function (userId, chatRoomId, callback) {
        var request = new sql.Request(connection);

        request.input('ChatRoomId', sql.BigInt, chatRoomId);
        request.input('UserId', sql.BigInt, userId);

        request.execute('addChatRoomMember', function (err, recordSet) {
            if (err) {
                logger.error('addChatRoomMember', err);
                callback(err);
            }
            else {

                //todo: cần kiểm tra kết quả trả về từ SP
                console.log(recordSet[0]);

                callback(null, recordSet[0]);
            }
        });
    },

    //xóa member khỏi room
    removeChatRoomMember: function (userId, chatRoomId, callback) {
        var request = new sql.Request(connection);

        request.input('ChatRoomId', sql.BigInt, chatRoomId);
        request.input('UserId', sql.BigInt, userId);

        request.execute('removeChatRoomMember', function (err, recordSet) {
            if (err) {
                logger.error('removeChatRoomMember', err);
                callback(err);
            }
            else {

                //todo: cần kiểm tra kết quả trả về từ SP
                console.log(recordSet[0]);

                callback(null, recordSet[0]);
            }
        });
    },

    /**
    Các hàm liên quan chat log
    */
    //lấy danh sách chat log của 1 room
    getChatRoomLog: function (chatRoomId, lastLogId, callback) {
        var request = new sql.Request(connection);

        request.input('ChatRoomId', sql.BigInt, chatRoomId);
        request.input('LastLogId', sql.BigInt, lastLogId);
        request.input('RowCount', sql.BigInt, defaultChatRowCount);

        request.execute('getChatRoomHistoryLog', function (err, recordSet) {
            if (err) {
                logger.error('getChatRoomLog', err);
                callback(err);
            }
            else {
                callback(null, recordSet[0]);
            }
        });
    },

    //tìm kiếm trong room
    searchChatRoom: function (chatRoomId, keyword, lastId, callback) {
        var request = new sql.Request(connection);

        request.input('ChatRoomId', sql.BigInt, chatRoomId);
        request.input('Keyword', sql.NVarChar, keyword);
        request.input('LastId', sql.BigInt, lastId);
        request.input('ItemCount', sql.Int, defaultChatRowCount);

        request.execute('searchChatRoom', function (err, recordSet) {
            if (err) {
                logger.error('searchChatRoom', err);
                callback(err);
            }
            else {
                callback(null, recordSet[0]);
            }
        });
    },

    //lấy danh sách hình trong room
    getChatRoomImage: function (chatRoomId, lastLogId, callback) {
        var request = new sql.Request(connection);

        request.input('ChatRoomId', sql.BigInt, chatRoomId);
        request.input('LastLogId', sql.BigInt, lastLogId);
        request.input('RowCount', sql.Int, defaultChatRowCount);

        request.execute('getChatRoomImage', function (err, recordSet) {
            if (err) {
                logger.error('getChatRoomImage', err);
                callback(err);
            }
            else {
                callback(null, recordSet[0]);
            }
        });
    },

    //lấy danh sách link trong room
    getChatRoomLink: function (chatRoomId, lastLogId, callback) {
        var request = new sql.Request(connection);

        request.input('ChatRoomId', sql.BigInt, chatRoomId);
        request.input('LastLogId', sql.BigInt, lastLogId);
        request.input('RowCount', sql.Int, defaultChatRowCount);

        request.execute('getChatRoomLink', function (err, recordSet) {
            if (err) {
                logger.error('getChatRoomLink', err);
                callback(err);
            }
            else {
                callback(null, recordSet[0]);
            }
        });
    },

    //thêm chat log, đồng thời thêm vào chat log view của từng user trong room để đánh dấu là user chưa xem hoặc đã xem
    //trả về nội dung tin kèm với chatroom name, roomtypeId, username, và user avatar    
    addChatRoomLog: function (chatRoomId, userId, message, linkUrl, thumbnailUrl, title, isLink, isFile, isImage, isAction, isImageItem, imageItemId, createOS, callback) {
        var request = new sql.Request(connection);

        request.input('ChatRoomId', sql.BigInt, chatRoomId);
        request.input('UserId', sql.BigInt, userId);
        request.input('Message', sql.NVarChar, message);
        request.input('LinkUrl', sql.NVarChar, linkUrl);
        request.input('ThumbnailUrl', sql.NVarChar, thumbnailUrl);
        request.input('Title', sql.NVarChar, title);
        request.input('IsLink', sql.Bit, isLink);
        request.input('IsFile', sql.Bit, isFile);
        request.input('IsImage', sql.Bit, isImage);
        request.input('IsAction', sql.Bit, isAction);
        request.input('IsImageItem', sql.Bit, isImageItem);
        request.input('ImageItemId', sql.Bit, imageItemId);
        request.input('CreateOS', sql.VarChar, createOS);

        request.execute('addChatRoomLog', function (err, recordSet) {
            if (err) {
                logger.error('addChatRoomLog', err);
                callback(err);
            }
            else {
                callback(null, recordSet[0]);
            }
        });
    },

    //xóa chat log (đánh dấu xóa)
    deleteChatRoomLog: function (chatRoomId, userId, chatLogId, callback) {
        var request = new sql.Request(connection);

        request.input('ChatRoomId', sql.BigInt, chatRoomId);
        request.input('UserId', sql.BigInt, userId);
        request.input('ChatLogId', sql.BigInt, chatLogId);

        request.execute('deleteChatRoomLog', function (err, recordSet) {
            if (err) {
                logger.error('deleteChatRoomLog', err);
                callback(err);
            }
            else {

                //todo: cần check lại kết quả
                callback(null, recordSet[0][0].Success);
            }
        });
    },

    updateChatLogView: function (chatRoomId, userId, chatLogId, viewOS, callback) {
        var request = new sql.Request(connection);

        request.input('ChatRoomId', sql.BigInt, chatRoomId);
        request.input('UserId', sql.BigInt, userId);
        request.input('ChatLogId', sql.BigInt, chatLogId);
        request.input('ViewOS', sql.VarChar, viewOS);

        request.execute('updateChatLogView', function (err, recordSet) {
            if (err) {
                logger.error('updateChatLogView', err);
                callback(err);
            }
            else {

                //todo: cần check lại kết quả
                callback(null, recordSet[0]);
            }
        });
    },

    getChatRoomMemberLastView: function (chatRoomId, callback) {
        var request = new sql.Request(connection);

        request.input('ChatRoomId', sql.BigInt, chatRoomId);

        request.execute('getChatRoomMemberLastView', function (err, recordSet) {
            if (err) {
                logger.error('getChatRoomMemberLastView', err);
                callback(err);
            }
            else {

                //todo: cần check lại kết quả
                callback(null, recordSet[0]);
            }
        });
    }
};
