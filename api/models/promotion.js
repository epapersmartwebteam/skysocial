﻿var sql = require('mssql');

var config = require('../myConfig').dataConfig;
var defaultRowCount = require('../myConfig').defaultRowCount;

var CDNUrl = require('../myConfig').CDNUrl;
var myConfig = require('../myConfig');

var resultHelper = require('./result');
var utilityHelper = require('../helper/utility');

var errorCode = require('../errorCode');
var dateHelper = require('../helper/dateHelper');

var logger = require('../helper/logHelper').logger;
var loggerPayment = require('../helper/logHelper').loggerPayment;
var loggerPaymentInfo = require('../helper/logHelper').loggerPaymentInfo;

var connection = new sql.ConnectionPool(config);
connection.connect().catch((error) => {
    logger.error('database connection error', error);
});

connection.on('error', function (err) {
    logger.error('database connection error', err);
    connection = new sql.ConnectionPool(config);
    connection.connect();
});

module.exports = {
    promoType: {
        VIPTRIAL: "VIPTRIAL",
        VIP: "VIP",
        NONE: "NONE"
    },
    promoTargetUser: {
        NEW: "NEW",
        UNVIP: "UNVIP",
        VIP: "VIP",
        ALL: "ALL"
    },
    getPromotionForUser: function (userId, callback) {
        var request = new sql.Request(connection);

        request.input('UserId', sql.BigInt, userId);

        var query = 'GetUserPromotions';

        request.execute(query, (err, result) => {
            if (err) {
                logger.error('getPromotionForUser', err);
                callback(err);
            }
            else {
                var records = result.recordset;
                callback(null, records);
            }
        });
    },
    getPromotion: function (promotionId, callback) {
        var request = new sql.Request(connection);

        request.input('PromotionId', sql.Int, promotionId);

        var query = 'Select * From Promotions Where PromotionId = @PromotionId';

        request.query(query, (err, result) => {
            if (err) {
                logger.error('getPromotion', err);
                callback(err);
            }
            else {
                var item;
                if (result.recordset && result.recordset.length > 0) {
                    item = result.recordset[0];
                }
                callback(null, item);
            }
        });
    },
    checkUserPromotion: function (userId, promotionId, callback) {
        var request = new sql.Request(connection);
        request.input('UserId', sql.BigInt, userId);
        request.input('PromotionId', sql.BigInt, promotionId);
        var query = 'if(Exists(Select * From UserPromotions Where UserId = @UserId and PromotionId = @PromotionId)) ';
        query += ' Begin ';
        query += ' Select IsUsed From UserPromotions Where UserId = @UserId and PromotionId = @PromotionId';
        query += ' End ';
        query += ' Else ';
        query += ' Begin ';
        query += ' Select -1 as IsUsed ';
        query += ' End ';

        request.query(query, (err, result) => {
            if (err) {
                logger.error('checkUserPromotion', err);
                callback(resultHelper.dataError(err));
            }
            else {
                var recordset = result.recordset;
                var res = -1;
                if (recordset) {
                    res = recordset[0].IsUsed;
                }
                callback(resultHelper.success(res));
            }
        });
    },
    addUserPromotion: function (userId, promotionId, isUsed, isRefuse, callback) {
        var request = new sql.Request(connection);
        request.input('PromotionId', sql.Int, promotionId);
        request.input('UserId', sql.BigInt, userId);
        request.input('IsUsed', sql.Bit, isUsed);
        request.input('IsRefuse', sql.Bit, isRefuse);

        var query = ' if(Exists(Select * From UserPromotions Where UserId = @UserId and PromotionId = @PromotionId)) ';
        query += ' Begin ';
        query += ' Update UserPromotions Set IsUsed = @IsUsed, IsRefuse = @IsRefuse, CreateDate = dbo.UNIX_TIMESTAMP(getutcdate()) Where UserId = @UserId and PromotionId = @PromotionId; ';
        query += ' End ';
        query += ' else ';
        query += ' Begin ';
        query += ' Insert into UserPromotions(UserId, PromotionId, IsUsed, IsRefuse) ';
        query += ' Values(@UserId, @PromotionId, @IsUsed, @IsRefuse); ';
        query += ' End ';

        request.query(query, (err, result) => {
            if (err) {
                loggerPayment.error('addUserPromotion', err);
            }
            else {
                loggerPaymentInfo.info('addUserPromotion', userId, promotionId, isUsed, isRefuse);
            }
            callback(err);
        });
    }
};