﻿var sql = require('mssql');
var logger = require('../helper/logHelper').logger;

//lấy config chuỗi kết nối
var config = require('../myConfig').dataConfig;
var defaultRowCount = require('../myConfig').defaultRowCount;

var connection = new sql.ConnectionPool(config);
connection.connect().catch((error) => {
    logger.error('database connection error', error);
});

connection.on('error', function (err) {
    logger.error('database connection error', err);
    connection = new sql.ConnectionPool(config);
    connection.connect();
});


module.exports = {
    addImageReport: function (userId, reportTypeId, imageItemId, comment, os, callback) {
        var request = new sql.Request(connection);

        request.input('ReportUserId', sql.BigInt, userId);
        request.input('ReportTypeId', sql.Int, reportTypeId);
        request.input('ImageItemId', sql.BigInt, imageItemId);
        request.input('Comment', sql.NVarChar, comment);
        request.input('CreateOS', sql.VarChar, os);

        var query = ' Insert Into ImageReports(ReportTypeId, ImageItemId, ReportUserId, Comment, CreateOS, IsChecked, IsCorrected, IsSolved) ';
        query += ' Values(@ReportTypeId, @ImageItemId, @ReportUserId, @Comment, @CreateOS, 0, 0, 0); ';
        query += ' Select SCOPE_IDENTITY() as ReportId;';
        request.query(query,
            function (err, result) {


                if (!err) {
                    var reportId;
                    var recordset = result.recordset;

                    if (recordset) {
                        reportId = recordset[0].ReportId;
                    }
                    callback(err, reportId);
                }
                else {
                    logger.error('addImageReport', err);
                    callback(err);
                }

            });
    },
    approveImageReport: function (reportId, isChecked, isSolved, isCorrected, userId, callback) {
        var request = new sql.Request(connection);

        request.input('ReportId', sql.BigInt, reportId);
        request.input('IsChecked', sql.Bit, isChecked);
        request.input('IsSolved', sql.Bit, isSolved);
        request.input('IsCorrected', sql.Bit, isCorrected);
        request.input('UserId', sql.BigInt, userId);

        var query = 'Update ImageReports ';
        query += ' Set IsChecked = @IsChecked, IsSolved = @IsSolved, IsCorrected = @IsCorrected, ';
        query += ' ApproveUserId = @UserId, ApproveDate = dbo.UNIX_TIMESTAMP(getUTCDate()) ';
        query += ' Where ReportId = @ReportId ';

        request.query(query, function (err, result) {
            if (err) {
                logger.error('addImageReport', err);
                callback(err);
            }
            else {
                callback(err, result);
            }
        });
    }
};