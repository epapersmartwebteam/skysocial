﻿var _ = require('lodash');
var sql = require('mssql');

//lấy config chuỗi kết nối
var config = require('../myConfig').dataConfig;
var defaultRowCount = require('../myConfig').defaultRowCount;

var logger = require('../helper/logHelper').logger;

var connection = new sql.ConnectionPool(config);
connection.connect().catch((error) => {
    logger.error('database connection error', error);
});

connection.on('error', function (err) {
    logger.error('database connection error', err);
    connection = new sql.ConnectionPool(config);
    connection.connect();
});

function parseSearchResult(item) {
    if (item.IsVip == 1) {
        item.IsVip = true;
    }
    else {
        item.IsVip = false;
    }
    return item;
}

module.exports = {
    searchTags: function (userId, keyword, rowCount, callback) {
        var request = new sql.Request(connection);
        keyword = keyword ? keyword.trim() : keyword;
        keyword = keyword.replace(/_/g, '[_]');
        request.input('Keyword', sql.VarChar, keyword);

        if (rowCount <= 0) {
            rowCount = defaultRowCount;
        }

        request.input('RowCount', sql.Int, rowCount);

        var query = 'Select top (@RowCount) \'\' as [Image], h.Hashtag as Title, ';
        query += ' Convert(nvarchar(10), (select count(*) From ImageHashtags ih Where ih.HashtagId = h.HashtagId)) as [Description], 0 as TypeId, 0 as IsVip ';
        query += ' From Hashtags h ';
        if (keyword.length == 1) {
            query += ' Where h.Hashtag = @Keyword';
        }
        else {
            query += " Where h.Hashtag like (@Keyword + '%') ";
            query += ' Order by len(h.Hashtag) ';
        }

        request.query(query,
            function (err, result) {
                if (err) {
                    logger.error('searchTags', err);
                    callback(err);
                }
                else {
                    var recordset = result.recordset;
                    _.each(recordset, (item, idx) => {
                        recordset[idx] = parseSearchResult(item);
                    });
                    return callback(err, recordset);
                }
            });
    },

    searchPeople: function (userId, keyword, rowCount, callback) {
        var request = new sql.Request(connection);

        keyword = keyword.replace(/_/g, '[_]');
        request.input('Keyword', sql.VarChar, keyword);

        if (rowCount <= 0) {
            rowCount = defaultRowCount;
        }

        request.input('RowCount', sql.Int, rowCount);

        var query = 'declare @ImageUrl varchar(255); ';
        query += ' set @ImageUrl = (select SettingValue From Settings where SetingKey = \'image_url\'); ';
        query += ' select top (@RowCount) (case when u1.AvatarSmall = \'\' or u1.AvatarSmall is null then \'\' else (@ImageUrl +  u1.AvatarSmall) end) as [Image], ';
        query += ' u1.UserName as Title,  u1.FullName as [Description], 1 as TypeId, cast(ISNULL(u1.IsVip, 0) as bit) as IsVip ';
        query += ' From Users u1 ';
        query += ' Where u1.UserName like (\'%\' + @Keyword + \'%\') ';
        query += ' and (u1.IsActive is null or u1.IsActive = 1) ';
        query += ' and (u1.IsBanned is null or u1.IsBanned = 0) ';
        query += ' Order by CASE';
        query += ' WHEN u1.UserName like (@Keyword + \'%\') THEN 0 ';
        query += ' WHEN u1.UserName like (@Keyword + \' %\') THEN 1 ';
        query += ' WHEN u1.UserName like (\'% \' + @Keyword) THEN 2 ';
        query += ' WHEN u1.UserName like (\'% \' + @Keyword + \' %\') THEN 3 ';
        query += ' ELSE 4  END, ';
        query += ' u1.UserName, len(u1.UserName) ';

        request.query(query,
            function (err, result) {
                if (err) {
                    logger.error('searchPeople', err);
                    callback(err);
                }
                else {
                    var recordset = result.recordset;
                    _.each(recordset, (item, idx) => {
                        recordset[idx] = parseSearchResult(item);
                    });
                    return callback(err, recordset);
                }
            });
    },

    searchTop: function (userId, keyword, rowCount, callback) {
        var request = new sql.Request(connection);

        keyword = keyword.replace(/_/g, '[_]');
        request.input('Keyword', sql.VarChar, keyword);

        if (rowCount <= 0) {
            rowCount = defaultRowCount;
        }

        request.input('RowCount', sql.Int, rowCount);

        var query = 'declare @ImageUrl varchar(255); ';
        query += ' set @ImageUrl = (select SettingValue From Settings where SetingKey = \'image_url\'); ';

        query += 'Select t.* ';
        query += ' From (';

        query += 'Select top (@RowCount) \'\' as [Image], h.Hashtag as Title, ';
        query += ' Convert(nvarchar(10), (select count(*) From ImageHashtags ih Where ih.HashtagId = h.HashtagId)) as [Description], 0 as TypeId, 0 as IsVip ';
        query += ' From Hashtags h ';
        if (keyword.length == 1) {
            query += ' Where h.Hashtag = @Keyword';
        }
        else {
            query += " Where h.Hashtag like (@Keyword + '%') ";
            query += ' Order by len(h.Hashtag) ';
        }

        query += ' union ';

        query += ' select top (@RowCount) (case when u1.AvatarSmall = \'\' or u1.AvatarSmall is null then \'\' else (@ImageUrl +  u1.AvatarSmall) end) as [Image], ';
        query += ' u1.UserName as Title,  u1.FullName as [Description], 1 as TypeId, cast(ISNULL(u1.IsVip, 0) as bit) as IsVip';
        query += ' From Users u1 ';
        query += ' Where u1.UserName like (\'%\' + @Keyword + \'%\') ';
        query += ' and (u1.IsActive is null or u1.IsActive = 1) ';
        query += ' and (u1.IsBanned is null or u1.IsBanned = 0) ';
        query += ' Order by CASE';
        query += ' WHEN u1.UserName like (@Keyword + \'%\') THEN 0 ';
        query += ' WHEN u1.UserName like (@Keyword + \' %\') THEN 1 ';
        query += ' WHEN u1.UserName like (\'% \' + @Keyword) THEN 2 ';
        query += ' WHEN u1.UserName like (\'% \' + @Keyword + \' %\') THEN 3 ';
        query += ' ELSE 4  END, ';
        query += ' u1.UserName, len(u1.UserName) ';

        query += ' ) as t ';
        query += ' Order by CASE';
        query += ' WHEN Title like (@Keyword + \'%\') THEN 0 ';
        query += ' WHEN Title like (@Keyword + \' %\') THEN 1 ';
        query += ' WHEN Title like (\'% \' + @Keyword) THEN 2 ';
        query += ' WHEN Title like (\'% \' + @Keyword + \' %\') THEN 3 ';
        query += ' ELSE 4  END, ';
        query += ' Title';

        request.query(query,
            function (err, result) {
                if (err) {
                    logger.error('searchTop', err);
                    return callback(err);
                }
                else {
                    var recordset = result.recordset;
                    _.each(recordset, (item, idx) => {
                        recordset[idx] = parseSearchResult(item);
                    });
                    return callback(err, recordset);
                }
            });
    },

    searchToChat: (userId, followPriority, groupPriority, rowCount, keyword, callback) => {
        var request = new sql.Request(connection);

        request.input('UserId', sql.BigInt, userId);
        request.input('Keyword', sql.NVarChar, keyword);
        request.input('RowCount', sql.Int, rowCount);
        request.input('FollowPriority', sql.Bit, followPriority);
        request.input('GroupPriority', sql.Bit, groupPriority);
        

        request.execute('SearchUserForChat',
            function (err, result) {
                if (err) {
                    logger.error('SearchUserForChat', err);
                    callback(err);
                }
                else {
                    var recordset = result.recordset;
                    
                    callback(null, recordset);
                }
            });
    }
};