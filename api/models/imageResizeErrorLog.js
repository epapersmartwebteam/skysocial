﻿var sql = require('mssql');
var logger = require('../helper/logHelper').logger;

//lấy config chuỗi kết nối
var config = require('../myConfig').dataConfig;

var connection = new sql.ConnectionPool(config);
connection.connect().catch((error) => {
    logger.error('database connection error', error);
});

connection.on('error', function (err) {
    logger.error('database connection error', err);
    connection = new sql.ConnectionPool(config);
    connection.connect();
});


module.exports = {
    createImageResizeErrorLog: function (itemId, action, link, errorMessage, callback) {
        var request = new sql.Request(connection);

        request.input('ItemId', sql.BigInt, itemId);
        request.input('Action', sql.NVarChar, action);
        request.input('Link', sql.NVarChar, link);
        request.input('ErrorMessage', sql.NVarChar, errorMessage);

        var query = ' Insert into ImageResizeErrorLogs(ItemId, Action, Link, ErrorMessage, CreateDate) ';
        query += ' Values(@ItemId, @Action, @Link, @ErrorMessage, GetDate) ';

        request.query(query,
            function (err, result) {
                if (err) {
                    logger.error('createImageResizeErrorLog', err);
                    if (callback) {
                        return callback(err);
                    }
                }
                else {
                    //có callback mới trả về, ko thì thôi
                    if (callback) {
                        var recordset = result.recordset;
                        return callback(err, recordset);
                    }
                }
            });
    }
};