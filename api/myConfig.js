﻿//tên app
exports.appName = 'Sky Social';

//database
exports.dataConfig = {
    user: 'adminsrv',
    password: 'P@12345678vm',
    //server: 'localhost',
    //server: '45.119.214.134',
    server: 'skysocial.database.windows.net',
    database: 'skysocial',
    options: {
        encrypt: true
    }
};

//default system user
exports.SystemUserId = 1;
exports.RoleAdmin = 1;
exports.RoleMod = 2;

exports.redisConfig = {
    host: 'sky-social.redis.cache.windows.net',
    port: 6379,
    password: 'LaFEawbk+yXT6908J4NDE9iRt8ATQ2DXfwJhLhd8XwA='
};

exports.cacheConfig = {
    enableCache: true,
    initFTCache: false,
    MTPTabKey: 'MTP',
    FeatureTabHashKey: 'FT',
    FeatureTabPostCommentHashKey: 'FTC',
    FeatureTabSetKey: 'FTS',
    FeatureTabUpdatedSetKey: 'FTS-U',
    FeatureTabActivitySetKey: 'FTS-A',
    FeatureTabDeleteSetKey: 'FTS-D',
    itemPerSync: 50,
    commentPerPost: 3,
    parallelItemMax: 10
};

exports.oneSignalConfig = {
    appId: 'b188db12-d54d-4409-ae7a-7e6a3190fc41',
    apiKey: 'ZGZiZDA4YjYtMWYwYi00ZTUzLWE1ZjItMmYzODdmZGU5ZmY5',
    itemCount: 500
};

exports.defaultRowCount = 20;
exports.momentDateTimeFormat = 'YYYY-MM-DD HH:mm:ss';

//payment
exports.PayType = {
    Media: 1,
    Vip: 0
};

//suggest
exports.MinPostForFollowingSuggest = 10;
exports.MinFollowingStartSuggest = 20;
exports.MinPostForSuggest = 0;//50;
exports.MinFollowForSuggest = 0;//100;
exports.SystemSuggestNumber = 3;

//newsfeed rule
exports.MaxImagePerUserPerDay = 3;

//Đường dẫn hình
exports.CDNUrl = 'https://cdn.ehubstar.com/';
exports.NoAvatarLink = 'avatar/no-avatar.png';
//đường dẫn web
exports.WebLink = 'https://share.ehubstar.com/?id=';
exports.RedirectLink = 'https://share.ehubstar.com/exlink/';
exports.azureAccountName = 'ehubstar';
exports.azureAccessKey = 'QcC4mmOJNEcsfG6LZnOfi8Sc7hv/F4pOkcaR4/SwVWTi/IA9jn8xH2YYOS5qgvyU7zjpkgojWh22CHKN8KGPqQ==';

//chat
exports.defaultChatRowCount = 20;
exports.groupRoomTypeId = 2;

//kích thước hình
exports.LargeWidth = 1080;
exports.MediumWidth = 512;
exports.SmallWidth = 256;

//secret key


//sns service
exports.serviceHost = 'https://notify.ehubstar.com';
exports.servicePath = '/api/';

//toomarketer
exports.sendMailServiceUrl = 'http://api.toomarketer.com/api/';
exports.sendMailUserName = 'phonestagram@gmail.com';
exports.sendMailPassword = 'chupladep';
exports.sendMailEmail = 'phonestagram@gmail.com';
exports.sendMailName = 'Fiseo';

exports.forgetPasswordMailTemplate = '<div style="width: 700px; margin: auto"><div style="margin-top: 30px"> <h1 style="color: #6E7E91">Chào [USERNAME],</h1> <h3>Bạn đã yêu cầu gửi lại mật khẩu trên ứng dụng [APPNAME]. Nếu bạn không có yêu cầu hoặc có sự nhầm lẫn nào thì bạn có thể bỏ qua email này.</h3> <h3 style="color: #6E93A3">Mật khẩu hiện tại của bạn là:</h3> <b style="padding: 10px 15px; background-color: #1f8dd6; border: 1px solid #114d97; color: white; font-size: 18px; font-weight: bold; text-decoration: none">[PASSWORD]</b> </div> <div style="color: gray; margin-top: 30px; font-size: 13px">Bạn nhận được mail này là do bạn đã yêu cầu gửi lại mật khẩu trên ứng dụng [APPNAME]. Nếu có nhầm lẫn hay sai sót nào, bạn có thể bỏ qua email này hoặc phản hồi lại với chúng tôi <a href="mailto:[FROMEMAIL]">[FROMEMAIL]</a> </div> </div>';

//log config
exports.logConfigs = {
    level: 'debug',
    enableCallStack: true
};

exports.suggestConfigs = {
    categoryForRegion: 3,
    categoryForZodiac: 2,
    categoryForSuggestGroup: 5,
    havingCountForSuggestGroup: 2, //có 2 lựa chọn nên phải thoả cả 2 mới xét, ví dụ phả fcv fghdsdf i có cả HCM và Bọ cạp thì mới suggest ra
    suggestGroupTopCount: 3
};

exports.adminPostReportEachCount = 3;

exports.releaseDate = 0;//1578614400;
exports.releaseMessage = "Comming Soon";

exports.jwtSecret = "{Qg@f-9ymH6g8)#f";
exports.tokenExpireTime = 90 * 24 * 60 * 60; //3 tháng

exports.iOSInAppPurchaseSettings = {
    env: 'sandbox',
    sharedSecret: '',
    appStoreURL: {
        sandbox: 'https://sandbox.itunes.apple.com/',
        production: 'https://buy.itunes.apple.com/'
    },
    verifyReceiptFunction: 'verifyReceipt',
    methodId: 7
};

//chat core
exports.chatCore = {
    providerId: 'sky',
    apiKey: 'Q5dWINg7YuuzQjfjAQ7F710XMuPI357h',
    //chatCoreUrl: 'https://corechat.azurewebsites.net/',
    //chatCoreUrl: 'https://apichat.app/',
    chatCoreUrl: 'http://localhost:3009/',
    userPrefix: 'SKY-'
};


exports.firebase = {
    password: "7MUZL@84R!y$q%?J",
    databaseUrl: "https://sontungmtp-56bcc.firebaseio.com",
    apiKey: "AIzaSyBhniyXjkwCIXWT2noUMclu6wTeYcJ84ZE",
    authDomain: "sontungmtp-56bcc.firebaseapp.com",
    projectId: "sontungmtp-56bcc",
    storageBucket: "sontungmtp-56bcc.appspot.com",
    messagingSenderId: "745408950616",
    appId: "1:745408950616:web:c16bb5f796054ef1c4352d",
    dynamicLinkDomain: "skysocial.page.link",
    verifyLink: "https://verify.ehubstar.com/"
};

exports.storage = {
    provider: "cmcS3",
    containers: {
        post: 'post',
        media: 'media',
        avatar: 'avatar',
        resource: 'resource'
    },
    azure: {
        //Đường dẫn hình
        StorageUrl: 'https://cdn.ehubstar.com/',
        NoAvatarLink: 'avatar/no-avatar.png',
        azureAccountName: 'ehubstar',
        azureAccessKey: 'QcC4mmOJNEcsfG6LZnOfi8Sc7hv/F4pOkcaR4/SwVWTi/IA9jn8xH2YYOS5qgvyU7zjpkgojWh22CHKN8KGPqQ=='
    },
    cmcS3: {
        expireSecond: 600,
        accessKeyId: "U5E2XC65IJXKX14ZQBTJ",
        secretAccessKey: "96wmFs736Ejw846DGyVoF5UyNaD3UAVjdu8vVVHi",
        endpoint: 'https://s3.cloud.cmctelecom.vn',
        region: "us-east-1",
        bucket: "skysocial",
        StorageUrl: "https://skysocial.s3.cloud.cmctelecom.vn/"
    }
};