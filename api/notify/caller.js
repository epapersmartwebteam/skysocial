﻿var request = require('request');

var myConfig = require('../myConfig');

var serviceUrl = myConfig.serviceHost + myConfig.servicePath;
var logger = require('../helper/logHelper').logger;
//var keepaliveAgent = new http.Agent({
//    keepAlive:true
//});
module.exports = {   

    //notify tới follower khi có upload mới
    notifyNewImageUpload: function (userId, imageItemId, callback) {

        var apiName = 'NewImageUploadNotify';
        var url = serviceUrl + apiName + '?UserId=' + userId + '&ImageItemId=' + imageItemId;
       
        request({
            uri: url, method: 'GET'
        }, function (error, response, body) {
                if (error) {
                    logger.error('notifyNewImageUpload', error);
                }
            //ko làm gì
        });
        callback(null, true);
    },

    //notify tới chủ hình khi có comment mới
    notifyNewComment: function (userId, imageItemId, commentId, callback) {
        var apiName = 'NewCommentNotify';
        var url = serviceUrl + apiName + '?UserId=' + userId + '&ImageItemId=' + imageItemId + '&CommentId=' + commentId;
        
        request({ uri: url, method: 'GET', forever: true }, function (error, response, body) {
            if (error) {
                logger.error('notifyNewComment', error);
            }
            //ko làm gì
        });
        callback(null, true);
    },

    notifyNewMediaComment: function (userId, mediaId, commentId, callback) {
        var apiName = 'NewMediaCommentNotify';
        var url = serviceUrl + apiName + '?UserId=' + userId + '&MediaId=' + mediaId + '&CommentId=' + commentId;
        
        request({ uri: url, method: 'GET', forever: true }, function (error, response, body) {
            if (error) {
                logger.error('notifyNewMediaComment', error);
            }
            //ko làm gì
        });
        callback(null, true);
    },

    notifyLikeComment: function (userId, commentId, callback) {
        var apiName = 'LikeCommentNotify';
        var url = serviceUrl + apiName + '?UserId=' + userId  + '&CommentId=' + commentId;
        
        request({ uri: url, method: 'GET', forever: true }, function (error, response, body) {
            if (error) {
                logger.error('notifyLikeComment', error);
            }
            //ko làm gì
        });
        callback(null, true);
    },

    notifyLikeMediaComment: function (userId, commentId, callback) {
        var apiName = 'LikeMediaCommentNotify';
        var url = serviceUrl + apiName + '?UserId=' + userId + '&CommentId=' + commentId;
        
        request({ uri: url, method: 'GET', forever: true }, function (error, response, body) {
            if (error) {
                logger.error('notifyLikeMediaComment', error);
            }
            //ko làm gì
        });
        callback(null, true);
    },

    //notify tới chủ hình khi có like mới
    notifyNewLike: function (userId, imageItemId, callback) {
        var apiName = 'NewLikeNotify';
        var url = serviceUrl + apiName + '?UserId=' + userId + '&ImageItemId=' + imageItemId;
        
        request({ uri: url, method: 'GET' }, function (error, response, body) {
            if (error) {
                logger.error('notifyNewLike', error);
            }
            //ko làm gì
        });
        callback(null, true);
    },

    //notify tới người đc follow
    notifyHasFollowing: function (userId, targetUserId, callback) {
        var apiName = 'FollowingNotify';
        var url = serviceUrl + apiName + '?UserId=' + userId + '&TargetUserId=' + targetUserId;

        request({ uri: url, method: 'GET' }, function (error, response, body) {
            if (error) {
                logger.error('notifyHasFollowing', error);
            }
            //ko làm gì

        });
        callback(null, true);
    },

    //notify tới người follow khi đc accept follow
    notifyAcceptedFollowing: function (userId, targetUserId, callback) {
        var apiName = 'AcceptFollowingNotify';
        var url = serviceUrl + apiName + '?UserId=' + userId + '&TargetUserId=' + targetUserId;

        request({ uri: url, method: 'GET' }, function (error, response, body) {
            if (error) {
                logger.error('notifyAcceptedFollowing', error);
            }
            //ko làm gì
        });
        callback(null, true);
    },

    //notify tới các member trong chat room khi có tin nhắn mới
    notifyNewChatMessage: function (userId, chatRoomId, chatLogId, callback) {
        var apiName = 'NewChatMessageNotify';
        var url = serviceUrl + apiName + '?UserId=' + userId + '&ChatRoomId=' + chatRoomId + '&ChatLogId=' + chatLogId;

        request({ uri: url, method: 'GET' }, function (error, response, body) {
            if (error) {
                logger.error('notifyNewChatMessage', error);
            }
            //ko làm gì
        });
        callback(null, true);
    },

    //notify tới các member trong chat room khi có file mới
    notifyNewChatFile: function (userId, chatRoomId, chatLogId, callback) {
        var apiName = 'NewChatFileNotify';
        var url = serviceUrl + apiName + '?UserId=' + userId + '&ChatRoomId=' + chatRoomId + '&ChatLogId=' + chatLogId;

        request({ uri: url, method: 'GET' }, function (error, response, body) {
            if (error) {
                logger.error('notifyNewChatFile', error);
            }
            //ko làm gì
        });
        callback(null, true);
    },

    //notify tới các follower khi có comment mới
    notifyFollowingHasComment: function (userId, imageItemId, commentId, callback) {        
        var apiName = 'FollowingHasCommentNotify';
        var url = serviceUrl + apiName + '?UserId=' + userId + '&ImageItemId=' + imageItemId + '&CommentId=' + commentId;

        request({ uri: url, method: 'GET' }, function (error, response, body) {
            if (error) {
                logger.error('notifyFollowingHasComment', error);
            }
            //ko làm gì
        });
        callback(null, true);
    },

    //notify tới các follower khi có like mới
    notifyFollowingHasLike: function (userId, imageItemId, callback) {
        var apiName = 'FollowingHasLikeNotify';
        var url = serviceUrl + apiName + '?UserId=' + userId + '&ImageItemId=' + imageItemId;

        request({ uri: url, method: 'GET' }, function (error, response, body) {
            if (error) {
                logger.error('notifyFollowingHasLike', error);
            }
            //ko làm gì
        });
        callback(null, true);
    },

    //notify tới các follower khi có share mới
    notifyFollowingHasShare: function (userId, imageItemId, callback) {
        var apiName = 'FollowingHasShareNotify';
        var url = serviceUrl + apiName + '?UserId=' + userId + '&ImageItemId=' + imageItemId;

        request({ uri: url, method: 'GET' }, function (error, response, body) {
            if (error) {
                logger.error('notifyFollowingHasShare', error);
            }
            //ko làm gì
        });
        callback(null, true);
    },

    updateUserTagLanguage: function (userId, onesignalId, languageCode, callback) {
        var apiName = 'UpdateUserTagLanguage';
        var url = serviceUrl + apiName + '?OnesignalId=' + onesignalId + '&LanguageCode=' + languageCode + "&UserId=" + userId;
        
        request({ uri: url, method: 'POST' }, function (error, response, body) {
            if (error) {
                logger.error('updateUserTagLanguage', error);
            }
            //ko làm gì
        });
        callback(null, true);
    }
};