var schedule = require('node-schedule');
const request = require('request');

var config = require('./config');
var log = require('./log');

function init() {
    var scheduleHandle = schedule.scheduleJob(config.cronJobSetting, function (fireDate) {
        log.info('Schedule run at ' + fireDate);
        pingAPI();
        pingPayment();
        pingShare();
        pingInsight();
        pingVerify();
    });
}

function pingAPI() {
    request.post({
        uri: config.api,
        form: {
            username: 'mmmmm',
            password: '123456'
        }
    },
        function (error, response, body) {
            log.error('Ping API', error);
        }
    );
}

function pingPayment() {
    request.get({
        uri: config.payment
    },
        function (error, response, body) {
            log.error('Ping payment', error);
        }
    );
}

function pingShare() {
    request.get({
        uri: config.share
    },
        function (error, response, body) {
            log.error('Ping share', error);
        }
    );
}

function pingInsight() {
    request.get({
        uri: config.insight
    },
        function (error, response, body) {
            log.error('Ping Insight', error);
        }
    );
}

function pingVerify() {
    request.get({
        uri: config.verify
    },
        function (error, response, body) {
            log.error('Ping Verify', error);
        }
    );
}

init();