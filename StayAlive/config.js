module.exports = {
    cronJobSetting: '0 */5 * * * *',
    api: 'https://api-sky.ehubstar.com/user/login',
    notify: 'https://notify.ehubstar.com/help',
    payment: 'https://pay-sky.ehubstar.com/',
    share: 'https://share.ehubstar.com',
    verify: 'https://verify.ehubstar.com',
    insight: 'https://insight-sky.ehubstar.com'
};