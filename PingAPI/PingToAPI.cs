using System;
using Microsoft.Azure.WebJobs;
using Microsoft.Azure.WebJobs.Host;
using Microsoft.Extensions.Logging;
using RestSharp;

namespace PingAPI
{
    public static class PingToAPI
    {
        private class Constant
        {
            public static string sqldb_connection = "Server=tcp:skysocial.database.windows.net,1433;Initial Catalog=skysocial;Persist Security Info=False;User ID=adminsrv;Password=P@12345678vm;MultipleActiveResultSets=False;Encrypt=True;TrustServerCertificate=False;Connection Timeout=30;";
            public static string apiUrl = "https://api-sky.ehubstar.com/";
            public static string loginMethod = "user/login";
            public static string username = "mmmmm";
            public static string password = "123456";
        }
        [FunctionName("Function1")]
        public static void Run([TimerTrigger("0 */5 * * * *")]TimerInfo myTimer, ILogger log)
        {
            log.LogInformation($"PingToAPI trigger function executed at: {DateTime.Now}");
            var client = new RestClient(Constant.apiUrl);
            var req = new RestRequest(Constant.loginMethod);
            req.AddHeader("Content-Type", "application/x-www-form-urlencoded");
            req.AddObject(new { username = Constant.username, password = Constant.password });
            var response = client.Post(req);

            log.LogInformation("result", response.ResponseStatus);
        }
    }
}
