﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using OnesignalNotify.Model;
using System.Resources;
using OneSignal.CSharp.SDK.Resources;

namespace OnesignalNotify
{
    public partial class BLL
    {
        public static int NewPostPeriodInMinute = Convert.ToInt32(System.Configuration.ConfigurationManager.AppSettings.Get("NewPostNotifyPeriodInMinute"));

        public static void MentionInANewImage(UserModel mUserAction, ImageItemModel mImage, List<long> lstUserMentionIds)
        {   
            int NotifyTypeId = (int)NotifyTypes.MentionInImage;
            string NotifyType = NotifyTypes.MentionInImage.ToString();

            Resources.MessageResource.Culture = new System.Globalization.CultureInfo("vi-VN");
            string Message = string.Format(Resources.MessageResource.MentionInImage, mUserAction.UserName);
            Resources.MessageResource.Culture = new System.Globalization.CultureInfo("EN");
            string MessageEN = string.Format(Resources.MessageResource.MentionInImage, mUserAction.UserName);

            string Subject = "";

            string ImageUrl = mImage.ImageMedium;

            foreach (long UserId in lstUserMentionIds)
            {
                if (UserId != mUserAction.UserId && UserId != MTPUserId)
                {
                    
                    long NotifyId = DAL.AddUserNotify(UserId, NotifyTypeId, NotifyType, Message, MessageEN, mUserAction.UserId, null, mImage.ImageItemId, null, null, null);

                    //if (!DAL.CheckUserHasCommentNotifyWithinAnPeriod(UserId, mImage.ImageItemId, InMinutes))
                    //{
                    UserNotifySettingModel mSetting = DAL.GetUserNotifySetting(UserId);

                    //cấu hình nhận notify thì mới tiếp tục
                    if (mSetting.MentionInPost)
                    {
                        //để trong này vì phải đợi lấy NotifyId và UserId của người nhận
                        NotifyData Data = new NotifyData
                        {
                            NotifyId = NotifyId,
                            NotifyTypeId = NotifyTypeId,
                            NotifyType = NotifyType,
                            ActionUserId = mUserAction.UserId,
                            ActionUserName = mUserAction.UserName,
                            ActionUserAvatar = mUserAction.UserAvatar,
                            ImageItemId = mImage.ImageItemId,
                            ImageUrl = ImageUrl,
                            
                        };

                        List<UserEndpointModel> lstEndpoints = DAL.GetUserEndpoints(UserId);
                        var lstEN = lstEndpoints.Where(c => c.LanguageCode == LanguageCodes.English.ToUpper()).ToList();
                        if (lstEN.Count > 0)
                        {
                            OnesignalHelper.SendNotifications(Subject, MessageEN, Data, lstEN);
                        }
                        var lstVI = lstEndpoints.Where(c => c.LanguageCode == LanguageCodes.Vietnamese.ToUpper()).ToList();
                        if (lstVI.Count > 0)
                        {
                            OnesignalHelper.SendNotifications(Subject, Message, Data, lstVI);
                        }
                    }
                    //}
                }
            }
        }

        //chỉ push notify => ko ghi log
        public static void NewImageUploadNotify(long ActionUserId, long ImageItemId, long? LastId, int Count)
        {

            int NotifyTypeId = (int)NotifyTypes.UploadNewPhoto;
            string NotifyType = NotifyTypes.UploadNewPhoto.ToString();

            List<UserModel> lst = DAL.GetUserFollowerList(ActionUserId, LastId, Count, PushNewPost: true);

            UserModel mUserAction = DAL.GetUserInfo(ActionUserId);

            //todo: nghiên cứu xem để gì trên này
            string Subject = "";

            ImageItemModel mImage = DAL.GetImageItemInfo(ImageItemId);
            string ImageUrl = mImage.ImageMedium;

            string Description = mImage.Description;

            List<string> MentionUserNames = Utility.GetMentionUserNames(Description);

            List<long> lstMentionIds = new List<long>();

            if(MentionUserNames.Count > 0)
            {
                lstMentionIds =  DAL.GetUserIdFromUserNameList(MentionUserNames);
                //bỏ người action ra
                lstMentionIds.Remove(ActionUserId);

                MentionInANewImage(mUserAction, mImage, lstMentionIds);

                lst.RemoveAll((x)=>lstMentionIds.Any((y) => x.UserId == y));
            }


            //gán Message tương ứng với sự kiện
            Resources.MessageResource.Culture = new System.Globalization.CultureInfo("vi-VN");
            string resourceVN = Resources.MessageResource.UploadNewPhoto;
            if(mImage.ItemContent.Count > 1)
            {
                resourceVN = Resources.MessageResource.UploadNewAlbum;
            }
            else if(mImage.ItemContent.Count > 0)
            {
                if(mImage.ItemContent[0].ItemType.ToUpper() == "VIDEO")
                {
                    resourceVN = Resources.MessageResource.UploadNewVideo;
                }
            }
            string Message = string.Format(resourceVN, mUserAction.UserName);


            Resources.MessageResource.Culture = new System.Globalization.CultureInfo("EN");
            string resourceEN = Resources.MessageResource.UploadNewPhoto;
            if (mImage.ItemContent.Count > 1)
            {
                resourceEN = Resources.MessageResource.UploadNewAlbum;
            }
            else if (mImage.ItemContent.Count > 0)
            {
                if (mImage.ItemContent[0].ItemType.ToUpper() == "VIDEO")
                {
                    resourceEN = Resources.MessageResource.UploadNewVideo;
                }
            }
            string MessageEN = string.Format(resourceEN, mUserAction.UserName);
            

            foreach (UserModel m in lst)
            {
                if (m.UserId != ActionUserId && m.UserId != MTPUserId)
                {
                    bool NeedNotify = !DAL.CheckUserHasPostNotifyWithinPeriod(m.UserId, ActionUserId, NewPostPeriodInMinute);
                    //ko cần ghi log vì quá nhiều cũng như ko cần thiết
                    //Update: ghi log để theo dõi ko push notify quá nhiều cho User
                    //long NotifyId = 0;// DAL.AddUserNotify(m.UserId, NotifyTypeId, NotifyType, Message, MessageEN, ActionUserId, null, ImageItemId, null, null, null);
                    long NotifyId = DAL.AddUserNotify(m.UserId, NotifyTypeId, NotifyType, Message, MessageEN, ActionUserId, null, ImageItemId, null, null, null);

                    if (NeedNotify)
                    {
                        UserNotifySettingModel mSetting = DAL.GetUserNotifySetting(m.UserId);

                        //todo: đọc cấu hình nhận push notify từ user khi có post mới, bỏ đọc theo cái setting cũ hiện giờ
                        //cấu hình nhận notify thì mới tiếp tục
                        if (mSetting.FollowUploadImage)
                        {
                            //để trong này vì phải đợi lấy NotifyId và UserId của người nhận
                            NotifyData Data = new NotifyData
                            {
                                NotifyId = NotifyId,
                                NotifyTypeId = NotifyTypeId,
                                NotifyType = NotifyType,
                                ActionUserId = ActionUserId,
                                ActionUserName = mUserAction.UserName,
                                ActionUserAvatar = mUserAction.UserAvatar,
                                ImageItemId = ImageItemId,
                                ImageUrl = ImageUrl
                            };

                            List<UserEndpointModel> lstEndpoints = DAL.GetUserEndpoints(m.UserId);
                            var lstEN = lstEndpoints.Where(c => c.LanguageCode == LanguageCodes.English.ToUpper()).ToList();
                            if (lstEN.Count > 0)
                            {
                                OnesignalHelper.SendNotifications(Subject, MessageEN, Data, lstEN);
                            }
                            var lstVI = lstEndpoints.Where(c => c.LanguageCode == LanguageCodes.Vietnamese.ToUpper()).ToList();
                            if (lstVI.Count > 0)
                            {
                                OnesignalHelper.SendNotifications(Subject, Message, Data, lstVI);
                            }
                            //string AndroidMessageString = AmazonSNSHelper.GenerateMessageString(AmazonSNSHelper.OSAndroid, Message, m.UserId, NotifyId, NotifyTypeId, NotifyType, ActionUserId, mUserAction.UserName, mUserAction.UserAvatar, ImageItemId: ImageItemId, ImageUrl: ImageUrl);
                            //string AndroidENMessageString = AmazonSNSHelper.GenerateMessageString(AmazonSNSHelper.OSAndroid, MessageEN, m.UserId, NotifyId, NotifyTypeId, NotifyType, ActionUserId, mUserAction.UserName, mUserAction.UserAvatar, ImageItemId: ImageItemId, ImageUrl: ImageUrl);
                            //string iOSMessageString = AmazonSNSHelper.GenerateMessageString(AmazonSNSHelper.OSiOS, Message, m.UserId, NotifyId, NotifyTypeId, NotifyType, ActionUserId, mUserAction.UserName, mUserAction.UserAvatar, ImageItemId: ImageItemId, ImageUrl: ImageUrl);
                            //string iOSENMessageString = AmazonSNSHelper.GenerateMessageString(AmazonSNSHelper.OSiOS, MessageEN, m.UserId, NotifyId, NotifyTypeId, NotifyType, ActionUserId, mUserAction.UserName, mUserAction.UserAvatar, ImageItemId: ImageItemId, ImageUrl: ImageUrl);

                            //foreach (UserEndpointModel mEndpoint in lstEndpoints)
                            //{
                            //    AmazonSNSHelper.PublishToEndpoint(m.UserId, mEndpoint.EndpointArn, Subject,
                            //        (mEndpoint.OS == AmazonSNSHelper.OSAndroid
                            //            ? ((mEndpoint.LanguageCode == AmazonSNSHelper.VNLanguageCode) ? AndroidMessageString : AndroidENMessageString)
                            //            : ((mEndpoint.LanguageCode == AmazonSNSHelper.VNLanguageCode) ? iOSMessageString : iOSENMessageString)));
                            //}
                        }
                    }
                }
            }

            //nếu còn thì gọi đệ quy
            if (lst.Count == Count)
            {
                NewImageUploadNotify(ActionUserId, ImageItemId, lst.LastOrDefault().UserId, Count);
            }
        }

        //chỉ ghi log ko push notify
        public static void FollowingHasShareNotify(long ActionUserId, long ImageItemId, long? LastId, int Count)
        {
            int NotifyTypeId = (int)NotifyTypes.FollowingShare;
            string NotifyType = NotifyTypes.FollowingShare.ToString();

            List<UserModel> lst = DAL.GetUserFollowerList(ActionUserId, LastId, Count);
            UserModel mUserAction = DAL.GetUserInfo(ActionUserId);

            ImageItemModel mImage = DAL.GetImageItemInfo(ImageItemId);
            long UserId = mImage.UserId;

            //todo: nghiên cứu xem để gì trên này
            string Subject = "";

            //gán Message tương ứng với sự kiện
            Resources.MessageResource.Culture = new System.Globalization.CultureInfo("vi-VN");
            string Message = string.Format(Resources.MessageResource.FollowingShare, mUserAction.UserName);
            Resources.MessageResource.Culture = new System.Globalization.CultureInfo("EN");
            string MessageEN = string.Format(Resources.MessageResource.FollowingShare, mUserAction.UserName);

            string ImageUrl = mImage.ImageMedium;

            foreach (UserModel m in lst)
            {
                if (m.UserId != ActionUserId && m.UserId != MTPUserId)
                {
                    long NotifyId = DAL.AddUserNotify(m.UserId, NotifyTypeId, NotifyType, Message, MessageEN, ActionUserId, null, ImageItemId, null, null, null);

                    //ko push notify vì quá nhiều sự kiện
                    //UserNotifySettingModel mSetting = DAL.GetUserNotifySetting(m.UserId);

                    ////cấu hình nhận notify thì mới tiếp tục
                    //if (mSetting.FollowingAction)
                    //{

                    //NotifyData Data = new NotifyData
                    //{
                    //    notifyId = NotifyId,
                    //    notifyTypeId = NotifyTypeId,
                    //    notifyType = NotifyType,
                    //    actionUserId = ActionUserId,
                    //    actionUserName = mUserAction.UserName,
                    //    actionUserAvatar = mUserAction.UserAvatar,
                    //    imageItemId = ImageItemId,
                    //    imageUrl = ImageUrl
                    //};

                    //List<UserEndpointModel> lstEndpoints = DAL.GetUserEndpoints(m.UserId);
                    //var lstEN = lstEndpoints.Where(c => c.LanguageCode == LanguageCodes.English.ToUpper()).ToList();
                    //if (lstEN.Count > 0)
                    //{
                    //    OnesignalHelper.SendNotifications(Subject, MessageEN, Data, lstEN);
                    //}
                    //var lstVI = lstEndpoints.Where(c => c.LanguageCode == LanguageCodes.Vietnamese.ToUpper()).ToList();
                    //if (lstVI.Count > 0)
                    //{
                    //    OnesignalHelper.SendNotifications(Subject, Message, Data, lstVI);
                    //}

                    //    //để trong này vì phải đợi lấy NotifyId và UserId của người nhận
                    //    string AndroidMessageString = AmazonSNSHelper.GenerateMessageString(AmazonSNSHelper.OSAndroid, Message, m.UserId, NotifyId, NotifyTypeId, NotifyType, ActionUserId, mUserAction.UserName, mUserAction.UserAvatar, ImageItemId: ImageItemId, ImageUrl: ImageUrl);
                    //    string AndroidENMessageString = AmazonSNSHelper.GenerateMessageString(AmazonSNSHelper.OSAndroid, MessageEN, m.UserId, NotifyId, NotifyTypeId, NotifyType, ActionUserId, mUserAction.UserName, mUserAction.UserAvatar, ImageItemId: ImageItemId, ImageUrl: ImageUrl);
                    //    string iOSMessageString = AmazonSNSHelper.GenerateMessageString(AmazonSNSHelper.OSiOS, Message, m.UserId, NotifyId, NotifyTypeId, NotifyType, ActionUserId, mUserAction.UserName, mUserAction.UserAvatar, ImageItemId: ImageItemId, ImageUrl: ImageUrl);
                    //    string iOSENMessageString = AmazonSNSHelper.GenerateMessageString(AmazonSNSHelper.OSiOS, MessageEN, m.UserId, NotifyId, NotifyTypeId, NotifyType, ActionUserId, mUserAction.UserName, mUserAction.UserAvatar, ImageItemId: ImageItemId, ImageUrl: ImageUrl);



                    //    List<UserEndpointModel> lstEndpoints = DAL.GetUserEndpoints(m.UserId);
                    //    foreach (UserEndpointModel mEndpoint in lstEndpoints)
                    //    {
                    //        AmazonSNSHelper.PublishToEndpoint(m.UserId, mEndpoint.EndpointArn, Subject,
                    //            (mEndpoint.OS == AmazonSNSHelper.OSAndroid
                    //                ? ((mEndpoint.LanguageCode == AmazonSNSHelper.VNLanguageCode) ? AndroidMessageString : AndroidENMessageString)
                    //                : ((mEndpoint.LanguageCode == AmazonSNSHelper.VNLanguageCode) ? iOSMessageString : iOSENMessageString)));
                    //    }
                    //}
                }
            }

            //nếu còn thì gọi đệ quy
            if (lst.Count == Count)
            {
                FollowingHasShareNotify(ActionUserId, ImageItemId, lst.LastOrDefault().UserId, Count);
            }
        }
    }
}