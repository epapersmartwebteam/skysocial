﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using OnesignalNotify.Model;
using OneSignal.CSharp.SDK.Resources;

namespace OnesignalNotify
{
    public partial class BLL
    {
        public static void NewLikeNotify(long ActionUserId, long ImageItemId)
        {
            int NotifyTypeId = (int)NotifyTypes.NewLike;
            string NotifyType = NotifyTypes.NewLike.ToString();

            ImageItemModel mImage = DAL.GetImageItemInfo(ImageItemId);
            long UserId = mImage.UserId;

            if (UserId != ActionUserId)
            {
                UserModel mUserAction = DAL.GetUserInfo(ActionUserId);
                Resources.MessageResource.Culture = new System.Globalization.CultureInfo("vi-VN");
                string Message = string.Format(Resources.MessageResource.NewLike, mUserAction.UserName);
                Resources.MessageResource.Culture = new System.Globalization.CultureInfo("EN");
                string MessageEN = string.Format(Resources.MessageResource.NewLike, mUserAction.UserName);

                //chưa notify like trước đó thì mới notify, đã từng thì ko notify lại nếu user unlike rồi like lại nữa
                if (!DAL.CheckUserHasNotifyLikeBefore(UserId, ActionUserId, ImageItemId))
                {                    
                    //todo: nghiên cứu xem để gì trên này
                    string Subject = "";

                    string ImageUrl = mImage.ImageMedium;

                    //gán Message tương ứng với sự kiện
                   

                    long NotifyId = DAL.AddUserNotify(UserId, NotifyTypeId, NotifyType, Message, MessageEN, ActionUserId, null, ImageItemId, null, null, null);

                    UserNotifySettingModel mSetting = DAL.GetUserNotifySetting(UserId);
                    if (mSetting.NewLike)
                    {

                        //để trong này vì phải đợi lấy NotifyId và UserId của người nhận
                        NotifyData Data = new NotifyData
                        {
                            NotifyId = NotifyId,
                            NotifyTypeId = NotifyTypeId,
                            NotifyType = NotifyType,
                            ActionUserId = ActionUserId,
                            ActionUserName = mUserAction.UserName,
                            ActionUserAvatar = mUserAction.UserAvatar,
                            ImageItemId = ImageItemId,
                            ImageUrl = ImageUrl
                        };

                        List<UserEndpointModel> lstEndpoints = DAL.GetUserEndpoints(UserId);
                        var lstEN = lstEndpoints.Where(c => c.LanguageCode == LanguageCodes.English.ToUpper()).ToList();
                        if (lstEN.Count > 0)
                        {
                            OnesignalHelper.SendNotifications(Subject, MessageEN, Data, lstEN);
                        }
                        var lstVI = lstEndpoints.Where(c => c.LanguageCode == LanguageCodes.Vietnamese.ToUpper()).ToList();
                        if (lstVI.Count > 0)
                        {
                            OnesignalHelper.SendNotifications(Subject, Message, Data, lstVI);
                        }

                        //string AndroidMessageString = AmazonSNSHelper.GenerateMessageString(AmazonSNSHelper.OSAndroid, Message, UserId, NotifyId, NotifyTypeId, NotifyType, ActionUserId, mUserAction.UserName, mUserAction.UserAvatar, ImageItemId: ImageItemId, ImageUrl: ImageUrl);
                        //string AndroidENMessageString = AmazonSNSHelper.GenerateMessageString(AmazonSNSHelper.OSAndroid, MessageEN, UserId, NotifyId, NotifyTypeId, NotifyType, ActionUserId, mUserAction.UserName, mUserAction.UserAvatar, ImageItemId: ImageItemId, ImageUrl: ImageUrl);
                        //string iOSMessageString = AmazonSNSHelper.GenerateMessageString(AmazonSNSHelper.OSiOS, Message, UserId, NotifyId, NotifyTypeId, NotifyType, ActionUserId, mUserAction.UserName, mUserAction.UserAvatar, ImageItemId: ImageItemId, ImageUrl: ImageUrl);
                        //string iOSENMessageString = AmazonSNSHelper.GenerateMessageString(AmazonSNSHelper.OSiOS, MessageEN, UserId, NotifyId, NotifyTypeId, NotifyType, ActionUserId, mUserAction.UserName, mUserAction.UserAvatar, ImageItemId: ImageItemId, ImageUrl: ImageUrl);

                        //List<UserEndpointModel> lstEndpoints = DAL.GetUserEndpoints(UserId);

                        //foreach (UserEndpointModel mEndpoint in lstEndpoints)
                        //{
                        //    AmazonSNSHelper.PublishToEndpoint(UserId, mEndpoint.EndpointArn, Subject,
                        //        (mEndpoint.OS == AmazonSNSHelper.OSAndroid
                        //            ? ((mEndpoint.LanguageCode == AmazonSNSHelper.VNLanguageCode) ? AndroidMessageString : AndroidENMessageString)
                        //            : ((mEndpoint.LanguageCode == AmazonSNSHelper.VNLanguageCode) ? iOSMessageString : iOSENMessageString)));
                        //}
                    }
                }
                else
                {
                    if(DAL.DeleteOldLikeNotify(UserId, ActionUserId, ImageItemId, NotifyType))
                    {
                        long NotifyId = DAL.AddUserNotify(UserId, NotifyTypeId, NotifyType, Message, MessageEN, ActionUserId, null, ImageItemId, null, null, null);
                    }

                }
            }
        }

        //ko push notify sự kiện này mà chỉ ghi log
        public static void FollowingHasLikeNotify(long ActionUserId, long ImageItemId, long? LastId, int Count)
        {
            //int NotifyTypeId = (int)NotifyTypes.FollowingLike;
            //string NotifyType = NotifyTypes.FollowingLike.ToString();

            //List<UserModel> lst = DAL.GetUserFollowerList(ActionUserId, LastId, Count, PushNewActivity: true);
            //UserModel mUserAction = DAL.GetUserInfo(ActionUserId);

            //ImageItemModel mImage = DAL.GetImageItemInfo(ImageItemId);
            //long UserId = mImage.UserId;

            ////todo: nghiên cứu xem để gì trên này
            //string Subject = "";

            ////gán Message tương ứng với sự kiện
            //Resources.MessageResource.Culture = new System.Globalization.CultureInfo("vi-VN");
            //string Message = string.Format(Resources.MessageResource.FollowingLike, mUserAction.UserName);
            //Resources.MessageResource.Culture = new System.Globalization.CultureInfo("EN");
            //string MessageEN = string.Format(Resources.MessageResource.FollowingLike, mUserAction.UserName);

            //string ImageUrl = mImage.ImageMedium;

            //foreach (UserModel m in lst)
            //{
            //    if (m.UserId != UserId && m.UserId != ActionUserId && m.UserId != MTPUserId)
            //    {
            //        if (!DAL.CheckUserHasNotifyLikeBefore(m.UserId, ActionUserId, ImageItemId))
            //        {
            //            long NotifyId = DAL.AddUserNotify(m.UserId, NotifyTypeId, NotifyType, Message, MessageEN, ActionUserId, null, ImageItemId, null, null, null);

            //            //ko push notify sự kiện này vì sẽ quá nhiều

            //            //UserNotifySettingModel mSetting = DAL.GetUserNotifySetting(m.UserId);

            //            ////cấu hình nhận notify thì mới tiếp tục
            //            //if (mSetting.FollowingAction)
            //            //{
            //            //NotifyData Data = new NotifyData
            //            //{
            //            //    notifyId = NotifyId,
            //            //    notifyTypeId = NotifyTypeId,
            //            //    notifyType = NotifyType,
            //            //    actionUserId = ActionUserId,
            //            //    actionUserName = mUserAction.UserName,
            //            //    actionUserAvatar = mUserAction.UserAvatar,
            //            //    imageItemId = ImageItemId,
            //            //    imageUrl = ImageUrl
            //            //};

            //            //List<UserEndpointModel> lstEndpoints = DAL.GetUserEndpoints(m.UserId);
            //            //var lstEN = lstEndpoints.Where(c => c.LanguageCode == LanguageCodes.English.ToUpper()).ToList();
            //            //if (lstEN.Count > 0)
            //            //{
            //            //    OnesignalHelper.SendNotifications(Subject, MessageEN, Data, lstEN);
            //            //}
            //            //var lstVI = lstEndpoints.Where(c => c.LanguageCode == LanguageCodes.Vietnamese.ToUpper()).ToList();
            //            //if (lstVI.Count > 0)
            //            //{
            //            //    OnesignalHelper.SendNotifications(Subject, Message, Data, lstVI);
            //            //}

            //            //    //để trong này vì phải đợi lấy NotifyId và UserId của người nhận
            //            //    string AndroidMessageString = AmazonSNSHelper.GenerateMessageString(AmazonSNSHelper.OSAndroid, Message, m.UserId, NotifyId, NotifyTypeId, NotifyType, ActionUserId, mUserAction.UserName, mUserAction.UserAvatar, ImageItemId: ImageItemId, ImageUrl: ImageUrl);
            //            //    string AndroidENMessageString = AmazonSNSHelper.GenerateMessageString(AmazonSNSHelper.OSAndroid, MessageEN, m.UserId, NotifyId, NotifyTypeId, NotifyType, ActionUserId, mUserAction.UserName, mUserAction.UserAvatar, ImageItemId: ImageItemId, ImageUrl: ImageUrl);
            //            //    string iOSMessageString = AmazonSNSHelper.GenerateMessageString(AmazonSNSHelper.OSiOS, Message, m.UserId, NotifyId, NotifyTypeId, NotifyType, ActionUserId, mUserAction.UserName, mUserAction.UserAvatar, ImageItemId: ImageItemId, ImageUrl: ImageUrl);
            //            //    string iOSENMessageString = AmazonSNSHelper.GenerateMessageString(AmazonSNSHelper.OSiOS, MessageEN, m.UserId, NotifyId, NotifyTypeId, NotifyType, ActionUserId, mUserAction.UserName, mUserAction.UserAvatar, ImageItemId: ImageItemId, ImageUrl: ImageUrl);

            //            //    List<UserEndpointModel> lstEndpoints = DAL.GetUserEndpoints(m.UserId);
            //            //    foreach (UserEndpointModel mEndpoint in lstEndpoints)
            //            //    {
            //            //        AmazonSNSHelper.PublishToEndpoint(m.UserId, mEndpoint.EndpointArn, Subject,
            //            //            (mEndpoint.OS == AmazonSNSHelper.OSAndroid
            //            //                ? ((mEndpoint.LanguageCode == AmazonSNSHelper.VNLanguageCode) ? AndroidMessageString : AndroidENMessageString)
            //            //                : ((mEndpoint.LanguageCode == AmazonSNSHelper.VNLanguageCode) ? iOSMessageString : iOSENMessageString)));
            //            //    }
            //            //}
            //        }
            //        else
            //        {
            //            if (DAL.DeleteOldLikeNotify(m.UserId, ActionUserId, ImageItemId, NotifyType))
            //            {
            //                long NotifyId = DAL.AddUserNotify(m.UserId, NotifyTypeId, NotifyType, Message, MessageEN, ActionUserId, null, ImageItemId, null, null, null);
            //            }

            //        }
            //    }
            //}

            ////nếu còn thì gọi đệ quy
            //if (lst.Count == Count)
            //{
            //    FollowingHasLikeNotify(ActionUserId, ImageItemId, lst.LastOrDefault().UserId, Count);
            //}
        }
    }
}