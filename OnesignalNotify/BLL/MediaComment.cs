﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using OnesignalNotify.Model;
using OneSignal.CSharp.SDK.Resources;

namespace OnesignalNotify
{
    public partial class BLL
    {
        public static void NewMediaCommentNotify(long ActionUserId, int MediaId, long CommentId)
        {
            MediaComment mComment = DAL.GetMediaComment(CommentId);

            MediaModel mMedia = DAL.GetMedia(MediaId);

            UserModel mUserAction = DAL.GetUserInfo(ActionUserId);

            long? ReplyToUserId = null;
            bool HasReplyNotify = false;

            if (mComment.ReplyToId.HasValue)
            {
                MediaComment mReplyToComment = DAL.GetMediaComment(mComment.ReplyToId.Value);
                ReplyToUserId = mReplyToComment.UserId;

                UserNotifySettingModel mReplyToSetting = DAL.GetUserNotifySetting(ReplyToUserId.Value);
                if (mReplyToSetting.CommentHasReply)
                {
                    HasReplyNotify = true;
                    MediaCommentHasReply(mUserAction, mMedia, CommentId, ReplyToUserId.Value, mComment.Comment);
                }
            }

            List<string> lstUserName = Utility.GetMentionUserNames(mComment.Comment);

            if (lstUserName.Count > 0)
            {
                List<long> lstUserMentionIds = DAL.GetUserIdFromUserNameList(lstUserName);

                //bỏ người action ra
                lstUserMentionIds.Remove(ActionUserId);

                if (HasReplyNotify)
                {
                    lstUserMentionIds.Remove(ReplyToUserId.Value);
                }

                MentionInMediaCommentNotify(mUserAction, mMedia, CommentId, lstUserMentionIds, mComment.Comment);
            }
        }

        public static void MediaCommentHasReply(UserModel mUserAction, MediaModel mMedia, long CommentId, long UserId, string Comment)
        {
            int NotifyTypeId = (int)NotifyTypes.CommentHasReply;
            string NotifyType = NotifyTypes.CommentHasReply.ToString();

            //gán Message tương ứng với sự kiện
            Resources.MessageResource.Culture = new System.Globalization.CultureInfo("vi-VN");
            string Message = string.Format(Resources.MessageResource.CommentHasReply, mUserAction.UserName, Comment);
            Resources.MessageResource.Culture = new System.Globalization.CultureInfo("EN");
            string MessageEN = string.Format(Resources.MessageResource.CommentHasReply, mUserAction.UserName, Comment);

            string ImageUrl = mMedia.Cover;

            string Subject = "";
            if (UserId != mUserAction.UserId && UserId != MTPUserId)
            {
                long NotifyId = DAL.AddUserNotify(UserId, NotifyTypeId, NotifyType, Message, MessageEN, mUserAction.UserId, null, null, CommentId, null, null, Comment, mMedia.MediaId);

                //if (!DAL.CheckUserHasCommentNotifyWithinAnPeriod(UserId, mImage.ImageItemId, InMinutes))
                //{
                UserNotifySettingModel mSetting = DAL.GetUserNotifySetting(UserId);

                //cấu hình nhận notify thì mới tiếp tục
                if (mSetting.CommentHasReply)
                {
                    //để trong này vì phải đợi lấy NotifyId và UserId của người nhận
                    NotifyData Data = new NotifyData
                    {
                        NotifyId = NotifyId,
                        NotifyTypeId = NotifyTypeId,
                        NotifyType = NotifyType,
                        ActionUserId = mUserAction.UserId,
                        ActionUserName = mUserAction.UserName,
                        ActionUserAvatar = mUserAction.UserAvatar,
                        MediaId = mMedia.MediaId,
                        ImageUrl = ImageUrl,
                        CommentId = CommentId
                    };

                    List<UserEndpointModel> lstEndpoints = DAL.GetUserEndpoints(UserId);
                    var lstEN = lstEndpoints.Where(c => c.LanguageCode == LanguageCodes.English.ToUpper()).ToList();
                    if (lstEN.Count > 0)
                    {
                        OnesignalHelper.SendNotifications(Subject, MessageEN, Data, lstEN);
                    }
                    var lstVI = lstEndpoints.Where(c => c.LanguageCode == LanguageCodes.Vietnamese.ToUpper()).ToList();
                    if (lstVI.Count > 0)
                    {
                        OnesignalHelper.SendNotifications(Subject, Message, Data, lstVI);
                    }
                }
                //}
            }
        }

        public static void MentionInMediaCommentNotify(UserModel mUserAction, MediaModel mMedia, long CommentId, List<long> lstUserMentionIds, string Comment)
        {
            int NotifyTypeId = (int)NotifyTypes.MentionInComment;
            string NotifyType = NotifyTypes.MentionInComment.ToString();

            //gán Message tương ứng với sự kiện
            Resources.MessageResource.Culture = new System.Globalization.CultureInfo("vi-VN");
            string Message = string.Format(Resources.MessageResource.MentionInComment, mUserAction.UserName, Comment);
            Resources.MessageResource.Culture = new System.Globalization.CultureInfo("EN");
            string MessageEN = string.Format(Resources.MessageResource.MentionInComment, mUserAction.UserName, Comment);

            string ImageUrl = mMedia.Cover;
            //todo: nghiên cứu xem để gì trên này
            string Subject = "";

            foreach (long UserId in lstUserMentionIds)
            {
                if (UserId != mUserAction.UserId && UserId != MTPUserId)
                {
                    long NotifyId = DAL.AddUserNotify(UserId, NotifyTypeId, NotifyType, Message, MessageEN, mUserAction.UserId, null, null, CommentId, null, null, Comment, mMedia.MediaId);

                    //if (!DAL.CheckUserHasCommentNotifyWithinAnPeriod(UserId, mImage.ImageItemId, InMinutes))
                    //{
                    UserNotifySettingModel mSetting = DAL.GetUserNotifySetting(UserId);

                    //cấu hình nhận notify thì mới tiếp tục
                    if (mSetting.MentionInComment)
                    {
                        //để trong này vì phải đợi lấy NotifyId và UserId của người nhận
                        NotifyData Data = new NotifyData
                        {
                            NotifyId = NotifyId,
                            NotifyTypeId = NotifyTypeId,
                            NotifyType = NotifyType,
                            ActionUserId = mUserAction.UserId,
                            ActionUserName = mUserAction.UserName,
                            ActionUserAvatar = mUserAction.UserAvatar,
                            MediaId = mMedia.MediaId,
                            ImageUrl = ImageUrl,
                            CommentId = CommentId
                        };

                        List<UserEndpointModel> lstEndpoints = DAL.GetUserEndpoints(UserId);
                        var lstEN = lstEndpoints.Where(c => c.LanguageCode == LanguageCodes.English.ToUpper()).ToList();
                        if (lstEN.Count > 0)
                        {
                            OnesignalHelper.SendNotifications(Subject, MessageEN, Data, lstEN);
                        }
                        var lstVI = lstEndpoints.Where(c => c.LanguageCode == LanguageCodes.Vietnamese.ToUpper()).ToList();
                        if (lstVI.Count > 0)
                        {
                            OnesignalHelper.SendNotifications(Subject, Message, Data, lstVI);
                        }
                    }
                    //}
                }
            }
        }

        public static void LikeMediaCommentNotify(long ActionUserId, long CommentId)
        {
            int NotifyTypeId = (int)NotifyTypes.LikeComment;
            string NotifyType = NotifyTypes.LikeComment.ToString();

            UserModel mUserAction = DAL.GetUserInfo(ActionUserId);

            MediaComment mComment = DAL.GetMediaComment(CommentId);

            int MediaId = (int)mComment.MediaId.Value;
            MediaModel mMedia = DAL.GetMedia(MediaId);

            long UserId = mComment.UserId.Value;
            if (ActionUserId != UserId && UserId != MTPUserId)
            {
                if (!DAL.CheckUserHasNotifyLikeCommentMediaBefore(UserId, ActionUserId, CommentId))
                {

                    //todo: nghiên cứu xem để gì trên này
                    string Subject = "";

                    //gán Message tương ứng với sự kiện
                    Resources.MessageResource.Culture = new System.Globalization.CultureInfo("vi-VN");
                    string Message = string.Format(Resources.MessageResource.LikeComment, mUserAction.UserName, mComment.Comment);
                    Resources.MessageResource.Culture = new System.Globalization.CultureInfo("EN");
                    string MessageEN = string.Format(Resources.MessageResource.LikeComment, mUserAction.UserName, mComment.Comment);

                    string ImageUrl = mMedia.Cover;

                    long NotifyId = DAL.AddUserNotify(UserId, NotifyTypeId, NotifyType, Message, MessageEN, ActionUserId, null, null, CommentId, null, null, mComment.Comment, MediaId);

                    if (!DAL.CheckUserHasNotifyLikeCommentMediaWithinPeriod(NotifyId, UserId, CommentId, LikeCommentNotifyPeriodInMinute))
                    {
                        UserNotifySettingModel mSetting = DAL.GetUserNotifySetting(UserId);
                        if (mSetting.CommentHasLike)
                        {
                            //để trong này vì phải đợi lấy NotifyId và UserId của người nhận
                            NotifyData Data = new NotifyData
                            {
                                NotifyId = NotifyId,
                                NotifyTypeId = NotifyTypeId,
                                NotifyType = NotifyType,
                                ActionUserId = ActionUserId,
                                ActionUserName = mUserAction.UserName,
                                ActionUserAvatar = mUserAction.UserAvatar,
                                MediaId = MediaId,
                                ImageUrl = ImageUrl,
                                CommentId = CommentId
                            };

                            List<UserEndpointModel> lstEndpoints = DAL.GetUserEndpoints(UserId);
                            var lstEN = lstEndpoints.Where(c => c.LanguageCode == LanguageCodes.English.ToUpper()).ToList();
                            if (lstEN.Count > 0)
                            {
                                OnesignalHelper.SendNotifications(Subject, MessageEN, Data, lstEN);
                            }
                            var lstVI = lstEndpoints.Where(c => c.LanguageCode == LanguageCodes.Vietnamese.ToUpper()).ToList();
                            if (lstVI.Count > 0)
                            {
                                OnesignalHelper.SendNotifications(Subject, Message, Data, lstVI);
                            }
                        }
                    }
                }
            }
        }
    }
}