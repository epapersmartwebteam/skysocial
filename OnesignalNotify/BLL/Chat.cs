﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using SNSNotify.Model;

namespace SNSNotify
{
    public partial class BLL
    {
        public static void NewChatMessageNotify(long ActionUserId, long ChatRoomId, long ChatLogId)
        {
            int NotifyTypeId = (int)NotifyTypes.NewChatMessage;
            string NotifyType = NotifyTypes.NewChatMessage.ToString();

            ChatNotifyLog mLastNotifyLog = DAL.GetLastestChatNotifyLog(ChatRoomId, ActionUserId);

            //nếu cùng 1 user chat trong 1 room đã notify trong vòng 1 phút thì ko notify nữa
            if (mLastNotifyLog == null || DateTime.Now.Subtract(mLastNotifyLog.CreateDate.Value).Minutes > 1)
            {
                ChatLogModel mChatLog = DAL.GetChatLogInfo(ChatLogId);
                ChatRoomModel mChatRoom = DAL.GetChatRoomInfo(ChatRoomId);

                DAL.AddChatNotifyLog(ChatRoomId, ActionUserId, mChatLog.Message, mChatLog.IsFile, mChatLog.IsImage, mChatLog.IsLink, mChatLog.IsAction);

                UserModel mUserAction = DAL.GetUserInfo(ActionUserId);

                List<UserModel> lst = DAL.GetChatRoomMemberEnableNotify(ChatRoomId);                

                //todo: nghiên cứu xem để gì trên này
                string Subject = "";

                string Message = "";
                string MessageEN = "";

                string ChatRoomName = mChatRoom.ChatRoomName;

                foreach (UserModel m in lst)
                {
                    //ko phải là user thực hiện thì mới tiếp tục
                    if (m.UserId != ActionUserId)
                    {
                        UserNotifySettingModel mSetting = DAL.GetUserNotifySetting(m.UserId);

                        //cấu hình nhận notify thì mới tiếp tục
                        if (mSetting.Chat)
                        {
                            if (mChatRoom.RoomTypeId == (int)RoomTypes.Private)
                            {
                                //gán Message tương ứng với sự kiện
                                Resources.MessageResource.Culture = new System.Globalization.CultureInfo("vi-VN");
                                Message = string.Format(Resources.MessageResource.NewChatMessagePrivate, mUserAction.UserName, mChatLog.Message);
                                Resources.MessageResource.Culture = new System.Globalization.CultureInfo("EN");
                                MessageEN = string.Format(Resources.MessageResource.NewChatMessagePrivate, mUserAction.UserName, mChatLog.Message);
                            }
                            else
                            {
                                if (string.IsNullOrEmpty(ChatRoomName))
                                {
                                    ChatRoomName = string.Join(", ", lst.Where(u => u.UserId != m.UserId).Take(5).Select(u => u.UserName));
                                }
                                //gán Message tương ứng với sự kiện
                                Resources.MessageResource.Culture = new System.Globalization.CultureInfo("vi-VN");
                                Message = string.Format(Resources.MessageResource.NewChatMessage, mUserAction.UserName, ChatRoomName);
                                Resources.MessageResource.Culture = new System.Globalization.CultureInfo("EN");
                                MessageEN = string.Format(Resources.MessageResource.NewChatMessage, mUserAction.UserName, ChatRoomName);
                            }

                            //ko cần thêm vào notify
                            long NotifyId = 0;

                            //để trong này vì phải đợi lấy NotifyId và UserId của người nhận
                            string AndroidMessageString = AmazonSNSHelper.GenerateMessageString(AmazonSNSHelper.OSAndroid, Message, m.UserId, NotifyId, NotifyTypeId, NotifyType, ActionUserId, mUserAction.UserName, mUserAction.UserAvatar, ChatRoomId: ChatRoomId, ChatLogId: ChatLogId);
                            string AndroidENMessageString = AmazonSNSHelper.GenerateMessageString(AmazonSNSHelper.OSAndroid, MessageEN, m.UserId, NotifyId, NotifyTypeId, NotifyType, ActionUserId, mUserAction.UserName, mUserAction.UserAvatar, ChatRoomId: ChatRoomId, ChatLogId: ChatLogId);
                            string iOSMessageString = AmazonSNSHelper.GenerateMessageString(AmazonSNSHelper.OSiOS, Message, m.UserId, NotifyId, NotifyTypeId, NotifyType, ActionUserId, mUserAction.UserName, mUserAction.UserAvatar, ChatRoomId: ChatRoomId, ChatLogId: ChatLogId);
                            string iOSENMessageString = AmazonSNSHelper.GenerateMessageString(AmazonSNSHelper.OSiOS, MessageEN, m.UserId, NotifyId, NotifyTypeId, NotifyType, ActionUserId, mUserAction.UserName, mUserAction.UserAvatar, ChatRoomId: ChatRoomId, ChatLogId: ChatLogId);

                            List<UserEndpointModel> lstEndpoints = DAL.GetUserEndpoints(m.UserId);
                            foreach (UserEndpointModel mEndpoint in lstEndpoints)
                            {
                                AmazonSNSHelper.PublishToEndpoint(m.UserId, mEndpoint.EndpointArn, Subject,
                                    (mEndpoint.OS == AmazonSNSHelper.OSAndroid
                                        ? ((mEndpoint.LanguageCode == AmazonSNSHelper.VNLanguageCode) ? AndroidMessageString : AndroidENMessageString)
                                        : ((mEndpoint.LanguageCode == AmazonSNSHelper.VNLanguageCode) ? iOSMessageString : iOSENMessageString)));
                            }
                        }
                    }
                }
            }
        }

        public static void NewChatFileNotify(long ActionUserId, long ChatRoomId, long ChatLogId)
        {
            int NotifyTypeId = (int)NotifyTypes.NewChatFile;
            string NotifyType = NotifyTypes.NewChatFile.ToString();

            ChatNotifyLog mLastNotifyLog = DAL.GetLastestChatNotifyLog(ChatRoomId, ActionUserId);

            //nếu cùng 1 user chat trong 1 room đã notify trong vòng 1 phút thì ko notify nữa
            if (mLastNotifyLog == null || DateTime.Now.Subtract(mLastNotifyLog.CreateDate.Value).Minutes > 1)
            {
                ChatLogModel mChatLog = DAL.GetChatLogInfo(ChatLogId);
                ChatRoomModel mChatRoom = DAL.GetChatRoomInfo(ChatRoomId);

                DAL.AddChatNotifyLog(ChatRoomId, ActionUserId, mChatLog.Message, mChatLog.IsFile, mChatLog.IsImage, mChatLog.IsLink, mChatLog.IsAction);

                UserModel mUserAction = DAL.GetUserInfo(ActionUserId);

                List<UserModel> lst = DAL.GetChatRoomMemberEnableNotify(ChatRoomId);

                //todo: nghiên cứu xem để gì trên này
                string Subject = "";

                string Message = "";
                string MessageEN = "";

                string ChatRoomName = mChatRoom.ChatRoomName;

                string ImageUrl = mChatLog.LinkUrl;

                foreach (UserModel m in lst)
                {
                    //ko phải là user thực hiện thì mới tiếp tục
                    if (m.UserId != ActionUserId)
                    {
                        UserNotifySettingModel mSetting = DAL.GetUserNotifySetting(m.UserId);

                        //cấu hình nhận notify thì mới tiếp tục
                        if (mSetting.Chat)
                        {
                            if (mChatRoom.RoomTypeId == (int)RoomTypes.Private)
                            {
                                //gán Message tương ứng với sự kiện
                                Resources.MessageResource.Culture = new System.Globalization.CultureInfo("vi-VN");
                                Message = string.Format(Resources.MessageResource.NewChatFilePrivate, mUserAction.UserName, mChatLog.Message);
                                Resources.MessageResource.Culture = new System.Globalization.CultureInfo("EN");
                                MessageEN = string.Format(Resources.MessageResource.NewChatFilePrivate, mUserAction.UserName, mChatLog.Message);
                            }
                            else
                            {
                                if (string.IsNullOrEmpty(ChatRoomName))
                                {
                                    ChatRoomName = string.Join(", ", lst.Where(u => u.UserId != m.UserId).Take(5).Select(u => u.UserName));
                                }
                                //gán Message tương ứng với sự kiện
                                Resources.MessageResource.Culture = new System.Globalization.CultureInfo("vi-VN");
                                Message = string.Format(Resources.MessageResource.NewChatFile, mUserAction.UserName, ChatRoomName);
                                Resources.MessageResource.Culture = new System.Globalization.CultureInfo("EN");
                                MessageEN = string.Format(Resources.MessageResource.NewChatFile, mUserAction.UserName, ChatRoomName);
                            }

                            //ko cần thêm vào notify
                            long NotifyId = 0;

                            //để trong này vì phải đợi lấy NotifyId và UserId của người nhận
                            string AndroidMessageString = AmazonSNSHelper.GenerateMessageString(AmazonSNSHelper.OSAndroid, Message, m.UserId, NotifyId, NotifyTypeId, NotifyType, ActionUserId, mUserAction.UserName, mUserAction.UserAvatar, ChatRoomId: ChatRoomId, ChatLogId: ChatLogId, ImageUrl: ImageUrl);
                            string AndroidENMessageString = AmazonSNSHelper.GenerateMessageString(AmazonSNSHelper.OSAndroid, MessageEN, m.UserId, NotifyId, NotifyTypeId, NotifyType, ActionUserId, mUserAction.UserName, mUserAction.UserAvatar, ChatRoomId: ChatRoomId, ChatLogId: ChatLogId, ImageUrl: ImageUrl);
                            string iOSMessageString = AmazonSNSHelper.GenerateMessageString(AmazonSNSHelper.OSiOS, Message, m.UserId, NotifyId, NotifyTypeId, NotifyType, ActionUserId, mUserAction.UserName, mUserAction.UserAvatar, ChatRoomId: ChatRoomId, ChatLogId: ChatLogId, ImageUrl: ImageUrl);
                            string iOSENMessageString = AmazonSNSHelper.GenerateMessageString(AmazonSNSHelper.OSiOS, MessageEN, m.UserId, NotifyId, NotifyTypeId, NotifyType, ActionUserId, mUserAction.UserName, mUserAction.UserAvatar, ChatRoomId: ChatRoomId, ChatLogId: ChatLogId, ImageUrl: ImageUrl);

                            List<UserEndpointModel> lstEndpoints = DAL.GetUserEndpoints(m.UserId);
                            foreach (UserEndpointModel mEndpoint in lstEndpoints)
                            {
                                AmazonSNSHelper.PublishToEndpoint(m.UserId, mEndpoint.EndpointArn, Subject,
                                    (mEndpoint.OS == AmazonSNSHelper.OSAndroid
                                        ? ((mEndpoint.LanguageCode == AmazonSNSHelper.VNLanguageCode) ? AndroidMessageString : AndroidENMessageString)
                                        : ((mEndpoint.LanguageCode == AmazonSNSHelper.VNLanguageCode) ? iOSMessageString : iOSENMessageString)));
                            }
                        }
                    }
                }
            }
        }
    }
}