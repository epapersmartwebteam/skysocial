﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using OnesignalNotify.Model;
using OneSignal.CSharp.SDK.Resources;

namespace OnesignalNotify
{
    public partial class BLL
    {
        public static int CommentPeriodInMinutes = Convert.ToInt32(System.Configuration.ConfigurationManager.AppSettings.Get("CommentNotifyPeriodInMinute"));
        public static int LikeCommentNotifyPeriodInMinute = Convert.ToInt32(System.Configuration.ConfigurationManager.AppSettings.Get("LikeCommentNotifyPeriodInMinute"));

        //Ưu tiên notify theo thứ tự: Đc trả lời, Mention, Có comment mới trong hình của mình và tuỳ thuộc vào cấu hình notify.

        //Tách danh sách mention trong Comment ra
        //Nếu người đc trả lời nằm trong danh sách mention và có cấu hình nhận notify trả lời thì bỏ người trả lời khỏi mention 
        //=> notify trả lời + notify cho danh sách mention còn lại. 
        //=> Nếu người chủ hình không là người đc trả lời & ko có trong danh sách mention
        //=> notify có comment mới cho chủ hình theo cấu hình nhận notify comment mới

        //Nếu người đc trả lời ko là chủ hình và cấu hình notify trả lời
        //Nếu chủ hình trong danh sách mention và có cấu hình nhận notify comment mới thì remove chủ hình khỏi ds mention
        //=> notify trả lời + notify cho chủ hình + notify cho danh sách mention còn lại. => Done

        public static void NewCommentNotify(long ActionUserId, long ImageItemId, long CommentId)
        {
            int NotifyTypeId = (int)NotifyTypes.NewComment;
            string NotifyType = NotifyTypes.NewComment.ToString();

            ImageItemModel mImage = DAL.GetImageItemInfo(ImageItemId);
            long UserId = mImage.UserId;

            UserModel mUserAction = DAL.GetUserInfo(ActionUserId);

            ImageComment mComment = DAL.GetImageComment(CommentId);

            long? ReplyToUserId = null;
            bool HasReplyNotify = false;

            if (mComment.ReplyToId.HasValue)
            {
                ImageComment mReplyToComment = DAL.GetImageComment(mComment.ReplyToId.Value);
                ReplyToUserId = mReplyToComment.UserId;

                UserNotifySettingModel mReplyToSetting = DAL.GetUserNotifySetting(ReplyToUserId.Value);
                if (mReplyToSetting.CommentHasReply)
                {
                    HasReplyNotify = true;
                    CommentHasReply(mUserAction, mImage, CommentId, mComment.ReplyToId.Value, ReplyToUserId.Value, mComment.Comment);
                }
            }

            bool HasNotifyMentionToOwner = false;
            UserNotifySettingModel mSetting = DAL.GetUserNotifySetting(UserId);

            List<string> lstUserName = Utility.GetMentionUserNames(mComment.Comment);
            if (lstUserName.Count > 0)
            {
                List<long> lstUserMentionIds = DAL.GetUserIdFromUserNameList(lstUserName);

                //bỏ người action ra
                lstUserMentionIds.Remove(ActionUserId);

                if (HasReplyNotify)
                {
                    lstUserMentionIds.Remove(ReplyToUserId.Value);
                }

                //kiểm tra có owner của hình nằm trong mention thì đánh dấu đã notify
                if (lstUserMentionIds.Contains(UserId))
                {
                    if (mSetting.MentionInComment)
                    {
                        HasNotifyMentionToOwner = true;
                    }
                    else
                    {
                        lstUserMentionIds.Remove(UserId);
                    }
                }

                MentionInCommentNotify(mUserAction, mImage, CommentId, lstUserMentionIds, mComment.Comment);
            }
            //todo: nghiên cứu xem để gì trên này
            string Subject = "";

            string ImageUrl = mImage.ImageMedium;

            //ko là người action
            //chưa notify trong mention
            //ko là người đc reply hoặc là người đc reply nhưng ko nhận notify reply
            if (UserId != ActionUserId && !HasNotifyMentionToOwner
                && ((ReplyToUserId == UserId && !HasReplyNotify) || ReplyToUserId != UserId))
            {
                //gán Message tương ứng với sự kiện
                Resources.MessageResource.Culture = new System.Globalization.CultureInfo("vi-VN");
                string Message = string.Format(Resources.MessageResource.NewComment, mUserAction.UserName, mComment.Comment);
                Resources.MessageResource.Culture = new System.Globalization.CultureInfo("EN");
                string MessageEN = string.Format(Resources.MessageResource.NewComment, mUserAction.UserName, mComment.Comment);

                bool NeedNotify = !DAL.CheckUserHasCommentNotifyWithinPeriod(UserId, ImageItemId, CommentPeriodInMinutes);

                long NotifyId = DAL.AddUserNotify(UserId, NotifyTypeId, NotifyType, Message, MessageEN, ActionUserId, null, ImageItemId, CommentId, null, null, mComment.Comment);

                if (NeedNotify)
                {

                    if (mSetting.NewComment)
                    {
                        //để trong này vì phải đợi lấy NotifyId và UserId của người nhận
                        NotifyData Data = new NotifyData
                        {
                            NotifyId = NotifyId,
                            NotifyTypeId = NotifyTypeId,
                            NotifyType = NotifyType,
                            ActionUserId = ActionUserId,
                            ActionUserName = mUserAction.UserName,
                            ActionUserAvatar = mUserAction.UserAvatar,
                            ImageItemId = ImageItemId,
                            ImageUrl = ImageUrl,
                            CommentId = CommentId
                        };

                        List<UserEndpointModel> lstEndpoints = DAL.GetUserEndpoints(UserId);

                        var lstEN = lstEndpoints.Where(c => c.LanguageCode == LanguageCodes.English.ToUpper()).ToList();
                        if (lstEN.Count > 0)
                        {
                            OnesignalHelper.SendNotifications(Subject, MessageEN, Data, lstEN);
                        }
                        var lstVI = lstEndpoints.Where(c => c.LanguageCode == LanguageCodes.Vietnamese.ToUpper()).ToList();
                        if (lstVI.Count > 0)
                        {
                            OnesignalHelper.SendNotifications(Subject, Message, Data, lstVI);
                        }
                    }
                }
            }
        }
        public static void CommentHasReply(UserModel mUserAction, ImageItemModel mImage, long CommentId, long ReplyToId, long UserId, string Comment)
        {
            int NotifyTypeId = (int)NotifyTypes.CommentHasReply;
            string NotifyType = NotifyTypes.CommentHasReply.ToString();

            //gán Message tương ứng với sự kiện
            Resources.MessageResource.Culture = new System.Globalization.CultureInfo("vi-VN");
            string Message = string.Format(Resources.MessageResource.CommentHasReply, mUserAction.UserName, Comment);
            Resources.MessageResource.Culture = new System.Globalization.CultureInfo("EN");
            string MessageEN = string.Format(Resources.MessageResource.CommentHasReply, mUserAction.UserName, Comment);

            string ImageUrl = mImage.ImageMedium;
            //todo: nghiên cứu xem để gì trên này
            string Subject = "";

            if (UserId != mUserAction.UserId && UserId != MTPUserId)
            {
                long NotifyId = DAL.AddUserNotify(UserId, NotifyTypeId, NotifyType, Message, MessageEN, mUserAction.UserId, null, mImage.ImageItemId, CommentId, null, null, Comment);

                //if (!DAL.CheckUserHasCommentNotifyWithinAnPeriod(UserId, mImage.ImageItemId, InMinutes))
                //{
                UserNotifySettingModel mSetting = DAL.GetUserNotifySetting(UserId);

                //cấu hình nhận notify thì mới tiếp tục
                if (mSetting.CommentHasReply)
                {
                    //để trong này vì phải đợi lấy NotifyId và UserId của người nhận
                    NotifyData Data = new NotifyData
                    {
                        NotifyId = NotifyId,
                        NotifyTypeId = NotifyTypeId,
                        NotifyType = NotifyType,
                        ActionUserId = mUserAction.UserId,
                        ActionUserName = mUserAction.UserName,
                        ActionUserAvatar = mUserAction.UserAvatar,
                        ImageItemId = mImage.ImageItemId,
                        ImageUrl = ImageUrl,
                        CommentId = CommentId
                    };

                    List<UserEndpointModel> lstEndpoints = DAL.GetUserEndpoints(UserId);
                    var lstEN = lstEndpoints.Where(c => c.LanguageCode == LanguageCodes.English.ToUpper()).ToList();
                    if (lstEN.Count > 0)
                    {
                        OnesignalHelper.SendNotifications(Subject, MessageEN, Data, lstEN);
                    }
                    var lstVI = lstEndpoints.Where(c => c.LanguageCode == LanguageCodes.Vietnamese.ToUpper()).ToList();
                    if (lstVI.Count > 0)
                    {
                        OnesignalHelper.SendNotifications(Subject, Message, Data, lstVI);
                    }
                }
                //}
            }
        }
        public static void MentionInCommentNotify(UserModel mUserAction, ImageItemModel mImage, long CommentId, List<long> lstUserMentionIds, string Comment)
        {
            int NotifyTypeId = (int)NotifyTypes.MentionInComment;
            string NotifyType = NotifyTypes.MentionInComment.ToString();

            //gán Message tương ứng với sự kiện
            Resources.MessageResource.Culture = new System.Globalization.CultureInfo("vi-VN");
            string Message = string.Format(Resources.MessageResource.MentionInComment, mUserAction.UserName, Comment);
            Resources.MessageResource.Culture = new System.Globalization.CultureInfo("EN");
            string MessageEN = string.Format(Resources.MessageResource.MentionInComment, mUserAction.UserName, Comment);

            string ImageUrl = mImage.ImageMedium;
            //todo: nghiên cứu xem để gì trên này
            string Subject = "";

            foreach (long UserId in lstUserMentionIds)
            {
                if (UserId != mUserAction.UserId && UserId != MTPUserId)
                {
                    long NotifyId = DAL.AddUserNotify(UserId, NotifyTypeId, NotifyType, Message, MessageEN, mUserAction.UserId, null, mImage.ImageItemId, CommentId, null, null, Comment);

                    //if (!DAL.CheckUserHasCommentNotifyWithinAnPeriod(UserId, mImage.ImageItemId, InMinutes))
                    //{
                    UserNotifySettingModel mSetting = DAL.GetUserNotifySetting(UserId);

                    //cấu hình nhận notify thì mới tiếp tục
                    if (mSetting.MentionInComment)
                    {
                        //để trong này vì phải đợi lấy NotifyId và UserId của người nhận
                        NotifyData Data = new NotifyData
                        {
                            NotifyId = NotifyId,
                            NotifyTypeId = NotifyTypeId,
                            NotifyType = NotifyType,
                            ActionUserId = mUserAction.UserId,
                            ActionUserName = mUserAction.UserName,
                            ActionUserAvatar = mUserAction.UserAvatar,
                            ImageItemId = mImage.ImageItemId,
                            ImageUrl = ImageUrl,
                            CommentId = CommentId
                        };

                        List<UserEndpointModel> lstEndpoints = DAL.GetUserEndpoints(UserId);
                        var lstEN = lstEndpoints.Where(c => c.LanguageCode == LanguageCodes.English.ToUpper()).ToList();
                        if (lstEN.Count > 0)
                        {
                            OnesignalHelper.SendNotifications(Subject, MessageEN, Data, lstEN);
                        }
                        var lstVI = lstEndpoints.Where(c => c.LanguageCode == LanguageCodes.Vietnamese.ToUpper()).ToList();
                        if (lstVI.Count > 0)
                        {
                            OnesignalHelper.SendNotifications(Subject, Message, Data, lstVI);
                        }
                    }
                    //}
                }
            }
        }
        //ko dùng (nhưng đã cập nhật theo setting mới)
        public static void FollowingHasCommentNotify(long ActionUserId, long ImageItemId, long CommentId, long? LastId, int Count)
        {
            //int NotifyTypeId = (int)NotifyTypes.FollowingComment;
            //string NotifyType = NotifyTypes.FollowingComment.ToString();

            //List<UserModel> lst = DAL.GetUserFollowerList(ActionUserId, LastId, Count, PushNewActivity: true);
            //UserModel mUserAction = DAL.GetUserInfo(ActionUserId);

            //ImageItemModel mImage = DAL.GetImageItemInfo(ImageItemId);
            //long UserId = mImage.UserId;

            //ImageComment mComment = DAL.GetImageComment(CommentId);

            ////todo: nghiên cứu xem để gì trên này
            //string Subject = "";

            ////gán Message tương ứng với sự kiện
            //Resources.MessageResource.Culture = new System.Globalization.CultureInfo("vi-VN");
            //string Message = string.Format(Resources.MessageResource.FollowingComment, mUserAction.UserName, mComment.Comment);
            //Resources.MessageResource.Culture = new System.Globalization.CultureInfo("EN");
            //string MessageEN = string.Format(Resources.MessageResource.FollowingComment, mUserAction.UserName, mComment.Comment);

            //string ImageUrl = mImage.ImageMedium;

            //foreach (UserModel m in lst)
            //{
            //    if (m.UserId != UserId && m.UserId != ActionUserId)
            //    {
            //        long NotifyId = DAL.AddUserNotify(m.UserId, NotifyTypeId, NotifyType, Message, MessageEN, ActionUserId, null, ImageItemId, CommentId, null, null, mComment.Comment);

            //        if (!DAL.CheckUserHasCommentNotifyWithinPeriod(m.UserId, ImageItemId, CommentPeriodInMinutes))
            //        {
            //            UserNotifySettingModel mSetting = DAL.GetUserNotifySetting(m.UserId);

            //            //cấu hình nhận notify thì mới tiếp tục
            //            if (mSetting.FollowingAction)
            //            {
            //                //để trong này vì phải đợi lấy NotifyId và UserId của người nhận
            //                NotifyData Data = new NotifyData
            //                {
            //                    NotifyId = NotifyId,
            //                    NotifyTypeId = NotifyTypeId,
            //                    NotifyType = NotifyType,
            //                    ActionUserId = ActionUserId,
            //                    ActionUserName = mUserAction.UserName,
            //                    ActionUserAvatar = mUserAction.UserAvatar,
            //                    ImageItemId = ImageItemId,
            //                    ImageUrl = ImageUrl,
            //                    CommentId = CommentId
            //                };

            //                List<UserEndpointModel> lstEndpoints = DAL.GetUserEndpoints(m.UserId);
            //                var lstEN = lstEndpoints.Where(c => c.LanguageCode == LanguageCodes.English.ToUpper()).ToList();
            //                if (lstEN.Count > 0)
            //                {
            //                    OnesignalHelper.SendNotifications(Subject, MessageEN, Data, lstEN);
            //                }
            //                var lstVI = lstEndpoints.Where(c => c.LanguageCode == LanguageCodes.Vietnamese.ToUpper()).ToList();
            //                if (lstVI.Count > 0)
            //                {
            //                    OnesignalHelper.SendNotifications(Subject, Message, Data, lstVI);
            //                }

            //            }
            //        }
            //    }
            //}

            ////nếu còn thì gọi đệ quy
            //if (lst.Count == Count)
            //{
            //    FollowingHasCommentNotify(ActionUserId, ImageItemId, CommentId, lst.LastOrDefault().UserId, Count);
            //}
        }

        public static void LikeCommentNotify(long ActionUserId, long CommentId)
        {
            int NotifyTypeId = (int)NotifyTypes.LikeComment;
            string NotifyType = NotifyTypes.LikeComment.ToString();

            UserModel mUserAction = DAL.GetUserInfo(ActionUserId);

            ImageComment mComment = DAL.GetImageComment(CommentId);

            long ImageItemId = mComment.ImageItemId.Value;
            ImageItemModel mImage = DAL.GetImageItemInfo(ImageItemId);

            long UserId = mComment.UserId.Value;
            if (ActionUserId != UserId && UserId != MTPUserId)
            {
                if (!DAL.CheckUserHasNotifyLikeCommentBefore(UserId, ActionUserId, CommentId))
                {
                    //todo: nghiên cứu xem để gì trên này
                    string Subject = "";

                    //gán Message tương ứng với sự kiện
                    Resources.MessageResource.Culture = new System.Globalization.CultureInfo("vi-VN");
                    string Message = string.Format(Resources.MessageResource.LikeComment, mUserAction.UserName, mComment.Comment);
                    Resources.MessageResource.Culture = new System.Globalization.CultureInfo("EN");
                    string MessageEN = string.Format(Resources.MessageResource.LikeComment, mUserAction.UserName, mComment.Comment);

                    string ImageUrl = mImage.ImageMedium;

                    long NotifyId = DAL.AddUserNotify(UserId, NotifyTypeId, NotifyType, Message, MessageEN, ActionUserId, null, ImageItemId, CommentId, null, null, mComment.Comment);
                    if (!DAL.CheckUserHasNotifyLikeCommentWithinPeriod(NotifyId, UserId, CommentId, LikeCommentNotifyPeriodInMinute))
                    {
                        UserNotifySettingModel mSetting = DAL.GetUserNotifySetting(UserId);
                        if (mSetting.CommentHasLike)
                        {
                            //để trong này vì phải đợi lấy NotifyId và UserId của người nhận
                            NotifyData Data = new NotifyData
                            {
                                NotifyId = NotifyId,
                                NotifyTypeId = NotifyTypeId,
                                NotifyType = NotifyType,
                                ActionUserId = ActionUserId,
                                ActionUserName = mUserAction.UserName,
                                ActionUserAvatar = mUserAction.UserAvatar,
                                ImageItemId = ImageItemId,
                                ImageUrl = ImageUrl,
                                CommentId = CommentId
                            };

                            List<UserEndpointModel> lstEndpoints = DAL.GetUserEndpoints(UserId);
                            var lstEN = lstEndpoints.Where(c => c.LanguageCode == LanguageCodes.English.ToUpper()).ToList();
                            if (lstEN.Count > 0)
                            {
                                OnesignalHelper.SendNotifications(Subject, MessageEN, Data, lstEN);
                            }
                            var lstVI = lstEndpoints.Where(c => c.LanguageCode == LanguageCodes.Vietnamese.ToUpper()).ToList();
                            if (lstVI.Count > 0)
                            {
                                OnesignalHelper.SendNotifications(Subject, Message, Data, lstVI);
                            }
                        }
                    }
                }
            }
        }


    }
}