﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

using OnesignalNotify.Model;

namespace OnesignalNotify
{
    public partial class BLL
    {
        public static int MTPUserId = Convert.ToInt32(System.Configuration.ConfigurationManager.AppSettings.Get("MTPUserId"));
        static int SystemUserId = 0;
        static string SytemUserName = "";
        static string SytemUserAvatar = "";
        public static void SystemNotify(SystemNotifyModel Notify)
        {
            switch (Notify.UserType)
            {
                case 0:
                    {
                        SystemNotifyToAllUser(Notify);
                        break;
                    }
            }
        }

        private static void SystemNotifyToAllUser(SystemNotifyModel Notify)
        {
            int NotifyTypeId = Notify.NotifyType;
            string NotifyType = ((NotifyTypes)Notify.NotifyType).ToString();

            string Subject = "";

            //để trong này vì phải đợi lấy NotifyId và UserId của người nhận
            NotifyData Data = new NotifyData
            {
                NotifyId = 0,
                NotifyTypeId = NotifyTypeId,
                NotifyType = NotifyType,
                ActionUserId = SystemUserId,
                ActionUserName = SytemUserName,
                ActionUserAvatar = SytemUserAvatar
            };

            switch (Notify.TargetType)
            {
                case 1: //media
                    {
                        Data.MediaId = Notify.TargetId;
                        break;
                    }
            }

            OnesignalHelper.SendNotificationToSegment(Subject, Notify.MessageVN, Data, new List<string> { OnesignalHelper.SegmentVI });
            OnesignalHelper.SendNotificationToSegment(Subject, Notify.MessageEN, Data, new List<string> { OnesignalHelper.SegmentEN });
        }
    }
}