﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using OnesignalNotify.Model;
using OneSignal.CSharp.SDK.Resources;
namespace OnesignalNotify
{
    public partial class BLL
    {
        public static void FollowingNotify(long ActionUserId, long TargetUserId)
        {
            int NotifyTypeId = (int)NotifyTypes.Following;
            string NotifyType = NotifyTypes.Following.ToString();

            UserModel mUserAction = DAL.GetUserInfo(ActionUserId);
            UserModel mUserTarget = DAL.GetUserInfo(TargetUserId);

            //người nhận notify chính là người đc theo dõi
            long UserId = TargetUserId;

            //chưa có notify trước đó mới notify
            if (DAL.CheckUserHasFollowBefore(UserId, ActionUserId, TargetUserId) == false)
            {
                UserNotifySettingModel mSetting = DAL.GetUserNotifySetting(UserId);

                //gán Message tương ứng với sự kiện

                string Message = "";// string.Format(Resources.MessageResource.NewLike, mUserAction.UserName);            
                string MessageEN = "";// string.Format(Resources.MessageResource.NewLike, mUserAction.UserName);

                //biến theo dõi ko nhận push notify nhưng vẫn vào notify trạng thái để theo vết và accept
                bool NeedNotify = false;

                if (mUserTarget.IsPublic)
                {
                    Resources.MessageResource.Culture = new System.Globalization.CultureInfo("vi-VN");
                    Message = string.Format(Resources.MessageResource.Following, mUserAction.UserName);
                    Resources.MessageResource.Culture = new System.Globalization.CultureInfo("EN");
                    MessageEN = string.Format(Resources.MessageResource.Following, mUserAction.UserName);
                }
                else
                {
                    Resources.MessageResource.Culture = new System.Globalization.CultureInfo("vi-VN");
                    Message = string.Format(Resources.MessageResource.FollowingNeedAccept, mUserAction.UserName);
                    Resources.MessageResource.Culture = new System.Globalization.CultureInfo("EN");
                    MessageEN = string.Format(Resources.MessageResource.FollowingNeedAccept, mUserAction.UserName);

                    //nếu profile user đang set là private thì sẽ nhận notify để còn accept
                    NeedNotify = true;
                }

                long NotifyId = DAL.AddUserNotify(UserId, NotifyTypeId, NotifyType, Message, MessageEN, ActionUserId, TargetUserId, null, null, null, null);

                if (mSetting.NewFollower || NeedNotify)
                {
                    if (mSetting.NewFollower)
                    {
                        string Subject = "";

                        //để trong này vì phải đợi lấy NotifyId và UserId của người nhận
                        NotifyData Data = new NotifyData
                        {
                            NotifyId = NotifyId,
                            NotifyTypeId = NotifyTypeId,
                            NotifyType = NotifyType,
                            ActionUserId = ActionUserId,
                            ActionUserName = mUserAction.UserName,
                            ActionUserAvatar = mUserAction.UserAvatar,
                            TargetUserId = TargetUserId
                        };

                        List<UserEndpointModel> lstEndpoints = DAL.GetUserEndpoints(UserId);
                        var lstEN = lstEndpoints.Where(c => c.LanguageCode == LanguageCodes.English.ToUpper()).ToList();
                        if (lstEN.Count > 0)
                        {
                            OnesignalHelper.SendNotifications(Subject, MessageEN, Data, lstEN);
                        }
                        var lstVI = lstEndpoints.Where(c => c.LanguageCode == LanguageCodes.Vietnamese.ToUpper()).ToList();
                        if (lstVI.Count > 0)
                        {
                            OnesignalHelper.SendNotifications(Subject, Message, Data, lstVI);
                        }

                        //string AndroidMessageString = AmazonSNSHelper.GenerateMessageString(AmazonSNSHelper.OSAndroid, Message, UserId, NotifyId, NotifyTypeId, NotifyType, ActionUserId, mUserAction.UserName, mUserAction.UserAvatar, TargetUserId: TargetUserId);
                        //string AndroidENMessageString = AmazonSNSHelper.GenerateMessageString(AmazonSNSHelper.OSAndroid, MessageEN, UserId, NotifyId, NotifyTypeId, NotifyType, ActionUserId, mUserAction.UserName, mUserAction.UserAvatar, TargetUserId: TargetUserId);
                        //string iOSMessageString = AmazonSNSHelper.GenerateMessageString(AmazonSNSHelper.OSiOS, Message, UserId, NotifyId, NotifyTypeId, NotifyType, ActionUserId, mUserAction.UserName, mUserAction.UserAvatar, TargetUserId: TargetUserId);
                        //string iOSENMessageString = AmazonSNSHelper.GenerateMessageString(AmazonSNSHelper.OSiOS, MessageEN, UserId, NotifyId, NotifyTypeId, NotifyType, ActionUserId, mUserAction.UserName, mUserAction.UserAvatar, TargetUserId: TargetUserId);

                        //List<UserEndpointModel> lstEndpoints = DAL.GetUserEndpoints(UserId);

                        //foreach (UserEndpointModel mEndpoint in lstEndpoints)
                        //{
                        //    AmazonSNSHelper.PublishToEndpoint(UserId, mEndpoint.EndpointArn, Subject,
                        //        (mEndpoint.OS == AmazonSNSHelper.OSAndroid
                        //            ? ((mEndpoint.LanguageCode == AmazonSNSHelper.VNLanguageCode) ? AndroidMessageString : AndroidENMessageString)
                        //            : ((mEndpoint.LanguageCode == AmazonSNSHelper.VNLanguageCode) ? iOSMessageString : iOSENMessageString)));
                        //}
                    }
                }
            }
        }

        public static void AcceptFollowingNotify(long ActionUserId, long TargetUserId)
        {
            int NotifyTypeId = (int)NotifyTypes.AcceptedFollowing;
            string NotifyType = NotifyTypes.AcceptedFollowing.ToString();

            UserModel mUserAction = DAL.GetUserInfo(ActionUserId);
            UserModel mUserTarget = DAL.GetUserInfo(TargetUserId);

            //người nhận notify chính là người đc theo dõi
            long UserId = TargetUserId;
            UserNotifySettingModel mSetting = DAL.GetUserNotifySetting(UserId);

            //gán Message tương ứng với sự kiện

            Resources.MessageResource.Culture = new System.Globalization.CultureInfo("vi-VN");
            string Message = string.Format(Resources.MessageResource.AcceptedFollowing, mUserAction.UserName);
            Resources.MessageResource.Culture = new System.Globalization.CultureInfo("EN");
            string MessageEN = string.Format(Resources.MessageResource.AcceptedFollowing, mUserAction.UserName);

            long NotifyId = DAL.AddUserNotify(UserId, NotifyTypeId, NotifyType, Message, MessageEN, ActionUserId, TargetUserId, null, null, null, null);

            if (mSetting.FollowAccepted)
            {
                string Subject = "";

                //để trong này vì phải đợi lấy NotifyId và UserId của người nhận
                NotifyData Data = new NotifyData
                {
                    NotifyId = NotifyId,
                    NotifyTypeId = NotifyTypeId,
                    NotifyType = NotifyType,
                    ActionUserId = ActionUserId,
                    ActionUserName = mUserAction.UserName,
                    ActionUserAvatar = mUserAction.UserAvatar,
                    TargetUserId = TargetUserId
                };

                List<UserEndpointModel> lstEndpoints = DAL.GetUserEndpoints(UserId);
                var lstEN = lstEndpoints.Where(c => c.LanguageCode == LanguageCodes.English.ToUpper()).ToList();
                if (lstEN.Count > 0)
                {
                    OnesignalHelper.SendNotifications(Subject, MessageEN, Data, lstEN);
                }
                var lstVI = lstEndpoints.Where(c => c.LanguageCode == LanguageCodes.Vietnamese.ToUpper()).ToList();
                if (lstVI.Count > 0)
                {
                    OnesignalHelper.SendNotifications(Subject, Message, Data, lstVI);
                }

                //string AndroidMessageString = AmazonSNSHelper.GenerateMessageString(AmazonSNSHelper.OSAndroid, Message, UserId, NotifyId, NotifyTypeId, NotifyType, ActionUserId, mUserAction.UserName, mUserAction.UserAvatar, TargetUserId: TargetUserId);
                //string AndroidENMessageString = AmazonSNSHelper.GenerateMessageString(AmazonSNSHelper.OSAndroid, MessageEN, UserId, NotifyId, NotifyTypeId, NotifyType, ActionUserId, mUserAction.UserName, mUserAction.UserAvatar, TargetUserId: TargetUserId);
                //string iOSMessageString = AmazonSNSHelper.GenerateMessageString(AmazonSNSHelper.OSiOS, Message, UserId, NotifyId, NotifyTypeId, NotifyType, ActionUserId, mUserAction.UserName, mUserAction.UserAvatar, TargetUserId: TargetUserId);
                //string iOSENMessageString = AmazonSNSHelper.GenerateMessageString(AmazonSNSHelper.OSiOS, MessageEN, UserId, NotifyId, NotifyTypeId, NotifyType, ActionUserId, mUserAction.UserName, mUserAction.UserAvatar, TargetUserId: TargetUserId);

                //List<UserEndpointModel> lstEndpoints = DAL.GetUserEndpoints(UserId);

                //foreach (UserEndpointModel mEndpoint in lstEndpoints)
                //{
                //    AmazonSNSHelper.PublishToEndpoint(UserId, mEndpoint.EndpointArn, Subject,
                //        (mEndpoint.OS == AmazonSNSHelper.OSAndroid
                //            ? ((mEndpoint.LanguageCode == AmazonSNSHelper.VNLanguageCode) ? AndroidMessageString : AndroidENMessageString)
                //            : ((mEndpoint.LanguageCode == AmazonSNSHelper.VNLanguageCode) ? iOSMessageString : iOSENMessageString)));
                //}
            }
        }
    }
}