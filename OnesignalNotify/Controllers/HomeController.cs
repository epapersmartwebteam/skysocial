﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Http;

using OnesignalNotify.Model;

namespace OnesignalNotify.Controllers
{
    [RoutePrefix("api")]
    public class HomeController : ApiController
    {
        public static long SystemUserId = 1;
        public static int ItemCount = Convert.ToInt32(System.Configuration.ConfigurationManager.AppSettings.Get("ItemCount"));

        [HttpGet]
        [Route("Get")]
        public void Get()
        {
            BLL.LikeCommentNotify(8, 541);
            //var result = Utility.GetMentionUserNames("@hung.le.mtp @huytrieu123 sadsa @12s");
        }

        [HttpGet]
        [Route("NewImageUploadNotify")]
        public void NewImageUploadNotify(long UserId, long ImageItemId)
        {
            int Count = ItemCount;
            BLL.NewImageUploadNotify(UserId, ImageItemId, null, Count);
        }

        [HttpGet]
        [Route("NewCommentNotify")]
        public void NewCommentNotify(long UserId, long ImageItemId, long CommentId)
        {
            BLL.NewCommentNotify(UserId, ImageItemId, CommentId);
        }

        [HttpGet]
        [Route("NewMediaCommentNotify")]
        public void NewMediaCommentNotify(long UserId, int MediaId, long CommentId)
        {
            BLL.NewMediaCommentNotify(UserId, MediaId, CommentId);
        }

        [HttpGet]
        [Route("LikeCommentNotify")]
        public void LikeCommentNotify(long UserId, long CommentId)
        {
            BLL.LikeCommentNotify(UserId, CommentId);
        }

        [HttpGet]
        [Route("LikeMediaCommentNotify")]
        public void LikeMediaCommentNotify(long UserId, long CommentId)
        {
            BLL.LikeMediaCommentNotify(UserId, CommentId);
        }

        [HttpGet]
        [Route("NewLikeNotify")]
        public void NewLikeNotify(long UserId, long ImageItemId)
        {
            BLL.NewLikeNotify(UserId, ImageItemId);
            //System.Threading.Tasks.Task.Factory.StartNew(() => BLL.NewLikeNotify(UserId, ImageItemId));
            //return true;
        }

        [HttpGet]
        [Route("FollowingNotify")]
        public void FollowingNotify(long UserId, long TargetUserId)
        {
            BLL.FollowingNotify(UserId, TargetUserId);
            //System.Threading.Tasks.Task.Factory.StartNew(() => BLL.FollowingNotify(UserId, TargetUserId));
            //return true;
        }

        [HttpGet]
        [Route("AcceptFollowingNotify")]
        public void AcceptFollowingNotify(long UserId, long TargetUserId)
        {
            BLL.AcceptFollowingNotify(UserId, TargetUserId);
            //System.Threading.Tasks.Task.Factory.StartNew(() => BLL.AcceptFollowingNotify(UserId, TargetUserId));
            //return true;
        }

        [HttpGet]
        [Route("FollowingHasCommentNotify")]
        public void FollowingHasCommentNotify(long UserId, long ImageItemId, long CommentId)
        {
            //int Count = ItemCount;

            //System.Threading.Tasks.Task.Factory.StartNew(() => BLL.FollowingHasCommentNotify(UserId, ImageItemId, CommentId, null, Count));
            //return true;
        }
        [HttpGet]
        [Route("FollowingHasLikeNotify")]
        public void FollowingHasLikeNotify(long UserId, long ImageItemId)
        {
            //int Count = ItemCount;
            //if (UserId != SystemUserId)
            //{
            //    BLL.FollowingHasLikeNotify(UserId, ImageItemId, null, Count);
            //    //System.Threading.Tasks.Task.Factory.StartNew(() => BLL.FollowingHasLikeNotify(UserId, ImageItemId, null, Count));
            //}
            //return true;
        }

        [HttpGet]
        [Route("FollowingHasShareNotify")]
        public void FollowingHasShareNotify(long UserId, long ImageItemId)
        {
            //int Count = ItemCount;
            //if (UserId != SystemUserId)
            //{
            //    BLL.FollowingHasShareNotify(UserId, ImageItemId, null, Count);
            //    //System.Threading.Tasks.Task.Factory.StartNew(() => BLL.FollowingHasShareNotify(UserId, ImageItemId, null, Count));
            //}
            //return true;
        }

        [HttpPost]
        [Route("SystemNotify")]
        public void SystemNotify(SystemNotifyModel Notify)
        {
            BLL.SystemNotify(Notify);
        }

        [HttpPost]
        [Route("UpdateUserTagLanguage")]
        public void UpdateUserTagLanguage(long UserId, string OnesignalId, string LanguageCode)
        {
            OnesignalHelper.UpdateUserTagLanguage(UserId, OnesignalId, LanguageCode);
        }
    }
}
