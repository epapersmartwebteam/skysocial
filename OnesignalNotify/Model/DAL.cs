﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using EntityFramework.Extensions;
using System.Data.Entity;

namespace OnesignalNotify.Model
{
    public class DAL
    {
        public static string CDNUrl = System.Configuration.ConfigurationManager.AppSettings.Get("CDNUrl");

        public static long GetUnixTimeStamp(DateTime d)
        {
            return (long)d.ToUniversalTime().Subtract(new DateTime(1970, 1, 1)).TotalSeconds;
        }

        #region ImageItem
        public static ImageItemModel GetImageItemInfo(long ImageItemId)
        {
            ImageItemModel m = new ImageItemModel();
            
            using (CHD_DataEntities db = new CHD_DataEntities())
            {   
                ImageItem mI = db.ImageItems.SingleOrDefault(i => i.ImageItemId == ImageItemId);
                m = new ImageItemModel(mI, CDNUrl);
            }
            return m;
        }
        #endregion

        #region Comment
        public static ImageComment GetImageComment(long CommentId)
        {
            ImageComment Comment = new ImageComment();
            using (CHD_DataEntities db = new CHD_DataEntities())
            {
                Comment = db.ImageComments.SingleOrDefault(c => c.ItemId == CommentId);
            }
            return Comment;
        }
        #endregion

        #region MediaComment
        public static MediaComment GetMediaComment(long CommentId)
        {
            MediaComment Comment = new MediaComment();
            using (CHD_DataEntities db = new CHD_DataEntities())
            {
                Comment = db.MediaComments.FirstOrDefault(c => c.ItemId == CommentId);
            }
            return Comment;
        }
        #endregion

        #region Media
        public static MediaModel GetMedia(int MediaId)
        {
            MediaModel media = new MediaModel();
            using (CHD_DataEntities db = new CHD_DataEntities())
            {
                Media mM = db.Medias.FirstOrDefault(c => c.MediaId == MediaId);
                media = new MediaModel(mM, CDNUrl);
            }
            return media;
        }
        #endregion

        #region User
        public static UserModel GetUserInfo(long UserId)
        {
            UserModel m = new UserModel();
            using (CHD_DataEntities db = new CHD_DataEntities())
            {
                User mU = db.Users.SingleOrDefault(u => u.UserId == UserId);
                m = new UserModel(mU, CDNUrl);
            }
            return m;
        }
        public static List<long> GetUserIdFromUserNameList(List<string> lstUserName)
        {
            List<long> lstUserId = new List<long>();
            using (CHD_DataEntities db = new CHD_DataEntities())
            {
                lstUserId = db.Users.Where(c => lstUserName.Contains(c.UserName)).Select(c => c.UserId).ToList();
            }
            return lstUserId;
        }
        public static List<UserModel> GetUserFollowerList(long UserId, long? LastId, int Count, bool? PushNewPost = null, bool? PushNewActivity = null)
        {
            List<UserModel> lst = new List<UserModel>();
            using (CHD_DataEntities db = new CHD_DataEntities())
            {
                var query = db.UserFollowers.Where(u => u.ToUserId == UserId && u.IsAccepted == true);

                if (LastId.HasValue)
                {
                    query = query.Where(u => u.FromUserId < LastId);
                }

                if (PushNewPost.HasValue)
                {
                    query = query.Where(u => u.PushNotifyNewPost == PushNewPost);
                }

                if (PushNewActivity.HasValue)
                {
                    query = query.Where(u => u.PushNotifyNewActivity == PushNewActivity);
                }

                query = query.OrderByDescending(u => u.FromUserId).Take(Count);
                lst = query.Select(u => new UserModel() { UserId = u.FromUserId.Value, FollowingDate = u.AcceptedDate.Value }).ToList();
            }
            return lst;
        }

        public static bool CheckUserHasNotifyLikeBefore(long UserId, long ActionUserId, long ImageItemId)
        {
            bool result = false;
            using (CHD_DataEntities db = new CHD_DataEntities())
            {
                string NewLikeEvent = NotifyTypes.NewLike.ToString();
                string FollowingLikeEvent = NotifyTypes.FollowingLike.ToString();

                if (db.UserNotifies.Any(c => c.UserId == UserId && c.ActionUserId == ActionUserId && c.ImageItemId == ImageItemId && (c.NotifyType == NewLikeEvent || c.NotifyType == FollowingLikeEvent)))
                    result = true;
            }
            return result;
        }

        public static bool DeleteOldLikeNotify(long UserId, long ActionUserId, long ImageItemId, string NotifyType)
        {
            bool result = true;
            using (CHD_DataEntities db = new CHD_DataEntities())
            {
                try
                {
                    db.UserNotifies.Where(s => s.UserId == UserId && s.ActionUserId == ActionUserId && s.ImageItemId == ImageItemId && s.NotifyType == NotifyType)
                        .Delete();
                }
                catch (Exception ex)
                {
                    result = false;
                }
            }
            return result;
        }

        public static bool CheckUserHasCommentNotifyWithinPeriod(long UserId, long ImageItemId, int InMinutes)
        {
            bool result = false;
            using (CHD_DataEntities db = new CHD_DataEntities())
            {

                long Now = DAL.GetUnixTimeStamp(DateTime.UtcNow);

                if (db.UserNotifies.Any(s => s.UserId == UserId && s.ImageItemId == ImageItemId
                        && (Now - s.CreateDate) < (InMinutes * 60)))
                {
                    result = true;
                }
            }
            return result;
        }

        public static bool CheckUserHasPostNotifyWithinPeriod(long UserId, long FromUserId, int InMinutes)
        {
            bool result = false;
            using (CHD_DataEntities db = new CHD_DataEntities())
            {
                long Now = DAL.GetUnixTimeStamp(DateTime.UtcNow);

                if (db.UserNotifies.Any(s => s.UserId == UserId && s.ActionUserId == FromUserId
                            && s.NotifyTypeId == (int)NotifyTypes.UploadNewPhoto
                            && (Now - s.CreateDate) < (InMinutes * 60)))
                {
                    result = true;
                }
            }
            return result;
        }

        public static bool CheckUserHasNotifyLikeCommentWithinPeriod(long NewNotifyId, long UserId, long CommentId, int InMinutes)
        {
            bool result = false;
            using (CHD_DataEntities db = new CHD_DataEntities())
            {
                string LikeComment = NotifyTypes.LikeComment.ToString();
                long Now = DAL.GetUnixTimeStamp(DateTime.UtcNow);

                if (db.UserNotifies.Any(c => c.UserId == UserId && c.CommentId == CommentId
                        && c.NotifyType == LikeComment && c.ImageItemId != null
                        && c.NotifyId != NewNotifyId
                        && (Now - c.CreateDate) < (InMinutes * 60)))
                    result = true;
            }
            return result;
        }

        public static bool CheckUserHasNotifyLikeCommentMediaWithinPeriod(long NewNotifyId, long UserId, long CommentId, int InMinutes)
        {
            bool result = false;
            using (CHD_DataEntities db = new CHD_DataEntities())
            {
                string LikeComment = NotifyTypes.LikeComment.ToString();
                long Now = DAL.GetUnixTimeStamp(DateTime.UtcNow);

                if (db.UserNotifies.Any(c => c.UserId == UserId && c.CommentId == CommentId
                        && c.NotifyType == LikeComment && c.MediaId != null
                        && c.NotifyId != NewNotifyId
                         && (Now - c.CreateDate) < (InMinutes * 60)))
                    result = true;
            }
            return result;
        }

        public static bool CheckUserHasNotifyLikeCommentBefore(long UserId, long ActionUserId, long CommentId)
        {
            bool result = false;
            using (CHD_DataEntities db = new CHD_DataEntities())
            {
                string LikeComment = NotifyTypes.LikeComment.ToString();

                if (db.UserNotifies.Any(c => c.UserId == UserId && c.ActionUserId == ActionUserId && c.CommentId == CommentId
                        && c.NotifyType == LikeComment && c.ImageItemId != null))
                    result = true;
            }
            return result;
        }

        public static bool CheckUserHasNotifyLikeCommentMediaBefore(long UserId, long ActionUserId, long CommentId)
        {
            bool result = false;
            using (CHD_DataEntities db = new CHD_DataEntities())
            {
                string LikeComment = NotifyTypes.LikeComment.ToString();

                if (db.UserNotifies.Any(c => c.UserId == UserId && c.ActionUserId == ActionUserId && c.CommentId == CommentId
                        && c.NotifyType == LikeComment && c.MediaId != null))
                    result = true;
            }
            return result;
        }
        public static bool CheckUserHasFollowBefore(long UserId, long ActionUserId, long TargetUserId)
        {
            bool result = false;
            using (CHD_DataEntities db = new CHD_DataEntities())
            {
                string Following = NotifyTypes.Following.ToString();
                if (db.UserNotifies.Any(c => c.UserId == UserId && c.ActionUserId == ActionUserId 
                        && c.TargetUserId == TargetUserId && c.NotifyType == Following))
                {
                    result = true;
                }
            }
            return result;
        }
        public static long AddUserNotify(long UserId, int NotifyTypeId, string NotifyType, string Message, string MessageEN, long ActionUserId, long? TargetUserId, long? ImageItemId, long? CommentId, long? ChatRoomId, long? ChatLogId, string Comment = "", int? MediaId = null)
        {
            long NotifyId = 0;
            using (CHD_DataEntities db = new CHD_DataEntities())
            {
                UserNotify m = new UserNotify()
                {
                    UserId = UserId,
                    NotifyTypeId = NotifyTypeId,
                    NotifyType = NotifyType,
                    Message = Message,
                    MessageEN = MessageEN,
                    ActionUserId = ActionUserId,
                    TargetUserId = TargetUserId,
                    ImageItemId = ImageItemId,
                    CommentId = CommentId,
                    ChatRoomId = ChatRoomId,
                    ChatLogId = ChatLogId,
                    CreateDate = DAL.GetUnixTimeStamp(DateTime.UtcNow),
                    IsViewed = false,
                    IsDelete = false,
                    Comment = Comment,
                    MediaId = MediaId
                };

                db.UserNotifies.Add(m);

                try
                {
                    db.SaveChanges();

                    NotifyId = m.NotifyId;
                }
                catch
                {

                }
            }
            return NotifyId;
        }

        public static UserNotifySettingModel GetUserNotifySetting(long UserId)
        {
            UserNotifySettingModel m = new UserNotifySettingModel();
            using (CHD_DataEntities db = new CHD_DataEntities())
            {
                UserNotifySetting mU = db.UserNotifySettings.FirstOrDefault(u => u.UserId == UserId);
                m = new UserNotifySettingModel(mU);
            }
            return m;
        }

        public static List<UserEndpointModel> GetUserEndpoints(long UserId)
        {
            List<UserEndpointModel> lst = new List<UserEndpointModel>();
            using (CHD_DataEntities db = new CHD_DataEntities())
            {
                lst = db.UserDevices.Where(u => u.UserId == UserId)
                            .Select(u => new UserEndpointModel()
                            {
                                OS = u.OS,
                                DeviceId = u.DeviceId,
                                OnesignalId = u.OneSignalId,
                                OnesignalAppId = u.OneSignalAppId,
                                LanguageCode = u.LanguageCode
                            })
                            .ToList();
            }
            return lst;
        }

        public static bool DeleteUserEndpoint(string OnesignalId)
        {
            bool result = true;
            using (CHD_DataEntities db = new CHD_DataEntities())
            {
                try
                {
                    db.UserDevices.Where(c => c.OneSignalId == OnesignalId).Delete();
                }
                catch (Exception ex)
                {
                    result = false;
                }
            }
            return result;
        }
        #endregion

        //#region Chat
        //public static ChatRoomModel GetChatRoomInfo(long ChatRoomId)
        //{
        //    ChatRoomModel m = new ChatRoomModel();
        //    using (CHD_DataEntities db = new CHD_DataEntities())
        //    {
        //        ChatRoom mC = db.ChatRooms.SingleOrDefault(c => c.ChatRoomId == ChatRoomId);
        //        m = new ChatRoomModel(mC);
        //    }
        //    return m;
        //}
        //public static List<UserModel> GetChatRoomMemberEnableNotify(long ChatRoomId)
        //{
        //    List<UserModel> lst = new List<UserModel>();
        //    using (CHD_DataEntities db = new CHD_DataEntities())
        //    {
        //        lst = db.getRoomMemberEnableNotify(ChatRoomId)
        //                .Select(r => new UserModel()
        //                {
        //                    UserId = r.UserId.Value,
        //                    UserName = r.UserName
        //                })
        //                .ToList();
        //    }
        //    return lst;
        //}

        //public static ChatLogModel GetChatLogInfo(long ChatLogId)
        //{
        //    ChatLogModel m = new ChatLogModel();
        //    using (CHD_DataEntities db = new CHD_DataEntities())
        //    {
        //        ChatLog mC = db.ChatLogs.SingleOrDefault(c => c.ChatLogId == ChatLogId);
        //        m = new ChatLogModel(mC);
        //    }
        //    return m;
        //}


        //public static ChatNotifyLog GetLastestChatNotifyLog(long ChatRoomId, long UserId)
        //{
        //    ChatNotifyLog m = new ChatNotifyLog();
        //    using (CHD_DataEntities db = new CHD_DataEntities())
        //    {
        //        m = db.ChatNotifyLogs.LastOrDefault(c => c.ChatRoomId == ChatRoomId && c.UserId == c.UserId);
        //    }
        //    return m;
        //}

        //public static bool AddChatNotifyLog(long ChatRoomId, long UserId, string Message, bool? IsFile, bool? IsImage, bool? IsLink, bool? IsAction)
        //{
        //    bool result = false;
        //    using (CHD_DataEntities db = new CHD_DataEntities())
        //    {
        //        ChatNotifyLog m = new ChatNotifyLog()
        //        {
        //            ChatRoomId = ChatRoomId,
        //            UserId = UserId,
        //            Message = Message,
        //            IsFile = IsFile,
        //            IsImage = IsImage,
        //            IsLink = IsLink,
        //            IsAction = IsAction
        //        };

        //        db.ChatNotifyLogs.Add(m);

        //        try
        //        {
        //            db.SaveChanges();
        //        }
        //        catch
        //        {
        //            result = false;
        //        }
        //    }
        //    return result;
        //}
        //#endregion
    }
}