//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace OnesignalNotify.Model
{
    using System;
    using System.Collections.Generic;
    
    public partial class UserDevice
    {
        public long ItemId { get; set; }
        public Nullable<long> UserId { get; set; }
        public string OS { get; set; }
        public string DeviceId { get; set; }
        public string OneSignalAppId { get; set; }
        public string OneSignalId { get; set; }
        public Nullable<int> ErrorTime { get; set; }
        public Nullable<long> CreateDate { get; set; }
        public Nullable<long> LastModifyDate { get; set; }
        public string LanguageCode { get; set; }
    }
}
