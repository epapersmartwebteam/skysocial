﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace OnesignalNotify.Model
{
    public enum NotifyTypes
    {
        Following = 1,
        AcceptedFollowing = 2,
        UploadNewPhoto = 3,
        NewLike = 4,
        NewComment = 5,
        NewChatMessage = 6,
        NewChatFile = 7,
        MentionInComment = 8,
        FollowingComment = 9,
        FollowingLike = 10,
        FollowingShare = 11,
        System = 0,
        NewMedia = 12,
        CommentHasReply = 13,
        LikeComment = 14,
        MentionInImage = 15
    }

    public class NotifyData
    {
        public long NotifyId { get; set; }
        public int NotifyTypeId { get; set; }
        public string NotifyType { get; set; }
        public long ActionUserId { get; set; }
        public string ActionUserName { get; set; }
        public string ActionUserAvatar { get; set; }
        public long TargetUserId { get; set; }
        public long ImageItemId { get; set; }
        public long CommentId { get; set; }
        public long ChatRoomId { get; set; }
        public long ChatLogId { get; set; }
        public long Date { get; set; }
        public string ImageUrl { get; set; }
        public int MediaId { get; set; }
    }
}