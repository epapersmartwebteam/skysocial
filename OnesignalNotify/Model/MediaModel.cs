﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace OnesignalNotify.Model
{
    public class MediaModel
    {
        public int MediaId { get; set; }
        public string MediaGUID { get; set; }
        public string MediaType { get; set; }
        public string Title { get; set; }
        public string Description { get; set; }
        public string Link { get; set; }
        public string Cover { get; set; }
        public string Thumbnail { get; set; }
        public MediaModel()
        {

        }
        public MediaModel(Media m, string CDNUrl)
        {
            if (m == null)
            {
                new MediaModel();
            }
            else
            {
                MediaId = m.MediaId;
                MediaGUID = m.MediaGUID;
                MediaType = m.MediaType;
                Title = m.Title;
                Description = m.Description;
                Link = CDNUrl + m.Link;
                Cover = CDNUrl + m.Cover;
                Thumbnail = CDNUrl + m.Thumbnail;
            }
        }
    }
}