﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Newtonsoft.Json;

namespace OnesignalNotify.Model
{
    public class MediaObjectModel
    {
        public string Link { get; set; }
        public float SizeInKb { get; set; }
        public int Width { get; set; }
        public int Height { get; set; }
    }
    public class ItemModel
    {
        public string ItemType { get; set; }
        public MediaObjectModel Large { get; set; }
        public MediaObjectModel Medium { get; set; }
        public MediaObjectModel Small { get; set; }
    }
    public class ImageItemModel
    {
        public long ImageItemId { get; set; }
        public string ItemGUID { get; set; }
        public long UserId { get; set; }
        public string ImageMedium { get; set; }
        public string ImageSmall { get; set; }
        public List<ItemModel> ItemContent { get; set; }
        public long CreateDate { get; set; }
        public string Description { get; set; }
        public ImageItemModel()
        {

        }

        public ImageItemModel(ImageItem m, string CDNUrl)
        {
            if (m == null)
            {
                new ImageItemModel();
            }
            else
            {
                ItemContent = JsonConvert.DeserializeObject<List<ItemModel>>(m.ItemContent);

                UserId = m.UserId ?? 0;
                ImageItemId = m.ImageItemId;
                if (ItemContent != null && ItemContent.Count > 0)
                {
                    
                    ImageMedium = CDNUrl + ItemContent[0].Medium.Link;
                    ImageSmall = CDNUrl + ItemContent[0].Small.Link;
                }
                ItemGUID = m.ItemGUID;
                CreateDate = m.CreateDate ?? DAL.GetUnixTimeStamp(DateTime.UtcNow);
                Description = string.IsNullOrEmpty(m.Description) ? string.Empty : m.Description;
            }
        }
    }
}