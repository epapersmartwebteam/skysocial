﻿using System;
using System.Collections.Generic;
using System.Collections;
using System.Linq;
using System.Web;
using System.Text.RegularExpressions;

namespace OnesignalNotify.Model
{
    public class Utility
    {
        public static List<string> GetMentionUserNames(string Comment)
        {
            List<string> UserNames = new List<string>();
            Regex regex = new Regex(@"@([\w-_.]+)");
            var collection = regex.Matches(Comment);
            for(var i = 0; i < collection.Count; i++)
            {
                UserNames.Add(collection[i].Groups[1].Value);
            }
            return UserNames;
        }
    }
}