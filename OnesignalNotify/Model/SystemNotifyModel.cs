﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Runtime.Serialization;

namespace OnesignalNotify.Model
{
    [DataContract]
    public class SystemNotifyModel
    {
        [DataMember]
        public int UserType { get; set; }
        [DataMember]
        public int NotifyType { get; set; }
        [DataMember]
        public string MessageVN { get; set; }
        [DataMember]
        public string MessageEN { get; set; }
        [DataMember]
        public int TargetType { get; set; }
        [DataMember]
        public int TargetId { get; set; }
    }
}