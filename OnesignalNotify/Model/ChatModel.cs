﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace SNSNotify.Model
{
    public enum RoomTypes
    {
        Private = 1,
        Group = 2
    }
    public class ChatRoomModel
    {
        public long ChatRoomId { get; set; }
        public string ChatRoomName { get; set; }
        public int RoomTypeId { get; set; }

        public ChatRoomModel()
        {

        }

        public ChatRoomModel(ChatRoom m)
        {
            if (m == null)
            {
                new ChatRoomModel();
            }
            else
            {
                ChatRoomId = m.ChatRoomId;
                ChatRoomName = m.ChatRoomName;
                RoomTypeId = m.RoomTypeId ?? (int)RoomTypes.Private;
            }
        }
    }

    public class ChatLogModel
    {
        public long ChatLogId { get; set; }
        public long ChatRoomId { get; set; }
        public long UserId { get; set; }
        public string Message { get; set; }
        public string LinkUrl { get; set; }
        public string ThumbnailUrl { get; set; }
        public string Title { get; set; }
        public Nullable<bool> IsFile { get; set; }
        public Nullable<bool> IsImage { get; set; }
        public Nullable<bool> IsLink { get; set; }
        public Nullable<bool> IsAction { get; set; }
        public Nullable<bool> IsImageItem { get; set; }
        public Nullable<long> ImageItemId { get; set; }

        public ChatLogModel()
        {

        }

        public ChatLogModel(ChatLog m)
        {
            if (m == null)
            {
                new ChatLogModel();
            }
            else
            {
                ChatLogId = m.ChatLogId;
                ChatRoomId = m.ChatRoomId ?? 0;
                UserId = m.UserId ?? 0;
                Message = m.Message;
                LinkUrl = m.LinkUrl;
                ThumbnailUrl = m.ThumbnailUrl;
                Title = m.Title;
                IsFile = m.IsFile;
                IsImage = m.IsImage;
                IsLink = m.IsLink;
                IsAction = m.IsAction;
                IsImageItem = m.IsImageItem;
                ImageItemId = m.ImageItemId;
            }
        }        
    }

    public class ChatNotifyLogModel
    {
        public long ItemId { get; set; }
        public long ChatRoomId { get; set; }
        public long UserId { get; set; }
        public string Message { get; set; }
        public Nullable<bool> IsFile { get; set; }
        public Nullable<bool> IsImage { get; set; }
        public Nullable<bool> IsLink { get; set; }
        public Nullable<bool> IsAction { get; set; }
        public Nullable<System.DateTime> CreateDate { get; set; }
        
    }
}