﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace OnesignalNotify.Model
{
    public class UserNotifySettingModel
    {
        public bool NewComment { get; set; }
        public bool NewLike { get; set; }
        public bool NewFollower { get; set; }
        public bool FollowAccepted { get; set; }
        public bool FollowUploadImage { get; set; }
        public bool FollowingAction { get; set; }
        public bool MentionInComment { get; set; }
        public bool CommentHasReply { get; set; }
        public bool CommentHasLike { get; set; }
        public bool Chat { get; set; }
        public bool MentionInPost { get; set; }

        public UserNotifySettingModel()
        {
            NewComment = true;
            NewLike = true;
            NewFollower = true;
            FollowAccepted = true;
            FollowUploadImage = true;
            FollowingAction = true;
            MentionInComment = true;
            CommentHasReply = true;
            CommentHasLike = true;
            Chat = true;
            MentionInPost = true;
        }

        public UserNotifySettingModel(UserNotifySetting m)
        {
            if(m == null)
            {
                NewComment = true;
                NewLike = true;
                NewFollower = true;
                FollowAccepted = true;
                FollowUploadImage = true;
                FollowingAction = true;
                MentionInComment = true;
                CommentHasReply = true;
                CommentHasLike = true;
                Chat = true;
                MentionInPost = true;
            }
            else
            {
                NewComment = m.NewComment ?? true;
                NewLike = m.NewLike ?? true;
                NewFollower = m.NewFollower ?? true;
                FollowAccepted = m.FollowAccepted ?? true;
                FollowUploadImage = m.FollowUploadImage ?? true;
                FollowingAction = m.FollowingAction ?? true;
                MentionInComment = m.MentionInComment ?? true;
                CommentHasReply = m.CommentHasReply ?? true;
                CommentHasLike = m.CommentHasLike ?? true;
                Chat = m.Chat ?? true;
                MentionInPost = m.MentionInPost ?? true;
            }
        }
    }
    public class UserEndpointModel
    {
        public string OS { get; set; }
        public string DeviceId { get; set; }
        public string Token { get; set; }
        public string OnesignalId { get; set; }
        public string OnesignalAppId { get; set; }
        public string LanguageCode { get; set; }
    }
    public class UserModel
    {
        public long UserId { get; set; }
        public string UserName { get; set; }
        public string UserAvatar { get; set; }
        public string Email { get; set; }
        public long FollowingDate { get; set; }
        public long CreateDate { get; set; }     
        public bool IsPublic { get; set; }           
        public UserModel()
        {

        }

        public UserModel(User m, string CDNUrl)
        {
            if(m == null)
            {
                new UserModel();
            }
            else
            {
                UserId = m.UserId;
                UserName = m.UserName;
                UserAvatar = CDNUrl + m.AvatarSmall;
                Email = m.Email;
                CreateDate = m.CreateDate ?? DAL.GetUnixTimeStamp(DateTime.UtcNow);
                IsPublic = m.IsPublic ?? true;
            }
        }
    }
}