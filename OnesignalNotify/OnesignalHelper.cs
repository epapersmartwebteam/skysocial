﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.ComponentModel;
using System.Collections.Specialized;
using OneSignal.CSharp.SDK;
using OneSignal.CSharp.SDK.Resources;
using OneSignal.CSharp.SDK.Resources.Devices;
using OneSignal.CSharp.SDK.Resources.Notifications;
using OnesignalNotify.Model;

using RestSharp;

namespace OnesignalNotify
{
    public class OnesignalHelper
    {
        public static string OnesignalApiKey = System.Configuration.ConfigurationManager.AppSettings.Get("OnesignalApiKey");
        public static string OnesignalAppId = System.Configuration.ConfigurationManager.AppSettings.Get("OnesignalAppId");

        public static string SegmentEN = "EN";
        public static string SegmentVI = "VI";


        public static void SendNotifications(string Subject, string Message, NotifyData Data, List<UserEndpointModel> PlayIds)
        {
            var client = new OneSignalClient(OnesignalApiKey);
            NotificationCreateOptions options = new NotificationCreateOptions();
            options.AppId = Guid.Parse(OnesignalAppId);
            options.IncludePlayerIds = PlayIds.Select(c => c.OnesignalId).ToList();
            options.Headings = new Dictionary<string, string>
            {
                { LanguageCodes.English,  Subject },
                { LanguageCodes.Vietnamese, Subject }
            };
            options.Contents = new Dictionary<string, string>
            {
                { LanguageCodes.English, Message },
                {LanguageCodes.Vietnamese, Message }
            };
            options.Data = Data.GetType().GetProperties(System.Reflection.BindingFlags.Instance | System.Reflection.BindingFlags.Public)
                                      .ToDictionary(prop => prop.Name, prop => prop.GetValue(Data, null).ToString());


            //iOS
            options.MutableContent = true;
            options.IosBadgeCount = 1;
            options.IosBadgeType = IosBadgeTypeEnum.Increase;

            //có hình thì gửi luôn hình
            if (!string.IsNullOrEmpty(Data.ImageUrl))
            {
                options.IosAttachments = new Dictionary<string, string>
                {
                    { "id1", Data.ImageUrl }
                };

                //Android
                options.BigPictureForAndroid = Data.ImageUrl;
                //Chrome nếu có
                options.BigPictureForChrome = Data.ImageUrl;
            }

            try
            {
                NotificationCreateResult result = client.Notifications.Create(options);
                
                if (result.Error != null && (result.Error.ErrorResultType == ErrorResultTypeEnum.HasAllInvalidPlayerIds || result.Error.ErrorResultType == ErrorResultTypeEnum.HasInvalidPlayerIds))
                {
                    List<string> InvalidPlayerIds = result.Error.InvalidPlayerIds.ToList();
                    InvalidPlayerIds.ForEach((item) =>
                    {
                        DAL.DeleteUserEndpoint(item);
                    });
                }
            }
            catch (Exception ex)
            {

            }
        }

        public static void SendNotificationToSegment(string Subject, string Message, NotifyData Data, List<string> Segments)
        {
            var client = new OneSignalClient(OnesignalApiKey);
            NotificationCreateOptions options = new NotificationCreateOptions();
            options.AppId = Guid.Parse(OnesignalAppId);

            options.IncludedSegments = Segments;

            options.Headings = new Dictionary<string, string>
            {
                { LanguageCodes.English,  Subject },
                { LanguageCodes.Vietnamese, Subject }
            };
            options.Contents = new Dictionary<string, string>
            {
                { LanguageCodes.English, Message },
                {LanguageCodes.Vietnamese, Message }
            };
            options.Data = Data.GetType().GetProperties(System.Reflection.BindingFlags.Instance | System.Reflection.BindingFlags.Public)
                                      .ToDictionary(prop => prop.Name, prop => (prop.GetValue(Data, null) == null) ? "" : prop.GetValue(Data, null).ToString());


            //iOS
            options.MutableContent = true;
            options.IosBadgeCount = 1;
            options.IosBadgeType = IosBadgeTypeEnum.Increase;

            //có hình thì gửi luôn hình
            if (!string.IsNullOrEmpty(Data.ImageUrl))
            {
                options.IosAttachments = new Dictionary<string, string>
                {
                    { "id1", Data.ImageUrl }
                };


                //Android
                options.BigPictureForAndroid = Data.ImageUrl;
                //Chrome nếu có
                options.BigPictureForChrome = Data.ImageUrl;
            }

            try
            {
                NotificationCreateResult result = client.Notifications.Create(options);
                if (result.Error != null && (result.Error.ErrorResultType == ErrorResultTypeEnum.HasAllInvalidPlayerIds || result.Error.ErrorResultType == ErrorResultTypeEnum.HasInvalidPlayerIds))
                {
                    List<string> InvalidPlayerIds = result.Error.InvalidPlayerIds.ToList();
                    InvalidPlayerIds.ForEach((item) =>
                    {
                        DAL.DeleteUserEndpoint(item);
                    });
                }
            }
            catch (Exception ex)
            {

            }
        }

        public static void UpdateUserTagLanguage(long UserId, string OnesignalId, string LanguageCode)
        {
            var client = new OneSignalClient(OnesignalApiKey);

            DeviceEditOptions options = new DeviceEditOptions();
            options.AppId = Guid.Parse(OnesignalAppId);
            options.Tags = new Dictionary<string, object>();
            options.Tags.Add("Lang", LanguageCode);
            options.ExternalUserId = UserId.ToString();

            client.Devices.Edit(OnesignalId, options);
        }

        
    }
}